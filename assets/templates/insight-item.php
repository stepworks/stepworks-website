[[chunk?&file=`header.php`]]

<body class="page-insights page-insights-item">
    [[chunk?file=`page_header.php`]]


    <section>
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="title">[[if?&is=`[*menutitle*]:notempty`&then=`[*menutitle*]`&else=`[*pagetitle*]`]]</h1>
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start3 end6">
                    [[if?&is=`[*insight_audio*]:notempty`&then=`
                    <div class="audio-player-area">
                        [[if?&is=`[*tldr_link*]:notempty`&then=`<div class="link-summary"><p><a class="tldr_link" href="[~[*id*]~]#tldr">[*tldr_link*]</a></p></div>`]]
                        <div class="audio-box">
                            <div class="text">Listen</div>
                            <audio controls>
                                <source src="[*insight_audio*]" type="audio/mp3">
                            </audio>
                        </div>
                    </div>
                    <br />
                    <br />
                    `]]
                    <!-- [[if?&is=`[*tldr_link*]:notempty`&then=`<p><a class="tldr_link" href="[~[*id*]~]#tldr">[*tldr_link*]</a></p>`]] -->
                    <div class="dropcap">[*content*]</div>
                </div>
            </div>
            [[multiTv?&tvName=`insight_block`&display=`all`]]
        </div>
    </section>
    [[if?&is=`[*tldr_link*]:notempty`&then=`
    <section id="tldr_content" style="display:none;padding-top:0;">
        <a name="tldr"></a>
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3"></div>
                <div class="cell start3 end6">
                    [*tldr_content*]
                </div>
            </div>
        </div>
    </section>
    `]]
    <section class="takeaway">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end6">
                    [*insight_takeaway*]
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start1 end3"></div>
                <div class="cell start3 end6">
                    <div class="social-share">
                        <span>Share this article</span>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=[(site_url)][~[*id*]~]&title=[*pagetitle*]" class="icon-linkedin" target="_blank">LinkedIn</a>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=[(site_url)][~[*id*]~]" class="icon-facebook" target="_blank">Facebook</a>
                        <a href="https://twitter.com/intent/tweet?text=[*pagetitle*]&url=[(site_url)][~[*id*]~]" class="icon-twitter" target="_blank">Twitter</a>
                        <a href="mailto:?subject=[*pagetitle*]" class="icon-mail" target="_blank">Mail</a>
                    </div>
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start1 end4">
                    [*insight_bottom*]
                </div>
            </div>
        </div>
        [[if?&is=`[*insight_widgetbox*]:notempty`&then=`
        <div class='insights-item-widget'>
            <div class='widget-wrapper'>
                <div class="widget-text">[*insight_widgetbox*]</div>
                <div class="widget-get-in-touch"><a href='[~6~]'>Get in touch</a></div>
            </div>
        </div>
        `]]
        [[if?&is=`[*insight_widgetbox*]:notempty`&then=`
        <div class='insights-item-widget-hidden'>
            <div class='widget-wrapper'>
                <div class="widget-text">[*insight_widgetbox*]</div>
                <div class="widget-get-in-touch"><a href='[~6~]'>Get in touch</a></div>
            </div>
        </div>
        `]]
    </section>

    <section class="siblings">
        <div class="container">
            [[prevnextPage? &folderid=`[*parent*]`&curId=`[*id*]`&prevTpl=`nextInsight`&nextTpl=`prevInsight`]]
            <div class="grid grid-3">
                [+pnp_next+][+pnp_prev+]
            </div>
        </div>
    </section>

    <div class="progress">
        <div class="bar"></div>
    </div>

    [[chunk?&file=`footer.php`]]
    <script src="assets/js/tooltip.js"></script>
    <script src="assets/js/jquery.glossarize.js"></script>
    <script src="assets/js/audioplayer.js"></script>
    <script>
        $(function(){
            $('audio').audioPlayer();
            
            $('section .container').glossarizer({
                sourceURL: '[~[*id*]~]tooltip',
                replaceOnce: true,
                callback: function(){
                    new tooltip();
                }   
            });
        });
    </script>
</body>

</html>