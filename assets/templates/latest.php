[[chunk?&file=`header.php`]]
<body class="page-news latest">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <h1 class="super">[[lang?&text=`news_title`]]</h1>
            [*content*]
        </div>
    </section>

    <section>
        <div class="container">
            <div class="news-filter">
                <a href="#all" data-param="all" class="selected">All</a>
                [[getNewsFilters]]
            </div>
        </div>
        <div class="container">
            <div class="news-list grid grid-3">
                [[DocLister?&parents=`[!newsParent!]`&depth=`2`&tpl=`news-list-item-tags`&orderBy=`STR_TO_DATE(longtitle, '%e %M %Y') DESC`&addWhereList=`template=6 AND hidemenu=0`&tvList=`news_cat`]]
                <div class="gutter-sizer" style="position:relative!important;display:block!important"></div>
            </div>
        </div>
    </section>
    [[chunk?&file=`footer.php`]]
</body>
</html>