[[chunk?&file=`header.php`]]
<body class="page-glossary">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    [*content*]
                </div>
            </div>
        </div>
    </section>

    <section class="glossary-list">
        <div class="container alphatop">
            <div class="grid grid-6">
                <div class="cell start3 end7" style="-ms-grid-column-span:3;">
                    <div class="alphaindex">
                        [[getGlossaryIndex]]
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="grid grid-6 terms">
                [[getGlossaryTerms]]
            </div>
        </div>
    </section>

    <section class="suggestion">
        <div class="container">
            <h2>Suggest a term</h2>
            <form id="glossary-form" action="[~394~]">
                <input type="hidden">
                <div class="form-row">
                    <input id="comp" type="text" name="company" class="visually-hidden">
                    <input id="term" type="text" name="term" placeholder="Term"><button>Submit</button>
                </div>
            </form>
            <div class="glossty" style="display:none;">
                <p>Thank you. We’ll be considering your suggestion soon.</p>
            </div>
            </div>
        </div>
    </section>

    [[chunk?&file=`footer.php`]]
    <script>
        // $("html").attr('itemscope', '');
        // $("html").attr('itemtype', "https://schema.org/FAQPage");

        $('.alphaindex a').each(function() {
            var letter = $(this).text().toLowerCase(),
                $grp = $('[data-alpha="'+letter+'"]');
            if (!!$grp.length) {
                $grp.wrapAll('<div class="group"></div>').parent().prepend('<a name="'+letter+'"></a>');
            }
        });

        $('.terms-list .def').hide();
        $('.terms-list h2 span').on('click', function() {
            $(this).parent().toggleClass('open').next('.def').slideToggle();
        });

        $('#glossary-form').on('submit', function (e) {
            e.preventDefault();
            var $form = $(this);
            $('.glossty').slideUp();
            if ($form.find(':invalid').length == 0) {
                $.ajax({
                    url: $form.attr('action'),
                    type: 'post',
                    data: $form.serialize(),
                    success: function (data, status, xhr) {
                        if (data=="success") {
                            $('.glossty').slideDown();
                            ga("send", "event", "Glossary form", "submit", $form.find("#term").val());
                        }
                    }
                });
            }
        });


        $(window).on('hashchange', function() {
            showHashTerm();
        });

        $(window).on('load', function() {
            if (!!window.IntersectionObserver) {
                const observer = new IntersectionObserver(entries => {
                    entries.forEach(entry => {
                        const id = $(entry.target).find('a').attr('name');
                        if (entry.intersectionRatio > 0) {
                            $('.alphaindex a[href$="#'+id+'"]').addClass('on');
                        } else {
                            $('.alphaindex a[href$="#'+id+'"]').removeClass('on');
                        }
                    });
                });

                // Track all sections that have an `id` applied
                $('.terms .letter').each(function() {
                    observer.observe(this);
                })
                // document.querySelectorAll('section[id]').forEach((section) => {
                //     observer.observe(section);
                // });
            }
        })

        if (window.location.hash != '') {
            showHashTerm();
        }

        function showHashTerm() {
            var h = window.location.hash.substr(1);
            if (!!$('a[name="'+h+'"]').length) {
                $('a[name="'+h+'"]').parent().next().slideDown();
                setTimeout(function() {
                    $('header').addClass('shrink');
                }, 100);
            }
        }
    </script>
</body>
</html>