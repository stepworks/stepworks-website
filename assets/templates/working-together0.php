[[chunk?&file=`header.php`]]
<body class="page-working-together">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <div class="intro-content">
                <h1 class="super"><u>W</u>holehearted<br>Brand Building<br>and <span class="red">you</span></h1>
                <div class="grid grid-3">
                    <div class="cell">
                        <dl>
                            <dt>wholehearted</dt>
                            <dd>| həʊlˈhɑːtɪd |<br><em>adjective</em><br>showing or characterised by complete <strong>sincerity</strong> and <strong>commitment</strong></dd>
                        </dl>
                    </div>
                </div>
                <div class="grid grid-3">
                    <div class="cell cell-left2">
                        <p class="intro">Wholehearted Brand Building is how we create valuable competitive advantages for the people and organisations we work with. </p>
                        <p>Effective brand building can give you a more valuable business, a more resilient organisation, stronger sales, happier teams.... Like any opportunity, there are risks.</p>
                        <p>Our approach aims to minimise risk and maximise returns. This means being completely open and realistic about the potential of your relationship with us.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="how">
        <div class="container">
            <div class="grid grid-6 animate">
                <div class="cell start1 end6 span5"><img src="assets/images/how1.jpg" alt=""></div>
            </div>
            <div class="grid grid-6 animate">
                <div class="cell start1 end3 span2"><h1>The brand building journey</h1></div>
                <div class="cell start3 end5 span2">
                    <p>Brand building is a journey of exploration. On our journey we face many possible paths.</p>
                    <p>Some lead to treasure. Some lead to danger. Some lead nowhere. To find our way we survey the terrain and explore various routes.</p>
                    <p>On any journey into the unknown, your first big decision is who will you take with you?</p>
                </div>
            </div>
            <div class="grid grid-6 animate">
                <div class="cell start2 end7 span5"><img src="assets/images/how2.jpg" alt=""></div>
            </div>
            <div class="grid grid-6 lm animate">
                <div class="cell start1 end3 span2"><h1>Choosing the right crew</h1></div>
                <div class="cell start3 end5 span2">
                    <p>Wholehearted Brand Building emphasises teamwork. This works because strong brands must resonate with many different types of people. A diverse crew creates more value.</p>
                    <p>Working with people from different disciplines, cultures and backgrounds widens creative inputs. Experience in different sectors cross-pollinates thinking.</p>
                    <p>Our approach values listening and learning from the unique insights and experiences of everyone involved.</p>
                </div>
            </div>
            <div class="grid grid-6 animate">
                <div class="cell start3 end6 span3"><img src="assets/images/how3.jpg" alt=""></div>
            </div>
            <div class="grid grid-6 lm animate">
                <div class="cell start1 end3 span2"><h1>The mindset for great results</h1></div>
                <div class="cell start3 end5 span2">
                    <p>In most of our branding projects we’re privileged to work with people who have an <em>ikigai</em> approach to life.</p>
                    <p>You will work with such people on our Wholehearted Brand Building teams because that’s what we look for when we recruit new colleagues.</p>
                </div>
            </div>
            <div class="grid grid-6 animate">
                <div class="cell start3 end6 span3"><img src="assets/images/ikigai.svg" alt=""></div>
            </div>
        </div>
    </section>

    <section class="team">
        <div class="container">
            <div class="grid grid-3 animate">
                <div class="cell cell-left2">
                    <h1>People with <em>ikigai</em></h1>
                    <p class="intro">You’ll encounter caring people who are really good at building brands on our multicultural, multidisciplined team. We make a conscious effort to be helpful, kind and understanding to everyone we encounter.</p>
                    <p>Working across different industry sectors helps us cross-pollinate ideas and introduces us to new innovations. We offer global reach as a member of the Magnet marketing and advertising global network.</p>
                </div>
            </div>
            <div class="team-list animate">
                [[DocLister?&parents=`9`&tpl=`team-item`&orderBy=`RAND()`&tvList=`team_photo`&addWhereList=`hidemenu=0`]]
            </div>
            <a name="careers"></a>
            <div class="grid grid-3 animate">
                <div class="cell cell-left2">
                    <h1>Sincerity and commitment</h1>
                    <h2>We encourage a caring environment so everyone feels free to share their thoughts. Wholehearted Brand Building seeks balance, and aligns what we believe, express, and do.</h2>
                    <p>Being wholehearted means commitment. A commitment to doing what’s right, what’s wow and what’s valuable. Not what’s easy or expedient.</p>
                    <p>Most people we work with value our approach because it’s the same approach they take to living their life.</p>
                </div>
            </div>

            <div class="grid grid-3 openings animate">
                <div class="cell cell-right2">
                    <div class="openings-content">
                        <div>
                            <h4>Creative roles</h4>
                            [[DocLister?&parents=`243`&tpl=`@CODE:<p>[+tv.job_blurb+]</p>`&filters=`tvd:role:is:creative`&noneTPL=`@CODE:<p><a href="mailto:careers@stepworks.co">Email one clickable link</a> to an online introduction to yourself. Show us no more than 10 examples of your strongest work. Explain your role and goal for each example. Aim to wow us.</p>`&addWhereList=`hidemenu=0`&tvList=`job_blurb`]]
                        </div>
                        <div>
                            <h4>Account management roles</h4>
                            [[DocLister?&parents=`243`&tpl=`@CODE:<p>[+tv.job_blurb+]</p>`&filters=`tvd:role:is:account`&noneTPL=`@CODE:<p><a href="mailto:careers@stepworks.co">Email your expected salary with resume.</a><br>Be meticulous.</p>`&addWhereList=`hidemenu=0`&tvList=`job_blurb`]]
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    [[chunk?&file=`footer.php`]]
</body>
</html>