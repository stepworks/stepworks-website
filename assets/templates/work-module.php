[[chunk?&file=`header.php`]]
<body class="page-work-item module">
    [[chunk?file=`page_header.php`]]

    <section class="hero">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end5">
                    <h1 class="super">[[DocInfo?&docid=`[*parent*]`&field=`longtitle`]]</h1>
                </div>
            </div>
        </div>
        <img src="[[DocInfo?&docid=`[*parent*]`&field=`work_cover_image`]]" alt="">
    </section>

    <section id="[*alias*]" class="module-intro">
        <div class="container">
            <div class="grid grid-6 animate">
                <div class="cell start3 end6">
                    <h1>[*longtitle*]</h1>
                    <p class="intro">[*description*]</p>
                    [*content*]
                </div>
            </div>
        </div>
    </section>
    <section class="module-content">
        <div class="container">
            [[multiTV?&docid=`[*id*]`&tvName=`work_block`&display=`all`]]
        </div>
    </section>

    <section class="related">
        <div class="container">
            <h3>Related work</h3>
            <div class="grid grid-3">
                [[DocLister?&documents=`[[getRelatedWork]]`&tpl=`related-work`&tvList=`work_cover_image`]]
            </div>
        </div>
    </section>

    <div class="progress"><div class="bar"></div></div>
    [[chunk?&file=`footer.php`]]
</body>
</html>