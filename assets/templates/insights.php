[[chunk?&file=`header.php`]]
<body class="page-insights">
    [[chunk?file=`page_header.php`]]

    <!-- <section id="brief-engine" class="intro animate">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    [*toolbox_cbe_intro*]
                </div>
            </div>
        </div>
    </section> -->

    <section id="insights" class="intro">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    [*toolbox_insight_heading*]
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start3 end6" style="-ms-grid-column-span:3;">
                    <figure>
                        <img src="[*toolbox_insight_image*]" alt="">
                    </figure>
                    <p><br></p>
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start3 end6" style="-ms-grid-column-span:3;">
                    [*toolbox_insight_intro*]
                </div>
            </div>
        </div>
    </section>

    <section class="insight-list">
        <div class="container">
            <div class="grid grid-3 list-inner">
                [[DocLister?&parents=`[*id*]`&tpl=`insight-list-item`&orderBy=`menuindex ASC`&tvList=`read_time`&addWhereList=`hidemenu=0`]]
            </div>
        </div>
    </section>
    [[chunk?&file=`footer.php`]]
</body>
</html>