[[chunk?&file=`header.php`]]

<body class="page-careers">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <!-- [*content*]  -->
                    <h1 class="super">Wholehearted careers</h1>
                    <p class="intro">We’re a brand building and digital design firm on the lookout for talented people. </p>
                    <p>Stepworks is a great place to work, and a great place to come from. Team members who have moved on tell
                        us their time here added a lot of credibility to their CV. It’s often sad to say goodbye but we love to see people
                        get ahead. </p>
                    <p>Joining Stepworks has proved to be a positive career move for many colleagues and former colleagues. Where possible,
                        we help people progress by aligning projects with their passions and ambitions. </p>
                    <!-- <p><a href="[~[*id*]~]#careers">See current openings</a></p> -->
                </div>
            </div>
        </div>
    </section>
    
    <!-- <section>
        <a name="careers"></a>
        <div class="container">
            <h1>Current openings</h1>
            <div class="grid grid-6 openings animate">
                <div class="cell start1 end7">
                    <div class="openings-content"> -->
                        <!-- <div>
                            <h4>Account management role</h4>
                            [[DocLister?&parents=`243`&tpl=`@CODE:<p>[+tv.job_blurb+]</p>`&filters=`tvd:role:is:account`&noneTPL=`@CODE:<p>
                                <a href="mailto:careers@stepworks.co">Email your expected salary with resume.</a><br>Be meticulous.
                            </p>
                            `&addWhereList=`hidemenu=0`&tvList=`job_blurb`]]
                        </div> -->
                        <!-- <div>
                            <h4>Project management role</h4>
                            [[DocLister?&parents=`243`&tpl=`@CODE:<p>[+tv.job_blurb+]</p>`&filters=`tvd:role:is:project`&noneTPL=`@CODE:<p>
                                <a href="mailto:careers@stepworks.co">Email your expected salary with resume.</a><br>Be meticulous.
                            </p>
                            `&addWhereList=`hidemenu=0`&tvList=`job_blurb`]]
                        </div> -->
                        <!-- <div>
                            <h4>Administration role</h4>
                            [[DocLister?&parents=`243`&tpl=`@CODE:<p>[+tv.job_blurb+]</p>`&filters=`tvd:role:is:admin`&noneTPL=`@CODE:<p>
                                <a href="mailto:careers@stepworks.co">Email your expected salary with resume.</a><br>Be meticulous.
                            </p>
                            `&addWhereList=`hidemenu=0`&tvList=`job_blurb`]]
                        </div> -->
                        <!-- <div>
                            <h4>Creative role</h4>
                            [[DocLister?&parents=`243`&tpl=`@CODE:<p>[+tv.job_blurb+]</p>`&filters=`tvd:role:is:creative`&orderBy=`menuindex ASC`&noneTPL=`@CODE:<p>
                                <a href="mailto:careers@stepworks.co">Email one clickable link</a> to an online introduction to yourself.
                                Show us no more than 10 examples of your strongest work. Explain your role and goal for each example. Aim to
                                wow us.
                            </p>`&addWhereList=`hidemenu=0`&tvList=`job_blurb`]]
                        </div> -->
                        <!-- <div>
                            <h4>Development role</h4>
                            [[DocLister?&parents=`243`&tpl=`@CODE:<p>[+tv.job_blurb+]</p>`&filters=`tvd:role:is:dev`&noneTPL=`@CODE:<p><a
                                    href="mailto:careers@stepworks.co">Email your expected salary with resume.</a><br>Be meticulous.</p>
                            `&addWhereList=`hidemenu=0`&tvList=`job_blurb`]]
                        </div> -->
                        <!-- <div>
                            <h4>Business development role (Singapore)</h4>
                            [[DocLister?&parents=`243`&tpl=`@CODE:<p>[+tv.job_blurb+]</p>`&filters=`tvd:role:is:business`&orderBy=`menuindex ASC`&noneTPL=`@CODE:<p>
                                <a href="mailto:careers@stepworks.co">Email your expected salary with resume.</a><br>Be meticulous.
                            </p>`&addWhereList=`hidemenu=0`&tvList=`job_blurb`]]
                        </div> -->
                        <!-- <div></div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->


    <section class="animate">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-right2">
                    <figure>
                        <img src="assets/images/careers-HK-branding-digital-agency.jpg" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="mindset animate">
        <div class="container">
            <div class="grid grid-6">
                <div class='cell start1 end3'>
                    <h1>The mindset for a great career</h1>
                </div>
                <div class="cell cell start3 end6">
                    <p>In our work we’re privileged to collaborate with people with a centred approach to life.</p>
                    <figure>
                        <!-- <img src="assets/images/ikigai.svg" alt=""> -->
                        <script src="assets/js/lottie.min.js"></script>
                        <script src="assets/js/lottie-interactivity.min.js"></script>
                        <lottie-player id="ikigai" src="/assets/files/lottie.json"  background="transparent"  speed="1"  style="width: 100%; height: auto;"></lottie-player>
                        <script>
                            LottieInteractivity.create({
                                player: '#ikigai',
                                mode: 'scroll',
                                actions: [
                                    {
                                        visibility: [0.50, 0.98],
                                        type: 'play',
                                    },
                                    {
                                        visibility: [0.98, 1.0],
                                        type: "loop",
                                        frames: [56, 85]
                                    },
                                ]
                            });
                        </script>
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="animate">
        <div class="container">
            <div class="grid grid-6">
                <div class='cell start1 end3'>
                    <h1>Get ahead with teamwork</h1>
                </div>
                <div class="cell cell start3 end5">
                    <p>Teamwork works because strong brands must resonate with many different types of people. We can create more value with
                        diversity.</p>

                    <p>Working with people from different disciplines, cultures and backgrounds widens creative inputs. Experience in
                        different sectors cross-pollinates thinking.</p>

                    <p>Our team-first culture values listening, and learning from the unique insights and experiences of everyone present.
                    </p>
                </div>
                <div class="cell cell start3 end6">
                    <figure>
                        <img src="assets/images/shadow-eric-careers.png" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="animate">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell cell start3 end5">
                    <h2>Here’s what to expect as a Stepworks colleague</h2>

                    <p>The chance to do your best work in a caring environment where everyone is welcomed to share their thoughts. It’s
                        important to us that people in our team have the opportunity to grow. </p>

                    <p>Our standards are pretty high. We love seeing team members take ownership and grow into leaders in their own right
                        (both here and beyond).</p>
                </div>
            </div>
        </div>
    </section>

    <section class="animate">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end5">
                    <figure>
                        <img src="assets/images/create-positive-change-careers.png" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="animate">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell cell start3 end5">
                    <h2>Some of the ways we look after each other</h2>

                    <p>Stepworks people enjoy competitive remuneration. You can look forward to:
                    <ul>
                        <li>Flexible work from home, and understanding for you, your family, children and pet time<br><br></li>
                        <li>Many opportunities and encouragement for career and personal development including training and education
                            sponsorship<br><br></li>
                        <li>Medical and dental, and annual body check allowance<br><br></li>
                        <li>Living a carbon negative life as we offset your estimated work and home carbon emissions through <a
                                href="https://ecologi.com/business/climate-positive-workforce" target="_blank">this program</a><br><br></li>
                        <li>Lots of little perks and fun team activities and annual get togethers</li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="animate">
        <div class="container">
            <div class="grid grid-2 margin-bottom">
                <div class="cell">
                    <figure>
                        <img src="assets/images/sean-careers.png" alt="">
                    </figure>
                </div>
                <div class="cell">
                    <figure>
                        <img src="assets/images/climbing-careers.jpg" alt="">
                    </figure>
                </div>
            </div>
            <div class="grid grid-3">
                <div class="cell">
                    <figure>
                        <img src="assets/images/hong-kong-branding-digital-agency-careers.jpg" alt="">
                    </figure>
                </div>
                <div class="cell">
                    <figure>
                        <img src="assets/images/kd-careers.png" alt="">
                    </figure>

                </div>
                <div class="cell">
                    <figure>
                        <img src="assets/images/fox-careers.png" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    [[chunk?&file=`footer.php`]]
</body>

</html>