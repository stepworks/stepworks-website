[[chunk?&file=`header.php`]]

<body class="page-methods">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    [*content*]
                </div>
            </div>
        </div>
    </section>

    <section class="brand-building animate" id="brand-building">
        <div class="container">
            <div class="grid grid-3" style="position:relative;z-index:100">
                <div class="cell cell-left2">
                    <h1>[*method_building_process_heading*]</h1>
                </div>
            </div>
            <div class="brand-building-block">
                <div class="brand-building-container">
                    <div class="process-container">
                        <ul class="process-list">
                            <li class="process-1"><strong>[*method_building_process_1_heading*]</strong></li>
                            <li class="process-2"><strong>[*method_building_process_2_heading*]</strong></li>
                            <li class="process-3"><strong>[*method_building_process_3_heading*]</strong></li>
                            <li class="process-4"><strong>[*method_building_process_4_heading*]</strong></li>
                            <li class="process-5"><strong>[*method_building_process_5_heading*]</strong></li>
                            <li class="process-6"><strong>[*method_building_process_6_heading*]</strong></li>
                            <li class="process-7"><strong>[*method_building_process_7_heading*]</strong></li>
                            <li class="process-8"><strong>[*method_building_process_8_heading*]</strong></li>
                        </ul>
                    </div>

                    <div class="capabilities-block">
                        <a href="#" class="link-btn">[[lang?&text=`see_more`]]</a>
                        <div class="capabilities-list">
                            <div class="capabilities-container">
                                <div class="capabilities-box">[*method_building_process_1_content*]</div>
                                <div class="capabilities-box">[*method_building_process_2_content*]</div>
                                <div class="capabilities-box">[*method_building_process_3_content*]</div>
                                <div class="capabilities-box">[*method_building_process_4_content*]</div>
                                <div class="capabilities-box">[*method_building_process_5_content*]</div>
                                <div class="capabilities-box">[*method_building_process_6_content*]</div>
                                <div class="capabilities-box">[*method_building_process_7_content*]</div>
                                <div class="capabilities-box">[*method_building_process_8_content*]</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="animate pb0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3">
                    <h1>[*method_alignment_heading*]</h1>
                </div>
                <div id="alignment" class="cell start3 end6">
                    <img src="[*method_alignment_image*]" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="process animate">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell">
                    <h2>[*method_process_heading*]</h2>
                </div>
            </div>
            <div class="grid grid-3">
                <div class="cell steps-container">
                    <div class="steps">
                        <a href="#clarify">[*method_clarify_heading*]</a>
                        <a href="#simplify">[*method_simplify_heading*]</a>
                        <a href="#amplify">[*method_amplify_heading*]</a>
                    </div>
                </div>
                <div class="cell cell-right2 illustrations">
                    <figure id="clarify">
                        <img src="[*method_clarify_image*]" alt="">
                        <figcaption>[*method_clarify_content*]</figcaption>
                    </figure>
                    <figure id="simplify">
                        <img src="[*method_simplify_image*]" alt="">
                        <figcaption>[*method_simplify_content*]</figcaption>
                    </figure>
                    <figure id="amplify">
                        <img src="[*method_amplify_image*]" alt="" style="margin-top: 9rem;">
                        <figcaption>[*method_amplify_content*]</figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-red reputation animate">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1>[*method_reputation_heading*]</h1>
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start3 end5">
                    <figure class="branding">
                        <img src="assets/images/method-branding.svg" alt="">
                        <figcaption>[*method_reputation_branding_content*]</figcaption>
                    </figure>
                </div>
                <div class="cell start5 end7">
                    <figure class="marcomms">
                        <img src="assets/images/method-marcomms.svg" alt="">
                        <figcaption>[*method_reputation_marketing_content*]</figcaption> 
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="advantage animate">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1>[*method_advantage_heading*]</h1>
                </div>
            </div>
            <div class="grid grid-3 graphics">
                [[multiTv?&tvName=`method_adv_list`&display=`all`]]
            </div>
        </div>
    </section>

    <!-- <section class="capabilities animate">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1>Capabilities</h1>
                </div>
            </div>
            <div class="grid grid-3">
                <div class="cell"></div>
                <div class="cell cell-right2">
                    <div class="cap-list">
                        [[multiTv?&tvName=`results_cap_list`]]
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    [[chunk?&file=`footer.php`]]

    <script src="assets/js/vendor/gsap.min.js"></script>
    <script src="assets/js/vendor/scroll-hint.min.js"></script>
    <script src="assets/js/vendor/ScrollTrigger.min.js"></script>
    <script>
        ScrollTrigger.create({
            once: true,
            trigger: ".process-list",
            start: "top bottom",
            end: "+=1500",
            toggleClass: 'active'
        });

        new ScrollHint('.brand-building-block', {
            suggestiveShadow: true
        });

    </script>
</body>
</html>