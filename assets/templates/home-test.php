[[chunk?&file=`header_dev.php`]]
<body class="page-home new2">
    [[chunk?file=`page_header_alt2.php`]]
    <div id="fp-wrapper">
        <section class="intro">
            <div class="container a0">
                <h1 class="super">Brand-led <br>transformation</h1>
                <h2 class="a1">Transformation begins in the mind. Transform <br>what people think of your business with <br><a href="[~3~]">Wholehearted Brand Building</a> and website design.</h2>
                <h2 class="caps" style="margin-top: 1.5rem; color: #b5b5b2;"><span><a href="[~2~]?filter=branding&type=cap">Branding.</a> </span><span><a href="[~2~]?filter=digital&type=cap">Digital.</a> </span><span><a href="[~2~]?filter=campaigns&type=cap">Campaigns.</a></span> <span><a href="[~2~]?filter=comms&type=cap">Communications.</a></span></h2>
            </div>
        </section>

        [[DocLister? &parents=`2`&tpl=`home-work-item-2-section` &orderBy=`menuindex ASC` &tvList=`work_cover_image,work_cover_image_md,work_cover_image_sm,work_cover_video` &addWhereList=`hidemenu=0` &filters=`tv:featured:is:yes` &display=`3`]]
        <!-- <section class="work">
        </section> -->

        <section class="video">
            <!-- <div class="container">
                <a class="more" href="[~2~]">More brands we&rsquo;ve built</a>
                <p><br><br><br></p>
            </div> -->
            <div class="container">
                <div class="grid grid-3">
                    <div class="cell">
                        <h2>Hong Kong-based brand building agency Stepworks is trusted by leading businesses for our branding and digital expertise. We can be your branding and digital agency, creative agency, advertising agency, and website design agency.</h2>
                        <a href="[~7~]" class="more">More about us</a>
                    </div>
                    <div class="cell cell-right2">
                        <figure class="iframe-wrapper"><div class="iframe-container"><video class="cover" autoplay loop muted playsinline><source src="assets/media/SW-Brand-Video-Thumbnail-v12.mp4" type="video/mp4"></video><div id="player"></div></div><figcaption class="h1">Stepworks in <br>60 seconds</figcaption></figure>
                    </div>
                </div>
            </div>
        </section>

        <section class="results animate fp-auto-height">
            <div class="container">
                <h1>Getting results</h1>
                <div class="grid grid-3">
                    <div class="cell">
                        <h2>You get valuable competitive advantages from our proven, results-driven Wholehearted Brand Building methodology. We&rsquo;ve worked with ambitious businesses to successfully complete over 270 complex branding and digital initiatives, many targeting global audiences.</h2>
                        <a class="more" href="[~3~]">How we get results</a>
                    </div>
                    <div class="cell cell-right2 graphic">
                        <video class="d" autoplay muted loop playsinline>
                            <source src="assets/media/approach2.mp4" type="video/mp4">
                        </video>
                        <img class="m" src="assets/images/approach-m.png" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="news animate fp-auto-height">
            <div class="container">
                <h1>Latest news</h1>
                [*home_news_text*]
                <div class="grid grid-3">
                    [[DocLister?&parents=`8`&tpl=`home-news-item`&display=`3`&orderBy=`STR_TO_DATE(longtitle, '%e %M %Y') DESC`&addWhereList=`hidemenu=0`]]
                </div>
                <a class="more" href="[~8~]">See all news</a>
            </div>
        </section>


        <div class='video-background'>
            <iframe id='sw-video' width="1280" height="720" src="https://www.youtube.com/embed/uH2BTOO1puc?controls=0&modestbranding=1&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        [[chunk?&file=`footer.php`]]

    </div>
    <!-- <script src="assets/js/vendor/gsap.min.js"></script>
    <script src="assets/js/vendor/ScrollTrigger.min.js"></script> -->
    <script src="assets/js/vendor/fullpage.extensions.min.js"></script>
    <script async>
        $(document).ready(function() {
           $('.intro').addClass('show');
        });

        $('#fp-wrapper').fullpage({
            licenseKey: '89E0D8C6-FFE34DA5-B54492D1-39EEE869',
            autoScrolling: $(window).width() > 768,
            scrollingSpeed: 700,
            easingcss3: 'ease-out',
            scrollBar: true,
            lazyLoading: false,
            // responsiveWidth: 768,
            sectionSelector: 'section,footer',
            fixedElements: 'header,.acsb-trigger,.acsb-widget,.acsb-main',
            normalScrollElements: '.acsb-main,nav',
            bigSectionsDestination: 'top'
            // onLeave: function(origin, dest, dir) {
            //     if ($(origin.item).hasClass('work-item') && $(dest.item).hasClass('video')) {
            //         // fullpage_api.fitToSection();
            //         setTimeout(function() {
            //             fullpage_api.setFitToSection(false);
            //             fullpage_api.setAutoScrolling(false);
            //         }, 300);
            //         // console.log('fit to section & auto scrolling off');
            //     }

            //     if ($(origin.item).hasClass('video') && dir == 'up') {
            //         fullpage_api.fitToSection();
            //         setTimeout(function() {
            //             fullpage_api.setFitToSection(true);
            //             fullpage_api.setAutoScrolling(true);
            //         },300);
            //         // console.log('fit to section & auto scrolling on');
            //     }
            // }
        });

        // var tl = gsap.timeline();
        // if ($(window).scrollTop() == 0) {
        //     tl.from('.intro .super', { opacity: 0, duration: 1, ease: "power1.out" });
        //     tl.from('.intro .a1', { opacity: 0, duration: 1, ease: "power1.out" }, ">0.5");
        //     tl.add(function() { $('.caps').addClass('show'); });
        // } else {
        //     if not starting from the top
        //     $('.caps').addClass('show');
        // }

        var player, playercontainer;

        $('.video .cover').on('click', function () {
            loadYTscripts();
            playercontainer = 'player';
        });

        function loadYTscripts() {
            if (!$('#yts').length) {
                // console.log('load scripts');
                var tag = document.createElement('script'),
                    scripts = document.getElementsByTagName('script'),
                    lastScriptTag = scripts[scripts.length - 1];
                tag.src = "https://www.youtube.com/iframe_api";
                tag.id = "yts";
                lastScriptTag.parentNode.insertBefore(tag, lastScriptTag.nextSibling);
            }
        }

        function onYouTubeIframeAPIReady() {
            // console.log('api ready');
            player = new YT.Player(playercontainer, {
                videoId: '[*video_id*]',
                playerVars: {
                    rel: 0,
                    width: 1280,
                    height: 720,
                    modestbranding: 1,
                    origin: '[(site_url)]',
                    controls: 1,
                    fs: 1
                },
                events: {
                    'onReady': onPlayerReady
                    // 'onStateChange': onPlayerStateChange
                }
            });
        }

        function onPlayerReady(event) {
            event.target.setPlaybackQuality('hd1080');
            $('.video .cover, .video figcaption').fadeOut('fast');
            event.target.playVideo();
            // ga('send', 'event', 'Video', 'play', 'Homepage video new');
        }
    </script>
</body>
</html>