[[chunk?&file=`header_brief.php`]]
<body class="page-website-audit-tool-report">
    [[chunk?file=`page_header_mini.php`]]
    <div id='main-container'>

            <div class='fetching'>
                <div id="loader" class='loader-wrapper'>
                    <div class="loader-inner">
                        <div class="loader"></div>
                    </div>
                    <div class="loader-text">Please wait. We are fetching your report.</div>
                    <div class="error" id="error"></div>
                </div>
            </div>

            <div id="webaudit" class="grid">
                <div class="cell">
                    <div class='questions-wrapper'>
                        <div class="results">
                            <div class='report-header'>
                                <div class='report-heading'>
                                    <h3>Overall website assessment result</h3>
                                    <p>Here’s how <span class="url" id="url"></span> scored for brand building effectiveness.</p>
                                </div>
                                <img class="logo" src="assets/images/sw_logo.svg" alt="Stepworks">
                            </div>
                            <p class="track final-score">
                                <span class="scorebar overall"><span id="overall-bar" class="score"></span></span><span id="overall-score" class="score-val"></span><br>
                            </p>
                            <div class="totalbar">
                                <div class="poor">0-59</div>
                                <div class="average">60-79</div>
                                <div class="great">80-100</div>
                            </div>

                            

                            <div class='action-buttons'>
                                <br />
                                <a href="#" class="btn" id="print">Save / Print the report</a></p>
                                <br/>
                            </div>

                            <p class="pleasenote">Please note that your score is an approximation of your website brand building effectiveness. A Stepworks specialist can offer a deeper analysis.</p>

                            <p class='see-other' style="margin:2rem 0 0;"><a href="#" class="showall">See other available assessment outcomes</a></p>
                            <div class="allresults" style="display:none;margin-top:2rem;">
                                <p class="result poor"><strong>Poor</strong> – Our preliminary website assessment shows that your website effectiveness is low. An online experience with your organisation is likely to frustrate and disappoint visitors. They will form negative perceptions of your organisation. Your current website may be working against you and not supporting your organisational goals.</p>
                                <p class="result average"><strong>Average</strong> – Our preliminary website assessment shows that your website effectiveness is OK. Visitors are likely to experience an adequate online encounter. You almost certainly have an opportunity for improvement so that your website better supports your organisational goals.</p>
                                <p class="result great"><strong>Great</strong> – Well done, our preliminary website assessment shows that you’re getting it right. Visitors are likely to perceive your organisation as efficient, helpful and contemporary.</p>
                                <p><br></p>
                            </div>

                            <!-- <p style="margin:2rem 0 0;"><a href="#" class="breakdown">Show detailed breakdown</a></p> -->
                            <div class="scores" id="brand" style="margin-top:2rem;">
                                <h4>Here’s a breakdown of your website brand building effectiveness score</h4>
                                <p>Our automated assessment reflects how likely people will understand the value of your business via your website. Branding is essentially human, so any machine analysis has limitations – it is a useful way for brand builders to get a more objective view of their website.</p>
                                <div class="brand">
                                    <input type="hidden" name="bb" value="">
                                    <p><strong>Website brand building effectiveness score</strong></p>

                                    <p class="track"><strong class="red">Digital brand experience</strong><br>
                                        <span class="scorebar bb-experience"><span id="experience-bar" class="score"></span></span><span id="experience-score" class="score-val"></span><br>
                                        This score reflects the degree visitors will engage positively with your website and brand based on modern user expectations.
                                    </p>

                                    <p class="track"><strong class="red">Value delivered to business</strong><br>
                                        <span class="scorebar bb-business"><span id="business-bar" class="score"></span></span><span id="business-score" class="score-val"></span><br>
                                        This score indicates how well your website builds brand and business value.
                                    </p>

                                    <p class="track"><strong class="red">Design effectiveness</strong><br>
                                        <span class="scorebar bb-design"><span id="design-bar" class="score"></span></span><span id="design-score" class="score-val"></span><br>
                                        This score indicates how effectively your website’s graphic design and brand identity positively influence visitors.
                                    </p>
                                </div>
                            </div>
                            <div class="scores" id="lighthouse">
                                <div class="lighthouse">
                                    <input type="hidden" name="lh" value="">
                                    <p><strong>Website technical effectiveness score</strong></p>
                                    <p>It’s important to pay attention to the technical side when building an effective and persuasive website. The following dimensions influence the overall user experience. Scores above [75] suggest above average effectiveness.</p>

                                    <p class="track"><strong class="red">Performance</strong><br>
                                        <span class="scorebar lh-performance"><span id="performance-bar" class="score"></span></span><span id="performance-score" class="score-val"></span><br>
                                        This reflects how quickly visitors see your website pages arrive (people are impatient and making someone wait forms a poor impression). It might look fast to you, but be slow on some devices or in some locations.</p>

                                    <p class="track"><strong class="red">Accessibility</strong><br>
                                        <span class="scorebar lh-accessibility"><span id="accessibility-bar" class="score"></span></span><span id="accessibility-score" class="score-val"></span><br>
                                        A website friendly to people with disabilities promotes an inclusive company culture and creates a positive impression on visitors.
                                    </p>
                                    <p class="track"><strong class="red">Best practices</strong><br>
                                        <span class="scorebar lh-bestpractices"><span id="bestpractices-bar" class="score"></span></span> <span id="bestpractices-score" class="score-val"></span><br>
                                        This checks for common mistakes in websites. Website standards change and this score reflects your site’s compliance with relevant modern standards.
                                    </p>
                                    <p class="track"><strong class="red">SEO effectiveness</strong><br>
                                        <span class="scorebar lh-seo"><span id="seo-bar" class="score"></span></span> <span id="seo-score" class="score-val"></span><br>
                                        Search engines aim to determine the relevance of content. A high score indicates your website content can be correctly categorised for valuable search engine visibility.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="cta-bottom">
                            <div class="cell">
                            <h3>Would you be interested to get our input on how to create more value from your website?</h3>
                                <p class='content'>Stepworks has considerable experience creating competitive advantages through developing effective websites that align business, brand and, most important, audience needs.</p>
                                <p class='get-in-touch'><a class="btn" href="mailto:hello@stepworks.co">Get in touch</a></p>
                                <p class='print-only'>Get in touch by phone <a href='tel:+852 3678 8700' class='link'>+852 3678 8700</a> or by email <a href='mailto:hello@stepworks.co' class='link'>hello@stepworks.co</a>.</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class='pageNumberOne-report'>Page 1 of 2</div>
                <div class='pageNumberTwo-report'>Page 2 of 2</div>
            </div>   
    <!-- <div class="progress">
        <div class="bar"></div>
    </div> -->
</div>
    [[chunk?&file=`footer.php`]]

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="assets/js/vendor/wNumb.min.js"></script>
    <script src="./assets/js/nouislider.js"></script>
    <script src="./assets/js/web-audit-tool.js"></script>
    <!-- <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-4948962-2', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script> -->
    <script>
        $("#report-url").attr("href",window.location.href);
        window.onbeforeunload = function () {
            window.scrollTo(0, 0);
        }
    </script>    
</body>
</html>