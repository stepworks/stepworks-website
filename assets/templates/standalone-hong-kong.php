[[chunk?&file=`header.php`]]
<body class="page-standalone">
    [[chunk?file=`page_header.php`]]
    <section class="intro mb0">
        <div class="container a0">
            <div class="grid grid-3">
              <div class="cell cell-left2">
                <h1 class="super">[*standalone_heading*]</h1>
                <h2 class="a1">[*standalone_sub_heading*]</h2>
                [*standalone_content*]
              </div>
              <div class="cell"></div>
            </div>
            <br>
            <br>
            <div class="grid grid-3">
              <div class="cell"></div>
              <div class="cell cell-right2 graphic">
                <img src="[*standalone_image*]" alt="Hong Kong" />
              </div>
            </div>
        </div>
    </section>

    <section class="content pb0">
        <div class="container">
          <div class="grid grid-3">
            <div class="cell">
              <h1>[*standalone_heading_business_hub*]</h1>
            </div>
            <div class="cell">[*standalone_content_business_hub*]</div>
          </div>

          <div class="client-list">
            <div class="logo-list">
              [[multiTv?&tvName=`standalone_logos_list`&display=`all`]]
              <!-- <img src="assets/images/client-logos/standalone/hk/Alliance-logo.svg" alt="Alliance" />
              <img src="assets/images/client-logos/standalone/hk/Daycraft-logo.svg" alt="Daycraft" />
              <img src="assets/images/client-logos/standalone/hk/Fullerton-logo.svg" alt="Fullerton" />
              <img src="assets/images/client-logos/standalone/hk/Hang-Lung-logo.svg" alt="Hang Lung" />
              <img src="assets/images/client-logos/standalone/hk/HKEX-logo.svg" alt="HKEX" />
              <img src="assets/images/client-logos/standalone/hk/HKTramways-logo.svg" alt="HKTramways" />
              <img src="assets/images/client-logos/standalone/hk/HKFWS-logo.svg" alt="HKFWS" />
              <img src="assets/images/client-logos/standalone/hk/ICBC-logo.svg" alt="ICBC" />
              <img src="assets/images/client-logos/standalone/hk/Judiciary-logo.svg" alt="Judiciary" />
              <img src="assets/images/client-logos/standalone/hk/KEF-logo.svg" alt="KEF" />
              <img src="assets/images/client-logos/standalone/hk/Kerry-hotel-logo.svg" alt="Kerry Hotels" />
              <img src="assets/images/client-logos/standalone/hk/marriott-bonvoy-logo.svg" alt="Marriott Bonvoy" />
              <img src="assets/images/client-logos/standalone/hk/Matilda-logo.svg" alt="Matilda" />
              <img src="assets/images/client-logos/standalone/hk/Swire-logo.svg" alt="Swire Pacific" />
              <img src="assets/images/client-logos/standalone/hk/YF-life-logo.svg" alt="YF Life" /> -->
            </div>
          </div>

          <div class="grid grid-3">
            <div class="cell"></div>
            <div class="cell">[*standalone_learn_more_content*]</div>
            <div class="cell"></div>
          </div>
        </div>
    </section>

    <section class="contact">
        <a name="contact-us"></a>
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1>[*standalone_lets_talk_heading*]</h1>
                </div>
            </div>
            <div class="grid grid-3">
                <div class="cell">
                  <h2>[*standalone_lets_talk_subheading*]</h2>
                </div>
                <div class="cell">[*standalone_lets_talk_content*]</div>
            </div>
        </div>
    </section>

    <section class="full-image mb0">
      <div id="map-hk" style="height:500px;max-height:50vh;"></div>
    </section>
    
    [[chunk?&file=`footer.php`]]
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeLAqr1b4yClL2h9mAk1h2tSClMhmr8Fo&callback=initMap"
    type="text/javascript"></script>
    
    <script async>
    $(document).ready(function() {
        $('.intro').addClass('show');
        $('a[href^="#"]').filter(function(){
          return $(this).attr('href').match(/\#\w+/)
        }).on('click', function(e){
          e.preventDefault();
          const anchor = $(this).attr('href').slice(1);
          $('html, body').animate({ scrollTop: $(`a[name="${anchor}"]`).offset().top }, 500);
        });
    });

    const onScrollStop = function onScrollStop(callback) {
        var isScrolling = undefined;
        window.addEventListener('scroll', function(e) {
            clearTimeout(isScrolling);
            isScrolling = setTimeout(function() {
                callback();
            }, 200);
        }, false);
    };
    </script>

    <script>
      var mapHK;
      function initMap() {
          var latlngHK = new google.maps.LatLng(22.280274, 114.15557);
          
          var styles = [
              {
                  "stylers": [
                      {"gamma": 0.5}, //Gama value.
                      {"hue": "#ff0022"}, //Hue value.
                      {"saturation": -100} //Saturation value.
                  ]
              }
          ];

          var mapOptions = {
              scrollwheel: false,
              zoom: 19,
              styles: styles,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              mapTypeControl: false,
              streetViewControl: false,
              scaleControl: true,
              zoomControl: true,
              zoomControlOptions: {
                  style: google.maps.ZoomControlStyle.SMALL
              }
          };

          mapHK = new google.maps.Map(document.getElementById("map-hk"), Object.assign({
            center: latlngHK
          }, mapOptions));
          
          var markerHK = new google.maps.Marker({
              position: latlngHK,
              icon: '[(site_url)]assets/images/sw-map-marker.png',
              map: mapHK,
          });

          // google.maps.event.addDomListener(window, 'load', initialize);
          google.maps.event.addDomListener(window, "resize", function () {
              var centerHK = mapHK.getCenter();
              google.maps.event.trigger(mapHK, "resize");
              mapHK.setCenter(centerHK);
          });
      }
    </script>
</body>

</html>