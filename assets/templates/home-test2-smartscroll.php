[[chunk?&file=`header.php`]]
<body class="page-home new2">
    [[chunk?file=`page_header_svg.php`]]

    <section class="intro">
        <div class="container a0">
            <h2 class="a1">Create competitive advantages with</h2>
            <h1 class="super"><span class="a2"><u>W</u>holehearted</span><br><span class="a3">Brand Building</span></h1>
            <h2 class="caps" style="margin-top: 1.5rem; color: #b5b5b2;"><span class="">Branding. </span><span class="">Digital. </span><span class="">Campaigns.</span> <span class="">Communications.</span></h2>
        </div>
    </section>

    <section class="work">
        [[DocLister?&parents=`2`&tpl=`home-work-item-2`&orderBy=`menuindex ASC`&tvList=`work_cover_image,work_cover_image_md,work_cover_image_sm,work_cover_video`&addWhereList=`hidemenu=0`&display=`3`]]
    </section>

    <section class="video">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell">
                    <h2>Hong Kong-based brand building agency Stepworks is trusted by leading businesses for our branding and digital expertise. We can be your branding and digital agency, creative agency, advertising agency, and website design agency.</h2>
                    <a href="[~7~]" class="more">More about us</a>
                </div>
                <div class="cell cell-right2">
                    <figure class="iframe-wrapper"><div class="iframe-container"><video class="cover" autoplay loop muted playsinline><source src="assets/media/SW-Brand-Video-Thumbnail-v12.mp4" type="video/mp4"></video><div id="player"></div></div><figcaption class="h1">Stepworks in <br>60 seconds</figcaption></figure>
                </div>
            </div>
        </div>
    </section>

    <section class="results animate">
        <div class="container">
            <h1>Getting results</h1>
            <div class="grid grid-3">
                <div class="cell">
                    <h2>You get valuable competitive advantages from our results driven Wholehearted Brand Building methodology. It’s proved a success for ambitious businesses with globally-minded audiences – over nearly [500] complex and challenging branding and digital initiatives.</h2>
                    <a class="more" href="[~3~]">How we get results</a>
                </div>
                <div class="cell cell-right2 graphic">
                    <video class="d" autoplay muted loop playsinline>
                        <source src="assets/media/approach2.mp4" type="video/mp4">
                    </video>
                    <img class="m" src="assets/images/approach-m.png" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="news animate">
        <div class="container">
            <h1>Latest news</h1>
            [*home_news_text*]
            <div class="grid grid-3">
                [[DocLister?&parents=`8`&tpl=`home-news-item`&display=`3`&orderBy=`STR_TO_DATE(longtitle, '%e %M %Y') DESC`&addWhereList=`hidemenu=0`]]
            </div>
            <a class="more" href="[~8~]">See all news</a>
        </div>
    </section>

    <div class='video-background'>
    <iframe id='sw-video' width="1280" height="720" src="https://www.youtube.com/embed/uH2BTOO1puc?controls=0&modestbranding=1&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

    [[chunk?&file=`footer_dev.php`]]
    <script src="assets/js/vendor/gsap.min.js"></script>
    <script src="assets/js/vendor/ScrollTrigger.min.js"></script>
    <script src="assets/js/vendor/lethargy.min.js"></script>
    <script src="assets/js/vendor/EventEmitter.min.js"></script>
    <script src="assets/js/vendor/smartscroll.min.js"></script>
    <!-- <script src="assets/js/vendor/fullpage.min.js"></script> -->
    <script async>
        var tl = gsap.timeline();

        // new fullpage('.work', {
        //     sectionSelector: '.work-item',
        //     // slideSelector: '.work-item'
        //     // fixedElements: 'header',
        //     normalScrollElements: '.video, .results, .news, .insights, .contact, .signup'
        // });

        var ee = new EventEmitter();
        var scrollStartListener = function (slideNumber) {
            // console.log("Scrolling to " + slideNumber);
            $('.work-list figure:nth-child('+slideNumber+')').removeClass('fadeout').addClass('active').siblings().removeClass('active');
            $('.work-list figure:nth-child('+slideNumber+')').prev('figure').addClass('fadeout');

            if (slideNumber > 0 && slideNumber < 4) {
                setTimeout(function() { $('header').addClass('white'); }, 300);
            } else {
                setTimeout(function() { $('header').removeClass('white'); }, 300);
            }
        }
        var scrollEndListener = function () {
            // console.log("Scrolling End");
        }
        ee.addListener('scrollStart', scrollStartListener);
        ee.addListener('scrollEnd', scrollEndListener);

        $.smartscroll({
            mode: 'vp',
            autoHash: false,
            sectionScroll: true,
            initialScroll: false,
            keepHistory: false,
            sectionWrapperSelector: '.work',
            sectionClass: 'work-item',
            eventEmitter: ee
        });

        if ($(window).scrollTop() == 0) {
            // tl.from('.a1', {y: -10, duration: 0.7, opacity: 0, ease: "power1.out"});
            // tl.from('.a2', {y: -10, duration: 0.7, opacity: 0, ease: "power1.out"});
            // tl.from('.a3', {y: -10, duration: 0.7, opacity: 0, ease: "power1.out"});
            tl.add(function() { $('.caps').addClass('show'); });
            tl.to('.a0', {scale: 1, duration: .3, ease: "power1.out"}, '>3');
            // tl.to('.a1', { width: 0, duration: 3, ease: "power1.out" });
            // tl.to('.super', { width: 0, duration: 3, ease: "power1.out" }, '<0.1');
            // tl.to('.a1,.super', { y: -100, duration: 1.4 }, '<0');
            // tl.from('.caps span:nth-child(1)', { opacity: 0, duration: 0.4, ease: "power1.out" }, '<1.5');
            // tl.from('.caps span:nth-child(2)', { opacity: 0, duration: 0.4, ease: "power1.out" }, '<0.4');
            // tl.from('.caps span:nth-child(3)', { opacity: 0, duration: 0.4, ease: "power1.out" }, '<0.4');
            // tl.from('.caps span:nth-child(4)', { opacity: 0, duration: 0.4, ease: "power1.out" }, '<0.4');
            // tl.from($('.work figure').first(), { duration: 3, y: 40, ease: "elastic.out"}, '>1');
            tl.to('.work-list figure:first-child img', { duration: 3, y: 0, ease: "elastic.out" }, '<');
            // tl.to('.work', { duration: 3, opacity: 1 }, '<');
            // tl.to('.work', { duration: 0, backgroundColor: '#16161d' });
            // tl.to('body', { duration: 0, overflow: 'auto' });

            // tl.add(function() {
            //     $.smartscroll({
            //         mode: 'vp',
            //         autoHash: false,
            //         sectionScroll: true,
            //         initialScroll: false,
            //         keepHistory: false,
            //         sectionWrapperSelector: '.work',
            //         sectionClass: 'work-item',
            //         eventEmitter: ee
            //     });
            // }, '<');
        } else {
            // if not starting from the top
            $('.caps').addClass('show');
        }

        // var sc = gsap.timeline({
        //     scrollTrigger: {
        //         trigger: '.work',
        //         pin: true,
        //         start: 'top top',
        //         end: 'bottom top',
        //         scrub: true,
        //         markers: true
        //     }
        // });

        // $('.work figure').not(':last').each(function() {
        //     gsap.to($(this), {
        //         scrollTrigger: {
        //             trigger: $(this),
        //             pin: true,
        //             pinSpacing: false,
        //             start: 'top top',
        //             end: 'bottom top',
        //             scrub: true,
        //            : {
        //                To: [0, 1],
        //                 duration: 0.3,
        //                 ease: "power1.out"
        //             }
        //             // markers: true
        //         },
        //         y: '-20vh',
        //         opacity: 0
        //     });

        var player, playercontainer;

        $('.video .cover').on('click', function () {
            loadYTscripts();
            playercontainer = 'player';
        });

        function loadYTscripts() {
            if (!$('#yts').length) {
                // console.log('load scripts');
                var tag = document.createElement('script'),
                    scripts = document.getElementsByTagName('script'),
                    lastScriptTag = scripts[scripts.length - 1];
                tag.src = "https://www.youtube.com/iframe_api";
                tag.id = "yts";
                lastScriptTag.parentNode.insertBefore(tag, lastScriptTag.nextSibling);
            }
        }

        function onYouTubeIframeAPIReady() {
            // console.log('api ready');
            player = new YT.Player(playercontainer, {
                videoId: '[*video_id*]',
                playerVars: {
                    rel: 0,
                    width: 1280,
                    height: 720,
                    modestbranding: 1,
                    origin: '[(site_url)]',
                    controls: 1,
                    fs: 1
                },
                events: {
                    'onReady': onPlayerReady
                    // 'onStateChange': onPlayerStateChange
                }
            });
        }

        function onPlayerReady(event) {
            event.target.setPlaybackQuality('hd1080');
            $('.video .cover, .video figcaption').fadeOut('fast');
            event.target.playVideo();
            // ga('send', 'event', 'Video', 'play', 'Homepage video new');
        }
    </script>
</body>
</html>