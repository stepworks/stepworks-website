[[chunk?&file=`header_jobs.php`]]
<body class="page-job-item">
    [[chunk?&file=`page_header.php`]]
    <section class="job-body">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end5">
                    <h1>[*pagetitle*]</h1>
                </div>
            </div>
            [[if?&is=`[*longtitle*]:notempty`&then=`<p class="date">[*longtitle*]</p>`]]

            <div class="grid grid-6 content">
                <div class="cell start3 end6">
                    [*content*]

                    <div class="cta">
                        [*job_cta*]
                    </div>
                </div>
            </div>
        </div>
    </section>
    [[chunk?&file=`footer.php]]
</body>
</html>