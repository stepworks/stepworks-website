[[chunk?&file=`header.php`]]

<body>
    [[chunk?file=`page_header.php`]]
    <section class="intro animate animating animated">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="super">[*pagetitle*]</h1>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start2 end6">
                    [*content*]
                </div>
            </div>
        </div>
    </section>

    [[chunk?&file=`footer.php`]]
</body>

</html>