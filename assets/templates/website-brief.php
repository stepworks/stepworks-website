<?php require_once 'assets/snippets/website_brief_functions.php'; ?>
[[chunk?&file=`header_brief.php`]]
<body class="page-website-brief">
    [[chunk?file=`page_header_mini.php`]]
    <?php 
        if (!empty($_GET['id'])) {
            $id = $_GET['id'];
            $brief = getBrief($id);
            $json = urldecode($brief['json']);
            $json = json_decode($json, true);
            // echo print_r($json, true);
        }
    ?>
    <form id="form-website-brief" class="<?php echo isset($_GET['id'][7]) ? "edit-mode" : ""; ?>">
        <input type="hidden" id="brief_id" name="brief_id" value="<?php echo isset($_GET['id'][7]) ? $_GET['id'] : ""; ?>">
        <input type="hidden" id="view_id" name="view_id" value="">
        <input type="hidden" id="ident_email" name="ident_email" value="<?= $brief['ident_email'] ?>">
        <input type="hidden" id="signup_insight" name="signup_insight" value="0">

        <section id="landing" class="step1">
            <div class="container">
                <h1>Website Brief Engine<sup>BETA</sup></h1>
                <p>A clear website brief helps the team building your website understand what you need for success.</p>
                <p>The Website Brief Engine makes it easier to get one.</p>
                <div class="form-row landing-email">
                    <label for="email">Please share your email to get your website brief’s unique link.</label>
                    <input type="email" id="email" name="email" placeholder="Email address" value="<?= $brief['ident_email'] ?>" required />
                    <span class="form-row__error"></span>
                </div>
                <div class="form-row">
                    <div class="d-flex justify-content-between">
                      <input type="checkbox" name="terms" id="tos"<?= isset($id) ? "checked" : "" ?>>
                      <label for="tos">I agree to the <a href="#tos">Terms of use</a></label>
                    </div>
                    <div class="d-flex justify-content-between">
                      <input type="checkbox" name="optin" id="optin" value="yes">
                      <label for="optin">Yes please send me regular brand building insights</label>
                    </div>
                </div>
                <?php if (!isset($id)) : ?>
                    <div class="button-container block landing-buttons">
                        <a id="start" href="#basic" class="btn next disabled">Let&rsquo;s do this</a>
                    </div>
                <?php endif; ?>
            </div>
        </section>

        <section id="basic" class="step2">
            <div class="container">
                <h1>Where are we coming from?</h1>
                <div class="form-row">
                    <label for="project-name">Name of your project *</label>
                    <input type="text" spellcheck="true" id="project-name" name="project-name" value="<?= $brief['project_name'] ?>" required>
                    <span class="form-row__error"></span>
                </div>
                <div class="form-row">
                    <label for="client-name">Your organisation&rsquo;s name *</label>
                    <input type="text" spellcheck="true" id="client-name" name="client-name" value="<?= $brief['client_name'] ?>" required>
                    <span class="form-row__error"></span>
                </div>
                <div class="form-row">
                    <label for="client-contact">Contact person</label>
                    <input type="text" spellcheck="true" id="client-contact" name="client-contact"  value="<?= $brief['client_contact'] ?>">
                </div>
                <div class="form-row">
                    <label for="ref">Reference #</label>
                    <p class="form-row__remarks">Internal use</p>
                    <input type="text" spellcheck="true" id="ref" name="ref" value="<?= $brief['ref'] ?>">
                </div>
                <div class="form-row">
                    <label for="date">Date</label>
                    <input type="text" spellcheck="true" id="date" name="date" class="datepicker" value="<?= isset($brief['date']) ? date("d F Y", strtotime($brief['date'])) : date("d F Y"); ?>">
                </div>
                <?php if (!isset($id)) : ?>
                    <div class="button-container">
                        <a href="#q-background-objectives-deliverables" class="btn next">Next</a>
                    </div>
                <?php endif; ?>
            </div>
        </section>

        <!-- Question Starts -->

        <section id="q-background-objectives-deliverables" class="step3">
            <div class="container">
                <h1>Website background, objectives and deliverables</h1>
                <div class="form-row">
                    <p>New website or rebuild?</p>
                    <div class="form-row__option">
                        <input type="radio" id="new-website" name="website" value="new_website" <?= $json['website'] == 'new_website' ? 'checked' : '' ?> />
                        <label for="new-website">We need a new website</label>
                    </div>
                    <div class="form-row__option">
                        <input type="radio" id="existing-website" name="website" value="existing_website" <?= $json['website'] == 'existing_website' ? 'checked' : '' ?> />
                        <label for="existing-website">We are upgrading an existing website</label>
                    </div>
                    <div class="form-row form-row__conditional<?= $json['website'] == 'existing_website' ? " show" : "" ?>">
                        <label for="existing-website-url">What’s the address of the website you want to upgrade?</label>
                        <input type="text" spellcheck="true" id="existing-website-url" name="existing-website-url" placeholder="e.g. www.stepworks.co" value="<?= $json['existing-website-url'] ?>" />
                    </div>                        
                </div>
                <div class="form-row">
                    <p>Do you already have a tentative scope for this project<br>For example a creative brief, functional specifications, site map, etc.</p>
                    <input type="radio" id="yes-tentative-scope" name="tentative-scope" value="yes" <?= $json['tentative-scope'] == 'yes' ? "checked" : "" ?> /><label for="yes-tentative-scope">Yes</label>
                    <input type="radio" id="no-tentative-scope" name="tentative-scope" value="no" <?= $json['tentative-scope'] == 'no' ? "checked" : "" ?> /><label for="no-tentative-scope">No</label>
                    <div class="form-row__conditional<?= $json['tentative-scope'] == 'yes' ? " show" : "" ?>">
                        <?php if (isset($id)) : ?>
                            <a href="<?= $json['tentative-scope-file'] ?>" target="_blank">Attachment</a>
                            <input type="hidden" name="attachment-tentative-scope-file" value="<?= $json['tentative-scope-file'] ?>" />
                        <?php else: ?>
                            <input type="file" accept=".doc, .docx, .pdf, .pages" name="tentative-scope-file" /> <span class="form-row__remarks">(accepts .doc, .docx, .pdf, .pages)</span>
                            <p class="form-row__remarks">The information you provide will be treated as confidential.</p>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-row">
                    <p>Benchmarking – Have you identified any reference websites relevant to this project that we can learn from?</p>
                    <input type="radio" id="yes-reference-websites" name="reference-websites" value="yes" <?= $json['reference-websites'] == 'yes' ? 'checked' : '' ?> /><label for="yes-reference-websites">Yes</label>
                    <input type="radio" id="no-reference-websites" name="reference-websites" value="no" <?= $json['reference-websites'] == 'no' ? 'checked' : '' ?> /><label for="no-reference-websites">No</label>
                    <div class="form-row__conditional<?= $json['reference-websites'] == 'yes' ? " show" : "" ?>">
                        <ol class="reference-website__list">
                            <?php 
                                $referenceReason = explodeData($json['reference-reason[]']);
                                foreach (explodeData($json['reference-url[]']) as $refIndex => $url) : 
                            ?>
                                <li>
                                    <span>
                                        <label for='reference-url<?= $refIndex + 1 ?>'>Website address</label>
                                        <input type='text' spellcheck='true' id='reference-url<?= $refIndex + 1 ?>' name='reference-url[]' value="<?= $url ?>" />
                                    </span>
                                    <span>
                                        <label for='reference-reason<?= $refIndex + 1 ?>'>Reason for reference</label>
                                        <input type='text' spellcheck='true' id='reference-reason$<?= $refIndex + 1 ?>' name='reference-reason[]' value="<?= $referenceReason[$refIndex] ?>"  />
                                    </span>
                                </li>
                            <?php endforeach; ?>
                        </ol>
                        <a id="add-reference-website" href="javascript:;">Add reference website</a>
                    </div>
                </div>
                <div class="form-row">
                    <p>What are the key objectives of this website project? (You can select more than one)</p>
                    <div class="option-sort option-sort--objectives">
                        <?php
                            $objectiveList = array(
                                "Introduce a new brand or business initiative",
                                "Manage organisational change or digital transformation",
                                "Increase brand awareness",
                                "Enhance brand image",
                                "Attract potential customers",
                                "Attract potential investors",
                                "Facilitate the customer journey",
                                "Facilitate customer service and support",
                                "Ecommerce",
                                "Publish news about our offering",
                                "Inform team, attract new talent, employee branding",
                            );
                        ?>
                        <div class="option-sort__tray">
                            <?php
                                foreach ($objectiveList as $objKey => $objective) {
                                    echo "<div class='form-row__option option-sort__tray-item' id='objective" . ($objKey + 1) . "'>$objective</div>";
                                }
                            ?>
                        </div>
                        <div class="option-sort__reversible"><i class='fas fa-arrow-down'></i></div>
                        <div class="option-sort__bucket sortable">
                            <?php
                                if (isset($json['objective[]'])) :
                                    foreach (explodeData($json['objective[]']) as $objective) : 
                                        if (strlen(trim($objective)) != 0) :
                            ?>
                                <div class="form-row__option option-sort__bucket-item" id="objective-selected<?= array_search($objective, $objectiveList) + 1 ?>"><i class="fas fa-sort option-sort__handle"></i><?= $objective ?><i class="fas fa-trash-alt option-sort__clear-btn"></i></div>
                            <?php   
                                        endif;
                                    endforeach;
                                endif; 
                            ?>
                        </div>
                        <input type="hidden" class="option-sort__result" name="objective[]" value="<?= $json['objective[]'] ?>" />
                    </div>
                    <p>Other</p>
                    <input type="text" spellcheck="true" id="objective-others" name="objective-others" value="<?= $json['objective-others'] ?>" />
                </div>
                <div class="form-row">
                    <p>Have you secured an appropriate domain or domains?</p>
                    <input type="radio" id="yes-secure-domain" name="secure-domain" value="yes" <?= $json['secure-domain'] == 'yes' ? "checked" : "" ?> /><label for="yes-secure-domain">Yes</label>
                    <input type="radio" id="no-secure-domain" name="secure-domain" value="no" <?= $json['secure-domain'] == 'no' ? "checked" : "" ?> /><label for="no-secure-domain">No</label>
                    <div class="form-row form-row__conditional<?= $json['secure-domain'] === 'yes' ? " show" : "" ?>">
                        <input type="text" spellcheck="true" id="secure-domain-url" name="secure-domain-url" placeholder="e.g. yourbrand.com" value="<?= $json['secure-domain-url'] ?>" />
                    </div>
                </div>
                <div class="form-row">
                    <p>Audiences – who must this website influence?</p>
                    <div class="option-sort option-sort--audience">
                        <?php
                            $audienceList = array(
                                "B2B customers",
                                "B2C customers",
                                "Existing team members",
                                "Potential employees",
                                "Partners and suppliers",
                                "News reporters and media producers",
                                "Regulators and government",
                                "Investors / potential investors",
                            );
                        ?>
                        <div class="option-sort__tray">
                            <?php
                                foreach ($audienceList as $audienceKey => $audience) {
                                    echo "<div class='form-row__option option-sort__tray-item' id='audience" . ($audienceKey + 1) . "'>$audience</div>";
                                }
                            ?>
                        </div>
                        <div class="option-sort__reversible"><i class='fas fa-arrow-down'></i></div>
                        <div class="option-sort__bucket sortable">
                            <?php
                                if (isset($json['audience[]'])) :
                                    foreach (explodeData($json['audience[]']) as $audience) : 
                                        if (strlen(trim($audience)) != 0) :
                            ?>
                                <div class="form-row__option option-sort__bucket-item" id="audience-selected<?= array_search($audience, $audienceList) + 1 ?>"><i class="fas fa-sort option-sort__handle"></i><?= $audience ?><i class="fas fa-trash-alt option-sort__clear-btn"></i></div>
                            <?php 
                                        endif;
                                    endforeach; 
                                endif;  
                            ?>
                        </div>
                        <input type="hidden" class="option-sort__result" name="audience[]" value="<?= $json['audience[]'] ?>" />
                    </div> 
                    <p>Please list any other possible audiences</p>
                    <input type="text" spellcheck="true" id="audience-others" name="audience-others" value="<?= $json['audience-others'] ?>" />            
                </div>
                <div class="form-row">
                    <p>What visitor behaviour do you want to encourage?</p>
                    <div class="option-sort option-sort--behaviour">
                        <?php 
                            $behaviourList = array(
                                "Join mailing list",
                                "Buy products",
                                "Make an enquiry / engage with customer management system (CRM)",
                                "Join as a member",
                                "Book a reservation",
                                "Access customer self-support",
                                "Download PDF files, forms, templates or tools for self-service",
                                "Complete and submit online forms (share data)",
                                "Submit files (resumes, reference materials etc)",
                                "Comment, review, rate or join discussions",
                                "Confirm ID or other compliance requirements",
                                "Click through to another resource such as social media or ecommerce site, etc",
                            );
                        ?>
                        <div class="option-sort__tray">
                            <?php 
                                foreach ($behaviourList as $behaviourKey => $behaviour) {
                                    echo "<div class='form-row__option option-sort__tray-item' id='behaviour" . ($behaviourKey + 1) . "'>$behaviour</div>";
                                }
                            ?>
                        </div>
                        <div class="option-sort__reversible"><i class='fas fa-arrow-down'></i></div>
                        <div class="option-sort__bucket sortable">
                            <?php 
                                if (isset($json['behaviour[]'])) :
                                    foreach (explodeData($json['behaviour[]']) as $behaviour) : 
                                        if (strlen(trim($behaviour))) :
                            ?>
                                <div class="form-row__option option-sort__bucket-item" id="behaviour-selected<?= array_search($behaviour, $behaviourList) + 1 ?>"><i class="fas fa-sort option-sort__handle"></i><?= $behaviour ?><i class="fas fa-trash-alt option-sort__clear-btn"></i></div>
                            <?php 
                                        endif;
                                    endforeach;
                                endif;
                            ?>
                        </div>
                        <input type="hidden" class="option-sort__result" name="behaviour[]" value="<?= $json['behaviour[]'] ?>" />
                    </div>
                    <p>Please list any other required visitor actions</p>
                    <input type="text" spellcheck="true" id="behaviour-others" name="behaviour-others" value="<?= $json['behaviour-others'] ?>" />
                </div>
                <div class="form-row">
                    <p>Where are your audiences located?</p>
                    <select class="custom-dropdown" multiple="multiple" name="audience-locations[]">
                        <?php 
                            $audienceLocations = explodeData($json['audience-locations[]']);
                        ?>
                        <optgroup>
                            <option <?= in_array('Hong Kong', $audienceLocations) ? 'selected' : '' ?>>Hong Kong</option>
                            <option <?= in_array('Mainland China', $audienceLocations) ? 'selected' : '' ?>>Mainland China</option>
                            <option <?= in_array('Singapore', $audienceLocations) ? 'selected' : '' ?>>Singapore</option>
                            <option <?= in_array('UK', $audienceLocations) ? 'selected' : '' ?>>UK</option>
                            <option <?= in_array('USA', $audienceLocations) ? 'selected' : '' ?>>USA</option>
                        </optgroup>
                        <optgroup>
                            <option <?= in_array('Asia Pacific', $audienceLocations) ? 'selected' : '' ?>>Asia Pacific</option>
                            <option <?= in_array('Europe', $audienceLocations) ? 'selected' : '' ?>>Europe</option>
                            <option <?= in_array('Middle East and Africa', $audienceLocations) ? 'selected' : '' ?>>Middle East and Africa</option>
                            <option <?= in_array('North America', $audienceLocations) ? 'selected' : '' ?>>North America</option>
                            <option <?= in_array('South America', $audienceLocations) ? 'selected' : '' ?>>South America</option>
                        </optgroup>
                        <optgroup>
                            <option <?= in_array('Global', $audienceLocations) ? 'selected' : '' ?>>Global</option>
                        </optgroup>
                    </select>
                    <p style="margin-top: 1rem;">Other (please write in locations separated by a comma) </p>
                    <input type="text" spellcheck="true" id="audience-locations-others" name="audience-locations-others" value="<?= $json['audience-locations-others'] ?>" />
                </div>
                <div class="form-row">
                    <p>Do you have any audience data or analytics?</p>
                    <div class='form-row__option'>
                        <input type='checkbox' id='audience-personas' name='audience-personas' value='yes' <?= $json['audience-personas'] === 'yes' ? 'checked' : '' ?>><label for='audience-personas'>Audience personas</label>
                    </div>
                    <div class="form-row form-row__conditional<?= $json['audience-personas'] === 'yes' ? ' show' : '' ?>">
                        <?php if (isset($id)) : ?>
                            <a href="<?= $json['audience-personas-file'] ?>" target="_blank">Attachment</a>
                            <input type="hidden" name="attachment-audience-personas-file" value="<?= $json['audience-personas-file'] ?>" />
                        <?php else: ?>
                            <input type="file" accept=".doc, .docx, .pdf, .pages" name="audience-personas-file" />
                            <span class="form-row__remarks">(accepts .doc, .docx, .pdf, .pages)</span>
                            <p class="form-row__remarks">The information you provide will be treated as confidential.</p>
                        <?php endif ?>
                    </div>
                </div>
                <div class="form-row no-margin-top">
                    <div class='form-row__option'>
                        <input type='checkbox' id='visitor-data' name='visitor-data' value='yes' <?= $json['visitor-data'] === 'yes' ? 'checked' : '' ?>><label for='visitor-data'>Visitor data (eg. Google Analytics etc)</label>
                    </div>
                    <div class="form-row form-row__conditional<?= $json['visitor-data'] === 'yes' ? ' show' : '' ?>">
                        <?php if (isset($id)) : ?>
                            <a href="<?= $json['visitor-data-file'] ?>" target="_blank">Attachment</a>
                            <input type="hidden" name="attachment-visitor-data-file" value="<?= $json['visitor-data-file'] ?>" />
                        <?php else: ?>
                            <input type="file" accept=".doc, .docx, .pdf, .pages" name="visitor-data-file" />
                            <span class="form-row__remarks">(accepts .doc, .docx, .pdf, .pages)</span>
                            <p class="form-row__remarks">The information you provide will be treated as confidential.</p>
                        <?php endif ?>
                    </div>
                </div>
                <div class="form-row">
                    <p>Which languages will your website need?</p>
                    <p class="form-row__remarks" >Choose as many as you need</p>
                    <?php 
                        $languageList = array(
                            "English",
                            "Traditional Chinese (繁體中文)",
                            "Simplified Chinese (简体中文)",
                            "Japanese (日本語)",
                            "Spanish (Español)",
                            "French (Français)",
                        );
                        foreach ($languageList as $languageKey => $language) {
                            echo "<div class='form-row__option'><input type='checkbox' id='language" . ($languageKey + 1) . "' name='language[]' value='$language'".(in_array($language, explodeData($json['language[]'])) ? 'checked' : '')."><label for='language" . ($languageKey + 1) . "'>$language</label></div>";
                        }
                    ?>
                    <p>Please list any other languages (separate with comma)</p>
                    <input type="text" spellcheck="true" id="language-others" name="language-others" value="<?= $json['language-others'] ?>" />
                </div>
                <div class="form-row">
                    <p>Do you need to migrate current website content or data to the new website?</p>
                    <input type="radio" id="yes-migrate-existing-website" name="migrate-existing-website" value="yes" <?= $json['migrate-existing-website'] == 'yes' ? 'checked' : '' ?> /><label for="yes-migrate-existing-website">Yes</label>
                    <input type="radio" id="no-migrate-existing-website" name="migrate-existing-website" value="no" <?= $json['migrate-existing-website'] == 'no' ? 'checked' : '' ?> /><label for="no-migrate-existing-website">No</label>
                </div>
                <div class="form-row">
                    <p>What are your website content creation requirements?</p>
                    <p class="form-row__remarks" >Choose as many as you need</p>
                    <?php 
                        $ccRqrmtList = array(
                            "Copywriting / editing / editorial",
                            "Photography",
                            "Video",
                            "Infographics",
                            "Icons / illustrations",
                            "Launch campaign",
                        );
                        foreach ($ccRqrmtList as $ccKey => $ccRqrmt) {
                            echo "<div class='form-row__option'><input type='checkbox' id='ccrqrmt" . ($ccKey + 1) . "'name='ccrqrmt[]' value='$ccRqrmt'".(in_array($ccRqrmt, explodeData($json['ccrqrmt[]'])) ? 'checked' : '')."> <label for='ccrqrmt" . ($ccKey + 1) . "'>$ccRqrmt</label></div>";
                        }
                    ?>
                </div>
                <?php if (!isset($id)) : ?>
                    <div class="button-container">
                        <a href="#q-functions-services-applications" class="btn next">Next</a>
                    </div>
                <?php endif; ?>
            </div>
        </section>

        <section id="q-functions-services-applications" class="step4">
            <div class="container">
                <h1>Website functions, services and applications</h1>
                <div class="form-row">
                    <p>Should the website take a mobile-first approach?</p>
                    <input type="radio" id="yes-mobile-first" name="mobile-first" value="yes" <?= $json['mobile-first'] == 'yes' ? 'checked' : '' ?> /><label for="yes-mobile-first">Yes</label>
                    <input type="radio" id="no-mobile-first" name="mobile-first" value="no" <?= $json['mobile-first'] == 'no' ? 'checked' : '' ?> /><label for="no-mobile-first">No</label>
                </div>
                <div class="form-row">
                  <p>Basic design functions</p>
                  <?php
                    $basicFunctionList = array(
                      "Long scroll page",
                      "Dark / Light modes ",
                      "Print friendly pages",
                      "Carousels / Sliders",
                      "Accordions",
                      "Animations on scroll",
                      "Pagination on archive/listing pages",
                      "Infinite scroll",
                      "Cookies integration",
                    );
                    foreach ($basicFunctionList as $basicFunctionKey => $basicFunction) {
                        echo "<div class='form-row__option'><input type='checkbox' id='basic-function" . ($basicFunctionKey + 1) . "' name='basic-functions[]' value='$basicFunction' ".(in_array($basicFunction, explodeData($json['basic-functions[]'])) ? 'checked' : '')." /> <label for='basic-function" . ($basicFunctionKey + 1) . "'>$basicFunction</label></div>";
                    }
                  ?>
                </div>
                <div class="form-row">
                  <p>CMS (Content Management System)</p>
                  <input type="radio" id="yes-cms" name="cms" value="yes" <?= $json['cms'] == 'yes' ? 'checked' : '' ?> /><label for="yes-cms">Yes</label>
                  <input type="radio" id="no-cms" name="cms" value="no" <?= $json['cms'] == 'no' ? 'checked' : '' ?> /><label for="no-cms">No</label>
                  <input type="radio" id="notsure-cms" name="cms" value="not sure" <?= $json['cms'] == 'not sure' ? 'checked' : '' ?> /><label for="notsure-cms">Not sure</label>
                  <div class="form-row form-row__conditional<?= $json['cms'] == 'yes' ? " show" : "" ?>">
                    <?php
                      $cmsFunctionList = array(
                        "We don’t yet know what we require for our CMS",
                        "Customisable templates to produce on-brand content design",
                        "Permission-based access control",
                        "Revision control to track content changes over time",
                        "Delegation between user groups",
                        "SEO-friendly URLs",
                        "Flexibility for creating content (drag and drop page builders)",
                        "Blog functionality",
                        "Event management",
                        "eCommerce management",
                        "Newsletter management (newsletter and email marketing system)",
                        "Membership management",
                      );
                      foreach ($cmsFunctionList as $cmsFunctionKey => $cmsFunction) {
                        echo "<div class='form-row__option'><input type='checkbox' id='cms-function" . ($cmsFunctionKey + 1) . "' name='cms-functions[]' value='$cmsFunction' ".(in_array($cmsFunction, explodeData($json['cms-functions[]'])) ? 'checked' : '')." /> <label for='cms-function" . ($cmsFunctionKey + 1) . "'>$cmsFunction</label></div>";
                        if ($cmsFunction == 'Blog functionality') {
                          echo "<div class='form-row form-row__conditional".(in_array('Blog functionality', explodeData($json['cms-functions[]'])) ? ' show' : '')."'>";
                          $blogFunctionList = array(
                            "Variety of post types (eg. testimonials, case studies, news)",
                            "Post categories ",
                            "Post tagging",
                            "Filtering functionality",
                            "Sorting functionality",                            
                          );
                          foreach ($blogFunctionList as $blogFunctionKey => $blogFunction) {
                            echo "<div class='form-row__option'><input type='checkbox' id='blog-function" . ($blogFunctionKey + 1) . "' name='blog-functions[]' value='$blogFunction' ".(in_array($blogFunction, explodeData($json['blog-functions[]'])) ? 'checked' : '')." /> <label for='blog-function" . ($blogFunctionKey + 1) . "'>$blogFunction</label></div>";
                          }
                          echo "</div>";
                        }
                        if ($cmsFunction == 'Event management') {
                          echo "<div class='form-row form-row__conditional".(in_array('Event management', explodeData($json['cms-functions[]'])) ? ' show' : '')."'>";
                          $eventList = array(
                            "Add, remove and sort event listings",
                            "Assign event location",
                            "Event registration and management",
                            "Event calendar (list upcoming events on website) ",
                            "Sell tickets (integration with eCommerce system)",
                          );
                          foreach ($eventList as $eventKey => $event) {
                            echo "<div class='form-row__option'><input type='checkbox' id='event" . ($eventKey + 1) . "' name='events[]' value='$event' ".(in_array($event, explodeData($json['events[]'])) ? 'checked' : '')." /> <label for='event" . ($eventKey + 1) . "'>$event</label></div>";
                          }
                          echo "</div>";
                        }
                        if ($cmsFunction == 'eCommerce management') {
                          echo "<div class='form-row form-row__conditional".(in_array('eCommerce management', explodeData($json['cms-functions[]'])) ? ' show' : '')."'>";
                          $eCommerceList = array(
                            "Product ratings and reviews",
                            "Product categorising and tagging",
                            "Custom attributes (size, weight, material, etc)",
                            "Shipping rates and options (eg. free shipping, pickup, local delivery or shipping)",
                            "Product gallery",
                            "Order tracking",
                            "Coupon codes",
                            "Guest checkout",
                            "Related products",
                            "Inventory management (track stock levels, hold stock after an order is cancelled, notifications for low and out-of-stock items, hide out-of-stock items) ",
                            "Order management (add customer notes, edit stock manually, mark items as shipped, manage the fulfilment process",
                            "Reporting (view sales, refunds, see your top products and categories)",
                          );
                          foreach ($eCommerceList as $eCommerceKey => $eCommerce) {
                            echo "<div class='form-row__option'><input type='checkbox' id='eCommerce" . ($eCommerceKey + 1) . "' name='eCommerces[]' value='$eCommerce' ".(in_array($eCommerce, explodeData($json['eCommerces[]'])) ? 'checked' : '')." /> <label for='eCommerce" . ($eCommerceKey + 1) . "'>$eCommerce</label></div>";
                          }
                          echo "</div>";
                        }
                        if ($cmsFunction == 'Newsletter management (newsletter and email marketing system)') {
                          echo "<div class='form-row form-row__conditional".(in_array('Newsletter management (newsletter and email marketing system)', explodeData($json['cms-functions[]'])) ? ' show' : '')."'>";
                          echo "<p>Please add requirements / objectives</p>";
                          echo "<input type='text' spellcheck='true' id='newsletter-requirements' name='newsletter-requirements' value='".($json['newsletter-requirements'])."' />";
                          echo "</div>";
                        }
                        if ($cmsFunction == 'Membership management') {
                          echo "<div class='form-row form-row__conditional".(in_array('Membership management', explodeData($json['cms-functions[]'])) ? ' show' : '')."'>";
                          $membershipList = array(
                            "2-factor authentication",
                            "Force strong passwords",
                          );
                          foreach ($membershipList as $membershipKey => $membership) {
                            echo "<div class='form-row__option'><input type='checkbox' id='membership" . ($membershipKey + 1) . "' name='memberships[]' value='$membership' ".(in_array($membership, explodeData($json['memberships[]'])) ? 'checked' : '')." /> <label for='membership" . ($membershipKey + 1) . "'>$membership</label></div>";
                          }
                          echo "<p>Please add requirements / objectives</p>";
                          echo "<input type='text' spellcheck='true' id='membership-requirements' name='membership-requirements' value='".($json['membership-requirements'])."' />";
                          echo "</div>";
                        }
                      }
                    ?>
                    <p style="margin-top: 1rem;">Do you have any CMS platform preferences?</p>
                    <input type="text" spellcheck="true" id="cms-preferences" name="cms-preferences" value="<?= $json['cms-preferences'] ?>" />
                  </div>
                </div>

                <div class="form-row">
                  <p>Site search</p>
                  <input type="radio" id="yes-site-search" name="site-search" value="yes" <?= $json['site-search'] == 'yes' ? 'checked' : '' ?> /><label for="yes-site-search">Yes</label>
                  <input type="radio" id="no-site-search" name="site-search" value="no" <?= $json['site-search'] == 'no' ? 'checked' : '' ?> /><label for="no-site-search">No</label>
                </div>

                <div class="form-row">
                  <p>Forms</p>
                  <?php
                    $formFunctionList = array(
                      "Form (contact, subscription, careers, surveys)",
                      "Forms with file upload function",
                      "Multi-step forms",
                      "Forms integrated with third party system (eg. Mailchimp, Campaign Monitor, Salesforce)",
                      "Verification / Quiz before form submission (eg. captcha)",
                      "Competition",
                    );
                    foreach ($formFunctionList as $formFunctionKey => $formFunction) {
                      echo "<div class='form-row__option'><input type='checkbox' id='form-function" . ($formFunctionKey + 1) . "' name='form-functions[]' value='$formFunction' ".(in_array($formFunction, explodeData($json['form-functions[]'])) ? 'checked' : '')." /> <label for='form-function" . ($formFunctionKey + 1) . "'>$formFunction</label></div>";
                      if ($formFunction == 'Forms integrated with third party system (eg. Mailchimp, Campaign Monitor, Salesforce)') {
                        echo "<div class='form-row form-row__conditional".(in_array('Forms integrated with third party system (eg. Mailchimp, Campaign Monitor, Salesforce)', explodeData($json['form-functions[]'])) ? ' show' : '')."'>";
                        echo "<p>Which system?</p>";
                        echo "<input type='text' spellcheck='true' id='integrated-form-system' name='integrated-form-system' value='".($json['integrated-form-system'])."' />";
                        echo "</div>";
                      }
                    }
                  ?>
                  <p>Please add any other form requirements / objectives</p>
                  <input type="text" spellcheck="true" id="form-requirement-others" name="form-requirement-others" value="<?= $json['form-requirement-others'] ?>" />
                </div>

                <div class="form-row">
                  <p>On-site visitor communication</p>
                  <?php
                    $visitorCommList = array(
                      "Live chat",
                      "Chatbot",
                      "Commenting capabilities",
                      "Forum",
                    );
                    foreach ($visitorCommList as $visitorCommKey => $visitorComm) {
                      echo "<div class='form-row__option'><input type='checkbox' id='visitor-comm" . ($visitorCommKey + 1) . "' name='visitor-comms[]' value='$visitorComm' ".(in_array($visitorComm, explodeData($json['visitor-comms[]'])) ? 'checked' : '')." /> <label for='visitor-comm" . ($visitorCommKey + 1) . "'>$visitorComm</label></div>";
                    }
                  ?>
                  <p>Please add any other visitor communications</p>
                  <input type="text" spellcheck="true" id="form-requirement-others" name="form-requirement-others" value="<?= $json['form-requirement-others'] ?>" />
                </div>

                <div class="form-row">
                  <p>Media</p>
                  <?php
                    $mediaList = array(
                      "Image gallery / sliders",
                      "Audio players",
                      "Video streaming",
                      "Podcasting",
                    );
                    foreach ($mediaList as $mediaKey => $media) {
                      echo "<div class='form-row__option'><input type='checkbox' id='media" . ($mediaKey + 1) . "' name='media[]' value='$media' ".(in_array($media, explodeData($json['media[]'])) ? 'checked' : '')." /> <label for='media" . ($mediaKey + 1) . "'>$media</label></div>";
                    }
                  ?>
                  <p>Please add any other media requirements / objectives</p>
                  <input type="text" spellcheck="true" id="media-requirement-others" name="media-requirement-others" value="<?= $json['media-requirement-others'] ?>" />
                </div>
                
                <div class="form-row">
                  <p>Third-party API integration</p>
                  <?php
                    $thirdPartyApiList = array(
                      "Social media integrations (eg. Instagram posts, LinkedIn feed, Facebook feed)",
                      "Analytics integrations (eg. Google Analytics, Google Tag, Baidu)",
                      "Maps integrations (eg. Google Maps, Bing Maps)",
                      "Payment integrations (eg. Paypal, Stripe)",
                      "CRM integrations (eg. HubSpot, Salesforce)",
                    );
                    foreach ($thirdPartyApiList as $thirdPartyApiKey => $thirdPartyApi) {
                      echo "<div class='form-row__option'><input type='checkbox' id='third-party-api" . ($thirdPartyApiKey + 1) . "' name='third-party-api[]' value='$thirdPartyApi' ".(in_array($thirdPartyApi, explodeData($json['third-party-api[]'])) ? 'checked' : '')." /> <label for='third-party-api" . ($thirdPartyApiKey + 1) . "'>$thirdPartyApi</label></div>";
                    }
                  ?>
                  <p>Please add any other API requirements</p>
                  <input type="text" spellcheck="true" id="third-party-api-others" name="third-party-api-others" value="<?= $json['third-party-api-others'] ?>" />
                </div>

                <div class="form-row">
                  <p>Security</p>
                  <?php
                    $securityList = array(
                      "Restricted access (for members, subscribers, registered users only)",
                      "SSL certificate",
                      "DDoS attack mitigation",
                      "Comment spam protection",
                      "Password protected website",
                      "IP Allow list",
                    );
                    foreach ($securityList as $securityKey => $security) {
                      echo "<div class='form-row__option'><input type='checkbox' id='security" . ($securityKey + 1) . "' name='security[]' value='$security' ".(in_array($security, explodeData($json['security[]'])) ? 'checked' : '')." /> <label for='security" . ($securityKey + 1) . "'>$security</label></div>";
                    }
                  ?>
                </div>

                <div class="form-row">
                  <p>Any website functions, services and applications not previously listed?</p>
                  <input type="text" spellcheck="true" id="website-function-others" name="website-function-others" value="<?= $json['website-function-others'] ?>" />
                </div>
      
                <div class="form-row">
                    <p>Will you need ongoing website maintenance support?</p>
                    <p class="form-row__remarks">Every website is a dynamic resource that requires regular attention for optimum performance and effectiveness</p>
                    <p class="form-row__remarks">Please note that a website maintenance support package is strongly recommended for websites not hosted in-house. Ad hoc maintenance may incur considerable costs.</p>
                    <input type="radio" id="yes-website-maintenance-support" name="website-maintenance-support" value="yes" <?= $json['website-maintenance-support'] == 'yes' ? 'checked' : '' ?> /><label for="yes-website-maintenance-support">Yes</label>
                    <input type="radio" id="no-website-maintenance-support" name="website-maintenance-support" value="no" <?= $json['website-maintenance-support'] == 'no' ? 'checked' : '' ?> /><label for="no-website-maintenance-support">No</label>
                    <div class="form-row form-row__conditional<?= $json['website-maintenance-support'] == 'yes' ? " show" : "" ?>">
                      <?php
                        $websiteSupportList = array(
                          "Website content updates (minor content development and updates, graphics and images updates, basic design tweaks )",
                          "Annual website strategic review, SEO and analytics (periodic website analytics and insights reports)",
                          "Website stability assurance (system core updates, bugs and broken link fixing, security updates and fixes, website backups)",
                          "CMS training for new team members",
                          "Website performance tune-up (speed optimisation, bugs and broken link fixing)",
                        );                     
                        foreach ($websiteSupportList as $websiteSupportKey => $websiteSupport) {
                          echo "<div class='form-row__option'><input type='checkbox' id='website-maintenance-support" . ($websiteSupportKey + 1) . "' name='website-maintenance-support[]' value='$websiteSupport' ".(in_array($websiteSupport, explodeData($json['website-maintenance-support[]'])) ? 'checked' : '')." /> <label for='website-maintenance-support" . ($websiteSupportKey + 1) . "'>$websiteSupport</label></div>";
                        }
                      ?>
                    </div>
                </div>
                <?php if (!isset($id)) : ?>
                    <div class="button-container">
                        <a href="#q-technical" class="btn next">Next</a>
                    </div>
                <?php endif; ?>
            </div>
        </section>

        <section id="q-technical" class="step5">
            <div class="container">
                <h1>Technical considerations</h1>
                <div class="form-row">
                    <p>What browsers does your organisation use?</p>
                    <?php 
                        $browserList = array(
                            "Firefox",
                            "Google Chrome",
                            "Microsoft Edge",
                            "Safari"
                        );
                        foreach ($browserList as $browserKey => $browser) {
                            echo "<div class='form-row__option'><input type='checkbox' id='browser" . ($browserKey + 1) . "' name='browser[]' value='$browser' ".(in_array($browser, explodeData($json['browser[]'])) ? 'checked' : '')."/> <label for='browser" . ($browserKey + 1) . "'>$browser</label></div>";
                        }
                    ?>
                    <input type="text" spellcheck="true" id="browser-others" name="browser-others" placeholder="Other browsers" value="<?= $json['browser-others'] ?>" />
                    <p class="form-row__remarks">*Please note that Internet Explorer has been discontinued and is no-longer supported</p>
                </div>
                <div class="form-row">
                    <p>Do you need website hosting?</p>
                    <input type="radio" id="yes-website-hosting" name="website-hosting" value="yes" <?= $json['website-hosting'] == 'yes' ? 'checked' : '' ?> /><label for="yes-website-hosting">Yes</label>
                    <input type="radio" id="no-website-hosting" name="website-hosting" value="no" <?= $json['website-hosting'] == 'no' ? 'checked' : '' ?> /><label for="no-website-hosting">No</label>
                </div>
                <div class="form-row">
                    <p>Do you have a hosting preference?</p>
                    <input type="radio" id="yes-hosting-preference" name="hosting-preference" value="yes" <?= $json['hosting-preference'] == 'yes' ? 'checked' : '' ?> /><label for="yes-hosting-preference">Yes</label>
                    <input type="radio" id="no-hosting-preference" name="hosting-preference" value="no" <?= $json['hosting-preference'] == 'no' ? 'checked' : '' ?> /><label for="no-hosting-preference">No</label>
                    <div class="form-row__conditional<?= $json['hosting-preference'] == 'yes' ? ' show' : '' ?>">
                        <?php 
                          $hostingMethodList = array(
                            "Shared hosting",
                            "VPS hosting",
                            "Cloud hosting",
                            "Dedicated hosting",
                          );
                          foreach ($hostingMethodList as $hostingMethodKey => $hostingMethod) {
                            echo "<div class='form-row__option'><input type='checkbox' id='hosting-method" . ($hostingMethodKey + 1) . "' name='hosting-method[]' value='$hostingMethod' ".(in_array($hostingMethod, explodeData($json['hosting-method[]'])) ? 'checked' : '')."/> <label for='hosting-method" . ($hostingMethodKey + 1) . "'>$hostingMethod</label></div>";
                          }
                        ?>
                    </div>
                </div>
                <div class="form-row">
                    <p>Do you have coding preferences?</p>
                    <input type="radio" id="yes-coding-preference" name="coding-preference" value="yes" <?= $json['coding-preference'] == 'yes' ? 'checked' : '' ?> /><label for="yes-coding-preference">Yes</label>
                    <input type="radio" id="no-coding-preference" name="coding-preference" value="no" <?= $json['coding-preference'] == 'no' ? 'checked' : '' ?> /><label for="no-coding-preference">No</label>
                    <div class="form-row__conditional<?= $json['coding-preference'] == 'yes' ? ' show' : '' ?>">
                        <input type="radio" id="coding-php" name="coding" value="PHP" <?= $json['coding'] == 'PHP' ? 'checked' : '' ?> /><label for="coding-php">PHP</label><br>
                        <input type="radio" id="coding-react" name="coding" value="React" <?= $json['coding'] == 'React' ? 'checked' : '' ?> /><label for="coding-react">React</label><br>
                        <input type="radio" id="coding-vue" name="coding" value="Vue" <?= $json['coding'] == 'Vue' ? 'checked' : '' ?> /><label for="coding-vue">Vue</label><br>
                        <input type="text" spellcheck="true" id="coding-others-text" name="coding-others-text" placeholder="Other coding preference" value="<?= $json['coding-others-text'] ?>" />
                    </div>
                </div>
                <div class="form-row">
                    <p>Do you have a database preference</p>
                    <input type="radio" id="yes-database-preference" name="database-preference" value="yes" <?= $json['database-preference'] == 'yes' ? 'checked' : '' ?> /><label for="yes-database-preference">Yes</label>
                    <input type="radio" id="no-database-preference" name="database-preference" value="no" <?= $json['database-preference'] == 'no' ? 'checked' : '' ?> /><label for="no-database-preference">No</label>
                    <div class="form-row__conditional<?= $json['database-preference'] == 'yes' ? ' show' : '' ?>">
                        <input type="radio" id="database-mysql" name="database" value="MySQL" <?= $json['database'] == 'MySQL' ? 'checked' : '' ?> /><label for="database-mysql">MySQL</label><br>
                        <input type="radio" id="database-postgre-sql" name="database" value="PostgreSQL" <?= $json['database'] == 'PostgreSQL' ? 'checked' : '' ?> /><label for="database-postgre-sql">PostgreSQL</label><br>
                        <input type="radio" id="database-nosql" name="database" value="NoSQL (MongoDB, ArangoDB)" <?= $json['database'] == 'NoSQL (MongoDB, ArangoDB)' ? 'checked' : '' ?> /><label for="database-nosql">NoSQL (MongoDB, ArangoDB)</label><br>
                        <input type="text" spellcheck="true" id="database-others-text" name="database-others-text" placeholder="Other database preference" value="<?= $json['database-others-text'] ?>" />
                    </div>
                </div>
                <?php if (!isset($id)) : ?>
                    <div class="button-container">
                        <a href="#q-challenges" class="btn next">Next</a>
                    </div>
                <?php endif; ?>
            </div>
        </section>

        <section id="q-challenges" class="step6">
            <div class="container">
                <h1>Challenges and opportunities</h1>
                <div class="form-row">
                    <label for="risk-opportunities">Please share any risks and opportunities you foresee for this website project</label><br>
                    <textarea id="risk-opportunities" name="risk-opportunities"><?= $json['risk-opportunities'] ?></textarea>
                </div>
                <div class="form-row">
                    <p>ESG is increasingly important. These third party services offer opportunities to contribute to your ESG initiatives</p>
                    <div class="form-row__option">
                        <input type="checkbox" id="csr-carbon-offsetting" name="csr-carbon-offsetting" value="Carbon offsetting – ensures your website is carbon neutral" <?= $json['csr-carbon-offsetting'] == 'Carbon offsetting – ensures your website is carbon neutral' ? 'checked' : '' ?> /> 
                        <label for="csr-carbon-offsetting">Carbon offsetting – ensures your website is carbon neutral</label>
                    </div>
                    <div class="form-row__option">
                        <input type="checkbox" id="csr-wcag" name="csr-wcag" value="WCAG compliance – ensures your website is accessible by people with certain disabilities" <?= $json['csr-wcag'] == 'WCAG compliance – ensures your website is accessible by people with certain disabilities' ? 'checked' : '' ?> /> 
                        <label for="csr-wcag">WCAG compliance – ensures your website is accessible by people with certain disabilities</label>
                    </div>
                    <div class="form-row__option">
                        <input type="checkbox" id="csr-carbon-footprint" name="csr-carbon-footprint" value="Climate friendly design to reduce carbon footprint" <?= $json['csr-carbon-footprint'] == 'Climate friendly design to reduce carbon footprint' ? 'checked' : '' ?> /> 
                        <label for="csr-carbon-footprint">Climate friendly design to reduce carbon footprint</label>
                    </div>
                </div>
                <div class="button-container">
                    <?php if (isset($id)) : ?>
                        <a id="reset_brief" class="btn">Reset</a>
                        <a id="update_brief" class="btn">Save</a>
                        <a id="gen_brief" class="btn">Generate report</a>
                    <?php else : ?>
                        <a id="gen_brief" class="btn">Generate your website brief</a>
                    <?php endif; ?>                    
                </div>
                <div class="form-row">
                  Maximise the value of your website by maximising its effectiveness. Build a brand-led website and check out these <a href="https://stepworks.co/en/insights/how-digital-design-agencies-create-effective-websites" target="_blank">website design best practices</a>. <u>Get in touch</u> to discuss your unique business situation.
                </div>
            </div>
        </section>
    </form>

    <div id="summary">
        <div class="container">
            <a href="#" class="close"></a>
            <div class="buttons">
                <!-- <a href="#" class="btn share" title="Share this brief"><i class="fal fa-share-alt"></i></a> -->
                <a id="open_brief" title="Print" href="#" class="btn"><i class="fal fa-print"></i></a>
            </div>
        </div>
    </div>

    <div id="tosmodal" class="popup_modal">
        <div class="popup_modal-content">
            <a href="#" class="close"></a>
            <h2>Terms of use</h2>
            <ol>
                <li>The following are the terms and conditions for the Use of Stepworks Website Brief Engine ("the Terms"). Stepworks agrees to provide services of Stepworks Website Brief Engine ("the Website Brief Engine") and you, as the user of the Website Brief Engine, agree to use the services of the Website Brief Engine in accordance with the Terms set out herein.</li>
            </ol>

            <p><strong>The Website Brief Engine</strong></p>

            <ol type="1" start="2">
                <li>The Website Brief Engine is a website of an integrated online service delivery platform which its user may use any of the services provided therein ("service") in accordance with the Terms set out herein.</li>
                <li>The Website Brief Engine services are provided on an “as is” basis. Stepworks may provide procedures for the use of any services from time to time as necessary. Stepworks may add, modify, suspend and/or terminate any services and procedures for use without giving prior notice to any user.</li>
            </ol>

            <p><strong>Website Brief Engine unique URLs</strong> </p>

            <ol type="1" start="4">
                <li>You may create a Website Brief Engine by providing your email address and  clicking agreement to the Terms of Use and following the steps herein. By agreeing, you agree to be bound by these Terms of Use. </li>
                <li>You agree the Website Brief Engine is only for your own use. You shall not enter an email addresses of another person.</li>
                <li>Your Website Brief Engine access creates a unique URL that represents your Website Brief Engine file in accordance with procedure provided in the Website Brief Engine.</li>
                <li>By accessing Stepworks Brief Engine, you may use any specific services of the Website Brief Engine by opening your unique URL in accordance with procedure provided in the Website Brief Engine.</li>
                <li>Keep your unique URL confidential. This provides access to your Website Brief Engine file.</li>
            </ol>

            <p><strong>Email alert function</strong></p>

            <ol type="1" start="9">
                <li>Stepworks may send messages to you via the Website Brief Engine and via the email address you supplied upon account opening.</li>
                <li>When you revise your email address you may lose access to your Website Brief Engine file.</li>
            </ol>

            <li><strong>Maintaining your Website Brief Engine files</strong></li>
            <ol type="1" start="11">
                <li>You may click the links provided in the service to access creative briefs you are developing.</li>
                <li>If you lose these links you may not be able to access the  creative briefs you are developing.</li>
            </ol>

            <p><strong>The Privacy Policy</strong></p>

            <ol type="1" start="13">
                <li>Stepworks is concerned to ensure all personal data submitted through and stored in the Website Brief Engine are handled in strict adherence to the relevant provisions of the Personal Data (Privacy) Ordinance (Cap 486), “the PD(P)O” (see www.pcpd.org.hk).</li>
                <li>Information (including personal data) kept in the personal profile of your Website Brief Engine account may be retained by Stepworks for matters arising out of, or in relation to, your use of the Website Brief Engine.</li>
                <li>If you maintain personal information in your Website Brief Engine files, you may access and correct the information/data kept there by logging into your Website Brief Engine pages. </li>
                <li>You may request access to or correction of your personal data by sending a data access request to Stepworks via email at <a href="mailto:info@stepworks.co">info@stepworks.co</a>.</li>
                <li>Provision of any of profile data in your Website Brief Engine file is voluntary.</li>
                <li>You hereby give consent to Stepworks for the retention of information (including personal data) contained in your Website Brief Engine file after termination of matters arising out of, or in relation to, the use of the Website Brief Engine.</li>
                <li>A cookie is a small amount of data created in a computer when a person visits a website through the computer. It often includes an anonymous unique identifier. A cookie can be used to identify a computer or to temporarily store non-personal information required by an online service. It is not used by the Website Brief Engine to collect any personal information. It does not have the function of identifying an individual user of the Website Brief Engine.</li>
                <li>The Website Brief Engine may use cookies. No personal information is contained in the cookies generated by the Website Brief Engine. If you configure your browser to reject cookies, you will not be able to use the Website Brief Engine. </li>
                <li>The Website Brief Engine requires JavaScript to function properly. If you disable JavaScript on your computer, you will not be able to use the Website Brief Engine.</li>
                <li>You may input interests in your Website Brief Engine file. Such information is for Stepworks to consider how to deliver relevant content and improve its services. If you have opted to receive specific information from Stepworks, Stepworks may pass you messages or information of your interest from time to time. </li>
            </ol>

            <p><strong>Disclaimer</strong></p>
            <ol start="23">
                <li>Unless provided otherwise in the Terms, the information on the Website Brief Engine is provided to you on an "as is" basis without any express or implied warranty or representation of any kind and is for a general, indicative purpose only. In particular, Stepworks does not make any express or implied warranty or representation as to the accuracy, completeness, fitness for a particular purpose, non-infringement, reliability, security or timeliness of such information. Stepworks will not be liable for any errors in, omissions from, or misstatements or misrepresentations (whether express or implied) concerning any such information, and will not have or accept any liability, obligation or responsibility whatsoever for any loss, destruction or damage (including without limitation consequential loss, destruction or damage) however arising from or in respect of any use or misuse of or reliance on, inability to use or the unavailability of the Website Brief Engine or any information and services delivered on the Website Brief Engine. Stepworks does not warrant or represent that the Website Brief Engine or any information or any electronic data or information transmitted to you through the Website Brief Engine is free of computer viruses. Stepworks shall not be liable for any loss, destruction or damage arising out of or in relation to any transmission from users to Stepworks or vice versa over the internet.</li>
                <li>Without limiting the generality of the foregoing, nothing on or provided via the Website Brief Engine shall constitute, give rise to or otherwise imply any endorsement, approval or recommendation on Stepworks’ part of any third party, be it a provider of goods or services or otherwise.</li>
                <li>Stepworks is not responsible for any loss or damage whatsoever arising out of or in relation to any information on the Website Brief Engine. Stepworks reserves the right to omit, suspend or edit all information compiled by Stepworks on the Website Brief Engine at any time in its sole discretion without giving any reason or prior notice. You are responsible for making your own assessment of all information contained on the Website Brief Engine and shall verify such information by making reference, for example, to original publications and obtaining independent advice before acting upon it.</li>
                <li>You shall ensure accuracy of Website Brief Engine data (including your personal data) kept in your Website Brief Engine file. Stepworks shall not be responsible/liable for any inaccuracy, mistake or false information (including your personal data) so kept and/or submitted by you.</li>
                <li>Stepworks does not give any warranty in relation to the use of the Website Brief Engine and / or any third party websites linked to the Website Brief Engine, and does not warrant against any possible interference or damage to your computer system arising from the use of the website. Stepworks makes no representations or warranties regarding the accuracy, functionality or performance of any third party software that may be used in connection with the Website Brief Engine. Stepworks makes no warranties that the Website Brief Engine is free of infection by computer viruses.</li>
            </ol>

            <p><strong>Security</strong></p>

            <ol type="1" start="28">
                <li>You must close all browser windows after use to prevent misuse of your Website Brief Engine account by third parties.</li>
            </ol>

            <p><strong>Copyright</strong></p>

            <ol type="1" start="29">
                <li>Unless otherwise indicated, the contents found on the Website Brief Engine are subject to copyright owned by Stepworks.</li>
                <li>Prior written consent to Stepworks is required if you intend to reproduce, distribute or otherwise use any non-text contents (including but not limited to photographs, graphics, illustrations and drawings, diagrams, audio files and video files) found on the Website Brief Engine in any way or for any purpose. Such requests for consent must be addressed to Stepworks via email enquiry at <a href="mailto:info@stepworks.co">info@stepworks.co</a>. </li>
                <li>Where third party copyright (ie. copyright belonging to anyone other than Stepworks is involved in the non-text contents found on the Website Brief Engine, authorisation or permission to reproduce or distribute r otherwise use any such non-text contents must be obtained from the copyright owners concerned. </li>
                <li>Stepworks copyright protected text contents found on the Website Brief Engine may be reproduced and distributed free of charge in any format or medium for personal or internal use within an organisation, subject to the following conditions:
                    <ol>
                        <li>The copy or copies made must not be for sale, or exchanged for benefit, gain, profit, reward or any other commercial purposes</li>
                        <li>Such text contents must be reproduced accurately and must not be used in a manner adversely affecting any moral rights of Stepworks</li>
                        <li>Stepworks must be acknowledged as the copyright owner of such text contents and acknowledgement must be given to “Stepworks (stepworks.co)” as the source of all such text contents. </li>
                    </ol>
                </li>
                <li>"Commercial purposes" includes without limitation the following:
                    <ol type="a">
                        <li>the purpose to offer to supply goods, services, facilities, land or an interest in land</li>
                        <li>the purpose to offer to provide a business opportunity or an investment opportunity</li>
                        <li>the purpose to advertise or promote goods, services, facilities, land or an interest in land</li>
                        <li>the purpose to advertise or promote a business opportunity or an investment opportunity</li>
                        <li>the purpose to advertise or promote a supplier, or a prospective supplier, of goods, services, facilities, land or an interest in land</li>
                        <li>the purpose to advertise or promote a provider, or a prospective provider, of a business opportunity or an investment opportunity, in the course of or in the furtherance of any business.</li>
                    </ol>
                </li>
                <li>Please note that the permission given in paragraph 33 above only applies to Stepworks copyright protected text contents found on the Website Brief Engine. Stepworks reserves the right to withdraw any permission given in paragraph 33 above at any time without any prior notice to you.</li>
                <li>Prior written consent of Stepworks is required if you intend to reproduce, distribute or otherwise use any Stepworks copyright protected text contents found on the Website Brief Engine in any way other than that permitted in paragraph 33 above, or for any purpose other than that permitted in paragraph 33 above. Such requests shall be addressed to Stepworks via email at info@stepworks.co.</li>
                <li>Where third party copyright (ie. copyright belonging to anyone other than Stepworks is involved in the text contents found on the Website Brief Engine, authorisation or permission to reproduce or distribute or otherwise use any such text contents must be obtained from the copyright owners concerned.</li>
                <li>For the avoidance of doubt, the permission given in paragraph 33 above does not extend to any contents on other websites linked to the Website Brief Engine. If you intend to reproduce, distribute or otherwise use any contents on any such linked websites, you shall obtain all necessary authorisation or permission from the copyright owners concerned.</li>
            </ol>

            <p><strong>Suspension of your Website Brief Engine access</strong></p>

            <ol type="1" start="38">
                <li>If you breach any of the Terms set out herein, Stepworks may suspend your Website Brief Engine access at any time without giving notice to you.</li>
                <li>If Stepworks suspects that the email address for your Website Brief Engine file access is used by any person (other than by yourself) to access your Website Brief Engine account and / or to access the services in the Website Brief Engine, Stepworks may suspend your access without notice.</li>
                <li>Upon suspension, you would not be able to access your Website Brief Engine files.</li>
            </ol>

            <p><strong>Termination of a Website Brief Engine login</strong></p>

            <ol type="1" start="41">
                <li>Stepworks may terminate your Website Brief Engine access without giving any notice if you breach any of the Terms or it is noted that your Website Brief Engine access has not been used for a long time (one year). Stepworks may notify you of the termination by sending an email message to the email address provided in the personal profile of your Website Brief Engine account.</li>
                <li>You may delete your Website Brief Engine files by contacting Stepworks.</li>
                <li>Upon deletion of your Website Brief Engine files,
                    <ol type="a">
                        <li>(a) your links will no longer work</li>
                        <li>(b) you cannot access your information any more</li>
                        <li>(c) you may not use any specific services provided in the Website Brief Engine</li>
                        <li>(d) information related to your Stepworks Creative Brief access may be retained by Stepworks (see paragraphs 36 and 43 above)</li>
                        <li>(e) you may not be able to create new Website Brief Engine documents in the Website Brief Engine.</li>
                    </ol>

                    This provision survives termination of your Website Brief Engine login.
                </li>
            </ol>

            <p><strong>Indemnity</strong></p>

            <ol type="1" start="44">
                <li>You shall indemnify and keep indemnified Stepworks against all claims, actions, proceedings, liabilities, demands, charges, damages, costs, losses or expenses arising out of or resulting from the performance or attempted performance of, the use or attempted use of the services of the Website Brief Engine (including your Website Brief Engine access) to the extent that the same are or have been caused by any negligent or reckless conduct, wilful misconduct, omission, defamation, breach of statutory duty or breach of any of the Terms by you. This provision survives termination of your Website Brief Engine access.</p>
            </ol>

            <p><strong>Modifications of the Terms</strong></p>

            <ol type="1" start="45">
                <li>Stepworks may from time to time vary, modify, delete and/or add any terms or conditions in the Terms at its own discretion. The revised Terms (if made) will be displayed to you in the Website Brief Engine when you access the Website Brief Engine. You must read the revised Terms. By clicking the checkbox against "I accept the revised Terms (including The Privacy Policy, Disclaimer and Copyright clauses)" in the Website Brief Engine, you agree to be bound by the revised Terms and the revised Terms will supersede previous versions of the Terms accepted by you.</li>
            </ol>

            <p><strong>Governing Law</strong></p>

            <ol type="1" start="46">
                <li>The Terms shall be governed and construed in accordance with the laws of the Hong Kong Special Administrative Region. You also agree to submit to the jurisdiction of the Hong Kong Courts.</li>
            </ol>
        </div>
    </div>

    <div id="feedback">
        <a href="#" class="btn-feedback"><img src="assets/images/feedback.svg"> <span>Feedback</span></a>
        <div class="report-content">
            <p id="q">What do you think of our Website Brief Engine?</p>
            <p id="t">Thank you for your feedback!</p>
            <form id="feedback_form">
                <input type="email" id="feedback_email" name="feedback_email" placeholder="Your contact email" required>
                <textarea name="feedback" id="feedback_content" class="nomde" placeholder="Message *" required></textarea>
                <div class="buttons">
                    <button type="submit">Send</button><button class="close">Close</button>
                </div>
            </form>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end7">
                    <div class="copyright">&copy; <?=date('Y')?> Stepworks | <a href="en/legal">Legal</a></div>
                </div>
            </div>
        </div>
    </footer>

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="assets/js/vendor/wNumb.min.js"></script>
    <script src="assets/js/brief_plugins.js"></script>
    <script src="assets/js/website_brief_scripts.js?v=202401261803"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-4948962-2', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <!-- AccessiBe -->
<style>
.acsb-hero-action {
    border-radius: 0px !important;
    white-space: nowrap !important;
    padding: 9px !important;
}

.acsb-header-option.acsb-header-option-close,
.acsb-header-option.acsb-header-option-position,
.acsb-header-option.acsb-header-option-statement {
    border-radius: 0px !important;
}

.acsb-language.acsb-header-option-language {
    display: none !important;
}

.acsb-range-base.acsb-color-lead {
    background-color: #ffffff !important;
}

div.acsb-widget .acsb-main .acsb-main-options .acsb-actions .acsb-actions-box .acsb-actions-group .acsb-action-box.acsb-action-box-big {
    background-color: #f4f4f4 !important;
}

.acsb-action-box {
    background-color: #f4f4f4 !important;
}

.acsb-range-button {
    border-radius: 0px !important;
}

.acsb-widget .acsb-main .acsb-main-options .acsb-actions .acsb-actions-box .acsb-profiles .acsb-profile .acsb-profile-content i {
    background-color: #f4f4f4 !important;
}
.acsb-trigger.acsb-trigger-position-y-bottom.acsb-mobile {
    bottom: 10px !important;
}
.acsb-trigger.acsb-trigger-position-x-right.acsb-mobile {
    right: 10px !important;
}
/* span.acsb-toggle-option {
    background-color: #f4f4f4 !important;
}

.acsb-toggle-option .acsb-toggle-option-off {
    background-color: #ffffff !important;
} */
</style>
<script>
(function() {
    var s = document.createElement('script'),
        e = !document.body ? document.querySelector('head') : document.body;
    s.src = 'https://acsbapp.com/apps/app/assets/js/acsb.js';
    s.async = s.defer = true;
    s.onload = function() {
        acsbJS.init({
            statementLink: '',
            feedbackLink: '',
            footerHtml: '© 2020 Stepworks',
            hideMobile: false,
            hideTrigger: false,
            language: 'en',
            position: 'right',
            leadColor: '#eb0c38',
            triggerColor: '#eb0c38',
            triggerRadius: '0%',
            triggerPositionX: 'right',
            triggerPositionY: 'bottom',
            triggerIcon: 'people',
            triggerSize: 'medium',
            triggerOffsetX: 20,
            triggerOffsetY: 20,
            mobile: {
                triggerSize: 'small',
                triggerPositionX: 'right',
                triggerPositionY: 'bottom',
                triggerOffsetX: 10,
                triggerOffsetY: 10,
                triggerRadius: '0'
            }
        });
    };
    e.appendChild(s);
}());
</script>
</body>
</html>