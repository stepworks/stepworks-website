[[chunk?&file=`header_webaudit.php`]]

<body class="page-insights webaudit">
    [[chunk?file=`page_header.php`]]
    <section>
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="title">[[if?&is=`[*menutitle*]:notempty`&then=`[*menutitle*]`&else=`[*pagetitle*]`]]</h1>
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start3 end6" style="-ms-grid-column-span:3;">
                    [[if?&is=`[*insight_audio*]:notempty`&then=`
                    <audio controls>
                        <source src="[*insight_audio*]" type="audio/mp3">
                    </audio>
                    <br />
                    <br />
                    `]]
                    <p><a href="[~[*id*]~]#webaudit">Jump to the Stepworks Website Assessment Tool</a></p>
                    <div class="dropcap">[*content*]</div>
                </div>
            </div>
            [[multiTv?&tvName=`insight_block`&display=`all`]]
            <div id="webaudit" class="grid grid-6" style="padding-top:10rem;">
                <div class="cell start3 end6" style="-ms-grid-column-span:3;">
                    <div class='questions-wrapper'>
                        <div id="q1" class="question">
                            <h3>Take 7 minutes to assess your website for digital brand building effectiveness:</h3>
                            <p><strong>Enter your website address</strong></p>
                            <form id="submit" method="post">
                                <input id="url" type="text" name="url" autocomplete="off" autocorrect="off" spellcheck="false"
                                    autocapitalize="off">
                                <button id="share-url">Assess website</button>
                            </form>
                            <!-- Loader -->
                            <div id="loader" class='loader-wrapper' style="display:none;">
                                <div class="loader-inner">
                                    <div class="loader"></div>
                                </div>
                                <div class="loader-text">Please wait. We need up to 60 seconds to examine your website.<br>Analysing <span
                                        class="analyse"></span>&hellip;</div>
                            </div>
                            <div id="error" style="display:none;">We can’t scan this website. Websites that can’t be machine-analysed may be
                                set up incorrectly. Try adding/removing www from the address. Or <a href="mailto:hello@stepworks.co">Contact
                                    us</a> if you’d like to know more.</div>
                        </div>

                        <div class="question" id="q2">
                            <h4>Step 1 of 7</h4>
                            <h3><strong>Do visitors get the right impression from your homepage?</strong></h3>
                            <p class="desktopss"><img class="loading" src="assets/css/ajax-loader.gif" alt="loading..."></p>
                            <p class="ssnote"><span class="d">Screenshot not loading? No problem, see it <a href="" class="urlhref"
                                        target="_blank" rel="noopener nofollow">here</a>.</span><span class="m">Screenshot not loading? No
                                    problem, take a look at your website using your laptop, desktop or iPad in landscape mode.</span></p>
                            <p><strong>How accurately does your homepage communicate the value of your organisation?</strong></p>
                            <p class="ssnote">Your self-assessed input here affects the end result.</p>
                            <p><span>Inaccurate</span><input type="range" name="acc" min="1" max="5" value="3"><span>Very accurate</span>
                            </p>
                            <p class='slider-helper'><span>Adjust your answer</span></p>
                        </div>
                        <div class="answer">
                            <h4>Brand building tip</h4>
                            <p>A homepage that immediately gives visitors a strong first impression adds real value. It’s often the first
                                encounter with your brand for important people like:</p>
                            <ul>
                                <li>New customers</li>
                                <li>Potential recruits</li>
                                <li>Potential investors and partners</li>
                                <li>People with influence, such as team family members and friends</li>
                                <li>Others who need to understand the value of your organisation</li>
                            </ul>
                            <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next">Next</a></p>
                        </div>

                        <div class="question" id="q3">
                            <h4>Step 2 of 7</h4>
                            <h3>Is your website helping address business challenges and opportunities?</h3>
                            <p><strong>Which of these areas are a major concern for your organisation?</strong></p>
                            <p><label><input type="radio" autocomplete="off" name="vip" value="new and current customers">
                                    Sales</label><label><input type="radio" autocomplete="off" name="vip"
                                        value="current and potential team members"> HR</label><label><input type="radio" autocomplete="off"
                                        name="vip" value="current and potential investors"> Fundraising</label><label><input type="radio"
                                        autocomplete="off" name="vip" value="current and potential supplier partners"> Supply
                                    chain</label><label><input type="radio" autocomplete="off" name="vip"
                                        value="current and potential strategic partners"> Partnerships</label><label><input type="radio"
                                        autocomplete="off" name="vip" value="journalists, producers and other influencers"> Media</label>
                        </div>
                        <div class="answer">
                            <p>This suggests your key stakeholders are <span class="vip" style="font-weight:700;"></span>.</p>
                            <p>Think about what these people need to understand about your business.</p>
                            <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next">Next</a></p>
                        </div>

                        <div class="question" id="q4">
                            <div class="desktopss"><img class="loading" src="assets/css/ajax-loader.gif" alt="loading..."></div>
                            <p class="ssnote"><span class="d">Screenshot not loading? No problem, see it <a href="" class="urlhref"
                                        target="_blank" rel="noopener nofollow">here</a>.</span><span class="m">Screenshot not loading? No
                                    problem, take a look at your website using your laptop, desktop or iPad in landscape mode.</span></p>
                            <p><strong>Can <span class="vip" style="font-weight:700;"></span> immediately see content that's relevant and
                                    appealing to them on your homepage?</strong></p>
                            <p><span>Slow understanding</span><input type="range" name="und" min="1" max="5" value="3"><span>Instant
                                    understanding</span></p>
                            <p class='slider-helper'><span>Adjust your answer</span></p>
                        </div>
                        <div class="answer">
                            <h4>Brand building tip</h4>
                            <p>People important to you should instantly obtain a relevant and accurate understanding of your organisation,
                                offering or brand.</p>
                            <p>Not every visitor is relevant, so precisely focus on what your most valuable visitors need to know.</p>
                            <p>Communicate your value in the first few words of headline. Navigation buttons can express value too.</p>
                            <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next">Next</a></p>
                        </div>

                        <div class="question" id="q5">
                            <h4>Step 3 of 7</h4>
                            <h3>How well does your website work on mobile devices?</h3>
                            <div class="mobiless"><img class="loading" src="assets/css/ajax-loader.gif" alt="loading..."></div>
                            <p class="ssnote"><span class="d">Screenshot not loading? No problem, take a look at your website on your
                                    phone.</span><span class="m">Screenshot not loading? No problem, see it <a href="" class="urlhref"
                                        target="_blank" rel="noopener nofollow">here</a></span></p>
                            <p><strong>Is your website optimised to convey your most important message on this small mobile screen?</strong>
                            </p>
                            <label><input type="radio" autocomplete="off" name="mobilemsg" value="0"> No</label><label><input type="radio"
                                    autocomplete="off" name="mobilemsg" value="4"> Yes</label>
                        </div>
                        <div class="answer">
                            <h4>Brand building tip</h4>
                            <p>What people need to remember most about your business should be expressed in a simple, clear, memorable way.
                            </p>
                            <p>Does it work on a compact mobile screen? That’s how many of your visitors will view it.</p>
                            <p>A message that’s strong on a small screen can scale up for even bigger impact.</p>
                            <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next">Next</a></p>
                        </div>

                        <div class="question" id="q6">
                            <h4>Step 4 of 7</h4>
                            <h3>Does your website design work for or against you?</h3>
                            <p><strong>Does your website design look simple, focused and uncluttered?</strong></p>
                            <p><span>Cluttered</span><input type="range" name="modern" min="1" max="5" value="3"><span>Uncluttered</span>
                            </p>
                            <p class='slider-helper'><span>Adjust your answer</span></p>
                        </div>
                        <div class="answer">
                            <h4>Brand building tip</h4>
                            <p>People can accurately sense a neglected or unprofessional design. A stagnant look suggests a stagnant
                                organisation.</p>
                            <p>Digital design aesthetics accurately communicate when your website was last updated. </p>
                            <p>Brand builders have learned simplicity and uncluttered clarity are more effective.</p>
                            <p>We can see this in professionally designed websites, which today look totally different to those designed ten
                                years ago. </p>
                            <p>Website design is about maximising message impact through typography. Readability has a profound effect on
                                visitor understanding.</p>
                            <p>You might “like” your website design but what do visitors think? What perception do they take away?</p>
                            <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next">Next</a></p>
                        </div>

                        <div class="question" id="q7">
                            <h4>Step 5 of 7</h4>
                            <h3>Does your website present your business with consistency?</h3>
                            <p><strong>How closely does your website match the colours, fonts and images you use in your presentation decks,
                                    brochures, stationery, and workspace design?</strong></p>
                            <p><span>Bad match</span><input type="range" name="match" min="1" max="5" value="3"><span>Good match</span></p>
                            <p class='slider-helper'><span>Adjust your answer</span></p>
                        </div>
                        <div class="answer">
                            <h4>Brand building tip</h4>
                            <p>Consistency communicates quality. </p>
                            <p>Well run companies project a unified brand image that reassures people with a smooth consistent experience.
                            </p>
                            <p>Your customer journey should feel seamless.</p>
                            <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next">Next</a></p>
                        </div>

                        <div class="question" id="q8">
                            <h4>Step 6 of 7</h4>
                            <h3>How regularly do you review your website data?</h3>
                            <p><label><input type="radio" autocomplete="off" name="review-seo" value="0"> Never</label><label><input
                                        type="radio" autocomplete="off" name="review-seo" value="1"> Every year</label><label><input
                                        type="radio" autocomplete="off" name="review-seo" value="2"> Every 6 months</label><label><input
                                        type="radio" autocomplete="off" name="review-seo" value="3"> Every month</label><label><input
                                        type="radio" autocomplete="off" name="review-seo" value="4"> Every week</label></p>
                        </div>
                        <div class="answer">
                            <h4>Brand building tip</h4>
                            <p>Your website is a dynamic channel that can be constantly monitored and improved for effectiveness.</p>
                            <p>It’s valuable to understand which data points matter. </p>
                            <p>Methodically introducing change to your website can have a measurable effect.</p>
                            <p>Changes to messages and design can often be tested to find the most effective approach.</p>
                            <p>Leaders should have at least a once-a-month review. Review more often if digital directly impacts mission
                                critical operations like sales.</p>
                            <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next">Next</a></p>
                        </div>

                        <div class="question" id="q9">
                            <h4>Step 7 of 7</h4>
                            <h3>How well does your website handle errors?</h3>
                            <p>The following <span class="sl">screenshot</span> shows what your audience likely sees when there’s an error
                                on your site.</p>
                            <div class="iframe-wrapper">
                                <img id="404_image" src=""></img>
                                <p class="ssnote"><span class="d">Screenshot not loading? No problem, see it <a href="" class="urlnfhref"
                                            target="_blank" rel="noopener nofollow">here</a>.</span><span class="m">Screenshot not loading?
                                        No problem, take a look at your 404 page using your laptop, desktop or iPad in landscape
                                        mode.</span></p>
                            </div>
                            <p><strong>How well does your error page (also called a 404 page) express your organisation’s brand? Score low
                                    if the page is missing.</strong></p>
                            <p><span>It poorly expresses our brand</span><input type="range" name="notfound" min="1" max="5"
                                    value="3"><span>It expresses our brand well</span></p>
                            <p class='slider-helper'><span>Adjust your answer</span></p>
                        </div>
                        <div class="answer">
                            <h4>Brand building tip</h4>
                            <p>Your 404 page appears when the web address is incorrect. Don’t waste it. It’s an opportunity to inform and
                                brighten your visitor’s experience.</p>
                            <p><a href="https://stepworks.co/404" target="_blank">Check out ours if you find this idea puzzling...</a></p>
                            <p>Reduce the negative impact of an error or problem with an interesting on-brand message.</p>
                            <p>Your website visitor’s journey is full of opportunities to deliver nice surprises.</p>
                            <p style="margin-top:2rem;text-align:right;"><a href="#" id="last" class="btn next">Next</a></p>
                        </div>

                        <div class="results">
                            <h3>Overall website assessment result</h3>
                            <p>Here’s how <span class="url"></span> scored for brand building effectiveness.</p>

                            <p class="track" style="margin-top:4rem;">
                                <span class="scorebar overall"><span id="overall-bar" class="score"></span></span><span id="overall-score"
                                    class="score-val"></span><br>
                            </p>
                            <div class="totalbar">
                                <div class="poor">0-59</div>
                                <div class="average">60-79</div>
                                <div class="great">80-100</div>
                            </div>

                            <p class="pleasenote">Please note that your score is an approximation of your website brand building
                                effectiveness. A Stepworks specialist can offer a deeper analysis.</p>

                            <p style="margin:2rem 0 0;"><a href="#" class="showall">See other available outcomes</a></p>
                            <div class="allresults" style="display:none;margin-top:2rem;">
                                <p class="result poor"><strong>Poor</strong> – Our preliminary website assessment shows that your website
                                    effectiveness is low. An online experience with your organisation is likely to frustrate and disappoint
                                    visitors. They will form negative perceptions of your organisation. Your current website may be working
                                    against you and not supporting your organisational goals.</p>
                                <p class="result average"><strong>Average</strong> – Our preliminary website assessment shows that your
                                    website effectiveness is OK. Visitors are likely to experience an adequate online encounter. You almost
                                    certainly have an opportunity for improvement so that your website better supports your organisational
                                    goals.</p>
                                <p class="result great"><strong>Great</strong> – Well done, our preliminary website assessment shows that
                                    you’re getting it right. Visitors are likely to perceive your organisation as efficient, helpful and
                                    contemporary.</p>
                                <p><br></p>
                            </div>

                            <!-- <p style="margin:2rem 0 0;"><a href="#" class="breakdown">Show detailed breakdown</a></p> -->
                            <div class="scores" style="margin-top:2rem;">
                                <h4>Here’s a breakdown of your website brand building effectiveness score</h4>
                                <p>Our automated assessment reflects how likely people will understand the value of your business via your
                                    website. Branding is essentially human, so any machine analysis has limitations – it is a useful way for
                                    brand builders to get a more objective view of their website.</p>
                                <div class="brand">
                                    <input type="hidden" name="bb" value="">
                                    <p><strong>Website brand building effectiveness score</strong></p>

                                    <p class="track"><strong class="red">Digital brand experience</strong><br>
                                        <span class="scorebar bb-experience"><span id="experience-bar" class="score"></span></span><span
                                            id="experience-score" class="score-val"></span><br>
                                        This score reflects the degree visitors will engage positively with your website and brand based on
                                        modern user expectations.
                                    </p>

                                    <p class="track"><strong class="red">Value delivered to business</strong><br>
                                        <span class="scorebar bb-business"><span id="business-bar" class="score"></span></span><span
                                            id="business-score" class="score-val"></span><br>
                                        This score indicates how well your website builds brand and business value.
                                    </p>

                                    <p class="track"><strong class="red">Design effectiveness</strong><br>
                                        <span class="scorebar bb-design"><span id="design-bar" class="score"></span></span><span
                                            id="design-score" class="score-val"></span><br>
                                        This score indicates how effectively your website’s graphic design and brand identity positively
                                        influence visitors.
                                    </p>
                                </div>

                                <div class="lighthouse">
                                    <input type="hidden" name="lh" value="">
                                    <p><strong>Website technical effectiveness score</strong></p>
                                    <p>It’s important to pay attention to the technical side when building an effective and persuasive
                                        website. The following dimensions influence the overall user experience. Scores above [75] suggest
                                        above average effectiveness.</p>

                                    <p class="track"><strong class="red">Performance</strong><br>
                                        <span class="scorebar lh-performance"><span id="performance-bar" class="score"></span></span><span
                                            id="performance-score" class="score-val"></span><br>
                                        This reflects how quickly visitors see your website pages arrive (people are impatient and making
                                        someone wait forms a poor impression). It might look fast to you, but be slow on some devices or in
                                        some locations.
                                    </p>

                                    <p class="track"><strong class="red">Accessibility</strong><br>
                                        <span class="scorebar lh-accessibility"><span id="accessibility-bar"
                                                class="score"></span></span><span id="accessibility-score" class="score-val"></span><br>
                                        A website friendly to people with disabilities promotes an inclusive company culture and creates a
                                        positive impression on visitors.
                                    </p>
                                    <p class="track"><strong class="red">Best practices</strong><br>
                                        <span class="scorebar lh-bestpractices"><span id="bestpractices-bar" class="score"></span></span>
                                        <span id="bestpractices-score" class="score-val"></span><br>
                                        This checks for common mistakes in websites. Website standards change and this score reflects your
                                        site’s compliance with relevant modern standards.
                                    </p>
                                    <p class="track"><strong class="red">SEO effectiveness</strong><br>
                                        <span class="scorebar lh-seo"><span id="seo-bar" class="score"></span></span> <span id="seo-score"
                                            class="score-val"></span><br>
                                        Search engines aim to determine the relevance of content. A high score indicates your website
                                        content can be correctly categorised for valuable search engine visibility.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="takeaway">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end6">
                    [*insight_takeaway*]
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start1 end4">
                    [*insight_bottom*]
                </div>
            </div>
        </div>
        [[if?&is=`[*insight_widgetbox*]:notempty`&then=`
        <div class='insights-item-widget'>
            <div class='widget-wrapper'>
                <div class="widget-text">[*insight_widgetbox*]</div>
                <div class="widget-get-in-touch"><a href='[~6~]'>Get in touch</a></div>
            </div>
        </div>
        `]]
        [[if?&is=`[*insight_widgetbox*]:notempty`&then=`
        <div class='insights-item-widget-hidden'>
            <div class='widget-wrapper'>
                <div class="widget-text">[*insight_widgetbox*]</div>
                <div class="widget-get-in-touch"><a href='[~6~]'>Get in touch</a></div>
            </div>
        </div>
        `]]
    </section>

    <section class="siblings">
        <div class="container">
        [[prevnextPage? &folderid=`[*parent*]`&curId=`[*id*]`&prevTpl=`nextInsight`&nextTpl=`prevInsight`]]
        <div class="grid grid-3">
                [+pnp_next+][+pnp_prev+]
            </div>
        </div>
    </section>

    <div class="progress">
        <div class="bar"></div>
    </div>
    <div id="feedback">
        <a href="#" class="btn-feedback"><img src="assets/images/feedback.svg"> <span>Feedback</span></a>
        <div class="report-content">
            <p id="q">What do you think of our Website Assessment Tool?</p>
            <p id="t">Thank you for your feedback!</p>
            <form id="feedback_form">
                <input type="email" id="feedback_email" name="feedback_email" placeholder="Your contact email" required>
                <textarea name="feedback" id="feedback_content" class="nomde" placeholder="Message *" required></textarea>
                <div class="buttons">
                    <button type="submit">Send</button><button class="close">Close</button>
                </div>
            </form>
        </div>
    </div>
    [[chunk?&file=`footer.php`]]
    <script src="./assets/js/web-audit-api.min.js?v=20200421"></script>
    <script src="assets/js/tooltip.js"></script>
    <script src="assets/js/jquery.glossarize.js"></script>
    <script src="assets/js/audioplayer.js"></script>
    <script>

  </script>
    <script>
        $(function() {
            $('audio').audioPlayer();
        });

        $(function(){
            $('.container').glossarizer({
                sourceURL: '[~[*id*]~]tooltip',
                callback: function(){
                    new tooltip();
                }   
            });
        });

    </script>
    
<style>
.invalid {
    border-bottom: 2px solid #eb0c38 !important;
}

#metadesc {
    font-style: italic;
}

#loader,
#error {
    margin: 3rem 0;
    display: none;
}

.warn {
    color: #eb0c38;
}

/* .loader,
.loader:before,
.loader:after {
    background: #000000;
    -webkit-animation: load1 1s infinite ease-in-out;
    animation: load1 1s infinite ease-in-out;
    width: 1em;
    height: 4em;
} */

/* .loader-inner {
    height: 50px;
    width: 100px;
    flex: 1 0 80px;
} */

.loader-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
}

.loader-text {
    font-weight: bold;
    margin-left: 20px;
}

/* .loader {
    color: #000000;
    text-indent: -9999em;
    margin: 5px auto;
    position: relative;
    font-size: 11px;
    -webkit-transform: translateZ(0);
    -ms-transform: translateZ(0);
    transform: translateZ(0);
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
}

.loader:before,
.loader:after {
    position: absolute;
    top: 0;
    content: '';
}

.loader:before {
    left: -1.5em;
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
}

.loader:after {
    left: 1.5em;
}

@-webkit-keyframes load1 {

    0%,
    80%,
    100% {
        box-shadow: 0 0;
        height: 4em;
    }

    40% {
        box-shadow: 0 -2em;
        height: 5em;
    }
}

@keyframes load1 {

    0%,
    80%,
    100% {
        box-shadow: 0 0;
        height: 4em;
    }

    40% {
        box-shadow: 0 -2em;
        height: 5em;
    }
} */
.loader{
    background: url(./assets/images/heartbeat.svg) no-repeat left top;
    background-size: 100% auto;
    width: 75px;
    height: 70px;
}

@media all and (max-width: 768px){
    .page-insights.webaudit #submit{
        flex-wrap: wrap;
    }
    .page-insights.webaudit #submit button{
        margin-left: 0;
        margin-top: 20px;
    }
}
</style>
</body>
</html>