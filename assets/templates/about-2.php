[[chunk?&file=`header.php`]]
<body class="page-about">
    [[chunk?file=`page_header.php`]]
    <section>
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="super">[*about_page_heading*]</h1>
                    <h2>[*about_page_description*]</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="fast-facts pt0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3">
                    <h1>[*about_fun_facts_heading*]</h1>
                </div>
                <div class="cell cell start3 end6">
                    <div class="grid grid-3">
                        [[multiTv?&tvName=`about_fun_facts`&display=`all`]]
                        <!-- <div class="cell">
                            <img src="assets/images/focus.svg" />
                            <p>
                                <strong>Focus</strong><br>
                                Supporting organisations at times of pivotal change
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/expertise.svg" />
                            <p>
                                <strong>Expertise</strong><br>
                                Brand strategy, messaging frameworks, design, campaigns and digital
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/founded.svg" />
                            <p> <strong>Founded</strong><br>
                                1994
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/base.svg" />
                            <p>
                                <strong>Locations</strong><br>
                                Hong Kong, Singapore
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/reach.svg" />
                            <p>
                                <strong>Reach</strong><br>
                                Global and across cultures
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/ownership.svg" />
                            <p>
                                <strong>Ownership</strong><br>
                                Partner-owned and operated
                            </p>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="animate mt0 pt0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell cell start3 end5">[*about_magnet_global_text*]</div>
            </div>
        </div>
    </section>
    <section class="animate pb0 mb0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3">
                    <h1>[*about_team_heading*]</h1>
                </div>
                <div class="cell start3 end5">[*about_team_content*]</div>
            </div>
        </div>
    </section>
    <section class="team mt0 pt0 pb0 mb0">
        <div class="container">
            <div class="team-list animate">
                [[DocLister?&parents=`[[if?&is=`[[currLang]]:is:en`&then=`9`&else=`1052`]]`&tpl=`team-item`&orderBy=`RAND()`&tvList=`team_photo`&addWhereList=`hidemenu=0`]]
            </div>
        </div>
    </section>
    <section class="partners animate pt0 pb0">
        <div class="container">
            <div class="grid grid-2 leadership">
                <div class="cell">
                    <img src="assets/images/team/leadership-team.jpg" alt="Stepworks leadership team">
                </div>
                <div class="cell">
                    <h2>[*about_leadership_heading*]</h2>
                    <p>[*about_leadership_text*]</p>
                    <div class="leadership-list">
                        [[DocLister?&parents=`[[if?&is=`[[currLang]]:is:en`&then=`9`&else=`1052`]]`&tpl=`leadership-item`&orderBy=`menuindex ASC`&addWhereList=`template=13`]]
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-values animate pb0 mb0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3">
                    <h1>[*about_value_heading*]</h1>
                </div>
                <div class="cell start3 end6">
                    [*about_value_content*]
                    
                    <div class="values-list">
                        [[multiTv?&tvName=`about_values_list`&display=`all`]]
                        <!-- <h4><a href="#">Together, wholeheartedly</a></h4>
                        <div class="values-content">
                            <p>It’s about the quality and effectiveness of what we do together, and not about our individual egos. We believe in the importance of open honest relationships and encourage sharing. As the proverb goes, “If you want to go fast, go alone. If you want to go far, go together.”</p>
                        </div>

                        <h4><a href="#">Be better, continuously</a></h4>
                        <div class="values-content">
                            <p>We believe in the importance of continuous improvement, kaizen. Always challenge the status quo. This helps us be better in ourselves, as a team, and do better for the clients we serve.</p>
                        </div>

                        <h4><a href="#">Simplify</a></h4>
                        <div class="values-content">
                            <p>Complexity is easy. Simplicity is hard. But simple is smart, effective and more sustainable. We work smart and hard to simplify without being simplistic.</p>
                        </div>

                        <h4><a href="#">Wow our clients, ourselves and others</a></h4>
                        <div class="values-content">
                            <p>Wow! Meticulously thought-through and executed brand building solutions help propel positive change that leads to opportunities and solves problems. We work to wow each other by doing our best.</p>
                        </div>

                        <h4><a href="#">Create value for one another</a></h4>
                        <div class="values-content">
                            <p>We work with purpose to create value for our customers and one another. We recognize success and share the rewards.</p>
                        </div>

                        <h4><a href="#">Love what we do, and love in what we do</a></h4>
                        <div class="values-content">
                            <p>We’re passionate about creating positive change through our brand building skills. We make a conscious effort to be helpful to people – our colleagues, clients, and the charity initiatives we support. We encourage one another to apply kindness, understanding, care and compassion to our thoughts and actions, because why wouldn’t we?</p>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    [[chunk?&file=`footer.php`]]
</body>

</html>