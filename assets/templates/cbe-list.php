[[chunk?&file=`header_brief.php`]]

<?php
const DB_HOST = "localhost";
// const DB_USER = "root";
// const DB_PASS = "root";
// const DB_USER = "stepworks_brief";
// const DB_PASS = "1zKb%8w2";
// const DB_NAME = "stepworks_brief";
const DB_USER = "stepworks_root";
const DB_PASS = "rootStep1929";
//const DB_PASS = "root";
const DB_NAME = "stepworks_brief";

function getDB() {
    $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    return $mysqli;
}

function getBriefs() {
    $mysqli = getDB();
    $sql = "SELECT * FROM `brief` ORDER BY `brief`.`id` DESC";
    $result = mysqli_query($mysqli, $sql);
    echo '<table>';
    echo '<tr><th width="20%">Email</th><th width="25%">Company Name</th><th width="20%">Contact Person</th><th width="15%">Date</th><th width="10%"></th></tr>';
    while ($row = mysqli_fetch_row($result)) {
        if( !strpos($row[2], 'stepworks.co') ){
            echo '<tr><td>'.$row[2].'</td><td>'.$row[4].'</td><td>'.$row[5].'</td><td>'.$row[7].'</td><td><a href="https://stepworks.co/en/view-brief?id='.$row[1].'" target="_blank">View brief</a></td></tr>';
        }
    }
    echo '</table>';
}
?>

<body class="page-website-brief">
    [[chunk?file=`page_header_mini.php`]]

    <section class="brief-list">
        <div class="container">
            <h1>Creative Briefs</h1>
            <?php getBriefs(); ?>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end7">
                    <div class="copyright">&copy; <?=date('Y')?> Stepworks | <a href="en/legal">Legal</a></div>
                </div>
            </div>
        </div>
    </footer>

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
</body>
</html>