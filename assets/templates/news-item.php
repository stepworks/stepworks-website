[[chunk?&file=`header.php`]]
<body class="page-news-item">
    [[chunk?&file=`page_header.php`]]
    <section class="news-body">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end5">
                    <h1>[*pagetitle*]</h1>
                </div>
            </div>
            <p class="date">[*longtitle*]</p>

            <div class="grid grid-6 images">
                <div class="cell start1 end5">
                    [[multiTV?&tvName=`news_image`]]
                </div>
            </div>

            <div class="grid grid-6 content">
                <div class="cell start3 end6">
                    [*content*]
                    <p><br></p>
                    <a href="[~[*parent*]~]">See all news</a>
                </div>
            </div>
        </div>
    </section>
    <section class="siblings">
        <div class="container">
            [[prevnextPage? &folderid=`[*parent*]`&curId=`[*id*]`&prevTpl=`prevNews`&nextTpl=`nextNews`]]
            <div class="grid grid-3">
                [+pnp_prev+][+pnp_next+]
            </div>
        </div>
    </section>
    [[chunk?&file=`footer.php]]
</body>
</html>