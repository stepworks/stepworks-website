[[chunk?&file=`header.php`]]

<body class="page-insights">
    [[chunk?file=`page_header.php`]]

    <section>
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="title">[[if?&is=`[*menutitle*]:notempty`&then=`[*menutitle*]`&else=`[*pagetitle*]`]]</h1>
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start3 end6">
                    [[if?&is=`[*insight_audio*]:notempty`&then=`
                    <audio controls>
                        <source src="[*insight_audio*]" type="audio/mp3">
                    </audio>
                    <br />
                    <br />
                    `]]
                    <p>[[if?&is=`[*tldr_link*]:notempty`&then=`<a class="tldr_link" href="[~[*id*]~]#tldr">[*tldr_link*]</a>`]]</p>
                    <div class="dropcap">[*content*]</div>
                </div>
            </div>
            [[multiTv?&tvName=`insight_block`&display=`all`]]
        </div>
    </section>

    [[if?&is=`[*tldr_link*]:notempty`&then=`
    <section id="tldr_content" style="display:none;padding-top:0;">
        <a name="tldr"></a>
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3"></div>
                <div class="cell start3 end6">
                    [*tldr_content*]
                </div>
            </div>
        </div>
    </section>
    `]]
    <section class="takeaway">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end6">
                    [*insight_takeaway*]
                </div>
            </div>
            <div class="grid grid-6">
                <div class="cell start1 end4">
                    [*insight_bottom*]
                </div>
            </div>
        </div>
        [[if?&is=`[*insight_widgetbox*]:notempty`&then=`
        <div class='insights-item-widget'>
            <div class='widget-wrapper'>
                <div class="widget-text">[*insight_widgetbox*]</div>
                <div class="widget-get-in-touch"><a href='[~6~]'>Get in touch</a></div>
            </div>
        </div>
        `]]
        [[if?&is=`[*insight_widgetbox*]:notempty`&then=`
        <div class='insights-item-widget-hidden'>
            <div class='widget-wrapper'>
                <div class="widget-text">[*insight_widgetbox*]</div>
                <div class="widget-get-in-touch"><a href='[~6~]'>Get in touch</a></div>
            </div>
        </div>
        `]]
    </section>

    <section class="siblings">
        <div class="container">
        [[prevnextPage? &folderid=`[*parent*]`&curId=`[*id*]`&prevTpl=`nextInsight`&nextTpl=`prevInsight`]]
        <div class="grid grid-3">
                [+pnp_next+][+pnp_prev+]
            </div>
        </div>
    </section>

    <div class="progress">
        <div class="bar"></div>
    </div>
    [[chunk?&file=`footer.php`]]
    <script src="assets/js/vendor/gsap.min.js"></script>
    <script src="assets/js/vendor/ScrollTrigger.min.js"></script>
    <script src="assets/js/tooltip.js"></script>
    <script src="assets/js/jquery.glossarize.js"></script>
    <script src="assets/js/audioplayer.js"></script>
    <script>

  </script>
    <script>
        $(function() {
            $('audio').audioPlayer();
        });

        $(function(){
            $('.container').glossarizer({
                sourceURL: '[~[*id*]~]tooltip',
                callback: function(){
                    new tooltip();
                }   
            });
        });

    if ($(window).width() > 940) {
        var $blk = $('.browserframe').closest('.insight-block-row');
        $blk.prepend($('.browserframe').detach());

        var tl = gsap.timeline({
            scrollTrigger: {
                trigger: '[data-index="5"]',
                start: 'top 12%',
                end: '+=1000% 50%',
                scrub: true,
                pin: true,
                toggleActions: 'play none reverse none',
                // markers: true
                // pinSpacing: 'margin'
            }
        });

        // set caption content
        tl.call(function() {
            $('#caption').html($('#a').data('content'));
        });

        // fade in label
        // at the same time, move/fade in caption
        // at the same time, set mask area
        tl.from('#a', {
            opacity: 0,
            duration: 0.5
        }, '<');
        tl.set('#caption', {
            css: {
                left: '50%',
                top: '2%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        }, '<');
        tl.to('#mask', {
            css: {
                duration: 1,
                opacity: 1,
                width: '98%',
                height: '15%',
                left: '1%',
                top: '1%'
            }
        }, '<');

        // fade out caption
        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        // set content (a again for reverse)
        tl.call(function() {
            $('#caption').html($('#a').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#c').data('content'));
        });

        tl.to('#a', {
            opacity: 0,
            duration: 0.5
        });

        tl.from('#c', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '22%',
                top: '17%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                width: '47%',
                height: '6%',
                left: '2%',
                top: '10%'
            }
        }, '<');

        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        tl.call(function() {
            $('#caption').html($('#c').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#d').data('content'));
        });

        tl.to('#c', {
            opacity: 0,
            duration: 0.5
        });

        tl.from('#d', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '36%',
                top: '33%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                width: '98%',
                height: '22%',
                left: '1%',
                top: '16%'
            }
        }, '<');

        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        tl.call(function() {
            $('#caption').html($('#d').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#e').data('content'));
        });

        tl.to('#d', {
            opacity: 0,
            duration: 0.5
        });

        tl.from('#e', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '30%',
                top: '20%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                width: '98%',
                height: '8%',
                left: '1%',
                top: '34%'
            }
        }, '<');

        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        tl.call(function() {
            $('#caption').html($('#e').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#f').data('content'));
        });

        tl.to('#e', {
            opacity: 0,
            duration: 0.5
        });

        tl.from('#f', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '30%',
                top: '24%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                width: '98%',
                height: '52%',
                left: '1%',
                top: '38%'
            }
        }, '<');

        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        tl.call(function() {
            $('#caption').html($('#f').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#g').data('content'));
        });

        tl.to('#caption, #mask', {
            css: {
                opacity: 0
            },
            delay: 1
        });
        tl.to('#f', {
            opacity: 0,
            duration: 0.5
        }, '<');
        tl.to('.browserframe-inner', {
            y: '-80%',
            delay: 1
        });

        tl.from('#g', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '57%',
                top: '75%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                opacity: 1,
                width: '98%',
                height: '56%',
                left: '1%',
                top: '28%'
            }
        }, '<');
        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        tl.call(function() {
            $('#caption').html($('#g').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#h').data('content'));
        });

        tl.to('#g', {
            opacity: 0,
            duration: 0.5
        });
        tl.to('#caption, #mask', {
            css: {
                opacity: 0
            },
            delay: 1
        });
        tl.to('.browserframe-inner', {
            y: '-191.4%',
            delay: 1
        });

        tl.from('#h', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '48%',
                top: '50%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                opacity: 1,
                width: '42%',
                height: '9%',
                left: '47%',
                top: '39%'
            }
        }, '<');

        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        tl.call(function() {
            $('#caption').html($('#h').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#i').data('content'));
        });

        tl.to('#h', {
            opacity: 0,
            duration: 0.5
        });

        tl.from('#i', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '4%',
                top: '54%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                width: '98%',
                height: '8%',
                left: '1%',
                top: '67%'
            }
        }, '<');

        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        tl.call(function() {
            $('#caption').html($('#i').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#j').data('content'));
        });

        tl.to('#i', {
            opacity: 0,
            duration: 0.5
        });

        tl.from('#j', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '4%',
                top: '60%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                width: '34%',
                height: '15%',
                left: '4%',
                top: '77%'
            }
        }, '<');
        tl.to('#caption', {
            css: {
                opacity: 0
            }
        }, '+=2');

        tl.call(function() {
            $('#caption').html($('#j').data('content'));
        });
        tl.call(function() {
            $('#caption').html($('#k').data('content'));
        });

        tl.to('#j', {
            opacity: 0,
            duration: 0.5
        });

        tl.from('#k', {
            opacity: 0,
            duration: 0.5,
            delay: 1
        });
        tl.set('#caption', {
            css: {
                left: '40%',
                top: '59%'
            }
        });
        tl.to('#caption', {
            css: {
                opacity: 1
            }
        });
        tl.to('#mask', {
            duration: 1,
            css: {
                width: '40%',
                height: '18%',
                left: '40%',
                top: '77%'
            }
        }, '<');

        $('.label').on('click', function() {
            var x = $(this).position().left + 45,
                y = $(this).position().top + 15;

            $('#caption').html($(this).data('content'));

            var cx = x > 450 ? x - 385 : x;
            var cy = y > 320 ? y - $('#caption').outerHeight() + 10 : y;
            if (!!$(this).closest('.browserframe-inner').length) {
                $('#caption').fadeIn().css({
                    left: 'calc(1.0715% + ' + cx + 'px)',
                    top: 'calc(15.2616% + ' + cy + 'px)'
                })
                my = my + 105;
                mx = mx + 7;
            } else {
                $('#caption').fadeIn().css({
                    left: cx + 'px',
                    top: cy + 'px'
                });
            }
            $('#mask').css({
                opacity: 1,
                left: mx - 840 - 24 + 'px',
                top: my - 688 + 'px'
            });
        });
    }

    // $('#caption').on('click', function() { $(this).fadeOut(); });
    </script>
</body>

</html>