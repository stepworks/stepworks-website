<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>[*pagetitle*]</title>
    <base href="[(site_url)]">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="assets/templates/manifest.json.php">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="apple-mobile-web-app-title" content="My Card">

    <style>
        *,*::before,*::after{box-sizing: border-box;}
        body{padding: 0; margin: 0; font-family: "Helvetica Neue", Arial, sans-serif; font-size: 1.1rem; font-weight: normal; color: #16161d;}
        img{max-width: 100%; height: auto; border: 0 none;}
        h1{margin: 0 0 1rem; font-size: 2rem; line-height: 1; font-weight: bold;}
        h2{margin: 0 0 2rem; font-size: 1.2rem; line-height: 1; font-weight: normal;}
        ul{margin: 0; padding: 0; list-style: none;}
        a{color: #eb0c38; display: inline-block; line-height: 1.5;}
        .container{width: 100%; max-width: 500px; padding: 2rem; margin: 0 auto;}
        figure{width: 160px; height: 100px; margin: 0 auto 1.5rem; position: relative;}
        figure img{background-color: #f0f0f0; display: block; height: 100%;}
        .add-contact{text-align: center; border: 1px solid #ddd; margin-bottom: 1rem;}
        .add-contact .title{padding: 1rem; border-bottom: 1px solid #ddd; cursor: pointer; position: relative;}
        .add-contact .title::after{position: absolute; content: ""; top: 50%; right: 1rem; background: url("data:image/svg+xml,%3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 12 8' style='enable-background:new 0 0 12 8;' xml:space='preserve'%3E%3Cpath d='M1,7.1l5.1-5.3c0,0,3.6,3.9,4.9,5.3' fill='none' stroke='%2316161d' stroke-width='2' /%3E%3C/svg%3E") no-repeat center; background-size: 100% auto; width: 12px; height: 8px; transform: translate(0, -50%) rotate(180deg);}
        .add-contact .title.closed{border-bottom: 0 none;}
        .add-contact .body{padding: 1rem;}
        .add-contact .body .btn{display: inline-block; text-decoration: none; padding: 1rem 2rem; margin-top: 2rem; color: #fff; background-color: #eb0c38; font-size: 1rem;}
        .link-block{margin-bottom: 1rem; text-align: center; border: 1px solid #ddd;}
        .link-block a{display: block; padding: 1rem;}
    </style>
</head>
<body>
    <div class="container">
        [[getMemberDetails &id=`[*parent*]`]]
    </div>

    <script src="https://code.jquery.com/jquery-3.6.3.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.title').click(function(){
                $(this).toggleClass('closed').next('.body').slideToggle('fast');
            });
        });
    </script>
</body>
</html>