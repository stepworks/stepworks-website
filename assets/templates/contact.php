[[chunk?&file=`header.php`]]
<body class="page-contact">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <div class="grid grid-6">
                <h1 class="super cell start1 end5">[*contact_page_heading*]</h1>
                <div class="address cell start1 end3">
                    <p>
                        <a href="mailto:hello@stepworks.co">hello@stepworks.co</a><br>
                    </p>
                </div>
                <div class="contact-content cell start3 end6">
                    <form id="contact-form" action="[~182~]">
                        [*contact_page_content*]

                        <input type="hidden" name="ua" value="<?=$_SERVER['HTTP_USER_AGENT']?>">

                        <div class="form-row">
                            <label for="name">[[lang?&text=`label_name`]]</label>
                            <input id="name" type="text" name="name" required>
                        </div>
                        <div class="form-row">
                            <label for="email">[[lang?&text=`label_email`]]</label>
                            <input id="email" type="email" name="email" required>
                        </div>
                        <div class="form-row company">
                            <label for="company">[[lang?&text=`label_company`]]</label>
                            <input id="company" type="text" name="company">
                        </div>
                        <div class="form-row">
                            <label for="message">[[lang?&text=`label_message`]]</label>
                            <textarea id="message" name="message" required></textarea>
                        </div>
                        <div class="form-row button">
                            <button>[[lang?&text=`label_send`]]</button>
                        </div>
                    </form>
                    <div class="ty">
                        <p class="intro">[[lang?&text=`thank_you_message`]]</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="full-image mb0 map">
      <div class="grid grid-3">
        <div class="cell map__address">
          <a href="[~686~]"><strong>[[lang?&text=`hong_kong`]]</strong></a>
          <br><br>
          [[lang?&text=`hong_kong_address`]]
          <br><br>
          [[lang?&text=`hong_kong_tel`]]
        </div>
        <div class="cell cell-right2">
          <div id="map-hk" style="height:500px;max-height:50vh;"></div>
        </div>
      </div>
      <div class="grid grid-3">
        <div class="cell cell-left2">
          <div id="map-sg" style="height:500px;max-height:50vh;"></div>
        </div>
        <div class="cell map__address">
          <a href="[~685~]"><strong>[[lang?&text=`singapore`]]</strong></a>
          <br><br>
          [[lang?&text=`singapore_address`]]
          <br><br>
          [[lang?&text=`singapore_tel`]]
        </div>
      </div>
    </section>

    [[chunk?&file=`footer.php`]]
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeLAqr1b4yClL2h9mAk1h2tSClMhmr8Fo&callback=initMap"
    type="text/javascript"></script>
    <script>
    var mapHK, mapSG;
    function initMap() {
        var latlngHK = new google.maps.LatLng(22.280274, 114.15557);
        var latlngSG = new google.maps.LatLng(1.278374, 103.847836);
        var styles = [
            {
                "stylers": [
                    {"gamma": 0.5}, //Gama value.
                    {"hue": "#ff0022"}, //Hue value.
                    {"saturation": -100} //Saturation value.
                ]
            }
        ];

        var mapOptions = {
            scrollwheel: false,
            zoom: 19,
            styles: styles,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false,
            scaleControl: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            }
        };

        mapHK = new google.maps.Map(document.getElementById("map-hk"), Object.assign({
          center: latlngHK
        }, mapOptions));
        mapSG = new google.maps.Map(document.getElementById("map-sg"), Object.assign({
          center: latlngSG,
        }, mapOptions));
        var markerHK = new google.maps.Marker({
            position: latlngHK,
            icon: '[(site_url)]assets/images/sw-map-marker.png',
            map: mapHK,
        });

        var markerSG = new google.maps.Marker({
            position: latlngSG,
            icon: '[(site_url)]assets/images/sw-map-marker.png',
            map: mapSG,
        });

        google.maps.event.addDomListener(window, 'load', initialize);
        google.maps.event.addDomListener(window, "resize", function () {
            var centerHK = mapHK.getCenter();
            google.maps.event.trigger(mapHK, "resize");
            mapHK.setCenter(centerHK);

            var centerSG = mapSG.getCenter();
            google.maps.event.trigger(mapSG, "resize");
            mapSG.setCenter(centerSG);
        });
    }
    </script>
</body>
</html>