[[chunk?&file=`header.php`]]

<body class="page-insights">
    [[chunk?file=`page_header.php`]]


    <section id="insights" class="intro">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    [*toolbox_insight_heading*]
                </div>
            </div>
        </div>
    </section>

    <section class="latest-insight animate">
        <div class="container">
            <h4 class="latest-insight-section-title">Latest article</h4>
            <div class="grid grid-2">
            [[DocLister?&parents=`[*id*]`&display=`1`&tpl=`insight-latest-item`&orderBy=`menuindex DESC`&tvList=`read_time,ogimg,iteration`]]
            </div>
        </div>
    </section>

    <section class="insight-list">
        <div class="container">
            <!-- <div class="insights-filter">
                <div class="all">
                    <a href="#all" data-param="all" class="selected">All topics</a>
                </div>
                <div class="list-no-children">
                    <a href="#branding" data-param="branding">Branding step-by-step</a>
                </div>
                
                <div class="list business">
                    <a href="#">Business context</a>
                    <div class="list-container">
                        <ul>
                            <li><a href="#" data-type="sec" data-param="transportation" data-blurb="">Children 1</a></li>
                            <li><a href="#" data-type="sec" data-param="transportation" data-blurb="">Children 2</a></li>
                            <li><a href="#" data-type="sec" data-param="transportation" data-blurb="">Children 3</a></li>
                        </ul>
                    </div>
                </div>
                <div class="list sector">
                    <a href="#">Sector specific</a>
                    <div class="list-container">
                        <ul>
                            <li><a href="#" data-type="sec" data-param="transportation" data-blurb="">Children 1</a></li>
                            <li><a href="#" data-type="sec" data-param="transportation" data-blurb="">Children 2</a></li>
                            <li><a href="#" data-type="sec" data-param="transportation" data-blurb="">Children 3</a></li>
                        </ul>
                    </div>
                </div>
            </div> -->
            <div class="list-inner insights-list">
                [[DocLister?&parents=`[*id*]`&tpl=`insight-list-item`&orderBy=`menuindex
                ASC`&tvList=`read_time,ogimg`&addWhereList=`hidemenu=0`]]
            </div>
        </div>
    </section>
    [[chunk?&file=`footer.php`]]
    <script src="assets/js/vendor/isotope.min.js"></script>
</body>

</html>