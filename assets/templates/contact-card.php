<?php
$doc = $modx->documentObject;
require 'assets/snippets/qrcode/QRCode.class.php';
$name = explode(' ',$doc['pagetitle']);
if(!empty($doc['team_first_name'][1])){ $name = [$doc['team_first_name'][1], $doc['team_last_name'][1]]; }
$qrc = new QRCode;
$qrc->url('https://stepworks.co/');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>[*pagetitle*]</title>
    <base href="[(site_url)]">

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="assets/templates/manifest.json.php">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="apple-mobile-web-app-title" content="My Card">
    <style>
        * { box-sizing: border-box }
        *:before, &:after { box-sizing: inherit; }
        body {
            margin: 0;
            font: 400 34px/1.4 "Helvetica Neue",Arial,sans-serif
        }
        .container {
            width: 100%;
            max-width: 500px;
            padding: 2rem;
            margin: 0 auto;
        }
        figure {
            position: relative;
            margin: 0;
            padding: 0;
        }
        .overlayphoto {
            width: 106px;
            height: 106px;
            position: absolute;
            left: calc(50% - 53px);
            top: calc(50% - 53px);
            /*overflow: hidden;*/
            background-color: #fff;
        }

        .overlayphoto img {
            /*border-radius: 50%;*/
        }

        img{
            display: block;
            max-width: 100%;
        }
        p {
            padding-left: 5px;
        }
        strong {
            font-weight: 700;
        }
        .logo {
            display: block;
            width: 300px;
        }
        @media screen and (max-width: 400px) {
            body {
                font-size: 28px;
            }
            .container {
                padding: 1rem;
            }
            .logo {
                max-width: 70%;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <figure>
            <img src="<?=$qrc->get(500,'L',0,'[(site_url)][~[*id*]~]details')?>" alt="QR code" class="qr" />
        </figure>
        <p>
            <strong>[[if?is=`[*team_first_name*]:notempty`&then=`[*team_first_name*] [*team_last_name*]`&else=`[*pagetitle*]`]]</strong><br>
            [[if?is=`[*description*]:notempty`&then=`[*description*]`]]
        </p>
        <p>
            <br>
            <img class="logo" src="assets/images/sw-logo-vcard.svg" alt="">
        </p>
        <p><a id="share" href="[~[*id*]~]details?action=share&id=[*id*]" class="align-right" style="line-height:32px;float:right;">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 57.5 73.9" style="enable-background:new 0 0 57.5 73.9;width:30px;height:30px;object-fit:contain;object-position:center;" xml:space="preserve">
                <style type="text/css">
                    .st0{fill:none;stroke:#000000;stroke-width:3;stroke-linecap:round;stroke-miterlimit:10;}
                    .st1{fill:none;stroke:#000000;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
                </style>
                <path class="st0" d="M37.3,21.4h16.9c1,0,1.8,0.8,1.8,1.7v47.6c0,0.9-0.8,1.7-1.8,1.7H3.3c-1,0-1.8-0.8-1.8-1.7V23.1c0-0.9,0.8-1.7,1.8-1.7h17"/>
                <line class="st0" x1="28.7" y1="3.5" x2="28.7" y2="49.6"/>
                <polyline class="st1" points="15.9,14.8 28.7,1.5 41.6,14.8 "/>
            </svg>
        </a></p>
    </div>
</body>
</html>