<?php require_once 'assets/snippets/brief_functions.php'; ?>
<!doctype html>
<html class="no-js no-anim" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Stepworks Briefing Engine</title>
        <base href="[(site_url)]">
        <meta name="robots" content="noindex" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/main.css">
    </head>
    <body class="page-viewbrief">
        <div class="container">
        <?php
        if (!empty($_GET['id'])) {
            // require_once 'functions.php';
            $id = $_GET['id'];
            $brief = getBrief($id);

            $json = urldecode($brief['json']);
            // echo "<pre>";
            // print_r($brief);
            // $json = json_decode($brief['json']);
            // echo $json;
            $json = json_decode($json, true);
            // print_r($json);
            // echo "</pre>";
        }
        ?>
        <h1>Creative Brief</h1>
        <h3>Project: <?=$brief['project_name']?></h3>
        <br>
        <p><strong>Client:</strong> <?=$brief['client_name']?><br>
        <strong>Email:</strong> <?=$brief['ident_email']?><br>
        <strong>Ref:</strong> <?=$brief['ref']?><br>
        <strong>Date:</strong> <?=$brief['date']?><br>
        <strong>Last modified:</strong> <?=$brief['modified']?><br>
        </p>

        <p><br></p>

        <p><strong>Project objectives</strong></p>
        <p><?=nl2br($json['objectives'])?></p>
        <p><br></p>

        <p><strong>Project deliverables</strong></p>
        <ol>
        <?php
        $schList = explode('||', $json['schItem[]']);
        $schTime = explode('||', $json['schTime[]']);
        for ($i = 0; $i < count($schList); $i++) {
            echo "<li>" . $schList[$i] . ", " . $schTime[$i] . "</li>";
        }
        ?>
        </ol>
        <p><br></p>

        <!-- <p><strong>Ballpark budget</strong></p>
        <p>
            Total campaign budget:<br>
            <?=strtoupper($json['currency'])?> <?=$json['budget-min']?> to <?=$json['budget-max']?><br>
            <br>
            Production budget:<br>
            <?=$json['prodbud-min']?> to <?=$json['prodbud-max']?> of the above<br>
            Approximately <?=$json['budget-est']?>
        </p>
        <p><br></p> -->

        <p><strong>Target audience</strong></p>
        <?php if ($json['audience-personas'] == 'no') { ?>
        <p>
            I'm trying to reach:<br>
            <?php
                $arr = explode('||', $json['audience-reach[]']);
                foreach ($arr as $item) {
                    echo "- " . $item . "<br>";
                }
            ?>
        </p>
        <p>
            Additional information:<br>
            Male / female ratio: <?=$json['gender-ratio']?><br>
            Age range: <?=$json['agemin']?> - <?=$json['agemax']?><br>
            <?=nl2br($json['more-audience-detail'])?>
        </p>
        <?php } else {
            echo "I have created audience personas (see notes)";
        } ?>
        <p><br></p>


        <p><strong>Promise (USP — Unique Selling Proposition)</strong></p>
        <p><?=nl2br($json['promise'])?></p>
        <p><br></p>

        <p><strong>Our audience should believe us because:</strong></p>
        <?php
        if (!empty($json['belief-reason[]'])) {
            $list = explode('||', $json['belief-reason[]']);
            echo "<ul>";
            foreach ($list as $item) {
                echo "<li>" . $item . "</li>";
            }
            echo "</ul>";
        }
        ?>
        <p><br></p>

        <p><strong>Project background</strong></p>
        <p>
            <?=nl2br($json['background'])?>
        </p>
        <p><br></p>

        <p><strong>Delivery channels</strong></p>
        <p>
            <?=nl2br($json['channels'])?>
        </p>
        <p><br></p>

        <!-- <p><strong>Our attitude towards our audience should be:</strong></p>
        <p>My brand, as a person, is:
        <?php
        // if (!empty($json['attitude[]'])) {
        //     $att = $json['attitude[]'];

        //     if (strpos($att, "||Others") !== FALSE) {
        //         $others = true;
        //         $att = str_replace('||Others', '', $att);
        //     }
        //     $att = strtolower(str_replace('||', ', ', $att));
        //     $att .= $others ? ', ' . $json['custom_attitude'] : '';

        //     echo $att;
        // }
        ?>
        </p>
        <p><br></p> -->

        <p><strong>Our personality towards our audience should be:</strong></p>
        <p>
            <?=nl2br($json['personality'])?>
        </p>
        <p><br></p>

        <p><strong>I want people to react this way:</strong></p>
        <p>
        <?php
        if (!empty($json['response[]'])) {
            $res = $json['response[]'];
            $list = explode('||', $res);
            foreach ($list as $item) {
                echo $item . "<br>";
            }
            if (!empty($json['custom_response'])) {
                echo $json['custom_response'];
            }
        }
        ?>
        </p>
        <p><br></p>

        <p><strong>Our success will be measured by:</strong></p>
        <p>
        <?php
        if (!empty($json['metric[]'])) {
            if (strpos($json['metric[]'], "||Response rates (please explain below)") != -1) {
                $others = true;
                $str = str_replace('||Response rates (please explain below)', '', $json['metric[]']);
            }
            echo "<p>" . str_replace('||', '<br>', $str) . ($others ? " - " . $json['custom_metric']: '') . "</p>";
        }
        ?>
        </p>
        <p><br></p>

        <p><strong>The following are compulsory:</strong></p>
        <p>
        <?php
        if (!empty($json['mandatory[]'])) {
            $list = explode('||', $json['mandatory[]']);
            foreach ($list as $item) {
                if ($item != 'More (add below)') echo $item . "<br>";
            }
            if (!empty($json['custom_mandatory'])) {
                echo nl2br($json['custom_mandatory']);
            }
        }
        ?>
        </p>
        <p><br></p>

        <p><strong>Additional notes</strong></p>
        <p><?=nl2br($json['notes'])?></p>

        </div>
        <p><br><br><br></p>
        <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-4948962-2', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    </body>
</html>