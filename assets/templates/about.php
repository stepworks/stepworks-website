[[chunk?&file=`header.php`]]
<body class="page-about">
    [[chunk?file=`page_header.php`]]

    <section id="partners" class="intro partners">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="super">The wholehearted international branding partnership</h1>
                    <h2>Stepworks is based in Hong Kong and works with organisations around the world. We offer integrated branding and marketing communications services.</h2>
                    <p>Our agency was started by Managing Partner Stephen Barry, as a one-man design shop in 1994. That business has flourished into a cross-discipline creative agency working across many industry sectors.</p>
                    <p>We have a track record of effectively creating value with ambitious people and their businesses. Our membership of the <a href="http://magnetglobal.org/">Magnet marketing and advertising global network</a> extends our reach, and the expertise we can offer.</p>
                </div>
            </div>

            <div class="grid grid-6 leadership animate">
                <div class="cell cell-left3">
                    <img src="assets/images/team/leadership-team.jpg" alt="Stepworks leadership team">
                </div>
                <div class="cell right">
                    <h3>Leadership</h3>
                    <p>From left to right</p>
                    <div class="leadership-list">
                        [[DocLister?&parents=`9`&tpl=`leadership-item`&orderBy=`menuindex ASC`&addWhereList=`template=13`]]
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- <section id="clients" class="client-list animate">
        <div class="container">
            <div class="client-intro">
                <h1>Our approach brings value to outstanding people and brands</h1>
            </div>
            <div class="logo-list">
                [[listLogos?&paths=`[[DocInfo?&docid=`154`&field=`client_logos`]]`&show=`1`]]
            </div>
        </div>
    </section> -->

    <section id="giving" class="social-initiatives">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1>Contributing to positive change</h1>
                    <h2>We strive to make a positive difference through our brand building skills.</h2>
                </div>
            </div>
            <div class="grid grid-3">
                <div class="cell">
                    <figure>
                        <img src="assets/images/socinit-hope-school.jpg" alt="Hope School">
                        <figcaption>
                            <p><strong>Hope School</strong><br>
                            Offering education and care to 350 children in Siem Reap, Cambodia</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="cell">
                    <figure>
                        <img src="assets/images/socinit-refugee-union.jpg" alt="Refugee Union">
                        <figcaption>
                            <p><strong>Refugee Union</strong><br>
                            Working to safeguard human rights in Hong Kong</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="cell">
                    <figure>
                        <img src="assets/images/socinit-hkfws.jpg" alt="Hong Kong Family Welfare Society">
                        <figcaption>
                            <p><strong>Hong Kong Family Welfare Society</strong><br>
                            Foster Care Service for children temporarily in need</p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </section>

    [[chunk?&file=`footer.php`]]
</body>
</html>