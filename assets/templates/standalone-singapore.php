[[chunk?&file=`header.php`]]
<body class="page-standalone">
    [[chunk?file=`page_header.php`]]
    <section class="intro mb0">
        <div class="container a0">
            <div class="grid grid-3">
              <div class="cell cell-left2">
                <h1 class="super">[*standalone_heading*]</h1>
                <h2 class="a1">[*standalone_sub_heading*]</h2>
                [*standalone_content*]
              </div>
              <div class="cell"></div>
            </div>
            <br>
            <br>
            <div class="grid grid-3">
              <div class="cell"></div>
              <div class="cell cell-right2 graphic">
                <img src="[*standalone_image*]" alt="Singapore" />
              </div>
            </div>
        </div>
    </section>

    <section class="content pb0">
        <div class="container">
          <div class="grid grid-3">
            <div class="cell">
              <h1>[*standalone_heading_business_hub*]</h1>
            </div>
            <div class="cell">[*standalone_content_business_hub*]</div>
          </div>

          <div class="client-list">
            <div class="logo-list">
              [[multiTv?&tvName=`standalone_logos_list`&display=`all`]]
              <!-- <img src="assets/images/client-logos/standalone/sg/Auriga-logo.svg" alt="DCH Auriga" />
              <img src="assets/images/client-logos/standalone/sg/cirkel-logo.svg" alt="Cirkel" />
              <img src="assets/images/client-logos/standalone/sg/Flow-logo.svg" alt="Flow Digital" />
              <img src="assets/images/client-logos/standalone/sg/GGC-logo.svg" alt="Global Green Connect" />
              <img src="assets/images/client-logos/standalone/sg/GSAM-logo.svg" alt="Goldman Sachs Asset Management" />
              <img src="assets/images/client-logos/standalone/sg/HHPG-logo.svg" alt="Pan Pacific Hotels Group" />
              <img src="assets/images/client-logos/standalone/sg/rackspace-logo.svg" alt="rackspace" />
              <img src="assets/images/client-logos/standalone/sg/SL-hotel-logo.svg" alt="Shangri-La Hotels and Resorts" />
              <img src="assets/images/client-logos/standalone/sg/singapore-pools-logo.svg" alt="Singapore Pools" />
              <img src="assets/images/client-logos/standalone/sg/Traders-logo.svg" alt="Traders Hotel" /> -->
            </div>
          </div>

          <div class="grid grid-3">
            <div class="cell"></div>
            <div class="cell">[*standalone_learn_more_content*]</div>
            <div class="cell"></div>
          </div>
        </div>
    </section>

    <section class="contact">
        <a name="contact-us"></a>
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1>[*standalone_lets_talk_heading*]</h1>
                </div>
            </div>
            <div class="grid grid-3">
                <div class="cell">
                  <h2>[*standalone_lets_talk_subheading*]</h2>
                </div>
                <div class="cell">[*standalone_lets_talk_content*]</div>
            </div>
        </div>
    </section>

    <section class="full-image mb0">
      <div id="map-sg" style="height:500px;max-height:50vh;"></div>
    </section>
    
    [[chunk?&file=`footer.php`]]
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeLAqr1b4yClL2h9mAk1h2tSClMhmr8Fo&callback=initMap"
    type="text/javascript"></script>
    
    <script async>
    $(document).ready(function() {
        $('.intro').addClass('show');
        $('a[href^="#"]').filter(function(){
          return $(this).attr('href').match(/\#\w+/)
        }).on('click', function(e){
          e.preventDefault();
          const anchor = $(this).attr('href').slice(1);
          $('html, body').animate({ scrollTop: $(`a[name="${anchor}"]`).offset().top }, 500);
        });
    });

    const onScrollStop = function onScrollStop(callback) {
        var isScrolling = undefined;
        window.addEventListener('scroll', function(e) {
            clearTimeout(isScrolling);
            isScrolling = setTimeout(function() {
                callback();
            }, 200);
        }, false);
    };
    </script>

    <script>
      var mapSG;
      function initMap() {
          var latlngSG = new google.maps.LatLng(1.278374, 103.847836);
          var styles = [
              {
                  "stylers": [
                      {"gamma": 0.5}, //Gama value.
                      {"hue": "#ff0022"}, //Hue value.
                      {"saturation": -100} //Saturation value.
                  ]
              }
          ];

          var mapOptions = {
              scrollwheel: false,
              zoom: 19,
              styles: styles,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              mapTypeControl: false,
              streetViewControl: false,
              scaleControl: true,
              zoomControl: true,
              zoomControlOptions: {
                  style: google.maps.ZoomControlStyle.SMALL
              }
          };

          mapSG = new google.maps.Map(document.getElementById("map-sg"), Object.assign({
            center: latlngSG,
          }, mapOptions));

          var markerSG = new google.maps.Marker({
              position: latlngSG,
              icon: '[(site_url)]assets/images/sw-map-marker.png',
              map: mapSG,
          });

          // google.maps.event.addDomListener(window, 'load', initialize);
          google.maps.event.addDomListener(window, "resize", function () {
              var centerSG = mapSG.getCenter();
              google.maps.event.trigger(mapSG, "resize");
              mapSG.setCenter(centerSG);
          });
      }
    </script>
</body>

</html>