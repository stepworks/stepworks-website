[[chunk?&file=`header.php`]]
<body class="page-home new2">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container a0">
            <h1 class="super">[*home_intro_heading*]</h1>
            <h2 class="a1">[*home_intro_description*]</h2>
        </div>
        <div class="home-filters">
            <div class="container">
                <div class="list-container">
                    <ul>
                        [[listFilterHome?&list=`[*sector_list*]`&type=`sec`]]
                    </ul>
                </div>
            </div>
        </div>
    </section>

    [[DocLister?&parents=`[[if?&is=`[[currLang]]:is:en`&then=`2`&else=`699`]]`&tpl=`home-work-item-2-section`&orderBy=`RAND()`&tvList=`work_cover_image,work_cover_image_md,work_cover_image_sm,work_cover_video`&addWhereList=`hidemenu=0`&filters=`tv:featured:is:yes` &display=`5`]]

    <section class="results animate">
        <div class="container">
            <h1>[*home_value_heading*]</h1>
            <div class="grid grid-3">
                <div class="cell">
                    <h2>[*home_value_content*]</h2>
                </div>
                <div class="cell cell-right2 graphic">
                    <img class="d lazy" data-src="assets/images/approach_2021.svg" alt="" width="1920" height="1080">
                    <img class="m lazy" data-src="assets/images/method-clarify-animated.svg" alt="" width="413" height="270">
                    <img class="m lazy" data-src="assets/images/method-simplify-animated.svg" alt="" width="413" height="200">
                    <img class="m lazy" data-src="assets/images/method-amplify-animated.svg" alt="" width="413" height="250">
                </div>
            </div>
        </div>
    </section>

    <section class="news animate">
        <div class="container">
            <h1>[*home_news_title*]</h1>
            [*home_news_text*]
            <div class="grid grid-3">
                [[DocLister?&parents=`[[if?&is=`[[currLang]]:is:en`&then=`8`&else=`878`]]`&display=`3`&tpl=`home-news-item`&orderBy=`STR_TO_DATE(longtitle, '%e %M %Y') DESC`&addWhereList=`hidemenu=0`]]
            </div>
            <a class="more" href="/[[currLang]]/news">See all news</a>
        </div>
    </section>
    [[chunk?&file=`footer.php`]]

    <script async>
    $(document).ready(function() {
        $('.intro').addClass('show');
        $(window).scroll(function() {
            let workItemOne = $('.work-item')[0].offsetTop,
                workItemLast = $('.work-item')[$('.work-item').length - 1].offsetTop + $('.work-item').height(),
                currentScrollTop = $('html, body').scrollTop();
            if (currentScrollTop >= workItemOne && currentScrollTop < workItemLast) {
                $('header').addClass('transparent');
            } else {
                $('header').removeClass('transparent');
            }
        });
    });

    const onScrollStop = function onScrollStop(callback) {
        var isScrolling = undefined;
        window.addEventListener('scroll', function(e) {
            clearTimeout(isScrolling);
            isScrolling = setTimeout(function() {
                callback();
            }, 200);
        }, false);
    };

    if ($(".work-item").length) {
        let count = 0;
        $(".work-item").each(function() {
            if (count === 0) {
                $(this).addClass("animating");
                $(this).addClass("animated");
            }
            count++;
        })
    }
    </script>
</body>

</html>