[[chunk?&file=`header.php`]]
<body class="page-not-found">
    [[chunk?file=`page_header.php`]]

    <section class="notfound">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    [*content*]
                </div>
            </div>
            <div id="heart"></div>
        </div>
    </section>

    <script src="assets/js/vendor/phaser.2.6.2.min.js"></script>
    <script>
    var PhaserGlobal = { hideBanner: true };
    var h = document.getElementById('heart'),
        d = h.clientWidth,
        d2 = d, // piece width
        starttime = Math.round((new Date()).getTime() / 10),
        endtime,
        record = false,
        moves = 0;

    if (d <= 480 && d > 360) {
        d2 = 360;
    } else if (d <= 360) {
        d2 = 300;
    }

    var game = new Phaser.Game(d2, d2, Phaser.CANVAS, 'heart', { preload: preload, create: create });

    var PIECE_WIDTH = d2 / 3,
        PIECE_HEIGHT = d2 / 3,
        BOARD_COLS,
        BOARD_ROWS;

    var piecesGroup,
        piecesAmount,
        shuffledIndexArray = [];
        // theBlackPiece;

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function preload() {
        // var w = window.innerWidth;
        if (d <= 480 && d > 360) {
            game.load.spritesheet("background", "assets/images/heart360.jpg", PIECE_WIDTH, PIECE_HEIGHT);
        } else if (d <= 360) {
            game.load.spritesheet("background", "assets/images/heart300.jpg", PIECE_WIDTH, PIECE_HEIGHT);
        } else {
            game.load.spritesheet("background", "assets/images/heart.jpg", PIECE_WIDTH, PIECE_HEIGHT);
        }
    }

    function create() {
        prepareBoard();

        setTimeout(shufflePieces(), 500);
    }

    function shufflePieces() {
        var count = Math.floor(Math.random() * 15) + 10,
            lastPiece = null;

        var interval = setInterval(function() {
            var thisPiece = piecesGroup.children[getRandomInt(0,8)];

            while (!canMove(thisPiece) || thisPiece == lastPiece) {
                thisPiece = piecesGroup.children[getRandomInt(0,8)];
            }

            lastPiece = thisPiece;

            selectPiece(thisPiece);
            count--;
            if (count <= 0) {
                clearInterval(interval);
                record = true;
            }
        }, 200);
    }

    function prepareBoard() {

        var piecesIndex = 0,
            i, j,
            piece;

        BOARD_COLS = Math.floor(game.world.width / PIECE_WIDTH);
        BOARD_ROWS = Math.floor(game.world.height / PIECE_HEIGHT);

        piecesAmount = BOARD_COLS * BOARD_ROWS;

        shuffledIndexArray = createShuffledIndexArray();

        piecesGroup = game.add.group();

        for (i = 0; i < BOARD_ROWS; i++)
        {
            for (j = 0; j < BOARD_COLS; j++)
            {
                if (shuffledIndexArray[piecesIndex]) {
                    piece = piecesGroup.create(j * PIECE_WIDTH, i * PIECE_HEIGHT, "background", shuffledIndexArray[piecesIndex]);
                }
                else { //initial position of black piece
                    piece = piecesGroup.create(j * PIECE_WIDTH, i * PIECE_HEIGHT, "background", shuffledIndexArray[piecesIndex]);;
                    piece.black = true;
                    piece.alpha = 0;
                    // theBlackPiece = piece;
                }
                piece.name = 'piece' + i.toString() + 'x' + j.toString();
                piece.currentIndex = piecesIndex;
                piece.destIndex = shuffledIndexArray[piecesIndex];
                piece.inputEnabled = true;
                piece.events.onInputDown.add(selectPiece, this);
                piece.posX = j;
                piece.posY = i;
                piecesIndex++;
            }
        }
    }

    function selectPiece(piece) {

        var blackPiece = canMove(piece);

        //if there is a black piece in neighborhood
        if (blackPiece) {
            movePiece(piece, blackPiece);
            return true
        }

        return false;

    }

    function canMove(piece) {

        if (!piece.inputEnabled) return false;

        var foundBlackElem = false;

        piecesGroup.children.forEach(function(element) {
            if (element.posX === (piece.posX - 1) && element.posY === piece.posY && element.black ||
                element.posX === (piece.posX + 1) && element.posY === piece.posY && element.black ||
                element.posY === (piece.posY - 1) && element.posX === piece.posX && element.black ||
                element.posY === (piece.posY + 1) && element.posX === piece.posX && element.black) {
                foundBlackElem = element;
                return;
            }
        });

        return foundBlackElem;
    }

    function movePiece(piece, blackPiece) {

        var tmpPiece = {
            posX: piece.posX,
            posY: piece.posY,
            currentIndex: piece.currentIndex
        };

        game.add.tween(piece).to({x: blackPiece.posX * PIECE_WIDTH, y: blackPiece.posY * PIECE_HEIGHT}, 300, Phaser.Easing.Linear.None, true);

        //change places of piece and blackPiece
        piece.posX = blackPiece.posX;
        piece.posY = blackPiece.posY;
        piece.currentIndex = blackPiece.currentIndex;
        piece.name ='piece' + piece.posX.toString() + 'x' + piece.posY.toString();

        //piece is the new black
        blackPiece.posX = tmpPiece.posX;
        blackPiece.posY = tmpPiece.posY;
        blackPiece.currentIndex = tmpPiece.currentIndex;
        blackPiece.name ='piece' + blackPiece.posX.toString() + 'x' + blackPiece.posY.toString();

        if (record) moves++;

        //after every move check if puzzle is completed
        checkIfFinished();
    }

    function checkIfFinished() {

        var isFinished = true;

        piecesGroup.children.forEach(function(element) {
            if (element.currentIndex !== element.destIndex) {
                isFinished = false;
                return;
            }
        });

        if (isFinished) {
        piecesGroup.children.forEach(function(element) {
            element.inputEnabled = false;

            if (element.black) {
            setTimeout(function() {
                game.add.tween(element).to({alpha: 1}, 300, Phaser.Easing.Linear.None, true);
                showFinishedText();
            }, 300);
            }
        });
        }
    }

    function showFinishedText() {

        var size = [32, 48, -75, 20, 100, 115, 130];
        if (d <= 480 && d > 360) {
            size = [24, 32, -60, 10, 70, 85, 100];
        } else if (d <= 360) {
            size = [20, 28, -50, 0, 45, 60, 75];
        }

        endtime = Math.round((new Date()).getTime() / 10);
        var duration = (endtime - starttime) / 100;

        var style = { font: size[0] + "px 'Helvetica Neue'", fill: "#fff", align: "center"};
        var style2 = { font: size[1] + "px 'Helvetica Neue'", fontWeight: "bold", fill: "#fff", align: "center"};

        var text = game.add.text(game.world.centerX, game.world.centerY + size[2], "Well done!", style2);
        var text2 = game.add.text(game.world.centerX, game.world.centerY + size[3], "We love your\nwholehearted\neffort.", style);
        text2.lineSpacing = -5;

        text.anchor.set(0.5);
        text2.anchor.set(0.5);

        setTimeout(function() {
        var text3 = game.add.text(game.world.centerX, game.world.centerY + size[4], "Moves: " + moves, {font:"11px 'Helvetica Neue'",align:"center",fill:"#fff"});
        text3.anchor.set(0.5);
        }, 1500);

        setTimeout(function() {
        var text4 = game.add.text(game.world.centerX, game.world.centerY + size[5], "Time: " + duration + "s", {font:"11px 'Helvetica Neue'",align:"center",fill:"#fff"});
        text4.anchor.set(0.5);
        }, 2000);

        setTimeout(function() {
        var text5 = game.add.text(game.world.centerX, game.world.centerY + size[6], "Now get back to work!", { font:"11px 'Helvetica Neue'", align:"center", fill:"#fff"});
        text5.anchor.set(0.5);
        }, 2500);

        ga('send', 'event', '404', 'complete puzzle', 'stats' , moves + ' moves, ' + duration + 's');

    }

    function createShuffledIndexArray() {

        var indexArray = [];

        for (var i = 0; i < piecesAmount; i++)
        {
            indexArray.push(i);
        }

        return indexArray;
        // return shuffle(indexArray);

    }

    function shuffle(array) {

        var counter = array.length,
            temp,
            index;

        while (counter > 0)
        {
            index = Math.floor(Math.random() * counter);

            counter--;

            temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }

        return array;

    }

    </script>

    [[chunk?&file=`footer.php`]]
</body>
</html>