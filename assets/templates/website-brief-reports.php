<?php require_once 'assets/snippets/website_brief_functions.php'; ?>
[[chunk?&file=`header_brief.php`]]
<body class="page-website-brief-report">
    [[chunk?file=`page_header_mini.php`]]
    <div id="main-container">
    <?php 
        if (!empty($_GET['id'])) {
            $id = $_GET['id'];
            $brief = getBrief($id);
            $json = urldecode($brief['json']);
            $json = json_decode($json, true);
            // echo print_r($json, true);
            $isIframe = $_GET['iframe'] == 'yes';
        }
    ?>
        <div id="website-brief" class="<?= $isIframe ? 'iframe-mode' : '' ?>">
            <div class="questions-wrapper">
                <div class="results">
                    <div class="report-header">
                        <div class="report-heading">
                            <p class="print-hidden"><img src="assets/images/sw_logo.svg" alt="Stepworks"></p>
                            <h3>Website development project brief</h3>
                            <table class="report-basic-info">
                                <tbody>
                                    <tr>
                                        <th>Project name: </th><td><?= $brief['project_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Project owner: </th><td><?= $brief['client_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Contact person: </th><td><?= isNonNull($brief['client_contact'], 'No contact person') ?></td>
                                    </tr>
                                    <tr>
                                        <th>Reference #: </th><td><?= isNonNull($brief['ref']) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Date: </th><td><?= date("d F Y", strtotime($brief['date'])) ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <section class="results__section">
                        <h4>Project overview</h4>

                        <div class="results__row">
                            <p>
                                <?php if ($json['website'] === 'existing_website') : ?>
                                    We are rebuilding the website 
                                    <?php if ($json['existing-website-url']) : ?>
                                        located here <a href="<?= $json['existing-website-url']?>" target="_blank"><?= $json['existing-website-url']?></a>
                                    <?php endif ?>
                                <?php else : ?>
                                    We are building a new website
                                <?php endif; ?>
                            </p>   
                            <?php if ($json['tentative-scope'] == 'yes' && isset($json['tentative-scope-file'])) : ?>
                                <ul><li><a href="<?= $json['tentative-scope-file'] ?>">Download this file</a> for more details about the project scope</li></ul>
                            <?php endif; ?>
                        </div>

                        <?php if (strlen(trim($json['reference-url[]'], " ,|")) > 0 && $json['reference-websites'] === 'yes') : ?>
                            <div class="results__row">    
                                <p>Potential benchmark websites</p>
                                <?php                     
                                    $refURLList = explodeData($json['reference-url[]']);
                                    $refReasonList = explodeData($json['reference-reason[]']);
                                    if (count($refURLList) > 0) {
                                        echo '<ol>';
                                        foreach ($refURLList as $id => $refURL) {
                                            if (strlen(trim($refURL)) > 0) {
                                                echo "<li><a href='http://$refURL'>$refURL</a>";
                                                if (strlen(trim($refReasonList[$id])) > 0) echo '<br>' . $refReasonList[$id];
                                                echo '</li>';                                        
                                            }
                                        }
                                        echo '</ol>';
                                    }
                                ?>
                            </div>
                        <?php endif; ?>

                        <?php if (strlen(trim($json['objective[]'])) > 0 || !empty($json['objective-others'])) : ?>
                            <div class="results__row">
                                <p>Key objectives</p>
                                <ul>
                                <?php 
                                    if (strlen(trim($json['objective[]'])) > 0) {
                                        $objectiveList = explodeData($json['objective[]']);
                                        foreach ($objectiveList as $objective) { 
                                            echo '<li>' . $objective . '</li>';
                                        }
                                    }                                    
                                    if ($json['objective-others']) {
                                        echo '<li><i>Other key objectives: </i>'.$json['objective-others'].'</li>';
                                    }
                                ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <div class="results__row">
                            <p>Proposed website domain</p>
                            <ul>
                                <li>
                                    <?php if ($json['secure-domain'] === 'yes') : ?>
                                        Domain secured <?php $json['secure-domain-ul'] ? 'at ' . $json['secure-domain-ul'] : '' ?>
                                    <?php else : ?>
                                        None confirmed
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </div>

                        <?php if (strlen(trim($json['audience[]'])) > 0 || !empty($json['audience-others'])) : ?>
                            <div class="results__row">
                                <p>People we need to influence</p>
                                <ul>
                                <?php 
                                    if (strlen(trim($json['audience[]'])) > 0) {
                                        $audienceList = explodeData($json['audience[]']);
                                        foreach ($audienceList as $audience) {
                                            echo '<li>' . $audience . '</li>';
                                        }
                                    }                                          
                                    if ($json['audience-others']) {
                                        echo '<li><i>Other possible audiences: </i>'.$json['audience-others'].'</li>';
                                    }
                                ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <?php if (strlen(trim($json['behaviour[]'])) > 0 || !empty($json['behaviour-others'])) : ?>
                            <div class="results__row">
                                <p>We want our visitors to</p>
                                <ul>
                                <?php 
                                    if (strlen(trim($json['behaviour[]'])) > 0) {
                                        $behaviourList = explodeData($json['behaviour[]']);
                                        foreach ($behaviourList as $behaviour) {
                                            echo '<li>' . $behaviour . '</li>';
                                        }
                                    }                                    
                                    if ($json['behaviour-others']) {
                                        echo '<li><i>Other visitor behaviour: </i>'.$json['behaviour-others'].'</li>';
                                    }
                                ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <?php if (strlen(trim($json['audience-locations[]'])) > 0 || !empty($json['audience-locations-others'])) : ?>
                            <div class="results__row">
                                <p>Audience locations</p>
                                <ul>
                                <?php
                                    if (strlen(trim($json['audience-locations[]'])) > 0) {
                                        $locationList = explodeData($json['audience-locations[]']);
                                        foreach ($locationList as $location) {
                                            echo '<li>' . $location . '</li>';
                                        }
                                    }                                    
                                    if ($json['audience-locations-others']) {
                                        echo '<li><i>Other audience located at: </i>'.$json['audience-locations-others'].'</li>';
                                    }                                
                                ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <?php if ($json['audience-personas'] === 'yes' || $json['visitor-data'] === 'yes') : ?>
                            <div class="results__row">
                                <p>More information about our visitors</p>
                                <ul>
                                <?php 
                                    if ($json['audience-personas'] === 'yes') {
                                        echo '<li><a href="'.$json['audience-personas-file'].'" target="_blank">Download audience personas here</a></li>';
                                    }
                                    if ($json['visitor-data'] === 'yes') {
                                        echo '<li><a href="'.$json['visitor-data-file'].'" target="_blank">Download visitor data here</a></li>';
                                    }
                                ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <?php if (strlen(trim($json['language[]'])) > 0 || !empty($json['language-others'])) : ?>
                        <div class="results__row">
                            <p>Our website will launch with the following languages</p>
                            <ul>
                            <?php 
                                if (strlen(trim($json['language[]'])) > 0) {
                                    $languageList = explodeData($json['language[]']);
                                    foreach ($languageList as $language) {
                                        echo '<li>' . $language . '</li>';
                                    }
                                }                                
                                if ($json['language-others']) {
                                    echo '<li><i>Other languages: </i>'.$json['language-others'].'</li>';
                                }
                            ?>
                            </ul>
                        </div>
                        <?php endif; ?>

                        <div class="results__row">
                            <p>We will<?= $json['migrate-existing-website'] === 'yes' ? '' : ' not'?> migrate data from an existing website</p>
                        </div>
                        
                        <?php if (strlen(trim($json['ccrqrmt[]'])) > 0) : ?>
                            <div class="results__row">
                                <p>Content we need to create includes</p>
                                <ul>
                                <?php 
                                    $ccrqrmtList = explodeData($json['ccrqrmt[]']);
                                    foreach ($ccrqrmtList as $ccrqrmt) {
                                        echo '<li>' . $ccrqrmt . '</li>';
                                    }
                                ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <!-- TODO: need to redo website functions -->

                        <h4>Website functions, services and applications</h4>
                        
                        <div class="results__row">
                            <p>The website will<?= $json['mobile-first'] === 'yes' ? '' : ' not' ?> take a mobile-first approach</p>
                        </div>
                        

                        <?php if (strlen(trim($json['basic-functions[]'])) > 0) : ?>
                            <div class="results__row">
                                <p>Basic design functions</p>
                                <ul>
                                  <?php 
                                    $basicFunctionList = explodeData($json['basic-functions[]']);
                                    foreach ($basicFunctionList as $basicFunction) { 
                                        echo "<li>$basicFunction</li>";
                                    }                               
                                  ?>
                                </ul>
                            </div>
                        <?php endif; ?>


                        <?php if ($json['cms'] == 'yes' && (strlen(trim($json['cms-functions[]'])) > 0 || !empty($json['cms-preferences']))) : ?>
                            <div class="results__row">
                                <p>Our preferred content management system (CMS) functions include</p>
                                <ul>
                                <?php 
                                    if (strlen(trim($json['cms-functions[]'])) > 0) {
                                        $cmsFunctionList = explodeData($json['cms-functions[]']);
                                        foreach ($cmsFunctionList as $cmsFunction) {
                                            echo '<li>' . $cmsFunction;
                                            if ($cmsFunction == 'Blog functionality') {
                                              echo '<ul>';
                                              $blogFunctionList = explodeData($json['blog-functions[]']);
                                              foreach ($blogFunctionList as $blogFunction) {
                                                echo "<li>$blogFunction</li>";
                                              }
                                              echo '</ul>';
                                            }
                                            if ($cmsFunction == 'Event management') {
                                              echo '<ul>';
                                              $eventList = explodeData($json['events[]']);
                                              foreach ($eventList as $event) {
                                                echo "<li>$event</li>";
                                              }
                                              echo '</ul>';
                                            }
                                            if ($cmsFunction == 'eCommerce management') {
                                              echo '<ul>';
                                              $eCommerceList = explodeData($json['eCommerces[]']);
                                              foreach ($eCommerceList as $eCommerce) {
                                                echo "<li>$eCommerce</li>";
                                              }
                                              echo '</ul>';
                                            }
                                            if ($cmsFunction == 'Newsletter management (newsletter and email marketing system)' && $json['newsletter-requirements']) {
                                              echo "<ul><li>".$json['newsletter-requirements']."</li></ul>";
                                            }
                                            if ($cmsFunction == 'Membership management') {
                                              echo '<ul>';
                                              $membershipList = explodeData($json['memberships[]']);
                                              foreach ($membershipList as $membership) {
                                                echo "<li>$membership</li>";
                                              }
                                              if ($json['membership-requirements']) {
                                                echo "<li>".$json['membership-requirements']."</li>";
                                              }                                              
                                              echo '</ul>';
                                            }
                                            echo '</li>';
                                        }
                                    }                                                               
                                ?>
                                </ul>
                                <?php
                                  if ($json['cms-preferences']) {
                                    echo '<ul><li><i>CMS platform preference: </i>'.$json['cms-preferences'].'</li></ul>';
                                  }     
                                ?>
                            </div>
                        <?php elseif ($json['cms'] == 'not sure') : ?>
                          <div class="results__row">
                            <p>We are not sure about what functions to be included in the content management system (CMS)</p>
                          </div>
                        <?php endif; ?>


                        
                        <div class="results__row">
                            <p>Please<?= $json['site-search'] === 'yes' ? '' : ' do not' ?> include site search in the website</p>
                        </div>

                        <?php if (strlen(trim($json['form-functions[]'])) > 0) : ?>
                            <div class="results__row">
                                <p>Forms</p>
                                <ul>
                                  <?php 
                                    $formFunctionList = explodeData($json['form-functions[]']);
                                    foreach ($formFunctionList as $formFunction) { 
                                        echo "<li>$formFunction";
                                        if ($formFunction == 'Forms integrated with third party system (eg. Mailchimp, Campaign Monitor, Salesforce)') {
                                          echo "<ul><li>".$json['integrated-form-system']."</li></ul>";
                                        }
                                        echo "</li>";
                                    }                               
                                  ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <?php if (strlen(trim($json['visitor-comms[]'])) > 0) : ?>
                            <div class="results__row">
                                <p>On-site visitor communication</p>
                                <ul>
                                  <?php 
                                    $visitorCommList = explodeData($json['visitor-comms[]']);
                                    foreach ($visitorCommList as $visitorComm) { 
                                        echo "<li>$visitorComm</li>";
                                    }
                                    if ($json['form-requirement-others']) {
                                      echo "<li>".$json['form-requirement-others']."</li>";
                                    }                           
                                  ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        
                        <?php if (strlen(trim($json['media[]'])) > 0) : ?>
                            <div class="results__row">
                                <p>Media</p>
                                <ul>
                                  <?php 
                                    $mediaList = explodeData($json['media[]']);
                                    foreach ($mediaList as $media) { 
                                        echo "<li>$media</li>";
                                    }
                                    if ($json['media-requirement-others']) {
                                      echo "<li>".$json['media-requirement-others']."</li>";
                                    }                           
                                  ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        
                        <?php if (strlen(trim($json['third-party-api[]'])) > 0) : ?>
                            <div class="results__row">
                                <p>Third-party API integration</p>
                                <ul>
                                  <?php 
                                    $thirdPartyApiList = explodeData($json['third-party-api[]']);
                                    foreach ($thirdPartyApiList as $thirdPartyApi) { 
                                        echo "<li>$thirdPartyApi</li>";
                                    }
                                    if ($json['third-party-api-others']) {
                                      echo "<li>".$json['third-party-api-others']."</li>";
                                    }                           
                                  ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        
                        <?php if (strlen(trim($json['security[]'])) > 0) : ?>
                            <div class="results__row">
                                <p>Security</p>
                                <ul>
                                  <?php 
                                    $securityList = explodeData($json['security[]']);
                                    foreach ($securityList as $security) { 
                                        echo "<li>$security</li>";
                                    }                         
                                  ?>
                                </ul>
                            </div>
                        <?php endif; ?>





                        

                        <?php if ($json['website-function-others']) : ?>
                          <div class="results__row">
                            <p>Other website functions, services and applications are <?= $json['website-function-others'] ?></p>
                          </div>
                        <?php endif; ?>
                        
                        
                        <div class="results__row">
                            <p>Ongoing website maintenance support will<?= $json['website-maintenance-support'] === 'yes' ? '' : ' not' ?> be required</p>
                            <?php if ($json['website-maintenance-support'] === 'yes') {
                              echo '<ul>';
                              $websiteMaintenanceSupportList = explodeData($json['website-maintenance-support[]']);
                              foreach ($websiteMaintenanceSupportList as $websiteMaintenanceSupport) {
                                echo "<li>$websiteMaintenanceSupport</li>";
                              }
                              echo '</ul>';
                            } ?>                            
                        </div>

                        <h4>Technical considerations</h4>
                        <div class="results__row">
                            <ul>
                            <?php 
                                if (strlen(trim($json['browser[]'])) > 0) { 
                                    $browserList = explodeData($json['browser[]']);
                                    if (strlen(trim($json['browser-others'])) > 0) {
                                        $browserList = array_merge($browserList, array($json['browser-others']));
                                    }
                                    echo "<li>Our organisation uses " . implode(', ', $browserList) . "</li>";
                                }
                                if ($json['website-hosting']) {
                                    echo '<li>We will'.($json['website-hosting'] === 'yes' ? '' : ' not' ).' need a website hosting package</li>';
                                }                                
                                if ($json['hosting-preference'] === 'yes') {
                                    echo '<li>Our hosting preferences are ';
                                    $hostingMethodList = explodeData($json['hosting-method[]']);
                                    echo implode(', ', $hostingMethodList);
                                    echo '</li>';                               
                                }
                                if ($json['coding-preference'] === 'yes') {
                                    echo '<li>Our coding language preferences are ';
                                    echo $json['coding'];
                                    if (strlen(trim($json['coding-others-text'])) > 0) {
                                        echo ', ' . $json['coding-others-text'];
                                    }
                                    echo '</li>';                               
                                }
                                if ($json['database-preference'] === 'yes') {
                                    echo '<li>Our database preferences are ';
                                    echo $json['database'];
                                    if (strlen(trim($json['database-others-text'])) > 0) {
                                        echo ', ' . $json['database-others-text'];
                                    }
                                    echo '</li>';                               
                                }
                            ?>
                            </ul>
                        </div>
                        
                        <h4>Challenges and opportunities</h4>
                        <div class="results__row">
                            <ul><li><?= isNonNull($json['risk-opportunities']) ?></li></ul>
                        </div>

                        <?php if (strlen($json['csr-carbon-offsetting']) > 0 || strlen($json['csr-wcag']) > 0) : ?>
                            <div class="results__row">
                                <p>We require these ESG initiatives</p>
                                <ul>
                                <?php
                                    echo strlen($json['csr-carbon-offsetting']) > 0 ? '<li>' . $json['csr-carbon-offsetting'] . '</li>' : '';
                                    echo strlen($json['csr-wcag']) > 0 ? '<li>' . $json['csr-wcag'] . '</li>' : '';
                                    echo strlen($json['csr-carbon-footprint']) > 0 ? '<li>' . $json['csr-carbon-footprint'] . '</li>' : '';
                                ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </section>
                </div>
                <div class="cta-bottom">
                    <h3>Would you be interested to get our input on how to create more value from your website?</h3>
                    <p class='content'>Stepworks has considerable experience creating competitive advantages through developing effective websites that align business, brand and, most important, audience needs.</p>
                    <p class='get-in-touch'><a class="btn" href="mailto:hello@stepworks.co">Get in touch</a></p>
                    <p class='print-only'>Get in touch by phone <a href='tel:+852 3678 8700' class='link'>+852 3678 8700</a> or by email <a href='mailto:hello@stepworks.co' class='link'>hello@stepworks.co</a>.</p>
                </div>
            </div>
            <!-- <div class='pageNumberOne-report'>Page 1 of 2</div>
            <div class='pageNumberTwo-report'>Page 2 of 2</div> -->
        </div>   
        
</div>
    <?php if (!$isIframe) : ?>
      [[chunk?&file=`footer.php`]]
    <?php endif; ?>

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="./assets/js/website_brief_report_scripts.js"></script>
    <!-- <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-4948962-2', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script> -->
</body>
</html>