[[chunk?&file=`header.php`]]
<body class="page-work-item">
    [[chunk?file=`page_header.php`]]

    <section class="hero">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end5">
                    <h1 class="super">[*longtitle*]</h1>
                    <div class="business-objectives"><strong>[[lang?&text=`blog_objectives`]]</strong><span>[!getBusinessObjectives?&biz=`[*biz_ctx*]`!]</span></div>
                </div>
            </div>
        </div>
        [[if?&is=`[*work_cover_video*]:notempty`&then=`
        <video poster="/[*work_cover_image*]" autoplay loop muted playsinline>
            <source src="/[*work_cover_video*]" type="video/mp4">
            <img src="/[*work_cover_image*]" alt="[*pagetitle*]">
        </video>
        `&else=`
		<img data-srcset="[!webpBackground? &src=`[[phpthumb?&input=`/[*work_cover_image*]`&options=`w=640`]]`!] 640w,
					 [!webpBackground? &src=`[[phpthumb?&input=`/[*work_cover_image*]`&options=`w=1280`]]`!] 1280w"
			 data-sizes="(max-width: 640px) 480px,
					(max-width: 1280px) 1000px,
					76vw"
			 src="/[*work_cover_image*]" alt="[*pagetitle*]">
        `]]
    </section>

    [[DocLister?&parents=`[*id*]`&tpl=`work-module`&orderBy=`menuindex ASC`&addWhereList=`template=16`]]

    [[if? &is=`[[langparent]]:is:283` &then=` ` &else=`
    <section class="related">
        <div class="container">
            <h3>Related work</h3>
            <div class="grid grid-3">
                [!getRelatedWork!]
            </div>
        </div>
    </section>
    `]]

    <div class="progress"><div class="bar"></div></div>
    [[chunk?&file=`footer.php`]]
</body>
</html>