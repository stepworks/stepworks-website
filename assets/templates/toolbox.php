[[chunk?&file=`header.php`]]

<body class="page-toolbox">
    [[chunk?file=`page_header.php`]]

    <!-- <section id="brief-engine" class="intro animate">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    [*toolbox_cbe_intro*]
                </div>
            </div>
        </div>
    </section> -->

    <section class="intro">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="super">[*pagetitle*]</h1>
                    [*content*]
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="grid grid-3 toollist">
                <!-- <div class="cell toolcard">
                    <div class="icon">
                        <svg width="211" height="84" viewBox="0 0 211 84" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0)">
                            <path d="M209.2 41.8C209.2 41.8 162.7 82.6 105.4 82.6C48.1 82.6 1.5 41.8 1.5 41.8C1.5 41.8 48 1 105.3 1C162.6 1 209.2 41.8 209.2 41.8Z" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10"/>
                            <path d="M105.3 81.4C127.17 81.4 144.9 63.6704 144.9 41.7999C144.9 19.9295 127.17 2.19995 105.3 2.19995C83.4295 2.19995 65.7 19.9295 65.7 41.7999C65.7 63.6704 83.4295 81.4 105.3 81.4Z" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10"/>
                            <path d="M105.3 25.5C109.2 19 117 18.1 122.2 20.4C128.3 23.1 131.8 29.5 130.7 36.2C129.4 44.1 124.7 49.9 118.9 54.9C115.1 58.2 110.8 61 106.7 63.9C106.1 64.3 105 64.5 104.4 64.1C96.5 59.3 89.2 53.9 84 46.2C80.8 41.4 78.8 36.1 80.2 30.2C81.6 24.5 85.3 20.9 91.1 19.7C97 18.4 101.8 20.2 105.3 25.5Z" fill="#EC1C24"/>
                            </g>
                            <defs>
                            <clipPath id="clip0">
                            <rect width="210.7" height="83.5" fill="white"/>
                            </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <h4><span>Wholehearted Brand Building insights</span></h4>
                    <p>Our brand building knowledge bank. There’s something here for brand owners, brand managers and anyone interested in creating competitive advantages through effective brand building.</p>
                    <a href="[~19~]"></a>
                </div> -->

                <div class="cell toolcard">
                    <div class="icon">
                      <svg id="Layer_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 246.84 200">
                        <defs>
                          <style>.cls-1{fill:#ec1c24;}.cls-2{fill:#1d1d1b;}.cls-3{stroke-width:2px;}.cls-3,.cls-4{fill:none;stroke:#1d1d1b;stroke-miterlimit:10;}</style>
                        </defs>
                        <g id="Layer_1-2">
                          <rect class="cls-3" x="47.21" y="51.42" width="152.42" height="94.29" rx="4.92" ry="4.92"/>
                          <rect class="cls-3" x="105.61" y="145.71" width="35.62" height="15.85"/>
                          <line class="cls-3" x1="155.59" y1="161.56" x2="91.25" y2="161.56"/>
                          <line class="cls-3" x1="199.63" y1="129.86" x2="47.21" y2="129.86"/>
                          <g>
                            <g>
                              <line class="cls-3" x1="122.8" y1="68.32" x2="157.33" y2="68.32"/>
                              <line class="cls-3" x1="122.8" y1="72.46" x2="157.33" y2="72.46"/>
                              <line class="cls-3" x1="122.8" y1="76.59" x2="157.33" y2="76.59"/>
                              <line class="cls-3" x1="122.8" y1="80.72" x2="157.33" y2="80.72"/>
                              <line class="cls-3" x1="122.8" y1="84.85" x2="157.33" y2="84.85"/>
                              <line class="cls-3" x1="122.8" y1="88.99" x2="157.33" y2="88.99"/>
                              <line class="cls-3" x1="122.8" y1="93.12" x2="157.33" y2="93.12"/>
                            </g>
                            <path class="cls-1" d="M99.43,70.31c2.31-3.83,6.87-4.35,9.93-3.01,3.6,1.58,5.63,5.34,4.98,9.28-.77,4.65-3.56,8.06-6.93,11-2.23,1.95-4.76,3.58-7.19,5.29-.33,.23-1.03,.33-1.34,.14-4.62-2.8-8.91-5.99-11.98-10.56-1.91-2.85-3.04-5.94-2.22-9.41,.8-3.37,2.99-5.5,6.39-6.21,3.46-.72,6.29,.35,8.38,3.46"/>
                            <polyline class="cls-3" points="80 124.41 80 38.44 168.46 38.44 168.46 124.41"/>
                            <circle class="cls-2" cx="85.83" cy="44.64" r="1.43"/>
                            <circle class="cls-2" cx="90.42" cy="44.64" r="1.43"/>
                            <circle class="cls-2" cx="95" cy="44.64" r="1.43"/>
                            <path class="cls-4" d="M101.68,43.15h59.64c.81,0,1.46,.65,1.46,1.46h0c0,.81-.65,1.46-1.46,1.46h-59.64c-.81,0-1.46-.65-1.46-1.46h0c0-.81,.65-1.46,1.46-1.46Z"/>
                            <line class="cls-3" x1="84.4" y1="57.6" x2="99.84" y2="57.6"/>
                          </g>
                        </g>
                      </svg>
                    </div>
                    <h4><span>Website Brief Engine</span></h4>
                    <p>Here’s an easier way to scope and budget your next website project. This tool will help you create an initial website development brief without missing key components for success.</p>
                    <a href="[~476~]" class="ext" target="_blank" rel="noopener nofollow"></a>
                </div>

                <div class="cell toolcard">
                    <div class="icon">
                        <svg width="195" height="121" viewBox="0 0 195 121" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip1)">
                                <path d="M170.5 1H1V120H193.3V30.4V23.7L170.5 1Z" stroke="#1D1D1B" stroke-width="2"
                                    stroke-miterlimit="10" />
                                <path d="M93.1 40.3H163.4" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M93.1 48H163.4" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M93.1 55.7H163.4" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M93.1 63.4001H163.4" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M93.1 71.1001H163.4" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M93.1 78.8H163.4" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M93.1 86.6001H163.4" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path
                                    d="M55.8 46.8001C59.8 40.1001 67.8 39.2001 73.1 41.6001C79.4 44.4001 82.9 50.9001 81.8 57.8001C80.5 65.9001 75.6 71.9001 69.7 77.0001C65.8 80.4001 61.4 83.2001 57.2 86.2001C56.6 86.6001 55.4 86.8001 54.9 86.4001C46.7 81.5001 39.3 75.9001 33.9 68.0001C30.6 63.0001 28.6 57.6001 30 51.6001C31.4 45.7001 35.2 42.0001 41.1 40.8001C47.2 39.5001 52.1 41.3001 55.8 46.8001Z"
                                    fill="#EC1C24" />
                                <path d="M170.5 1V23.7H193.3" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                            </g>
                            <defs>
                                <clipPath id="clip1">
                                    <rect width="194.3" height="121" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <h4><span>Creative Brief Engine</span></h4>
                    <p>A well-written creative brief keeps your brand building initiatives focused, on time and on budget. It’s an essential
                        tool for any brand building initiative.</p>
                    <a href="[~196~]" class="ext" target="_blank" rel="noopener nofollow"></a>
                </div>

                <div class="cell toolcard">
                    <div class="icon">
                        <svg width="211" height="128" viewBox="0 0 211 128" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip2)">
                                <path d="M150.5 74.3999V83.2999" stroke="#1D1D1B" stroke-miterlimit="10" />
                                <path d="M121.3 74.3999V83.2999" stroke="#1D1D1B" stroke-miterlimit="10" />
                                <path d="M92 74.3999V83.2999" stroke="#1D1D1B" stroke-miterlimit="10" />
                                <path d="M62.7 74.3999V83.2999" stroke="#1D1D1B" stroke-miterlimit="10" />
                                <path d="M33.5 74.3999V83.2999" stroke="#1D1D1B" stroke-miterlimit="10" />
                                <path d="M4.20001 74.3999V83.2999" stroke="#1D1D1B" stroke-miterlimit="10" />
                                <path d="M121.3 68.8001H13.5C6.9 68.8001 1.5 63.4 1.5 56.8C1.5 50.2 6.9 44.8 13.5 44.8H115.4"
                                    fill="#EC1C24" />
                                <path
                                    d="M121.3 48.5001C123.3 45.2001 127.3 44.7001 129.9 45.9001C133 47.3001 134.8 50.5001 134.2 54.0001C133.5 58.0001 131.1 61.0001 128.2 63.6001C126.3 65.3001 124.1 66.7001 121.9 68.2001C121.6 68.4001 121 68.5001 120.7 68.3001C116.7 65.9001 112.9 63.1001 110.3 59.1001C108.6 56.6001 107.7 53.9001 108.4 50.9001C109.1 48.0001 111 46.1001 114 45.5001C117 44.9001 119.5 45.8001 121.3 48.5001Z"
                                    fill="#F2F1F1" stroke="#F2F1F1" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"
                                    stroke-linejoin="round" />
                                <path
                                    d="M121.3 48.5001C123.3 45.2001 127.3 44.7001 129.9 45.9001C133 47.3001 134.8 50.5001 134.2 54.0001C133.5 58.0001 131.1 61.0001 128.2 63.6001C126.3 65.3001 124.1 66.7001 121.9 68.2001C121.6 68.4001 121 68.5001 120.7 68.3001C116.7 65.9001 112.9 63.1001 110.3 59.1001C108.6 56.6001 107.7 53.9001 108.4 50.9001C109.1 48.0001 111 46.1001 114 45.5001C117 44.9001 119.5 45.8001 121.3 48.5001Z"
                                    fill="#EC1C24" />
                                <path
                                    d="M12.8 44.3999H145C151.5 44.3999 156.8 49.6999 156.8 56.1999V57.3999C156.8 63.8999 151.5 69.1999 145 69.1999H12.8C6.3 69.2999 1 63.9999 1 57.3999V56.1999C1 49.6999 6.3 44.3999 12.8 44.3999Z"
                                    stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path
                                    d="M206.9 112.8L182.9 88.8C179.6 93.2 175.7 97.2 171.2 100.4L195.2 124.4C198.4 127.6 203.6 127.6 206.9 124.4C210.2 121.3 210.2 116.1 206.9 112.8Z"
                                    fill="#F2F1F1" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path
                                    d="M138.7 1C108.3 1 83.6 25.7 83.6 56.1C83.6 86.5 108.3 111.2 138.7 111.2C150.9 111.2 162.1 107.2 171.3 100.5C175.8 97.2 179.7 93.3 183 88.9C189.8 79.7 193.8 68.4 193.8 56.1C193.8 25.7 169.1 1 138.7 1ZM163.2 92.5C156.2 97.2 147.8 100 138.7 100C114.5 100 94.8 80.4 94.8 56.1C94.8 31.8 114.4 12.2 138.7 12.2C163 12.2 182.6 31.8 182.6 56.1C182.6 65.3 179.8 73.8 175 80.8C171.8 85.4 167.8 89.4 163.2 92.5Z"
                                    fill="#F2F1F1" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                            </g>
                            <defs>
                                <clipPath id="clip2">
                                    <rect width="210.3" height="127.9" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <h4><span>Website Assessment Tool</span></h4>
                    <p>Websites are your most powerful and potentially valuable brand building channel. This resource gives you an idea if
                        your website is working for you – or against you.</p>
                    <a href="[~392~]" class="ext" target="_blank" rel="noopener nofollow"></a>
                </div>

                <div class="cell toolcard">
                    <div class="icon">
                        <svg width="195" height="121" viewBox="0 0 195 121" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip3)">
                                <path d="M21 84.5H131.9" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M21 89.7H131.9" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M21 94.8H131.9" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M21 100H131.9" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M21 105.2H131.9" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M61.7 47.5H172.5" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M61.7 52.7H172.5" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M61.7 57.8999H172.5" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M61.7 63H172.5" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path d="M61.7 68.2H172.5" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path
                                    d="M155.2 83.8001C157.9 79.4001 163.1 78.8001 166.6 80.3001C170.8 82.1001 173.1 86.5001 172.3 91.0001C171.4 96.4001 168.2 100.3 164.3 103.7C161.7 105.9 158.8 107.8 156 109.8C155.6 110.1 154.8 110.2 154.4 110C149.1 106.8 144.1 103.1 140.6 97.8001C138.4 94.5001 137.1 90.9001 138 87.0001C138.9 83.1001 141.4 80.7001 145.4 79.8001C149.5 79.0001 152.8 80.2001 155.2 83.8001Z"
                                    fill="#EC1C24" />
                                <path d="M21.3 23.7H193.3V30.9" stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path
                                    d="M21.3 8.2H193.3V1H16.1C7.7 1 1 7.6 1 15.7C1 23.8 7.7 30.4 16.1 30.4L31.4 30.9H193.3V38.2V43.9V120H17.1C8.2 120 1 112.7 1 103.8V18.9"
                                    stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path
                                    d="M193.1 23.7001H190.3C186 23.7001 182.5 20.2001 182.5 15.9001C182.5 11.6001 186 8.1001 190.3 8.1001H193.1"
                                    stroke="#1D1D1B" stroke-width="2" stroke-miterlimit="10" />
                                <path
                                    d="M39.8 42.3L51.4 73.4001H44.3L42 66.5H30.4L28 73.4001H21L32.8 42.3H39.8ZM40.2 61.4001L36.3 50H36.2L32.1 61.4001H40.2Z"
                                    fill="black" />
                            </g>
                            <defs>
                                <clipPath id="clip3">
                                    <rect width="194.3" height="121" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <h4><span>Brand Glossary</span></h4>
                    <p>Every discipline has unique terms. Branding and marketing is no different. This glossary is a handy reference for
                        anyone working with brand builders.</p>
                    <a href="[~311~]"></a>
                </div>
            </div>
        </div>
    </section>
    [[chunk?&file=`footer.php`]]
</body>

</html>