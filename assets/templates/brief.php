[[chunk?&file=`header_brief.php`]]

<body class="page-brief locked">
    [[chunk?file=`page_header_mini.php`]]

    <form id="form-brief">
        <input type="hidden" id="brief_id" name="brief_id" value="<?php echo isset($_GET['id'][7]) ? $_GET['id'] : ""; ?>">
        <input type="hidden" id="view_id" name="view_id" value="">
        <input type="hidden" id="ident_email" name="ident_email" value="">
        <input type="hidden" id="signup_insight" name="signup_insight" value="0">

        <section id="landing">
            <div class="container">
                <h1>Creative Brief Engine</h1>
                <p>Effective creative work requires a clear creative brief.<br>Here’s the simple way to get one.</p>
                <div class="form-row landing-email">
                    <label for="email">Please share your email so we can send you your creative brief&rsquo;s unique link.</label>
                    <input type="email" id="email" name="email">
                </div>
                <div class="form-row">
                    <input type="checkbox" name="terms" id="tos"><label for="tos">I agree to the <a href="#tos">Terms of use</a></label><br>
                    <input type="checkbox" name="optin" id="optin" value="yes"><label for="optin">Yes please send me regular brand building
                        insights</label>
                </div>
                <div class="button-container block landing-buttons">
                    <a id="start" href="#basic" class="btn next disabled">Let&rsquo;s do this</a>
                </div>

            </div>
        </section>

        <section id="basic">
            <div class="container">
                <h1>Basic info</h1>
                <div class="form-row">
                    <label for="project-name">Name your project *</label>
                    <input type="text" spellcheck="true" id="project-name" name="project-name" required>
                </div>
                <div class="form-row">
                    <label for="client-name">Your organisation&rsquo;s name *</label>
                    <input type="text" spellcheck="true" id="client-name" name="client-name" required>
                </div>
                <div class="form-row">
                    <label for="client-contact">Contact person</label>
                    <input type="text" spellcheck="true" id="client-contact" name="client-contact">
                </div>
                <div class="form-row">
                    <label for="ref">Reference #</label>
                    <input type="text" spellcheck="true" id="ref" name="ref">
                </div>
                <div class="form-row">
                    <label for="date">Date</label>
                    <input type="text" spellcheck="true" id="date" name="date" class="datepicker" value="<?php echo date("d F Y"); ?>">
                </div>
                <div class="button-container">
                    <a href="#q-objectives" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-objectives">
            <div class="container">
                <h1>Project objectives</h1>
                <p>What positive change do you wish to create?<br>What are your organisation&rsquo;s goals?</p>
                <div class="form-row">
                    <textarea name="objectives" id="input-objectives"></textarea>
                </div>
                <div class="form-row">
                    <div class="button-container">
                        <a href="#" class="btn flat inline-modal">See examples of project objectives</a>
                        <div class="modal modal-right">
                            <div class="modal-content">
                                <a href="#" class="close"><i class="far fa-times"></i></a>
                                <h3>We want to</h3>
                                <ul class="append" data-target="#input-objectives">
                                    <li>Attract / retain talent</li>
                                    <li>Change or shape perceptions</li>
                                    <li>Develop brand consistency</li>
                                    <li>Elevate our customer experience</li>
                                    <li>Expand or enter a new market </li>
                                    <li>Facilitate change / digital transformation</li>
                                    <li>Increase sales</li>
                                    <li>IPO / raise investment funds</li>
                                    <li>Launch a new offering (product / venture)</li>
                                    <li>Manage brand architecture</li>
                                    <li>Strengthen culture</li>
                                    <li>Support a merger / acquisition</li>
                                    <li>Upgrade our brand image</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-container">
                    <a href="#q-deliverables" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-deliverables">
            <div class="container">
                <h1>Project deliverables</h1>
                <p>What are your proposed items for this project and when should they be delivered?</p>
                <div class="form-row">
                    <table class="tblSchedule">
                        <thead>
                            <tr>
                                <td></td>
                                <th style="width:50%;">Item</th>
                                <th>Launch date</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
                            <tr>
                                <td class="handle"><i class="fas fa-sort"></i></td>
                                <td><input name="schItem[]" placeholder="Item" value="" type="text"></td>
                                <td><input placeholder="Launch date" name="schTime[]" value="" type="text"></td>
                            </tr>
                            <tr>
                                <td class="handle"><i class="fas fa-sort"></i></td>
                                <td><input name="schItem[]" placeholder="Item" value="" type="text"></td>
                                <td><input placeholder="Launch date" name="schTime[]" value="" type="text"></td>
                            </tr>
                            <tr>
                                <td class="handle"><i class="fas fa-sort"></i></td>
                                <td><input name="schItem[]" placeholder="Item" value="" type="text"></td>
                                <td><input placeholder="Launch date" name="schTime[]" value="" type="text"></td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="#" id="add-deliverable" class="btn flat"><i class="fal fa-plus"></i>Add another</a>
                </div>
                <div class="button-container">
                    <a href="#q-target-audience" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <!-- <section id="q-budget">
            <div class="container">
                <h1>Budget</h1>
                <p>It’s helpful to know a ballpark budget. Otherwise the creative team might propose unaffordable ideas. You can come back
                    to this if you like.</p>
                <div class="form-row">
                    <input type="radio" id="currency-usd" name="currency" value="usd" checked><label for="currency-usd">USD</label>
                    <input type="radio" id="currency-hkd" name="currency" value="hkd"><label for="currency-hkd">HKD</label>
                    <br><br>
                    <h4>Entire campaign budget (including media spending)</h4>
                    <input type="hidden" name="budget-min" value="">
                    <input type="hidden" name="budget-max" value="">
                    <div class="slider-container">
                        <div id="budget_slider"></div>
                        <div class="budget_slider"><div id="budget-min" class="ui-slider-handle handle"></div><div id="budget-max" class="ui-slider-handle handle"></div></div>
                    </div>
                    <br><br>
                    <h4>Production budget (for film, photography, coding, illustrations, stock images etc.)</h4>
                    <p>Traditionally this is around 10 to 20% of an advertising campaign, but could be up to 80% of a branding project.</p>
                    <input type="hidden" name="prodbud-min" value="">
                    <input type="hidden" name="prodbud-max" value="">
                    <div class="slider-container">
                        <div id="prodbudget_slider"></div>
                        <div class="prodbudget_slider"><div id="prodbud-min" class="ui-slider-handle handle"></div><div id="prodbud-max" class="ui-slider-handle handle"></div></div>
                    </div>
                    <input type="hidden" name="budget-est" value="">
                    <br>
                    <p>Approximately: <span id="budest"></span></p>
                </div>
                <div class="button-container">
                    <a href="#q-target-audience" class="btn next">Next</a>
                </div>
            </div>
        </section> -->

        <section id="q-target-audience">
            <div class="container">
                <h1>Target audience</h1>
                <p>Have you developed audience personas?</p>
                <div class="form-row">
                    <input type="radio" id="yes-audience-personas" name="audience-personas" value="yes"><label
                        for="yes-audience-personas">Yes</label>
                    <div class="sub-row">
                        <p>Well done. Please add a link to your personas in the Notes section below.</p>
                    </div>
                    <br><br><input type="radio" id="no-audience-personas" name="audience-personas" value="no"><label
                        for="no-audience-personas">No</label>
                    <div class="sub-row">
                        <p>Which audiences are you reaching?</p>
                        <input type="checkbox" name="audience-reach[]" id="reach1" value="Customers"><label
                            for="reach1">Customers</label><br>
                        <input type="checkbox" name="audience-reach[]" id="reach2" value="Potential customers"><label for="reach2">Potential
                            customers</label><br>
                        <input type="checkbox" name="audience-reach[]" id="reach3" value="Colleagues"><label
                            for="reach3">Colleagues</label><br>
                        <input type="checkbox" name="audience-reach[]" id="reach4" value="Potential employees"><label for="reach4">Potential
                            employees</label><br>
                        <input type="checkbox" name="audience-reach[]" id="reach5" value="Business partners/investors"><label
                            for="reach5">Business partners/investors</label><br>
                        <input type="checkbox" name="audience-reach[]" id="reach6" value="Media"><label for="reach6">Media</label><br>
                        <input type="checkbox" class="trigger-custom-response" id="reach7" name="audience-reach[]" value="Other"><label
                            for="reach7">Other</label>
                        <div class="sub-row">
                            <input type="text" name="audience-reach-others" class="custom_response" maxlength="100"><span
                                class="charcount"></span>
                        </div>

                        <p>Does your audience know you already?</p>
                        <div class="form-row">
                            <input type="radio" id="audience-known" name="audience-knowledge" value="known"><label for="audience-known">Yes
                                they know us</label><br>
                            <input type="radio" id="audience-unknown" name="audience-knowledge" value="unknown"><label
                                for="audience-unknown">No they don&rsquo;t know us</label><br>
                            <input type="radio" id="audience-halfknown" name="audience-knowledge" value="halfknown"><label
                                for="audience-halfknown">Some know us, some don&rsquo;t</label><br><br>
                            <h4>Gender ratio (male/female)</h4>
                            <input type="hidden" name="gender-ratio" value="">
                            <div class="slider-container">
                                <div id="gender_slider"></div>
                                <!-- <div class="gender_slider"><div id="gender" class="ui-slider-handle handle"></div></div> -->
                            </div>
                            <br>
                            <h4>Age range</h4>
                            <input type="hidden" name="agemin" value="">
                            <input type="hidden" name="agemax" value="">
                            <div class="slider-container">
                                <div id="age_slider"></div>
                                <!-- <div class="age_slider"><div id="agemin" class="ui-slider-handle handle"></div><div id="agemax" class="ui-slider-handle handle"></div></div> -->
                            </div>
                            <br>
                            <h4>Share more about our audience</h4>
                            <textarea id="input-more-about-audience" name="more-audience-detail"></textarea>
                        </div>
                    </div>
                </div>

                <div class="button-container">
                    <a href="#q-promise" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-promise">
            <div class="container">
                <h1>Promise</h1>
                <p>What do you believe is your Unique Selling Proposition? The single biggest factor most likely to persuade your audience
                    to act?</p>
                <div class="form-row">
                    <input type="radio" name="promise_type" id="promise_text" value="promise_text"><label for="promise_text">I want to
                        promise the following:</label><br>
                    <textarea id="input-promise" name="promise" class="nomde"></textarea>
                </div>
                <div class="form-row">
                    <input type="radio" name="promise_type" id="promise_fill" value="promise_fill"><label for="promise_fill">Help me develop
                        my promise</label><br>
                    <div class="form-row">
                        <input type="radio" id="promise1" name="promise-choice" value="1">
                        <label for="promise1">With our <input type="text" spellcheck="true"
                                placeholder="service, product, innovation, brand"> you can <input type="text" spellcheck="true"
                                placeholder="solve this particular problem"> more <input type="text" spellcheck="true"
                                placeholder="faster, easier, convenient, cheap"> than ever.</label><br><br>
                        <input type="radio" id="promise2" name="promise-choice" value="2">
                        <label for="promise2">Only <input type="text" spellcheck="true" placeholder="service, product, innovation, brand">
                            can <input type="text" spellcheck="true" placeholder="solve this particular problem">.</label><br><br>
                        <input type="radio" id="promise3" name="promise-choice" value="3">
                        <label for="promise3">Our <input type="text" spellcheck="true" placeholder="service, product, innovation, brand">
                            will help <input type="text" spellcheck="true" placeholder="you, your business, your child"> to <input
                                type="text" spellcheck="true" placeholder="improve in some way">.</label>
                    </div>
                </div>
                <div class="button-container">
                    <a href="#intermission" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="intermission">
            <div class="container">
                <p>For creative teams who are already familiar with your brand or business</p>
                <div class="button-container">
                    <a id="gen_brief" class="btn" href="#summary">Generate my basic creative brief</a>
                </div>
                <br><br>
                <p>A more detailed brief should result in more effective creative solutions</p>
                <div class="button-container">
                    <a href="#q-background" class="btn next">Keep going for total brief perfection</a>
                </div>
            </div>
        </section>

        <section id="q-background">
            <div class="container">
                <h1>Project background</h1>
                <p>Share as much as you can about your project. Add links if you like. The more your creative team knows, the better they
                    can help.</p>
                <div class="form-row">
                    <textarea id="input-background" name="background"></textarea>
                </div>
                <div class="button-container">
                    <a href="#q-channels" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-channels">
            <div class="container">
                <h1>Channels</h1>
                <p>Which media channels will you use to take your message to your audience?</p>
                <div class="form-row">
                    <textarea name="channels" id="input-channels"></textarea>
                </div>
                <div class="form-row">
                    <div class="button-container">
                        <a href="#" class="btn flat inline-modal">Choose from a list</a>
                        <div class="modal modal-right">
                            <div class="modal-content">
                                <a href="#" class="close"><i class="far fa-times"></i></a>
                                <br>
                                <ul class="append" data-target="#input-channels">
                                    <li>Broadcast TV</li>
                                    <li>Digital</li>
                                    <li>Earned media (viral, media releases)</li>
                                    <li>Outdoors</li>
                                    <li>Owned media (our newsletter, blog, social media, intranet)</li>
                                    <li>Paid media (traditional advertising)</li>
                                    <li>Point of sale</li>
                                    <li>Print</li>
                                    <li>Trade show or environment</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-container">
                    <a href="#q-belief" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-belief">
            <div class="container">
                <h1>Reasons to believe</h1>
                <p>You have promised the following:</p>
                <p class="sum_response" id="from-promise"></p>
                <p>Now list all the reasons your promise can be believed&hellip;</p>
                <div class="form-row">
                    <div class="reason-list sortable">
                        <div class="reason">
                            <div class="handle"><i class="fas fa-sort"></i></div> <input type="text" spellcheck="true"
                                name="belief-reason[]">
                        </div>
                        <div class="reason">
                            <div class="handle"><i class="fas fa-sort"></i></div> <input type="text" spellcheck="true"
                                name="belief-reason[]">
                        </div>
                        <div class="reason">
                            <div class="handle"><i class="fas fa-sort"></i></div> <input type="text" spellcheck="true"
                                name="belief-reason[]">
                        </div>
                    </div>
                    <a href="#" id="add-reason" class="btn flat"><i class="far fa-plus"></i> Add another</a>
                </div>
                <p>Please prioritise these reasons to believe, from most important to least important.</p>
                <div class="button-container">
                    <a href="#q-attitude" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-attitude">
            <div class="container">
                <h1>Brand personality</h1>
                <p>Effective brands have a consistent tone and manner that their audiences can relate to. This is called brand personality. </p>
                <p>Brand personality often reflects how your main target audience sees themselves.</p>
                <h4>Choose one</h4>
                <div class="form-row">
                    <input type="radio" id="personality1" name="personality" value="Strong, authoritative, powerful"><label for="personality1">Strong, authoritative, powerful</label><br>
                    <input type="radio" id="personality2" name="personality" value="Inspired, creative, daring"><label for="personality2">Inspired, creative, daring</label><br>
                    <input type="radio" id="personality3" name="personality" value="Warm, sympathetic, reassuring"><label for="personality3">Warm, sympathetic, reassuring</label><br>
                    <input type="radio" id="personality4" name="personality" value="Simple, positive, humble"><label for="personality4">Simple, positive, humble</label><br>
                    <input type="radio" id="personality5" name="personality" value="Wise, knowledgeable, assured"><label for="personality5">Wise, knowledgeable, assured</label><br>
                    <input type="radio" id="personality6" name="personality" value="Adventurous, enthusiastic, bold"><label for="personality6">Adventurous, enthusiastic, bold</label><br>
                    <input type="radio" id="personality7" name="personality" value="Disruptive, challenging, daring"><label for="personality7">Disruptive, challenging, daring</label><br>
                    <input type="radio" id="personality8" name="personality" value="Charming, seductive, mysterious"><label for="personality8">Charming, seductive, mysterious</label><br>
                    <input type="radio" id="personality9" name="personality" value="Brave, inspiring, honest"><label for="personality9">Brave, inspiring, honest</label><br>
                    <input type="radio" id="personality10" name="personality" value="Seductive, romantic, arousing"><label for="personality10">Seductive, romantic, arousing</label><br>
                    <input type="radio" id="personality11" name="personality" value="Amusing, talented, optimistic"><label for="personality11">Amusing, talented, optimistic</label><br>
                    <input type="radio" id="personality12" name="personality" value="Pleasant, authentic, direct"><label for="personality12">Pleasant, authentic, direct</label>
                </div>
                <!-- <h4>Choose up to <span class="three">three</span></h4>
                <div class="form-row attitude-list">
                    <ul>
                        <li><input type="checkbox" id="attitude1" name="attitude[]" value="Authoritative"><label for="attitude1">Authoritative</label></li>
                        <li><input type="checkbox" id="attitude2" name="attitude[]" value="Caring"><label for="attitude2">Caring</label></li>
                        <li><input type="checkbox" id="attitude3" name="attitude[]" value="Casual"><label for="attitude3">Casual</label></li>
                        <li><input type="checkbox" id="attitude4" name="attitude[]" value="Clever"><label for="attitude4">Clever</label></li>
                        <li><input type="checkbox" id="attitude5" name="attitude[]" value="Clinical"><label for="attitude5">Clinical</label></li>
                        <li><input type="checkbox" id="attitude6" name="attitude[]" value="Cold"><label for="attitude6">Cold</label></li>
                        <li><input type="checkbox" id="attitude7" name="attitude[]" value="Conservative"><label for="attitude7">Conservative</label></li>
                        <li><input type="checkbox" id="attitude8" name="attitude[]" value="Dignified"><label for="attitude8">Dignified</label></li>
                        <li><input type="checkbox" id="attitude9" name="attitude[]" value="Disciplined"><label for="attitude9">Disciplined</label></li>
                    </ul>
                    <ul>
                        <li><input type="checkbox" id="attitude10" name="attitude[]" value="Energetic"><label for="attitude10">Energetic</label></li>
                        <li><input type="checkbox" id="attitude11" name="attitude[]" value="Fatherly"><label for="attitude11">Fatherly</label></li>
                        <li><input type="checkbox" id="attitude12" name="attitude[]" value="Formal"><label for="attitude12">Formal</label></li>
                        <li><input type="checkbox" id="attitude13" name="attitude[]" value="Friendly"><label for="attitude13">Friendly</label></li>
                        <li><input type="checkbox" id="attitude14" name="attitude[]" value="Fun"><label for="attitude14">Fun</label></li>
                        <li><input type="checkbox" id="attitude15" name="attitude[]" value="Humorous"><label for="attitude15">Humorous</label></li>
                        <li><input type="checkbox" id="attitude16" name="attitude[]" value="Inspiring"><label for="attitude16">Inspiring</label></li>
                        <li><input type="checkbox" id="attitude17" name="attitude[]" value="Logical"><label for="attitude17">Logical</label></li>
                        <li><input type="checkbox" id="attitude18" name="attitude[]" value="Modern"><label for="attitude18">Modern</label></li>
                    </ul>
                    <ul>
                        <li><input type="checkbox" id="attitude19" name="attitude[]" value="Motherly"><label for="attitude19">Motherly</label></li>
                        <li><input type="checkbox" id="attitude20" name="attitude[]" value="Oldfashioned"><label for="attitude20">Oldfashioned</label></li>
                        <li><input type="checkbox" id="attitude21" name="attitude[]" value="Party animal"><label for="attitude21">Party animal</label></li>
                        <li><input type="checkbox" id="attitude22" name="attitude[]" value="Philosophical"><label for="attitude22">Philosophical</label></li>
                        <li><input type="checkbox" id="attitude23" name="attitude[]" value="Scary"><label for="attitude23">Scary</label></li>
                        <li><input type="checkbox" id="attitude24" name="attitude[]" value="Scholarly"><label for="attitude24">Scholarly</label></li>
                        <li><input type="checkbox" id="attitude25" name="attitude[]" value="Scientific"><label for="attitude25">Scientific</label></li>
                        <li><input type="checkbox" id="attitude26" name="attitude[]" value="Sensational"><label for="attitude26">Sensational</label></li>
                        <li><input type="checkbox" id="attitude27" name="attitude[]" value="Sensual"><label for="attitude27">Sensual</label></li>
                    </ul>
                    <ul>
                        <li><input type="checkbox" id="attitude28" name="attitude[]" value="Sexy"><label for="attitude28">Sexy</label></li>
                        <li><input type="checkbox" id="attitude29" name="attitude[]" value="Sophisticated"><label for="attitude29">Sophisticated</label></li>
                        <li><input type="checkbox" id="attitude30" name="attitude[]" value="Sporty"><label for="attitude30">Sporty</label></li>
                        <li><input type="checkbox" id="attitude31" name="attitude[]" value="Superior"><label for="attitude31">Superior</label></li>
                        <li><input type="checkbox" id="attitude32" name="attitude[]" value="Trendy"><label for="attitude32">Trendy</label></li>
                        <li><input type="checkbox" id="attitude33" name="attitude[]" value="Warm"><label for="attitude33">Warm</label></li>
                        <li><input type="checkbox" id="attitude34" class="trigger-custom-response" name="attitude[]" value="Others"><label for="attitude34">Other</label>
                            <div class="sub-row">
                                <input type="text" spellcheck="true" class="custom_response" name="custom_attitude">
                            </div>
                        </li>
                    </ul>
                </div> -->
                <div class="button-container">
                    <a href="#q-response" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-response">
            <div class="container">
                <h1>Response</h1>
                <p>An effective message triggers a response in its audience. </p>
                <p>What do you want people to think?</p>
                <div class="form-row response-list">
                    <input type="checkbox" id="response1" name="response[]" value="That looks trustworthy"><label for="response1">That looks
                        trustworthy</label><br>
                    <input type="checkbox" id="response2" name="response[]" value="That looks high quality"><label for="response2">That
                        looks high quality</label><br>
                    <input type="checkbox" id="response3" name="response[]" value="Oh that's interesting! I'll look closer"><label
                        for="response3">Oh that&rsquo;s interesting! I&rsquo;ll look closer</label><br>
                    <input type="checkbox" id="response4" name="response[]" value="That's good to know"><label for="response4">That&rsquo;s
                        good to know</label>
                </div>
                <p>Or what do you want people to do?</p>
                <div class="form-row action-list">
                    <input type="checkbox" id="response5" name="response[]" value="I will visit that website"><label for="response5">I will
                        visit that website</label><br>
                    <input type="checkbox" id="response6" name="response[]" value="I will remember this message"><label for="response6">I
                        will remember this message</label><br>
                    <input type="checkbox" id="response7" name="response[]" value="I will share this message"><label for="response7">I will
                        share this message</label><br>
                    <input type="checkbox" id="response8" name="response[]" value="I’ll act on this (explain that action below)"><label
                        for="response8">I’ll act on this (explain that action below)</label>
                    <input type="text" spellcheck="true" class="custom_response" name="custom_response">
                </div>
                <div class="button-container">
                    <a href="#q-metrics" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-metrics">
            <div class="container">
                <h1>Metrics</h1>
                <p>How should we measure success?</p>
                <div class="form-row metric-list">
                    <input type="checkbox" id="metric1" name="metric[]" value="Sales figures"><label for="metric1">Sales figures</label><br>
                    <input type="checkbox" id="metric2" name="metric[]" value="Website tracking"><label for="metric2">Website
                        tracking</label><br>
                    <input type="checkbox" id="metric3" name="metric[]" value="Email click throughs"><label for="metric3">Email click
                        throughs</label><br>
                    <input type="checkbox" id="metric4" name="metric[]" value="Number of views "><label for="metric4">Number of views
                    </label><br>
                    <input type="checkbox" id="metric5" class="trigger-custom-response" name="metric[]"
                        value="Response rates (please explain below)"><label for="metric5">Response rates (please explain below)</label>
                    <div class="sub-row">
                        <input type="text" spellcheck="true" class="custom_response" name="custom_metric">
                    </div>
                    <br>
                    <input type="checkbox" id="metric6" class="trigger-custom-response" name="metric[]" value="Others"><label
                        for="metric6">Other</label>
                    <div class="sub-row">
                        <input type="text" spellcheck="true" class="custom_response" name="custom_metric_other">
                    </div>
                </div>
                <div class="button-container">
                    <a href="#q-mandatory" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-mandatory">
            <div class="container">
                <h1>Mandatory</h1>
                <p>Is there anything that is compulsory for the creative team to follow, or include?</p>
                <div class="form-row mandatory-list">
                    <input type="checkbox" id="mandatory1" name="mandatory[]" value="Follow our brand guidelines"><label
                        for="mandatory1">Follow our brand guidelines</label><br>
                    <input type="checkbox" id="mandatory2" name="mandatory[]" value="Include our logo"><label for="mandatory2">Include our
                        logo</label><br>
                    <input type="checkbox" id="mandatory3" name="mandatory[]" value="Include our slogan"><label for="mandatory3">Include our
                        slogan</label><br>
                    <input type="checkbox" id="mandatory4" name="mandatory[]" value="Add contact details "><label for="mandatory4">Add
                        contact details</label><br>
                    <input type="checkbox" id="mandatory5" class="trigger-custom-response" name="mandatory[]"
                        value="More (add below)"><label for="mandatory5">More (add below)</label>
                    <div class="sub-row">
                        <textarea name="custom_mandatory" class="custom_response"></textarea>
                    </div>
                </div>
                <div class="button-container">
                    <a href="#q-budgeting" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-budgeting">
            <div class="container">
                <h1>Guide to budgeting</h1>
                <p>Estimate the ROI of this brand building initiative in the first 12 months after it launches.</p>
                <p>How could this project impact the following?</p>
                <ul>
                    <li>Sales revenue </li>
                    <li>Talent recruitment and retention costs</li>
                    <li>Success of new market entry/product launch</li>
                    <li>Perceived value of business to buyer or investor</li>
                    <li>Outcome of successfully navigating a pivotal time of change. Such as a strategic business shift or focus, M&A, new leadership, digital transformation, culture development, community engagement, ESG/CSR, milestone anniversary</li>
                </ul>
                <br>
                <p><strong>Rule of thumb: your budget should be 10% of the estimated project ROI in year one</strong></p>
                <div class="button-container">
                    <a href="#q-notes" class="btn next">Next</a>
                </div>
            </div>
        </section>

        <section id="q-notes">
            <div class="container">
                <h1>Notes</h1>
                <p>Anything else useful you can share? Competitor references? Examples of previous campaigns?</p>
                <p>Add links or describe resources you have that may be useful to your creative team:</p>
                <div class="form-row">
                    <textarea id="input-notes" name="notes"></textarea>
                </div>
                <div class="button-container">
                    <a id="gen_brief" href="#summary" class="btn">Generate my creative brief</a>
                </div>
            </div>
        </section>
    </form>

    <div id="summary">
        <div class="container">
            <a href="#" class="close"></a>
            <div class="buttons">
                <!-- <a href="#" class="btn share" title="Share this brief"><i class="fal fa-share-alt"></i></a> -->
                <a title="Copy / print" href="#" class="btn copyversion"><i class="fal fa-print"></i></a>
            </div>
            <h1>Creative brief</h1>
            <h3>Project: <span id="sum_project-name"></span></h3>
            <p class="basic-info">
                <strong>Client</strong>: <span id="sum_client-name"></span><br>
                <!-- <strong>Prepared by</strong>: <span id="sum_prep_by"></span><br> -->
                <strong>Ref</strong>: <span id="sum_ref"></span><br>
                <strong>Date</strong>: <span id="sum_date"></span><br>
                <strong>Last modified</strong>: <span id="sum_lastmod"></span>
            </p>


            <div class="summary-item">
                <h4>Project objectives</h4>
                <div class="sum_response" id="sum_objectives"></div>
            </div>

            <div class="summary-item">
                <h4>Project deliverables</h4>
                <div class="sum_response" id="sum_deliverables"></div>
            </div>

            <!-- <div class="summary-item">
                <h4>Ballpark budget</h4>
                <div class="sum_response" id="sum_budget"></div>
            </div> -->

            <div class="summary-item">
                <h4>Target audience</h4>
                <div class="sum_response" id="sum_audience"></div>
            </div>

            <div class="summary-item">
                <h4>Promise (USP &mdash; Unique Selling Proposition)</h4>
                <div class="sum_response" id="sum_promise"></div>
            </div>

            <div class="summary-item">
                <h4>Our audience should believe us because:</h4>
                <div class="sum_response" id="sum_belief"></div>
            </div>

            <div class="summary-item">
                <h4>Project background</h4>
                <div class="sum_response" id="sum_background"></div>
            </div>

            <div class="summary-item">
                <h4>Delivery channels</h4>
                <div class="sum_response" id="sum_channels"></div>
            </div>

            <!-- <div class="summary-item">
                <h4>Our attitude towards our audience should be:</h4>
                <div class="sum_response" id="sum_attitude"></div>
            </div> -->

            <div class="summary-item">
                <h4>Our personality towards our audience should be:</h4>
                <div class="sum_response" id="sum_personality"></div>
            </div>

            <div class="summary-item">
                <h4>We want people to react this way:</h4>
                <div class="sum_response" id="sum_response"></div>
            </div>

            <div class="summary-item">
                <h4>Our success will be measured by:</h4>
                <div class="sum_response" id="sum_metrics"></div>
            </div>

            <div class="summary-item">
                <h4>The following are compulsory:</h4>
                <div class="sum_response" id="sum_mandatory"></div>
            </div>

            <div class="summary-item">
                <h4>Additional notes</h4>
                <div class="sum_response" id="sum_notes"></div>
            </div>
        </div>
    </div>

    <div id="signup2modal" class="popup_modal">
        <div class="signup2modal-content popup_modal-content">
            <h2>Thank you for using the creative brief engine</h2>
            <p>Share your email address and enable saving and editing. You don’t want to lose all your valuable work!</p>
            <input type="email" id="email2" name="email2" placeholder="Your email address">
            <div style="margin-top:0.5rem;"><input type="checkbox" name="optin2" id="optin2" value="yes"><label for="optin2">Yes please send
                    me regular brand building insights</label></div>
            <div class="button-container">
                <a id="signup2" href="#" class="btn">Save my brief</a><a id="cancelsignup2" href="#" class="btn secondary">Skip</a>
            </div>
        </div>
    </div>

    <div id="tosmodal" class="popup_modal">
        <div class="popup_modal-content">
            <a href="#" class="close"></a>
            <h2>Terms of use</h2>
            <ol>
                <li>The following are the terms and conditions for the Use of Stepworks Creative Brief Engine ("the Terms"). Stepworks
                    agrees to provide services of Stepworks Creative Brief Engine ("the Creative Brief Engine") and you, as the user of the
                    Creative Brief Engine, agree to use the services of the Creative Brief Engine in accordance with the Terms set out
                    herein.</li>
            </ol>

            <p><strong>The Creative Brief Engine</strong></p>

            <ol type="1" start="2">
                <li>The Creative Brief Engine is a website of an integrated online service delivery platform which its user may use any of
                    the services provided therein ("service") in accordance with the Terms set out herein.</li>
                <li>The Creative Brief Engine services are provided on an “as is” basis. Stepworks may provide procedures for the use of any
                    services from time to time as necessary. Stepworks may add, modify, suspend and/or terminate any services and procedures
                    for use without giving prior notice to any user.</li>
            </ol>

            <p><strong>Creative Brief Engine unique URLs</strong> </p>

            <ol type="1" start="4">
                <li>You may create a Creative Brief Engine by providing your email address and clicking agreement to the Terms of Use and
                    following the steps herein. By agreeing, you agree to be bound by these Terms of Use. </li>
                <li>You agree the Creative Brief Engine is only for your own use. You shall not enter an email addresses of another person.
                </li>
                <li>Your Creative Brief Engine access creates a unique URL that represents your Creative Brief Engine file in accordance
                    with procedure provided in the Creative Brief Engine.</li>
                <li>By accessing Stepworks Brief Engine, you may use any specific services of the Creative Brief Engine by opening your
                    unique URL in accordance with procedure provided in the Creative Brief Engine.</li>
                <li>Keep your unique URL confidential. This provides access to your Creative Brief Engine file.</li>
            </ol>

            <p><strong>Email alert function</strong></p>

            <ol type="1" start="9">
                <li>Stepworks may send messages to you via the Creative Brief Engine and via the email address you supplied upon account
                    opening.</li>
                <li>When you revise your email address you may lose access to your Creative Brief Engine file.</li>
            </ol>

            <li><strong>Maintaining your Creative Brief Engine files</strong></li>
            <ol type="1" start="11">
                <li>You may click the links provided in the service to access creative briefs you are developing.</li>
                <li>If you lose these links you may not be able to access the creative briefs you are developing.</li>
            </ol>

            <p><strong>The Privacy Policy</strong></p>

            <ol type="1" start="13">
                <li>Stepworks is concerned to ensure all personal data submitted through and stored in the Creative Brief Engine are handled
                    in strict adherence to the relevant provisions of the Personal Data (Privacy) Ordinance (Cap 486), “the PD(P)O” (see
                    www.pcpd.org.hk).</li>
                <li>Information (including personal data) kept in the personal profile of your Creative Brief Engine account may be retained
                    by Stepworks for matters arising out of, or in relation to, your use of the Creative Brief Engine.</li>
                <li>If you maintain personal information in your Creative Brief Engine files, you may access and correct the
                    information/data kept there by logging into your Creative Brief Engine pages. </li>
                <li>You may request access to or correction of your personal data by sending a data access request to Stepworks via email at
                    <a href="mailto:info@stepworks.co">info@stepworks.co</a>.</li>
                <li>Provision of any of profile data in your Creative Brief Engine file is voluntary.</li>
                <li>You hereby give consent to Stepworks for the retention of information (including personal data) contained in your
                    Creative Brief Engine file after termination of matters arising out of, or in relation to, the use of the Creative Brief
                    Engine.</li>
                <li>A cookie is a small amount of data created in a computer when a person visits a website through the computer. It often
                    includes an anonymous unique identifier. A cookie can be used to identify a computer or to temporarily store
                    non-personal information required by an online service. It is not used by the Creative Brief Engine to collect any
                    personal information. It does not have the function of identifying an individual user of the Creative Brief Engine.</li>
                <li>The Creative Brief Engine may use cookies. No personal information is contained in the cookies generated by the Creative
                    Brief Engine. If you configure your browser to reject cookies, you will not be able to use the Creative Brief Engine.
                </li>
                <li>The Creative Brief Engine requires JavaScript to function properly. If you disable JavaScript on your computer, you will
                    not be able to use the Creative Brief Engine.</li>
                <li>You may input interests in your Creative Brief Engine file. Such information is for Stepworks to consider how to deliver
                    relevant content and improve its services. If you have opted to receive specific information from Stepworks, Stepworks
                    may pass you messages or information of your interest from time to time. </li>
            </ol>

            <p><strong>Disclaimer</strong></p>
            <ol start="23">
                <li>Unless provided otherwise in the Terms, the information on the Creative Brief Engine is provided to you on an "as is"
                    basis without any express or implied warranty or representation of any kind and is for a general, indicative purpose
                    only. In particular, Stepworks does not make any express or implied warranty or representation as to the accuracy,
                    completeness, fitness for a particular purpose, non-infringement, reliability, security or timeliness of such
                    information. Stepworks will not be liable for any errors in, omissions from, or misstatements or misrepresentations
                    (whether express or implied) concerning any such information, and will not have or accept any liability, obligation or
                    responsibility whatsoever for any loss, destruction or damage (including without limitation consequential loss,
                    destruction or damage) however arising from or in respect of any use or misuse of or reliance on, inability to use or
                    the unavailability of the Creative Brief Engine or any information and services delivered on the Creative Brief Engine.
                    Stepworks does not warrant or represent that the Creative Brief Engine or any information or any electronic data or
                    information transmitted to you through the Creative Brief Engine is free of computer viruses. Stepworks shall not be
                    liable for any loss, destruction or damage arising out of or in relation to any transmission from users to Stepworks or
                    vice versa over the internet.</li>
                <li>Without limiting the generality of the foregoing, nothing on or provided via the Creative Brief Engine shall constitute,
                    give rise to or otherwise imply any endorsement, approval or recommendation on Stepworks’ part of any third party, be it
                    a provider of goods or services or otherwise.</li>
                <li>Stepworks is not responsible for any loss or damage whatsoever arising out of or in relation to any information on the
                    Creative Brief Engine. Stepworks reserves the right to omit, suspend or edit all information compiled by Stepworks on
                    the Creative Brief Engine at any time in its sole discretion without giving any reason or prior notice. You are
                    responsible for making your own assessment of all information contained on the Creative Brief Engine and shall verify
                    such information by making reference, for example, to original publications and obtaining independent advice before
                    acting upon it.</li>
                <li>You shall ensure accuracy of Creative Brief Engine data (including your personal data) kept in your Creative Brief
                    Engine file. Stepworks shall not be responsible/liable for any inaccuracy, mistake or false information (including your
                    personal data) so kept and/or submitted by you.</li>
                <li>Stepworks does not give any warranty in relation to the use of the Creative Brief Engine and / or any third party
                    websites linked to the Creative Brief Engine, and does not warrant against any possible interference or damage to your
                    computer system arising from the use of the website. Stepworks makes no representations or warranties regarding the
                    accuracy, functionality or performance of any third party software that may be used in connection with the Creative
                    Brief Engine. Stepworks makes no warranties that the Creative Brief Engine is free of infection by computer viruses.
                </li>
            </ol>

            <p><strong>Security</strong></p>

            <ol type="1" start="28">
                <li>You must close all browser windows after use to prevent misuse of your Creative Brief Engine account by third parties.
                </li>
            </ol>

            <p><strong>Copyright</strong></p>

            <ol type="1" start="29">
                <li>Unless otherwise indicated, the contents found on the Creative Brief Engine are subject to copyright owned by Stepworks.
                </li>
                <li>Prior written consent to Stepworks is required if you intend to reproduce, distribute or otherwise use any non-text
                    contents (including but not limited to photographs, graphics, illustrations and drawings, diagrams, audio files and
                    video files) found on the Creative Brief Engine in any way or for any purpose. Such requests for consent must be
                    addressed to Stepworks via email enquiry at <a href="mailto:info@stepworks.co">info@stepworks.co</a>. </li>
                <li>Where third party copyright (ie. copyright belonging to anyone other than Stepworks is involved in the non-text contents
                    found on the Creative Brief Engine, authorisation or permission to reproduce or distribute r otherwise use any such
                    non-text contents must be obtained from the copyright owners concerned. </li>
                <li>Stepworks copyright protected text contents found on the Creative Brief Engine may be reproduced and distributed free of
                    charge in any format or medium for personal or internal use within an organisation, subject to the following conditions:
                    <ol>
                        <li>The copy or copies made must not be for sale, or exchanged for benefit, gain, profit, reward or any other
                            commercial purposes</li>
                        <li>Such text contents must be reproduced accurately and must not be used in a manner adversely affecting any moral
                            rights of Stepworks</li>
                        <li>Stepworks must be acknowledged as the copyright owner of such text contents and acknowledgement must be given to
                            “Stepworks (stepworks.co)” as the source of all such text contents. </li>
                    </ol>
                </li>
                <li>"Commercial purposes" includes without limitation the following:
                    <ol type="a">
                        <li>the purpose to offer to supply goods, services, facilities, land or an interest in land</li>
                        <li>the purpose to offer to provide a business opportunity or an investment opportunity</li>
                        <li>the purpose to advertise or promote goods, services, facilities, land or an interest in land</li>
                        <li>the purpose to advertise or promote a business opportunity or an investment opportunity</li>
                        <li>the purpose to advertise or promote a supplier, or a prospective supplier, of goods, services, facilities, land
                            or an interest in land</li>
                        <li>the purpose to advertise or promote a provider, or a prospective provider, of a business opportunity or an
                            investment opportunity, in the course of or in the furtherance of any business.</li>
                    </ol>
                </li>
                <li>Please note that the permission given in paragraph 33 above only applies to Stepworks copyright protected text contents
                    found on the Creative Brief Engine. Stepworks reserves the right to withdraw any permission given in paragraph 33 above
                    at any time without any prior notice to you.</li>
                <li>Prior written consent of Stepworks is required if you intend to reproduce, distribute or otherwise use any Stepworks
                    copyright protected text contents found on the Creative Brief Engine in any way other than that permitted in paragraph
                    33 above, or for any purpose other than that permitted in paragraph 33 above. Such requests shall be addressed to
                    Stepworks via email at info@stepworks.co.</li>
                <li>Where third party copyright (ie. copyright belonging to anyone other than Stepworks is involved in the text contents
                    found on the Creative Brief Engine, authorisation or permission to reproduce or distribute or otherwise use any such
                    text contents must be obtained from the copyright owners concerned.</li>
                <li>For the avoidance of doubt, the permission given in paragraph 33 above does not extend to any contents on other websites
                    linked to the Creative Brief Engine. If you intend to reproduce, distribute or otherwise use any contents on any such
                    linked websites, you shall obtain all necessary authorisation or permission from the copyright owners concerned.</li>
            </ol>

            <p><strong>Suspension of your Creative Brief Engine access</strong></p>

            <ol type="1" start="38">
                <li>If you breach any of the Terms set out herein, Stepworks may suspend your Creative Brief Engine access at any time
                    without giving notice to you.</li>
                <li>If Stepworks suspects that the email address for your Creative Brief Engine file access is used by any person (other
                    than by yourself) to access your Creative Brief Engine account and / or to access the services in the Creative Brief
                    Engine, Stepworks may suspend your access without notice.</li>
                <li>Upon suspension, you would not be able to access your Creative Brief Engine files.</li>
            </ol>

            <p><strong>Termination of a Creative Brief Engine login</strong></p>

            <ol type="1" start="41">
                <li>Stepworks may terminate your Creative Brief Engine access without giving any notice if you breach any of the Terms or it
                    is noted that your Creative Brief Engine access has not been used for a long time (one year). Stepworks may notify you
                    of the termination by sending an email message to the email address provided in the personal profile of your Creative
                    Brief Engine account.</li>
                <li>You may delete your Creative Brief Engine files by contacting Stepworks.</li>
                <li>Upon deletion of your Creative Brief Engine files,
                    <ol type="a">
                        <li>(a) your links will no longer work</li>
                        <li>(b) you cannot access your information any more</li>
                        <li>(c) you may not use any specific services provided in the Creative Brief Engine</li>
                        <li>(d) information related to your Stepworks Creative Brief access may be retained by Stepworks (see paragraphs 36
                            and 43 above)</li>
                        <li>(e) you may not be able to create new Creative Brief Engine documents in the Creative Brief Engine.</li>
                    </ol>

                    This provision survives termination of your Creative Brief Engine login.
                </li>
            </ol>

            <p><strong>Indemnity</strong></p>

            <ol type="1" start="44">
                <li>You shall indemnify and keep indemnified Stepworks against all claims, actions, proceedings, liabilities, demands,
                    charges, damages, costs, losses or expenses arising out of or resulting from the performance or attempted performance
                    of, the use or attempted use of the services of the Creative Brief Engine (including your Creative Brief Engine access)
                    to the extent that the same are or have been caused by any negligent or reckless conduct, wilful misconduct, omission,
                    defamation, breach of statutory duty or breach of any of the Terms by you. This provision survives termination of your
                    Creative Brief Engine access.</p>
            </ol>

            <p><strong>Modifications of the Terms</strong></p>

            <ol type="1" start="45">
                <li>Stepworks may from time to time vary, modify, delete and/or add any terms or conditions in the Terms at its own
                    discretion. The revised Terms (if made) will be displayed to you in the Creative Brief Engine when you access the
                    Creative Brief Engine. You must read the revised Terms. By clicking the checkbox against "I accept the revised Terms
                    (including The Privacy Policy, Disclaimer and Copyright clauses)" in the Creative Brief Engine, you agree to be bound by
                    the revised Terms and the revised Terms will supersede previous versions of the Terms accepted by you.</li>
            </ol>

            <p><strong>Governing Law</strong></p>

            <ol type="1" start="46">
                <li>The Terms shall be governed and construed in accordance with the laws of the Hong Kong Special Administrative Region.
                    You also agree to submit to the jurisdiction of the Hong Kong Courts.</li>
            </ol>
        </div>
    </div>

    <!-- <div id="sharemodal" class="popup_modal">
        <div class="popup_modal-content">
            <a href="#" class="close"></a>
            <h2>Save and share your creative brief</h2>
            <div class="row"><label for="share_edit">View creative brief</label><input type="text" id="share_edit" readonly=""></div>
            <div class="row"><label for="share_view">Edit creative brief</label><input type="text" id="share_view" readonly=""></div>
            <a href="#" id="email-links" class="btn">Email me these links<span>Sent!</span></a><a href="#" id="share-sw" class="btn">Share
                this brief with Stepworks<span>Thank you!</span></a>
        </div>
    </div> -->

    <div id="feedback">
        <a href="#" class="btn-feedback"><img src="assets/images/feedback.svg"> <span>Feedback</span></a>
        <div class="report-content">
            <p id="q">What do you think of our Website Assessment Tool?</p>
            <p id="t">Thank you for your feedback!</p>
            <form id="feedback_form">
                <input type="email" id="feedback_email" name="feedback_email" placeholder="Your contact email" required>
                <textarea name="feedback" id="feedback_content" class="nomde" placeholder="Message *" required></textarea>
                <div class="buttons">
                    <button type="submit">Send</button><button class="close">Close</button>
                </div>
            </form>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end7">
                    <div class="copyright">&copy; <?=date('Y')?> Stepworks | <a href="en/legal">Legal</a></div>
                </div>
            </div>
        </div>
    </footer>

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="assets/js/vendor/wNumb.min.js"></script>
    <script src="assets/js/brief_plugins.js"></script>
    <script src="assets/js/brief_scripts.js?v=20240126142028"></script>
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-4948962-2', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
    </script>
    <!-- AccessiBe -->
    <style>
    .acsb-hero-action {
        border-radius: 0px !important;
        white-space: nowrap !important;
        padding: 9px !important;
    }

    .acsb-header-option.acsb-header-option-close,
    .acsb-header-option.acsb-header-option-position,
    .acsb-header-option.acsb-header-option-statement {
        border-radius: 0px !important;
    }

    .acsb-language.acsb-header-option-language {
        display: none !important;
    }

    .acsb-range-base.acsb-color-lead {
        background-color: #ffffff !important;
    }

    div.acsb-widget .acsb-main .acsb-main-options .acsb-actions .acsb-actions-box .acsb-actions-group .acsb-action-box.acsb-action-box-big {
        background-color: #f4f4f4 !important;
    }

    .acsb-action-box {
        background-color: #f4f4f4 !important;
    }

    .acsb-range-button {
        border-radius: 0px !important;
    }

    .acsb-widget .acsb-main .acsb-main-options .acsb-actions .acsb-actions-box .acsb-profiles .acsb-profile .acsb-profile-content i {
        background-color: #f4f4f4 !important;
    }

    .acsb-trigger.acsb-trigger-position-y-bottom.acsb-mobile {
        bottom: 10px !important;
    }

    .acsb-trigger.acsb-trigger-position-x-right.acsb-mobile {
        right: 10px !important;
    }

    /* span.acsb-toggle-option {
    background-color: #f4f4f4 !important;
}

.acsb-toggle-option .acsb-toggle-option-off {
    background-color: #ffffff !important;
} */
    </style>
    <script>
        (function() {
            var s = document.createElement('script'),
                e = !document.body ? document.querySelector('head') : document.body;
            s.src = 'https://acsbapp.com/apps/app/assets/js/acsb.js';
            s.async = s.defer = true;
            s.onload = function() {
                acsbJS.init({
                    statementLink: '',
                    feedbackLink: '',
                    footerHtml: '© 2020 Stepworks',
                    hideMobile: false,
                    hideTrigger: false,
                    language: 'en',
                    position: 'right',
                    leadColor: '#eb0c38',
                    triggerColor: '#eb0c38',
                    triggerRadius: '0%',
                    triggerPositionX: 'right',
                    triggerPositionY: 'bottom',
                    triggerIcon: 'people',
                    triggerSize: 'medium',
                    triggerOffsetX: 20,
                    triggerOffsetY: 20,
                    mobile: {
                        triggerSize: 'small',
                        triggerPositionX: 'right',
                        triggerPositionY: 'bottom',
                        triggerOffsetX: 10,
                        triggerOffsetY: 10,
                        triggerRadius: '0'
                    }
                });
            };
            e.appendChild(s);
        }());
    </script>
</body>

</html>