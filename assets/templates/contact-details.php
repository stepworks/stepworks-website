<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>[*pagetitle*]</title>
    <base href="[(site_url)]">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="assets/templates/manifest.json.php">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="apple-mobile-web-app-title" content="My Card">

    <style>
        @font-face{font-family:"Helvetica Neue";src:url(assets/css/fonts/5664093/522972a6-5867-47ae-82fc-a7cf9bd5d4d4.eot?#iefix);src:url(assets/css/fonts/5664093/522972a6-5867-47ae-82fc-a7cf9bd5d4d4.eot?#iefix) format("eot"),url(assets/css/fonts/5664093/08b57253-2e0d-4c12-9c57-107f6c67bc49.woff2) format("woff2"),url(assets/css/fonts/5664093/08edde9d-c27b-4731-a27f-d6cd9b01cd06.woff) format("woff"),url(assets/css/fonts/5664093/8f4a1705-214a-4dd0-80b8-72252c37e688.ttf) format("truetype");font-weight:400;font-style:normal;font-display:swap}@font-face{font-family:"Helvetica Neue";src:url(assets/css/fonts/5664150/4c21ab66-1566-4a85-b310-fbc649985f88.eot?#iefix);src:url(assets/css/fonts/5664150/4c21ab66-1566-4a85-b310-fbc649985f88.eot?#iefix) format("eot"),url(assets/css/fonts/5664150/800da3b0-675f-465f-892d-d76cecbdd5b1.woff2) format("woff2"),url(assets/css/fonts/5664150/7b415a05-784a-4a4c-8c94-67e9288312f5.woff) format("woff"),url(assets/css/fonts/5664150/f07c25ed-2d61-4e44-99ab-a0bc3ec67662.ttf) format("truetype");font-weight:700;font-style:normal;font-display:swap}
        *,*::before,*::after{box-sizing: border-box;}
        body{padding: 0; margin: 0; font-family: "Helvetica Neue", Arial, sans-serif; font-size: 1.1rem; line-height: 1.2; font-weight: normal; color: #16161d; text-align: center;}
        img{max-width: 100%; height: auto; border: 0 none;}
        ul{margin: 0 0 40px; padding: 0; list-style: none;}
        ul li{margin-bottom: 10px;}
        ul li:last-child{margin-bottom: 0;}
        a{color: #16161d; display: inline-block;}
        a:hover{color: #eb0c38;}
        .container{width: 100%; max-width: 400px; padding: 3rem 1rem; margin: 0 auto;}
        figure{margin: 0 0 40px;}
        figure img{margin: 0 auto;}
        h1{font-size: 26px; font-weight: bold; margin: 0 0 10px; line-height: 1;}
        h2{font-size: 26px; font-weight: normal; margin: 0; line-height: 1;}
        .member{margin-bottom: 20px;}
        .member img{display: block; margin: 0 auto 40px; width: 120px; height: 120px; object-fit: cover; object-position: center; border-radius: 50%; background-color: #f0f0f0;}
        .btn{display: inline-block; text-decoration: none; padding: 1rem 2rem; color: #fff; background-color: #eb0c38; font-size: 1rem;}
        .btn:hover{color: #fff;}
    </style>
</head>
<body>
    <div class="container">
        [[getMemberDetails &id=`[*parent*]`]]
    </div>
</body>
</html>