[[chunk?&file=`header_noindex_nofollow.php`]]
<body class="page-contact2">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <h1 class="super">Hello. Let&rsquo;s connect.</h1>
            <div class="contact-content">
                <h4><a href="#">I want to build my brand</a></h4>
                <div class="contact-expand-content">
                    <h2>Get the most from your marketing investment</h2>
                    <p>Who will you work with to build your brand? </p>
                    <p>It may be a leader’s most important marketing budget decision.</p>
                    <p>We’re very frank about any potential – and downside – of a brand building collaboration with Stepworks.</p>
                    <p>Tell us about your current business issues. The more we know, the better we can assess if a collaboration would offer you a valuable return on investment.</p>
                    <form id="contact-form" action="[~182~]">
                        <div class="form-row">
                            <input id="name" type="text" name="name" required>
                            <label for="name">Name *</label>
                        </div>
                        <div class="form-row">
                            <input id="email" type="email" name="email" required>
                            <label for="email">Email *</label>
                        </div>
                        <div class="form-row">
                            <input id="role" type="text" name="role" required>
                            <label for="role">Role</label>
                        </div>
                        <p>Please contact me about a potential brand building collaboration.</p>
                        <div class="form-row ta">
                            <textarea id="message" name="message" required></textarea>
                            <label for="message">Tell us about your current business issues. The more we know, the better we can assess if a collaboration would offer you a valuable return on investment. Or use our <a href="[~196~]">Creative Brief Engine</a> if you're ready to send us a formal brief.</label>
                        </div>
                        <div class="form-row button">
                            <button>Send</button>
                        </div>
                    </form>
                    <div class="ty">
                        <p class="intro">Thank you for your message. We sincerely appreciate your interest in Stepworks.</p>
                    </div>
                </div>
                <h4><a href="#">I want to build my career</a></h4>
                <div class="contact-expand-content">
                    <h2>Hi there! Before you contact Stepworks, please check out what we do and how we <a href="[~4~]">work together.</a></h2>
                    <h4>Creative roles</h4>
                    <p><a href="mailto:careers@stepworks.co">Email one clickable link</a> to an online introduction about yourself. Share up to 10 examples of your strongest work. Explain your role, and the objectives of each example. Aim to wow us.</p>
                    <p><br></p>
                    <h4>Management roles</h4>
                    <p><a href="mailto:careers@stepworks.co">Email your resume and expected salary.</a> Tell us why you think you’d make a good Wholehearted Brand Builder. Be meticulous.</p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container split">
            <div class="brand">
                <h4>I want to build my brand</h4>
                <div class="split-content">
                    <h2>Get the most from your marketing investment</h2>
                    <p>Who will you work with to build your brand? </p>
                    <p>It may be a leader’s most important marketing budget decision.</p>
                    <p>We’re very frank about any potential – and downside – of a brand building collaboration with Stepworks.</p>
                    <p>Tell us about your current business issues. The more we know, the better we can assess if a collaboration would offer you a valuable return on investment.</p>
                    <form id="contact-form" action="[~182~]">
                        <div class="form-row">
                            <input id="name" type="text" name="name" required>
                            <label for="name">Name *</label>
                        </div>
                        <div class="form-row">
                            <input id="email" type="email" name="email" required>
                            <label for="email">Email *</label>
                        </div>
                        <div class="form-row">
                            <input id="role" type="text" name="role" required>
                            <label for="role">Role</label>
                        </div>
                        <p>Please contact me about a potential brand building collaboration.</p>
                        <div class="form-row ta">
                            <textarea id="message" name="message" required></textarea>
                            <label for="message">Tell us about your current business issues. The more we know, the better we can assess if a collaboration would offer you a valuable return on investment. Or use our <a href="[~196~]">Creative Brief Engine</a> if you're ready to send us a formal brief.</label>
                        </div>
                        <div class="form-row button">
                            <button>Send</button>
                        </div>
                    </form>
                    <div class="ty">
                        <p class="intro">Thank you for your message. We sincerely appreciate your interest in Stepworks.</p>
                    </div>
                </div>
            </div>
            <div class="career">
                <h4>I want to build my career</h4>
                <div class="split-content">
                    <h2>Hi there! Before you contact Stepworks, please check out what we do and how we <a href="[~4~]">work together.</a></h2>
                    <h4>Creative roles</h4>
                    <p><a href="mailto:careers@stepworks.co">Email one clickable link</a> to an online introduction about yourself. Share up to 10 examples of your strongest work. Explain your role, and the objectives of each example. Aim to wow us.</p>
                    <p><br></p>
                    <h4>Management roles</h4>
                    <p><a href="mailto:careers@stepworks.co">Email your resume and expected salary.</a> Tell us why you think you’d make a good Wholehearted Brand Builder. Be meticulous.</p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div id="map" style="height:500px;max-height:50vh;"></div>
    </section>

    <!-- <section class="full-image">
        <img src="assets/images/contact-image.jpg" alt="">
    </section> -->

    [[chunk?&file=`footer.php`]]
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeLAqr1b4yClL2h9mAk1h2tSClMhmr8Fo&callback=initMap"
    type="text/javascript"></script>
    <script>
    var map;
    function initMap() {
        var myLatlng = new google.maps.LatLng(22.280274, 114.15557);
        var styles = [
            {
                "stylers": [
                    {"gamma": 0.5}, //Gama value.
                    {"hue": "#ff0022"}, //Hue value.
                    {"saturation": -100} //Saturation value.
                ]
            }
        ];

        var mapOptions = {
            center: myLatlng,
            scrollwheel: false,
            zoom: 19,
            styles: styles,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },
            streetViewControl: false,
            scaleControl: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            }
        };

        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            icon: '[(site_url)]assets/images/sw-map-marker.png',
            map: map,
        });

        // google.maps.event.addDomListener(window, 'load', initialize);
        google.maps.event.addDomListener(window, "resize", function () {
            var center = map.getCenter();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });
    }
    </script>
    <script>
        $('.brand, .career').on('click', function() {
            $('.split').removeClass(function(idx, className) {
                return (className.match(/(brand|career)-open/g) || []).join(' ');
            });

            $('.split').addClass($(this).attr('class') + '-open');
        });
    </script>
</body>
</html>