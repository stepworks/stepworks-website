[[chunk?&file=`header.php`]]
<body class="page-work new">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    [*content*]
                </div>
            </div>
        </div>
    </section>

    <section class="work-list">
        <div class="container">
            <div class="work-filter">
                <!-- <div class="feat">
                    <a href="#featured" data-param="featured">Featured</a>
                </div> -->
                <div class="all">
                    <a href="#all" data-param="all">All</a>
                </div>
                <div class="list cap">
                    <a href="#">Capability</a>
                    <div class="list-container">
                        <ul>
                            [[listFilter?&list=`[*capability_list*]`&type=`cap`]]
                        </ul>
                    </div>
                </div>
                <div class="list sec">
                    <a href="#">Sector</a>
                    <div class="list-container">
                        <ul>
                            [[listFilter?&list=`[*sector_list*]`&type=`sec`]]
                        </ul>
                    </div>
                </div>
                <div class="list biz">
                    <a href="#">Business objectives</a>
                    <div class="list-container">
                        <ul>
                            [[listFilter?&list=`[*biz_ctx_list*]`&type=`biz`]]
                        </ul>
                    </div>
                </div>
                <div class="list biz">
                    <a href="#">Reach</a>
                    <div class="list-container">
                        <ul>
                            [[listFilter?&list=`[*reach_list*]`&type=`reach`]]
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <h2 class="currentfilter"></h2>
            <div class="project-list" data-filter="">
                <div class="gutter-sizer"></div>
                [!DocLister?
                    &parents=`[[if? &is=`[!currLang!]`:is=`en` &then=`2` &else=`699`]]`
                    &depth=`2`
                    &showParent=`1`
                    &tpl=`work-item`
                    &orderBy=`menuindex ASC`
                    &tvList=`work_cover_image,capability,industry,client_name,biz_ctx,reach`
                    &addWhereList=`template=11 AND hidemenu=0`
                !]
            </div>
        </div>
    </section>

    [[chunk?&file=`footer.php`]]
</body>
</html>