[[chunk?&file=`header.php`]]

<body class="page-home new">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            [*content*]
        </div>
    </section>

    <section class="work">
        <div class="container">
            <div class="work-carousel">
                [[DocLister?&parents=`2`&tpl=`home-work-item`&orderBy=`menuindex
                ASC`&tvList=`work_cover_video,work_cover_image,work_cover_image_md,work_cover_image_sm`&addWhereList=`hidemenu=0`&filters=`tv:featured:is:yes`]]
                <figure class="iframe-wrapper">
                    <div class="iframe-container"><video class="cover" autoplay loop muted playsinline>
                            <source src="assets/media/SW-Brand-Video-Thumbnail-v13.mp4" type="video/mp4">
                        </video>
                        <div id="player"></div>
                    </div>
                    <figcaption>
                        <h2>Stepworks in 60 seconds</h2>
                    </figcaption>
                </figure>
            </div>
            <a class="more" href="[~2~]">See more of our work</a>
        </div>
    </section>

    <section class="results animate">
        <div class="container">
            <h1>Getting results</h1>
            <div class="grid grid-3">
                <div class="cell">
                    [*home_results_text*]
                    <a class="more" href="[~3~]">How we get results</a>
                </div>
                <div class="cell cell-right2 graphic">
                    <video class="d" autoplay muted loop playsinline>
                        <source src="assets/images/approach.mp4" type="video/mp4">
                    </video>
                    <img class="m" src="assets/images/approach-m.png" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="news animate">
        <div class="container">
            <h1>Latest news</h1>
            [*home_news_text*]
            <div class="grid grid-3">
                [[DocLister?&parents=`8`&tpl=`home-news-item`&display=`3`&orderBy=`STR_TO_DATE(longtitle, '%e %M %Y')
                ASC`&addWhereList=`hidemenu=0`]]
            </div>
            <a class="more" href="[~8~]">See all news</a>
        </div>
    </section>

    <div class='video-background'>
        <div id="sw-video"></div>
        <!-- <iframe id='sw-video' width="1280" height="720" src="https://www.youtube.com/embed/uH2BTOO1puc?controls=0&modestbranding=1&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
    </div>

    [[chunk?&file=`footer.php`]]
    <script async>
    var player, playercontainer;

    $(document).ready(function() {
        $('.caps').addClass('show');

        $('.video-background').on('click', function(event) {
            event.preventDefault();
            $('#sw-video').attr('src', '');
            $('body').css('overflow-y', 'auto');
            $(this).fadeOut();
            window.location.hash = '';
        });
    });

    $(window).on('load', function() {
        $('.work-carousel .cover').on('click', function() {
            loadYTscripts();
            playercontainer = 'player';
        });

        if (window.location.hash == '#stepworks-video' || window.location.hash == '#video') {
            loadYTscripts();
            playercontainer = 'sw-video';
            // $('html,body').animate({ scrollTop: $('.work').offset().top + 'px'}, 'slow', function() {
            //     $('.work-carousel .cover').trigger('click');
            //     // window.location.hash = '';
            //     // player.playVideo();
            // });
        }
    });

    function loadYTscripts() {
        if (!$('#yts').length) {
            // console.log('load scripts');
            var tag = document.createElement('script'),
                scripts = document.getElementsByTagName('script'),
                lastScriptTag = scripts[scripts.length - 1];
            tag.src = "https://www.youtube.com/iframe_api";
            tag.id = "yts";
            lastScriptTag.parentNode.insertBefore(tag, lastScriptTag.nextSibling);
        }
    }

    function onYouTubeIframeAPIReady() {
        // console.log('api ready');
        player = new YT.Player(playercontainer, {
            videoId: '[*video_id*]',
            playerVars: {
                rel: 0,
                width: 1280,
                height: 720,
                modestbranding: 1,
                origin: '[(site_url)]',
                controls: 1,
                fs: 1
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    function onPlayerReady(event) {
        // console.log('player ready');
        // console.log('container', playercontainer);
        event.target.setPlaybackQuality('hd1080');
        if (playercontainer == 'player') {
            $('.page-home .work-carousel .cover').fadeOut('fast');
            event.target.playVideo();
            ga('send', 'event', 'Video', 'play', 'Homepage video new');
        } else if (playercontainer = 'sw-video') {
            $('body').css('overflow-y', 'hidden');
            $('.video-background').fadeIn();
            event.target.playVideo();
            ga('send', 'event', 'Video', 'play via hash', 'Homepage video new');
        }
    }

    function onPlayerStateChange(event) {
        // console.log('player state change');
        if (event.data == YT.PlayerState.PLAYING) {
            $('.work-carousel').slick('slickPause');
        }
    }
    </script>
</body>

</html>