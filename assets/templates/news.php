[[chunk?&file=`header.php`]]
<body class="page-news">
    [[chunk?file=`page_header.php`]]

    <section class="intro">
        <div class="container">
            <h1>News</h1>
        </div>
    </section>

    <section class="news-list">
        <div class="container">
            <div class="grid grid-3">
                [[DocLister?&parents=`[*id*]`&depth=`2`&tpl=`news-list-item`&orderBy=`STR_TO_DATE(longtitle, '%e %M %Y') DESC`&addWhereList=`template=6 AND hidemenu=0`]]
            </div>
        </div>
    </section>
    [[chunk?&file=`footer.php`]]
</body>
</html>