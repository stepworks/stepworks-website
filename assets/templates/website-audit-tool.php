[[chunk?&file=`header_brief.php`]]

<body class="page-website-audit-tool">
    [[chunk?file=`page_header_mini.php`]]
    <div id='main-container'>
        <section id="landing">
            <div class="container">
                <h1>Website Assessment Tool</h1>
                <p>Many aspects of digital brand building can be tested and measured. <br />
                    The Stepworks website assessment tool helps you analyse your website<br /> for known factors that influence visitor
                    perceptions.</p>
                <form id='start'>
                    <div class="form-row landing-email">
                        <p>Please share your email so we can send you your website&rsquo;s assessment report.</p>
                        <input type="email" id="email" name="email" placeholder="Email address" required>
                    </div>

                    <div class="form-row">
                        <input type="checkbox" name="terms" id="tos" required><label for="tos">I agree to the <a href="#tos">Terms of
                                use</a></label><br>
                        <input type="checkbox" name="optin" id="optin" value="yes"><label for="optin">Yes, please send me regular brand
                            building insights</label>
                    </div>
                    <div class="button-container block landing-buttons">
                        <input type="submit" value="Let&rsquo;s do this" href="#basic" class="btn next"></input>
                    </div>
                </form>
            </div>
        </section>

        <section id='main' class='hidden'>
            <div class="container">
                <div id="webaudit" class="grid">
                    <div class="cell">
                        <div class='questions-wrapper'>
                            <div id="q1" class="question">
                                <h3>Take 7 minutes to assess your website for digital brand building effectiveness:</h3>
                                <p><strong>Enter your website address</strong></p>
                                <form id="submit" method="post">
                                    <input id="url" type="text" name="url" autocomplete="off" autocorrect="off" spellcheck="false"
                                        autocapitalize="off">
                                    <button id="share-url">Assess website</button>
                                </form>
                                <!-- Loader -->
                                <div id="loader" class='loader-wrapper' style="display:none;">
                                    <div class="loader-inner">
                                        <div class="loader"></div>
                                    </div>
                                    <div class="loader-text">Please wait. We need up to 60 seconds to examine your website.<br>Analysing
                                        <span class="analyse"></span>&hellip;</div>
                                </div>
                                <div id="error" style="display:none;">We can’t scan this website. Websites that can’t be machine-analysed
                                    may be set up incorrectly. Try adding/removing www from the address. Or <a
                                        href="mailto:hello@stepworks.co">Contact us</a> if you’d like to know more.</div>
                            </div>
                            <div class='step-wrapper' id="step1">
                                <div class="question" id="q2">
                                    <h4>Step 1 of 7</h4>
                                    <h3><strong>Do visitors get the right impression from your homepage?</strong></h3>
                                    <p class="desktopss"><img class="loading" src="assets/css/ajax-loader.gif" alt="loading..."></p>
                                    <p class="ssnote"><span class="d">Screenshot not loading? No problem, see it <a href="" class="urlhref"
                                                target="_blank" rel="noopener nofollow">here</a>.</span><span class="m">Screenshot not
                                            loading? No problem, take a look at your website using your laptop, desktop or iPad in landscape
                                            mode.</span></p>
                                    <p><strong>How accurately does your homepage communicate the value of your organisation?</strong></p>
                                    <p class="ssnote">Your self-assessed input here affects the end assessment result.</p>
                                    <p><span>Inaccurate</span><input type="range" name="acc" min="1" max="5" value="3"><span>Very
                                            accurate</span>
                                    </p>
                                    <p class='slider-helper'><span>Adjust your answer</span></p>
                                </div>
                                <div class="answer">
                                    <h4>Brand building tip</h4>
                                    <p>A homepage that immediately gives visitors a strong first impression adds real value. It’s often the
                                        first encounter with your brand for important people like:</p>
                                    <ul>
                                        <li>New customers</li>
                                        <li>Potential recruits</li>
                                        <li>Potential investors and partners</li>
                                        <li>People with influence, such as team family members and friends</li>
                                        <li>Others who need to understand the value of your organisation</li>
                                    </ul>
                                    <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next next-step">Next</a></p>
                                </div>
                            </div>
                            <div class='step-wrapper' id="step2">
                                <div class="question" id="q3">
                                    <h4>Step 2 of 7</h4>
                                    <h3>Is your website helping address business challenges and opportunities?</h3>
                                    <p><strong>Which of these areas are a major concern for your organisation?</strong></p>
                                    <p><label><input type="radio" autocomplete="off" name="vip" value="new and current customers">
                                            Sales</label><label><input type="radio" autocomplete="off" name="vip"
                                                value="current and potential team members"> HR</label><label><input type="radio"
                                                autocomplete="off" name="vip" value="current and potential investors">
                                            Fundraising</label><label><input type="radio" autocomplete="off" name="vip"
                                                value="current and potential supplier partners"> Supply chain</label><label><input
                                                type="radio" autocomplete="off" name="vip" value="current and potential strategic partners">
                                            Partnerships</label><label><input type="radio" autocomplete="off" name="vip"
                                                value="journalists, producers and other influencers"> Media</label>
                                </div>
                                <div class="answer">
                                    <p>This suggests your key stakeholders are <span class="vip" style="font-weight:700;"></span>.</p>
                                    <p>Think about what these people need to understand about your business.</p>
                                    <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next">Next</a></p>
                                </div>
                                <div class="question" id="q4">
                                    <div class="desktopss"><img class="loading" src="assets/css/ajax-loader.gif" alt="loading..."></div>
                                    <p class="ssnote"><span class="d">Screenshot not loading? No problem, see it <a href="" class="urlhref"
                                                target="_blank" rel="noopener nofollow">here</a>.</span><span class="m">Screenshot not
                                            loading? No problem, take a look at your website using your laptop, desktop or iPad in landscape
                                            mode.</span></p>
                                    <p><strong>Can <span class="vip" style="font-weight:700;"></span> immediately see content that&rsquo;s
                                            relevant and appealing to them on your homepage?</strong></p>
                                    <p><span>Slow understanding</span><input type="range" name="und" min="1" max="5" value="3"><span>Instant
                                            understanding</span></p>
                                    <p class='slider-helper'><span>Adjust your answer</span></p>
                                </div>
                                <div class="answer">
                                    <h4>Brand building tip</h4>
                                    <p>People important to you should instantly obtain a relevant and accurate understanding of your
                                        organisation, offering or brand.</p>
                                    <p>Not every visitor is relevant, so precisely focus on what your most valuable visitors need to know.
                                    </p>
                                    <p>Communicate your value in the first few words of headline. Navigation buttons can express value too.
                                    </p>
                                    <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next next-step">Next</a></p>
                                </div>
                            </div>
                            <div class='step-wrapper' id="step3">
                                <div class="question" id="q5">
                                    <h4>Step 3 of 7</h4>
                                    <h3>How well does your website work on mobile devices?</h3>
                                    <div class="mobiless"><img class="loading" src="assets/css/ajax-loader.gif" alt="loading..."></div>
                                    <p class="ssnote"><span class="d">Screenshot not loading? No problem, take a look at your website on
                                            your phone.</span><span class="m">Screenshot not loading? No problem, see it <a href=""
                                                class="urlhref" target="_blank" rel="noopener nofollow">here</a></span></p>
                                    <p><strong>Is your website optimised to convey your most important message on this small mobile
                                            screen?</strong></p>
                                    <label><input type="radio" autocomplete="off" name="mobilemsg" value="0"> No</label><label><input
                                            type="radio" autocomplete="off" name="mobilemsg" value="4"> Yes</label>
                                </div>
                                <div class="answer">
                                    <h4>Brand building tip</h4>
                                    <p>What people need to remember most about your business should be expressed in a simple, clear,
                                        memorable way.</p>
                                    <p>Does it work on a compact mobile screen? That’s how many of your visitors will view it.</p>
                                    <p>A message that’s strong on a small screen can scale up for even bigger impact.</p>
                                    <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next next-step">Next</a></p>
                                </div>
                            </div>
                            <div class='step-wrapper' id="step4">
                                <div class="question" id="q6">
                                    <h4>Step 4 of 7</h4>
                                    <h3>Does your website design work for or against you?</h3>
                                    <p><strong>Does your website design look simple, focused and uncluttered?</strong></p>
                                    <p><span>Cluttered</span><input type="range" name="modern" min="1" max="5"
                                            value="3"><span>Uncluttered</span></p>
                                    <p class='slider-helper'><span>Adjust your answer</span></p>
                                </div>
                                <div class="answer">
                                    <h4>Brand building tip</h4>
                                    <p>People can accurately sense a neglected or unprofessional design. A stagnant look suggests a stagnant
                                        organisation.</p>
                                    <p>Digital design aesthetics accurately communicate when your website was last updated. </p>
                                    <p>Brand builders have learned simplicity and uncluttered clarity are more effective.</p>
                                    <p>We can see this in professionally designed websites, which today look totally different to those
                                        designed ten years ago. </p>
                                    <p>Website design is about maximising message impact through typography. Readability has a profound
                                        effect on visitor understanding.</p>
                                    <p>You might “like” your website design but what do visitors think? What perception do they take away?
                                    </p>
                                    <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next next-step">Next</a></p>
                                </div>
                            </div>
                            <div class='step-wrapper' id="step5">
                                <div class="question" id="q7">
                                    <h4>Step 5 of 7</h4>
                                    <h3>Does your website present your business with consistency?</h3>
                                    <p><strong>How closely does your website match the colours, fonts and images you use in your
                                            presentation decks, brochures, stationery, and workspace design?</strong></p>
                                    <p><span>Bad match</span><input type="range" name="match" min="1" max="5" value="3"><span>Good
                                            match</span></p>
                                    <p class='slider-helper'><span>Adjust your answer</span></p>
                                </div>
                                <div class="answer">
                                    <h4>Brand building tip</h4>
                                    <p>Consistency communicates quality. </p>
                                    <p>Well run companies project a unified brand image that reassures people with a smooth consistent
                                        experience. </p>
                                    <p>Your customer journey should feel seamless.</p>
                                    <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next next-step">Next</a></p>
                                </div>
                            </div>
                            <div class='step-wrapper' id="step6">
                                <div class="question" id="q8">
                                    <h4>Step 6 of 7</h4>
                                    <h3>How regularly do you review your website data?</h3>
                                    <p><label><input type="radio" autocomplete="off" name="review-seo" value="0"> Never</label><label><input
                                                type="radio" autocomplete="off" name="review-seo" value="1"> Every year</label><label><input
                                                type="radio" autocomplete="off" name="review-seo" value="2"> Every 6
                                            months</label><label><input type="radio" autocomplete="off" name="review-seo" value="3"> Every
                                            month</label><label><input type="radio" autocomplete="off" name="review-seo" value="4"> Every
                                            week</label></p>
                                </div>
                                <div class="answer">
                                    <h4>Brand building tip</h4>
                                    <p>Your website is a dynamic channel that can be constantly monitored and improved for effectiveness.
                                    </p>
                                    <p>It’s valuable to understand which data points matter. </p>
                                    <p>Methodically introducing change to your website can have a measurable effect.</p>
                                    <p>Changes to messages and design can often be tested to find the most effective approach.</p>
                                    <p>Leaders should have at least a once-a-month review. Review more often if digital directly impacts
                                        mission critical operations like sales.</p>
                                    <p style="margin-top:2rem;text-align:right;"><a href="#" class="btn next next-step">Next</a></p>
                                </div>
                            </div>
                            <div class='step-wrapper' id="step7">
                                <div class="question" id="q9">
                                    <h4>Step 7 of 7</h4>
                                    <h3>How well does your website handle errors?</h3>
                                    <p>The following <span class="sl">screenshot</span> shows what your audience likely sees when there’s an
                                        error on your site.</p>
                                    <div class="iframe-wrapper">
                                        <img id="404_image" src=""></img>
                                        <p class="ssnote"><span class="d">Screenshot not loading? No problem, see it <a href=""
                                                    class="urlnfhref" target="_blank" rel="noopener nofollow">here</a>.</span><span
                                                class="m">Screenshot not loading? No problem, take a look at your 404 page using your
                                                laptop, desktop or iPad in landscape mode.</span></p>
                                    </div>
                                    <p><strong>How well does your error page (also called a 404 page) express your organisation’s brand?
                                            Score low if the page is missing.</strong></p>
                                    <p><span>It poorly expresses our brand</span><input type="range" name="notfound" min="1" max="5"
                                            value="3"><span>It expresses our brand well</span></p>
                                    <p class='slider-helper'><span>Adjust your answer</span></p>
                                </div>
                                <div class="answer">
                                    <h4>Brand building tip</h4>
                                    <p>Your 404 page appears when the web address is incorrect. Don’t waste it. It’s an opportunity to
                                        inform and brighten your visitor’s experience.</p>
                                    <p><a href="https://stepworks.co/404" target="_blank">Check out ours if you find this idea
                                            puzzling...</a></p>
                                    <p>Reduce the negative impact of an error or problem with an interesting on-brand message.</p>
                                    <p>Your website visitor’s journey is full of opportunities to deliver nice surprises.</p>
                                    <p style="margin-top:2rem;text-align:right;"><a href="#" id="last" class="btn next next-step">Next</a>
                                    </p>
                                </div>
                            </div>
                            <div class="results">
                                <div class='report-header'>
                                    <div class='report-heading'>
                                        <h3>Overall website assessment result</h3>
                                        <p>Here’s how <span class="url" id="url"></span> scored for brand building effectiveness.</p>
                                    </div>
                                    <img class="logo" src="assets/images/sw_logo.svg" alt="Stepworks">
                                </div>

                                <p class="track final-score">
                                    <span class="scorebar overall"><span id="overall-bar" class="score"></span></span><span
                                        id="overall-score" class="score-val"></span><br>
                                </p>
                                <div class="totalbar">
                                    <div class="poor">0-59</div>
                                    <div class="average">60-79</div>
                                    <div class="great">80-100</div>
                                </div>

                                <div class='action-buttons'>
                                    <br />
                                    <a href="#" class="btn" id="print">Save / Print the report</a></p>
                                    <br />
                                </div>

                                <p class="pleasenote">Please note that your score is an approximation of your website brand building
                                    effectiveness. A Stepworks specialist can offer a deeper analysis.</p>

                                <p class='see-other' style="margin:2rem 0 0;"><a href="#" class="showall">See other available assessment
                                        outcomes</a></p>
                                <div class="allresults" style="display:none;margin-top:2rem;">
                                    <p class="result poor"><strong>Poor</strong> – Our preliminary website assessment shows that your
                                        website effectiveness is low. An online experience with your organisation is likely to frustrate and
                                        disappoint visitors. They will form negative perceptions of your organisation. Your current website
                                        may be working against you and not supporting your organisational goals.</p>
                                    <p class="result average"><strong>Average</strong> – Our preliminary website assessment shows that your
                                        website effectiveness is OK. Visitors are likely to experience an adequate online encounter. You
                                        almost certainly have an opportunity for improvement so that your website better supports your
                                        organisational goals.</p>
                                    <p class="result great"><strong>Great</strong> – Well done, our preliminary website assessment shows
                                        that you’re getting it right. Visitors are likely to perceive your organisation as efficient,
                                        helpful and contemporary.</p>
                                    <p><br></p>
                                </div>

                                <!-- <p style="margin:2rem 0 0;"><a href="#" class="breakdown">Show detailed breakdown</a></p> -->
                                <div class="scores" id="brand" style="margin-top:2rem;">
                                    <h4>Here’s a breakdown of your website brand building effectiveness score</h4>
                                    <p>Our automated assessment reflects how likely people will understand the value of your business via
                                        your website. Branding is essentially human, so any machine analysis has limitations – it is a
                                        useful way for brand builders to get a more objective view of their website.</p>
                                    <div class="brand">
                                        <input type="hidden" name="bb" value="">
                                        <p><strong>Website brand building effectiveness score</strong></p>

                                        <p class="track"><strong class="red">Digital brand experience</strong><br>
                                            <span class="scorebar bb-experience"><span id="experience-bar" class="score"></span></span><span
                                                id="experience-score" class="score-val"></span><br>
                                            This score reflects the degree visitors will engage positively with your website and brand based
                                            on modern user expectations.
                                        </p>

                                        <p class="track"><strong class="red">Value delivered to business</strong><br>
                                            <span class="scorebar bb-business"><span id="business-bar" class="score"></span></span><span
                                                id="business-score" class="score-val"></span><br>
                                            This score indicates how well your website builds brand and business value.
                                        </p>

                                        <p class="track"><strong class="red">Design effectiveness</strong><br>
                                            <span class="scorebar bb-design"><span id="design-bar" class="score"></span></span><span
                                                id="design-score" class="score-val"></span><br>
                                            This score indicates how effectively your website’s graphic design and brand identity positively
                                            influence visitors.
                                        </p>
                                    </div>
                                </div>
                                <div class="scores" id="lighthouse">
                                    <div class="lighthouse">
                                        <input type="hidden" name="lh" value="">
                                        <p><strong>Website technical effectiveness score</strong></p>
                                        <p>It’s important to pay attention to the technical side when building an effective and persuasive
                                            website. The following dimensions influence the overall user experience. Scores above [75]
                                            suggest above average effectiveness.</p>

                                        <p class="track"><strong class="red">Performance</strong><br>
                                            <span class="scorebar lh-performance"><span id="performance-bar"
                                                    class="score"></span></span><span id="performance-score" class="score-val"></span><br>
                                            This reflects how quickly visitors see your website pages arrive (people are impatient and
                                            making someone wait forms a poor impression). It might look fast to you, but be slow on some
                                            devices or in some locations.
                                        </p>

                                        <p class="track"><strong class="red">Accessibility</strong><br>
                                            <span class="scorebar lh-accessibility"><span id="accessibility-bar"
                                                    class="score"></span></span><span id="accessibility-score" class="score-val"></span><br>
                                            A website friendly to people with disabilities promotes an inclusive company culture and creates
                                            a positive impression on visitors.
                                        </p>
                                        <p class="track"><strong class="red">Best practices</strong><br>
                                            <span class="scorebar lh-bestpractices"><span id="bestpractices-bar"
                                                    class="score"></span></span> <span id="bestpractices-score"
                                                class="score-val"></span><br>
                                            This checks for common mistakes in websites. Website standards change and this score reflects
                                            your site’s compliance with relevant modern standards.
                                        </p>
                                        <p class="track"><strong class="red">SEO effectiveness</strong><br>
                                            <span class="scorebar lh-seo"><span id="seo-bar" class="score"></span></span> <span
                                                id="seo-score" class="score-val"></span><br>
                                            Search engines aim to determine the relevance of content. A high score indicates your website
                                            content can be correctly categorised for valuable search engine visibility.
                                        </p>
                                    </div>
                                </div>

                                <div class="cta-bottom">
                                    <div class="cell">
                                        <h3>Would you be interested to get our input on how to create more value from your website?</h3>
                                        <p class='content'>Stepworks has considerable experience creating competitive advantages through
                                            developing effective websites that align business, brand and, most important, audience needs.
                                        </p>
                                        <p class='get-in-touch'><a class="btn" href="mailto:hello@stepworks.co">Get in touch</a></p>
                                        <p class='print-only'>Get in touch by phone <a href='tel:+852 3678 8700' class='link'>+852 3678
                                                8700</a> or by email <a href='mailto:hello@stepworks.co'
                                                class='link'>hello@stepworks.co</a>.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='pageNumberOne-tool'>Page 1 of 2</div>
                    <div class='pageNumberTwo-tool'>Page 2 of 2</div>
                </div>
            </div>
        </section>

        <div id="tosmodal" class="popup_modal">
            <div class="popup_modal-content">
                <a href="#" class="close"></a>
                <h2>Terms of use</h2>
                <ol>
                    <li>The following are the terms and conditions for the Use of Stepworks Website Assessment Tool ("the Terms"). Stepworks
                        agrees to provide services of Stepworks Website Assessment Tool ("the Website Assessment Tool") and you, as the user
                        of the Website Assessment Tool, agree to use the services of the Website Assessment Tool in accordance with the
                        Terms set out herein.</li>
                </ol>

                <p><strong>The Website Assessment Tool</strong></p>

                <ol type="1" start="2">
                    <li>The Website Assessment Tool is a website of an integrated online service delivery platform which its user may use
                        any of the services provided therein ("service") in accordance with the Terms set out herein.</li>
                    <li>The Website Assessment Tool services are provided on an “as is” basis. Stepworks may provide procedures for the use
                        of any services from time to time as necessary. Stepworks may add, modify, suspend and/or terminate any services and
                        procedures for use without giving prior notice to any user.</li>
                </ol>

                <p><strong>Website Assessment Tool unique URLs</strong> </p>

                <ol type="1" start="4">
                    <li>You may create a Website Assessment Tool by providing your email address and clicking agreement to the Terms of Use
                        and following the steps herein. By agreeing, you agree to be bound by these Terms of Use. </li>
                    <li>You agree the Website Assessment Tool is only for your own use. You shall not enter an email addresses of another
                        person.</li>
                    <li>Your Website Assessment Tool access creates a unique URL that represents your Website Assessment Tool file in
                        accordance with procedure provided in the Website Assessment Tool.</li>
                    <li>By accessing Stepworks Website Assessment Tool, you may use any specific services of the Website Assessment Tool by
                        opening your unique URL in accordance with procedure provided in the Website Assessment Tool.</li>
                    <li>Keep your unique URL confidential. This provides access to your Website Assessment Tool file.</li>
                </ol>

                <p><strong>Email alert function</strong></p>

                <ol type="1" start="9">
                    <li>Stepworks may send messages to you via the Website Assessment Tool and via the email address you supplied upon
                        account opening.</li>
                    <li>When you revise your email address you may lose access to your Website Assessment Tool file.</li>
                </ol>

                <!-- <li><strong>Maintaining your Website Assessment Tool files</strong></li>
            <ol type="1" start="11">
                <li>You may click the links provided in the service to access website assesment re you are developing.</li>
                <li>If you lose these links you may not be able to access the  creative briefs you are developing.</li>
            </ol> -->

                <p><strong>The Privacy Policy</strong></p>

                <ol type="1" start="11">
                    <li>Stepworks is concerned to ensure all personal data submitted through and stored in the Website Assessment Tool are
                        handled in strict adherence to the relevant provisions of the Personal Data (Privacy) Ordinance (Cap 486), “the
                        PD(P)O” (see www.pcpd.org.hk).</li>
                    <li>Information (including personal data) kept in the personal profile of your Website Assessment Tool account may be
                        retained by Stepworks for matters arising out of, or in relation to, your use of the Website Assessment Tool.</li>
                    <!-- <li>If you maintain personal information in your Website Assessment Tool files, you may access and correct the information/data kept there by logging into your Website Assessment Tool pages. </li> -->
                    <li>You may request access to or correction of your personal data by sending a data access request to Stepworks via
                        email at <a href="mailto:info@stepworks.co">info@stepworks.co</a>.</li>
                    <li>Provision of any of profile data in your Website Assessment Tool file is voluntary.</li>
                    <li>You hereby give consent to Stepworks for the retention of information (including personal data) contained in your
                        Website Assessment Tool file after termination of matters arising out of, or in relation to, the use of the Website
                        Assessment Tool.</li>
                    <!-- <li>A cookie is a small amount of data created in a computer when a person visits a website through the computer. It often includes an anonymous unique identifier. A cookie can be used to identify a computer or to temporarily store non-personal information required by an online service. It is not used by the Creative Brief Engine to collect any personal information. It does not have the function of identifying an individual user of the Creative Brief Engine.</li> -->
                    <!-- <li>The Website Assessment Tool may use cookies. No personal information is contained in the cookies generated by the Website Assessment Tool. If you configure your browser to reject cookies, you will not be able to use the Creative Brief Engine. </li> -->
                    <li>The Website Assessment Tool requires JavaScript to function properly. If you disable JavaScript on your computer,
                        you will not be able to use the Website Assessment Tool.</li>
                    <li>You may input interests in your Website Assessment Tool file. Such information is for Stepworks to consider how to
                        deliver relevant content and improve its services. If you have opted to receive specific information from Stepworks,
                        Stepworks may pass you messages or information of your interest from time to time. </li>
                </ol>

                <p><strong>Disclaimer</strong></p>
                <ol start="18">
                    <li>Unless provided otherwise in the Terms, the information on the Website Assessment Tool is provided to you on an "as
                        is" basis without any express or implied warranty or representation of any kind and is for a general, indicative
                        purpose only. In particular, Stepworks does not make any express or implied warranty or representation as to the
                        accuracy, completeness, fitness for a particular purpose, non-infringement, reliability, security or timeliness of
                        such information. Stepworks will not be liable for any errors in, omissions from, or misstatements or
                        misrepresentations (whether express or implied) concerning any such information, and will not have or accept any
                        liability, obligation or responsibility whatsoever for any loss, destruction or damage (including without limitation
                        consequential loss, destruction or damage) however arising from or in respect of any use or misuse of or reliance
                        on, inability to use or the unavailability of the Website Assessment Tool or any information and services delivered
                        on the Website Assessment Tool. Stepworks does not warrant or represent that the Website Assessment Tool or any
                        information or any electronic data or information transmitted to you through the Website Assessment Tool is free of
                        computer viruses. Stepworks shall not be liable for any loss, destruction or damage arising out of or in relation to
                        any transmission from users to Stepworks or vice versa over the internet.</li>
                    <li>Without limiting the generality of the foregoing, nothing on or provided via the Website Assessment Tool shall
                        constitute, give rise to or otherwise imply any endorsement, approval or recommendation on Stepworks’ part of any
                        third party, be it a provider of goods or services or otherwise.</li>
                    <li>Stepworks is not responsible for any loss or damage whatsoever arising out of or in relation to any information on
                        the Website Assessment Tool. Stepworks reserves the right to omit, suspend or edit all information compiled by
                        Stepworks on the Website Assessment Tool at any time in its sole discretion without giving any reason or prior
                        notice. You are responsible for making your own assessment of all information contained on the Website Assessment
                        Tool and shall verify such information by making reference, for example, to original publications and obtaining
                        independent advice before acting upon it.</li>
                    <li>You shall ensure accuracy of Website Assessment Tool data (including your personal data) kept in your Website
                        Assessment Tool file. Stepworks shall not be responsible/liable for any inaccuracy, mistake or false information
                        (including your personal data) so kept and/or submitted by you.</li>
                    <li>Stepworks does not give any warranty in relation to the use of the Website Assessment Tool and / or any third party
                        websites linked to the Website Assessment Tool, and does not warrant against any possible interference or damage to
                        your computer system arising from the use of the website. Stepworks makes no representations or warranties regarding
                        the accuracy, functionality or performance of any third party software that may be used in connection with the
                        Website Assessment Tool. Stepworks makes no warranties that the Website Assessment Tool is free of infection by
                        computer viruses.</li>
                </ol>

                <p><strong>Security</strong></p>

                <ol type="1" start="23">
                    <li>You must close all browser windows after use to prevent misuse of your Website Assessment Tool account by third
                        parties.</li>
                </ol>

                <p><strong>Copyright</strong></p>

                <ol type="1" start="24">
                    <li>Unless otherwise indicated, the contents found on the Website Assessment Tool are subject to copyright owned by
                        Stepworks.</li>
                    <li>Prior written consent to Stepworks is required if you intend to reproduce, distribute or otherwise use any non-text
                        contents (including but not limited to photographs, graphics, illustrations and drawings, diagrams, audio files and
                        video files) found on the Website Assessment Tool in any way or for any purpose. Such requests for consent must be
                        addressed to Stepworks via email enquiry at <a href="mailto:info@stepworks.co">info@stepworks.co</a>. </li>
                    <li>Where third party copyright (ie. copyright belonging to anyone other than Stepworks is involved in the non-text
                        contents found on the Website Assessment Tool, authorisation or permission to reproduce or distribute r otherwise
                        use any such non-text contents must be obtained from the copyright owners concerned. </li>
                    <li>Stepworks copyright protected text contents found on the Website Assessment Tool may be reproduced and distributed
                        free of charge in any format or medium for personal or internal use within an organisation, subject to the following
                        conditions:
                        <ol>
                            <li>The copy or copies made must not be for sale, or exchanged for benefit, gain, profit, reward or any other
                                commercial purposes</li>
                            <li>Such text contents must be reproduced accurately and must not be used in a manner adversely affecting any
                                moral rights of Stepworks</li>
                            <li>Stepworks must be acknowledged as the copyright owner of such text contents and acknowledgement must be
                                given to “Stepworks (stepworks.co)” as the source of all such text contents. </li>
                        </ol>
                    </li>
                    <li>"Commercial purposes" includes without limitation the following:
                        <ol type="a">
                            <li>the purpose to offer to supply goods, services, facilities, land or an interest in land</li>
                            <li>the purpose to offer to provide a business opportunity or an investment opportunity</li>
                            <li>the purpose to advertise or promote goods, services, facilities, land or an interest in land</li>
                            <li>the purpose to advertise or promote a business opportunity or an investment opportunity</li>
                            <li>the purpose to advertise or promote a supplier, or a prospective supplier, of goods, services, facilities,
                                land or an interest in land</li>
                            <li>the purpose to advertise or promote a provider, or a prospective provider, of a business opportunity or an
                                investment opportunity, in the course of or in the furtherance of any business.</li>
                        </ol>
                    </li>
                    <li>Please note that the permission given in paragraph 33 above only applies to Stepworks copyright protected text
                        contents found on the Website Assessment Tool. Stepworks reserves the right to withdraw any permission given in
                        paragraph 33 above at any time without any prior notice to you.</li>
                    <li>Prior written consent of Stepworks is required if you intend to reproduce, distribute or otherwise use any Stepworks
                        copyright protected text contents found on the Website Assessment Tool in any way other than that permitted in
                        paragraph 33 above, or for any purpose other than that permitted in paragraph 33 above. Such requests shall be
                        addressed to Stepworks via email at info@stepworks.co.</li>
                    <li>Where third party copyright (ie. copyright belonging to anyone other than Stepworks is involved in the text contents
                        found on the Website Assessment Tool, authorisation or permission to reproduce or distribute or otherwise use any
                        such text contents must be obtained from the copyright owners concerned.</li>
                    <li>For the avoidance of doubt, the permission given in paragraph 33 above does not extend to any contents on other
                        websites linked to the Website Assessment Tool. If you intend to reproduce, distribute or otherwise use any contents
                        on any such linked websites, you shall obtain all necessary authorisation or permission from the copyright owners
                        concerned.</li>
                </ol>

                <p><strong>Suspension of your Website Assessment Tool access</strong></p>

                <ol type="1" start="33">
                    <li>If you breach any of the Terms set out herein, Stepworks may suspend your Website Assessment Tool access at any time
                        without giving notice to you.</li>
                    <li>If Stepworks suspects that the email address for your Website Assessment Tool file access is used by any person
                        (other than by yourself) to access your Website Assessment Tool account and / or to access the services in the
                        Website Assessment Tool, Stepworks may suspend your access without notice.</li>
                    <li>Upon suspension, you would not be able to access your Website Assessment Tool files.</li>
                </ol>

                <!-- <p><strong>Termination of a Creative Brief Engine login</strong></p>

            <ol type="1" start="41">
                <li>Stepworks may terminate your Creative Brief Engine access without giving any notice if you breach any of the Terms or it is noted that your Creative Brief Engine access has not been used for a long time (one year). Stepworks may notify you of the termination by sending an email message to the email address provided in the personal profile of your Creative Brief Engine account.</li>
                <li>You may delete your Creative Brief Engine files by contacting Stepworks.</li>
                <li>Upon deletion of your Creative Brief Engine files,
                    <ol type="a">
                        <li>(a) your links will no longer work</li>
                        <li>(b) you cannot access your information any more</li>
                        <li>(c) you may not use any specific services provided in the Creative Brief Engine</li>
                        <li>(d) information related to your Stepworks Creative Brief access may be retained by Stepworks (see paragraphs 36 and 43 above)</li>
                        <li>(e) you may not be able to create new Creative Brief Engine documents in the Creative Brief Engine.</li>
                    </ol>

                    This provision survives termination of your Creative Brief Engine login.
                </li>
            </ol> -->

                <p><strong>Indemnity</strong></p>

                <ol type="1" start="36">
                    <li>You shall indemnify and keep indemnified Stepworks against all claims, actions, proceedings, liabilities, demands,
                        charges, damages, costs, losses or expenses arising out of or resulting from the performance or attempted
                        performance of, the use or attempted use of the services of the Website Assessment Tool to the extent that the same
                        are or have been caused by any negligent or reckless conduct, wilful misconduct, omission, defamation, breach of
                        statutory duty or breach of any of the Terms by you. This provision survives termination of your Website Assessment
                        Tool access.</p>
                </ol>

                <p><strong>Modifications of the Terms</strong></p>

                <ol type="1" start="37">
                    <li>Stepworks may from time to time vary, modify, delete and/or add any terms or conditions in the Terms at its own
                        discretion. The revised Terms (if made) will be displayed to you in the Website Assessment Tool when you access the
                        Website Assessment Tool. You must read the revised Terms. By clicking the checkbox against "I accept the revised
                        Terms (including The Privacy Policy, Disclaimer and Copyright clauses)" in the Website Assessment Tool, you agree to
                        be bound by the revised Terms and the revised Terms will supersede previous versions of the Terms accepted by you.
                    </li>
                </ol>

                <p><strong>Governing Law</strong></p>

                <ol type="1" start="38">
                    <li>The Terms shall be governed and construed in accordance with the laws of the Hong Kong Special Administrative
                        Region. You also agree to submit to the jurisdiction of the Hong Kong Courts.</li>
                </ol>
            </div>
        </div>


        <!-- <div class="progress">
        <div class="bar"></div>
    </div> -->
        <div id="feedback">
            <a href="#" class="btn-feedback"><img src="assets/images/feedback.svg"> <span>Feedback</span></a>
            <div class="report-content">
                <p id="q">What do you think of our Website Assessment Tool?</p>
                <p id="t">Thank you for your feedback!</p>
                <form id="feedback_form">
                    <input type="email" id="feedback_email" name="feedback_email" placeholder="Your contact email" required>
                    <textarea name="feedback" id="feedback_content" class="nomde" placeholder="Message *" required></textarea>
                    <div class="buttons">
                        <button type="submit">Send</button><button class="close">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    [[chunk?&file=`footer.php`]]

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="assets/js/vendor/wNumb.min.js"></script>
    <script src="./assets/js/nouislider.js"></script>
    <script src="./assets/js/web-audit-tool.js"></script>
    <!-- <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-4948962-2', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script> -->
    <script>
    window.onbeforeunload = function() {
        window.scrollTo(0, 0);
    }
    </script>
</body>

</html>