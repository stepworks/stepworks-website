[[chunk?&file=`header.php`]]
<body class="page-wholehearted">
    [[chunk?file=`page_header.php`]]
    <section>
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="super">[*about_page_heading*]</h1>
                    <h2>[*about_page_description*]</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="fast-facts pt0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3">
                    <h1>[*about_fun_facts_heading*]</h1>
                </div>
                <div class="cell cell start3 end6">
                    <div class="grid grid-3">
                        [[multiTv?&tvName=`about_fun_facts`&display=`all`]]
                        <!-- <div class="cell">
                            <img src="assets/images/focus.svg" />
                            <p>
                                <strong>Focus</strong><br>
                                Supporting organisations at times of pivotal change
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/expertise.svg" />
                            <p>
                                <strong>Expertise</strong><br>
                                Brand strategy, messaging frameworks, design, campaigns and digital
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/founded.svg" />
                            <p> <strong>Founded</strong><br>
                                1994
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/base.svg" />
                            <p>
                                <strong>Locations</strong><br>
                                Hong Kong, Singapore
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/reach.svg" />
                            <p>
                                <strong>Reach</strong><br>
                                Global and across cultures
                            </p>
                        </div>
                        <div class="cell">
                            <img src="assets/images/ownership.svg" />
                            <p>
                                <strong>Ownership</strong><br>
                                Partner-owned and operated
                            </p>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="animate mt0 pt0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell cell start3 end5">[*about_magnet_global_text*]</div>
            </div>
        </div>
    </section>
    <section class="animate pb0 mb0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3">
                    <h1>[*about_team_heading*]</h1>
                </div>
                <div class="cell start3 end5">[*about_team_content*]</div>
            </div>
        </div>
    </section>
    <section class="team mt0 pt0 pb0 mb0">
        <div class="container">
            <div class="team-list animate">
                [[DocLister?&parents=`[[if?&is=`[[currLang]]:is:en`&then=`9`&else=`1052`]]`&tpl=`team-item`&orderBy=`RAND()`&tvList=`team_photo`&addWhereList=`hidemenu=0`]]
            </div>
        </div>
    </section>
    <section class="partners animate pt0 pb0">
        <div class="container">
            <div class="grid grid-2 leadership">
                <div class="cell">
                    <img src="assets/images/team/leadership-team.jpg" alt="Stepworks leadership team">
                </div>
                <div class="cell">
                    <h2>[*about_leadership_heading*]</h2>
                    <p>[*about_leadership_text*]</p>
                    <div class="leadership-list">
                        [[DocLister?&parents=`[[if?&is=`[[currLang]]:is:en`&then=`9`&else=`1052`]]`&tpl=`leadership-item`&orderBy=`menuindex ASC`&addWhereList=`template=13`]]
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-values animate pb0 mb0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3">
                    <h1>[*about_value_heading*]</h1>
                </div>
                <div class="cell start3 end6">
                    [*about_value_content*]
                    
                    <div class="values-list">
                        [[multiTv?&tvName=`about_values_list`&display=`all`]]
                        <!-- <h4><a href="#">Together, wholeheartedly</a></h4>
                        <div class="values-content">
                            <p>It’s about the quality and effectiveness of what we do together, and not about our individual egos. We believe in the importance of open honest relationships and encourage sharing. As the proverb goes, “If you want to go fast, go alone. If you want to go far, go together.”</p>
                        </div>

                        <h4><a href="#">Be better, continuously</a></h4>
                        <div class="values-content">
                            <p>We believe in the importance of continuous improvement, kaizen. Always challenge the status quo. This helps us be better in ourselves, as a team, and do better for the clients we serve.</p>
                        </div>

                        <h4><a href="#">Simplify</a></h4>
                        <div class="values-content">
                            <p>Complexity is easy. Simplicity is hard. But simple is smart, effective and more sustainable. We work smart and hard to simplify without being simplistic.</p>
                        </div>

                        <h4><a href="#">Wow our clients, ourselves and others</a></h4>
                        <div class="values-content">
                            <p>Wow! Meticulously thought-through and executed brand building solutions help propel positive change that leads to opportunities and solves problems. We work to wow each other by doing our best.</p>
                        </div>

                        <h4><a href="#">Create value for one another</a></h4>
                        <div class="values-content">
                            <p>We work with purpose to create value for our customers and one another. We recognize success and share the rewards.</p>
                        </div>

                        <h4><a href="#">Love what we do, and love in what we do</a></h4>
                        <div class="values-content">
                            <p>We’re passionate about creating positive change through our brand building skills. We make a conscious effort to be helpful to people – our colleagues, clients, and the charity initiatives we support. We encourage one another to apply kindness, understanding, care and compassion to our thoughts and actions, because why wouldn’t we?</p>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb0">
        <div class="container">
            <div class="grid grid-3">
                <div class="cell cell-left2">
                    <h1 class="super">[*doing_good_page_heading*]</h1>
                    <h2>[*doing_good_page_sub_heading*]</h2>
                </div>
                <div class="cell">
                    <object type="image/svg+xml" data="assets/images/heart_hug.svg"></object>
                </div>
            </div>
        </div>
    </section>

    <section class="animate">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell cell start3 end5">[*doing_good_page_content*]</div>
            </div>
        </div>
    </section>
    <section class="slider-section pt0 mt0">
        <div class="grid grid-6">
            <div class="cell cell start3 end6">
                <div class="dg-slider">
                    <img src="assets/images/socinit-hope-school-0.png" />
                    <img src="assets/images/socinit-hope-school-3.jpeg" />
                    <img src="assets/images/socinit-hope-school-1.png" />
                    <img src="assets/images/socinit-hope-school-2.png" />
                </div>
            </div>
        </div>
    </section>

    <section class="animate pt0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell cell start3 end5">[*doing_good_refugee_content*]</div>
            </div>
        </div>
    </section>
    <section class="slider-section pt0 mt0">
        <div class="grid grid-6">
            <div class="cell cell start3 end6">
                <div class="dg-slider">
                    <img src="assets/images/refugee-union-1.jpeg" />
                    <img src="assets/images/refugee-union-2.jpg" />
                    <img src="assets/images/refugee-union-3.jpg" />
                </div>
            </div>
        </div>
    </section>

    <section class="pt0 mb0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3 icon-initiative" style="display: flex;align-items: center;">
                    <svg style="margin: 0 auto;" width="102" height="102" viewBox="0 0 102 102" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path d="M102 0H0V102H102V0Z" fill="#EB0C38" />
                        <path
                            d="M50.947 65.9652L45.245 78.1959C44.7469 79.2557 44.27 80.3262 43.7612 81.3754C42.7756 83.3573 40.7937 84.131 38.9177 83.2725C37.0418 82.4141 36.1303 80.3686 36.9676 78.3336C38.5786 74.3804 40.2319 70.4378 41.8535 66.4845C42.341 65.3081 42.7332 64.0787 43.2313 62.9022C45.5524 57.497 46.1883 51.7844 46.3049 45.9764C46.3367 44.6516 45.8491 44.0369 44.4713 43.6341C39.3841 42.1503 34.2968 40.6984 29.2095 39.1934C27.111 38.5787 25.9028 36.6815 26.3055 34.795C26.793 32.5693 29.0081 31.4141 31.467 32.1136C34.7313 33.0462 37.985 34.0531 41.2706 34.8798C43.9626 35.5581 46.6864 36.1198 49.4208 36.5437C52.6428 37.0419 55.7481 36.1198 58.8111 35.3355C62.8385 34.2969 66.8342 33.0886 70.8404 31.9652C72.5786 31.4777 74.3485 32.1136 75.26 33.5444C75.7035 34.2288 75.9 35.0442 75.8172 35.8555C75.7343 36.6669 75.3769 37.4256 74.8043 38.0063C74.2309 38.5587 73.5383 38.972 72.7799 39.2146C67.8304 40.7513 62.8492 42.2139 57.8785 43.7189C56.1403 44.2383 55.9707 44.6516 55.9177 46.4639C55.6952 54.7732 58.4932 62.3299 61.6303 69.8124C62.828 72.674 64.0468 75.525 65.2338 78.3972C65.8697 79.9446 65.5624 81.5238 64.4071 82.6366C63.2519 83.7495 61.8953 84.1522 60.4857 83.4421C59.6292 82.9958 58.9273 82.3014 58.472 81.4496C56.0767 76.6379 53.798 71.7732 51.4664 66.9403C51.3498 66.6859 51.2014 66.4421 50.947 65.9652Z"
                            fill="white" />
                        <path
                            d="M51.0212 32.6539C46.9832 32.6645 43.7506 29.4638 43.7506 25.4469C43.7423 24.0132 44.1592 22.6091 44.9488 21.4124C45.7384 20.2156 46.8652 19.2798 48.1866 18.7234C49.508 18.167 50.9647 18.0149 52.3726 18.2864C53.7804 18.5579 55.0761 19.2408 56.0959 20.2487C57.1156 21.2566 57.8136 22.5442 58.1016 23.9488C58.3895 25.3534 58.2545 26.8118 57.7136 28.1396C57.1726 29.4674 56.2501 30.605 55.0627 31.4086C53.8752 32.2121 52.4761 32.6455 51.0424 32.6539H51.0212Z" fill="white" />
                    </svg>

                </div>
                <div class="cell start3 end5">[*doing_good_ accessibility*]</div>
            </div>
        </div>
    </section>
    
    <!-- <section class="animate pt0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell start1 end3 icon-initiative" style="text-align:center;display: flex;align-items: center;justify-content: center;">
                    <a href="https://ecologi.com/stepworks?r=5fc9c04ed451a6001d683f55" target="_blank" rel="noopener noreferrer" title="View our Ecologi profile" style="width:200px;display:inline-block;">
                        <img alt="We offset our carbon footprint via Ecologi" src="https://ecologi-assets.imgix.net/badges/climate-positive-workforce-black-landscape.png" style="width:200px;" />
                    </a>
                </div>
                <div class="cell start3 end5">[*doing_good_climate*]</div>
            </div>
        </div>
    </section> -->


    <section class="animate pt0 mt0">
        <div class="container">
            <div class="grid grid-6">
                <div class="cell cell start1 end5">[*doing_good_get_in_touch*]</div>
            </div>
        </div>
    </section>

    [[chunk?&file=`footer.php`]]
</body>

</html>