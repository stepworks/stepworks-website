[[if?&is=`[*template*]:not_in:8,9,23,26,49,50`&then=`
<section class="contact animate fp-auto-height">
    <div class="container">
        <div class="grid grid-3">
            <div class="cell cell-left2">
                <h1>[[lang?&text=`contact_heading`]]</h1>
            </div>
        </div>
        <div class="grid grid-3">
            <div class="cell">
                <p>
                    <a href="mailto:hello@stepworks.co">hello@stepworks.co</a><br>
                    <a href="tel:+85236788700">+852 3678 8700</a><br><br>
                    <a href="[[if?&is=`[[currLang]]:is:en`&then=`[~6~]`&else=`[~695~]`]]">[[lang?&text=`contact_link`]]</a><br><br>
                    [[lang?&text=`address`]]<br><br>
                </p>
            </div>
        </div>
    </div>
</section>`]]

<section class="bg-red signup-footer animate fp-auto-height">
    <a name="connect"></a>
    <div class="container">
        <div class="grid grid-2">
            <div class="cell">
                <h2>[[lang?&text=`newsletter_heading`]]</h2>
            </div>
            <div class="cell">
                <form id="subForm" class="subscribe-form js-cm-form">
                    <input autocomplete="Email" aria-label="Email" class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200" name="email" required type="email" placeholder="[[lang?&text=`newsletter_field`]]">
                    <input class="name" id="Name" name="name" value="" hidden>
                    <button class="js-cm-submit-button" type="submit">[[lang?&text=`newsletter_button`]]</button>
                </form>
                <div class="ty2" style="display:none;">
                    <p>[[lang?&text=`newsletter_thankyou`]]</p>
                </div>
                <div class="error" style="display:none;">
                    <p>[[lang?&text=`newsletter_error`]]</p>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="fp-auto-height">
    <div class="container">
        <div class="grid grid-6">
            <div class="cell start1 span2 blurb">
                <p>[[lang?&text=`footer_content`]]</p>
                <div class="social">
                    <a href="https://www.instagram.com/wearestepworks/" rel="noopener" target="_blank">Instagram</a>
                    <a href="https://www.linkedin.com/company/stepworks/" rel="noopener" target="_blank">LinkedIn</a>
                </div>
                <div class="copyright d">&copy; [[year]] Stepworks | <a href="[~184~]">[[lang?&text=`legal`]]</a></div>
            </div>
            <div class="cell start4 end7 fnav">
                <ul class="nav" role="navigation">
                    [[DocLister?&parents=`[[langParent]]`&tpl=`top-nav-item`&orderBy=`menuindex ASC`&filters=`AND(tv:checkbox_footer_menu:is:Yes)`&noResults=` `]]
                </ul>
            </div>
            <div class="cbe-link cell">
            [[if?&is=`[[currLang]]:is:en`&then=`Try our <a href="[~196~]" target="_blank">Creative Brief Engine</a> and <a href="[~476~]" target="_blank">Website Brief Engine</a>
                `&else=`欢迎使用我们的<a href="[~196~]" target="_blank">创意简报工具 (Creative Brief Engine)</a> 和<a href="[~476~]" target="_blank">网站简报工具（Website Brief Engine)</a> 。
            `]]
                </div>
            <div class="cbe-link-m">
            [[if?&is=`[[currLang]]:is:en`&then=`Try our <a href="[~196~]" target="_blank">Creative Brief Engine</a> and <a href="[~476~]" target="_blank">Website Brief Engine</a>
                `&else=`欢迎使用我们的<a href="[~196~]" target="_blank">创意简报工具 (Creative Brief Engine)</a> 和<a href="[~476~]" target="_blank">网站简报工具（Website Brief Engine)</a> 。
            `]]</div>
        </div>
        <div class="copyright m">&copy; [[year]] Stepworks | <a href="[~184~]">Legal</a></div>
    </div>
</footer>

<!-- AccessiBe -->
<style>
.acsb-trigger {
    z-index: 9999 !important;
}

.acsb-hero-action {
    border-radius: 0px !important;
    white-space: nowrap !important;
    padding: 9px !important;
}

.acsb-header-option.acsb-header-option-close,
.acsb-header-option.acsb-header-option-position,
.acsb-header-option.acsb-header-option-statement {
    border-radius: 0px !important;
}

.acsb-language.acsb-header-option-language {
    display: none !important;
}

.acsb-range-base.acsb-color-lead {
    background-color: #ffffff !important;
}

div.acsb-widget .acsb-main .acsb-main-options .acsb-actions .acsb-actions-box .acsb-actions-group .acsb-action-box.acsb-action-box-big {
    background-color: #f4f4f4 !important;
}

.acsb-action-box {
    background-color: #f4f4f4 !important;
}

.acsb-range-button {
    border-radius: 0px !important;
}

.acsb-widget .acsb-main .acsb-main-options .acsb-actions .acsb-actions-box .acsb-profiles .acsb-profile .acsb-profile-content i {
    background-color: #f4f4f4 !important;
}

.acsb-trigger.acsb-trigger-position-y-bottom.acsb-mobile {
    bottom: 10px !important;
}

.acsb-trigger.acsb-trigger-position-x-right.acsb-mobile {
    right: 10px !important;
}

/* span.acsb-toggle-option {
    background-color: #f4f4f4 !important;
}

.acsb-toggle-option .acsb-toggle-option-off {
    background-color: #ffffff !important;
} */
</style>

<script src="assets/js/vendor/jquery-3.6.3.min.js"></script>

<script async>
(function() {
    //not run on mobile
    if ($(window).width() > 420) {
    var s = document.createElement('script'),
        e = !document.body ? document.querySelector('footer') : document.body;
    s.src = 'https://acsbapp.com/apps/app/assets/js/acsb.js';
    s.async = s.defer = true;
    s.onload = function() {
        acsbJS.init({
            statementLink: '',
            feedbackLink: '',
            footerHtml: `© ${new Date().getUTCFullYear()} Stepworks`,
            hideMobile: false,
            hideTrigger: false,
            language: 'en',
            position: 'right',
            leadColor: '#eb0c38',
            triggerColor: '#eb0c38',
            triggerRadius: '0%',
            triggerPositionX: 'right',
            triggerPositionY: 'bottom',
            triggerIcon: 'people',
            triggerSize: 'medium',
            triggerOffsetX: 20,
            triggerOffsetY: 20,
            mobile: {
                triggerSize: 'small',
                triggerPositionX: 'right',
                triggerPositionY: 'bottom',
                triggerOffsetX: 10,
                triggerOffsetY: 10,
                triggerRadius: '0'
            }
        });
    };
    e.appendChild(s);
}
}());
</script>
[[if?&is=`[*template*]:in:4,10`&then=`<script defer src="assets/js/yall.min.js"></script>`&else=`<script defer src="assets/js/plugins.js"></script>`]]
[*additionalScripts*]


<?php if (strpos($_SERVER['HTTP_HOST'], 'stepworks.co') !== false) { ?>
    [[if?&is=`[*template*]:in:4`
    &then=`<script defer src='assets/js/home.min.js?v=202405021244'></script>`
    &else=`<script defer src="assets/js/main.min.js?v=202408071244"></script>`
    ]]
<?php } else { ?>
    [[if?&is=`[*template*]:in:4`
    &then=`<script defer src='assets/js/home.js'></script>`
    &else=`<script defer src="assets/js/main.js"></script>`
    ]]
<?php }?>

<?php if (strpos($_SERVER['HTTP_HOST'], 'stepworks.co') !== false) { ?>

<!-- Global site tag (gtag.js) - Google Analytics -->

<script>
  window.addEventListener('load', function() {

    jQuery('body').on('mousedown', '[href*="tel:"]', function() {
      gtag('event', 'conversion', {
        'send_to': 'AW-707539319/1cPhCPKT1tEDEPfisNEC'
      });
    })

    jQuery('body').on('mousedown', '[href*="mailto:"]', function() {
      gtag('event', 'conversion', {
        'send_to': 'AW-707539319/1z4DCMaU1tEDEPfisNEC'
      });
    })

    jQuery('body').on('mousedown', '.js-cm-submit-button', function() {
      gtag('event', 'conversion', {
        'send_to': 'AW-707539319/rCryCOjJ39EDEPfisNEC'
      });
    })

    if (window.location.href.indexOf('/en/contact') != -1 || window.location.href.indexOf('/sc/contact') != -1) {
      gtag('event', 'conversion', {
        'send_to': 'AW-707539319/Do_oCPrD39EDEPfisNEC'
      });
    }

  });

</script>

<!-- Global site tag (gtag.js) - Google Ads: 707539319 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-707539319"></script>
<script async>
window.dataLayer = window.dataLayer || [];

function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());
gtag('config', 'AW-707539319');
</script>

<script type="text/javascript" async> _linkedin_partner_id = "2400628"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script>

<script type="text/javascript" async> (function(l) { if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])}; window.lintrk.q=[]} var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(window.lintrk); 
</script> 

<noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=2400628&fmt=gif" /> </noscript>

<!-- Matomo 
<script async>
var _paq = window._paq = window._paq || [];
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
    var u = "//analytics.stepworks.co/";
    _paq.push(['setTrackerUrl', u + 'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d = document,
        g = d.createElement('script'),
        s = d.getElementsByTagName('script')[0];
    g.async = true;
    g.src = u + 'matomo.js';
    s.parentNode.insertBefore(g, s);
})();
</script>
End Matomo Code -->

<?php } ?>