<?php
  $lang = $modx->runSnippet('currLang', array('full' => '1'));

  $canonical = '[(site_url)][~[*id*]~]';
  
  if($lang == 'zh-Hans'){
    $url = $_SERVER['REQUEST_URI'];
    if (strpos($url, 'sc/insights') !== false) {
      $newUrl = str_replace("/sc/", "/en/", $url);
      $canonical = (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST'] . $newUrl;
    }
  }
?>

<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>[[if? &is=`[*titl*]:empty` &then=`[*pagetitle*]` &else=`[*titl*]`]] | [(site_name)]</title>
    <link rel="canonical" href="<?php echo $canonical; ?>">
    <!--
    =========================================================================================================


     888       888 888               888          888                                888                 888
     888   o   888 888               888          888                                888                 888
     888  d8b  888 888               888          888                                888                 888
     888 d888b 888 88888b.   .d88b.  888  .d88b.  88888b.   .d88b.   8888b.  888d888 888888 .d88b.   .d88888
     888d88888b888 888 "88b d88""88b 888 d8P  Y8b 888 "88b d8P  Y8b     "88b 888P"   888   d8P  Y8b d88" 888
     88888P Y88888 888  888 888  888 888 88888888 888  888 88888888 .d888888 888     888   88888888 888  888
     8888P   Y8888 888  888 Y88..88P 888 Y8b.     888  888 Y8b.     888  888 888     Y88b. Y8b.     Y88b 888
     8882     Y889 88/  880  "Y88P5  88/  "Y8882  880  881  "Y8885  "Y888888 888      "Y888 "Y8888   "Y88888


    =========================================================================================================
    -->
    <base href="[(site_url)]">
    <meta name="description" content="[*desc*]">
    <meta name="keywords" content="[*keyw*]">
    <meta name="geo.position" content="22.280231;114.155575" />
    <meta name="geo.placename" content="29 LKF, 29 Wyndham St, Central, Hong Kong" />
    <meta name="geo.region" content="HK" />

    <meta property="og:type" content="website">
    <meta property="og:url" content="[(site_url)][~[*id*]~]">
    <meta property="og:title" content="[[if? &is=`[*titl*]:empty` &then=`[*pagetitle*]` &else=`[*titl*]`]] | [(site_name)]">
    <meta property="og:description" content="[*desc*]">
    [[if?&is=`[*ogimg*]:notempty`&then=`
    <meta property="og:image" content="[(site_url)][*ogimg*]">`]]

    [[if?&is=`[*template*]:in:51`&then=`
    <link class="slickcss" rel="stylesheet" href="assets/css/slick.min.css" disabled>
    <link class="slickcss" rel="stylesheet" href="assets/css/slick-theme.min.css" disabled>
    `]]
    <link rel="preload" href="assets/css/fonts/5664098/4bd56f95-e7ab-4a32-91fd-b8704cbd38bc.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="assets/css/fonts/5664093/08b57253-2e0d-4c12-9c57-107f6c67bc49.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="assets/css/fonts/5664150/800da3b0-675f-465f-892d-d76cecbdd5b1.woff2" as="font" type="font/woff2" crossorigin>

    [[if?&is=`[*template*]:in:4`&then=`
    <style><?php include ('assets/css/clean.min.css');?></style> <!-- INLINING THE CSS -->
    <!-- <link rel="stylesheet" href="assets/css/clean.min.css?v=17012023"> -->
    `&else=`
    <link rel="stylesheet" href="assets/css/main.min.css?v=20241210032707">
    `]]
    [[if?&is=`[*template*]:in:14`&then=`
    <link rel="stylesheet" href="assets/css/scroll-hint.css">
    `]]
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- <link href="https://www.googletagmanager.com/gtag/js?id=AW-707539319" crossorigin  rel="preload" as="script"> -->
    <link rel="dns-prefetch" href="https://www.googletagmanager.com/" >

    <link href="https://acsbapp.com/apps/app/assets/js/acsb.js" rel="preload" as="script">
    <link rel="dns-prefetch" href="https://acsbapp.com/apps/app/assets/js/acsb.js" >

    <script async defer type="text/javascript">
      (function(c,l,a,r,i,t,y){
          c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
          t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
          y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
      })(window, document, "clarity", "script", "ex41vxizlp");
    </script>
    
    <script>
      var _hmt = _hmt || [];
      (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?ff7ee3bfc4de3fbe7bd3bd1574e424d5";
        var s = document.getElementsByTagName("script")[0]; 
        s.parentNode.insertBefore(hm, s);
      })();
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T2RJ9M88');</script>
    <!-- End Google Tag Manager -->
</head>