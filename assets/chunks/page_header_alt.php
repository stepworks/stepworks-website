<!--[if lte IE 10]>
    <div class="browser">
        <p>Please note that our website is not optimised for your browser</p>
        <p>Don’t worry, this website is still viewable (sort of). But your experience would be improved if you <a href="https://browsehappy.com/" target="_blank">update your browser</a></p>
    </div>
<![endif]-->
<nav>
    <a href="[(site_url)]"><img class="logoflip" src="assets/images/sw_logo_flip.svg" alt=""></a>
        <ul>
            [[DocLister?&parents=`[[UltimateParent?&topLevel=`1`]]`&tpl=`mnav-item-alt`&addWhereList=`hidemenu=0`&orderBy=`menuindex ASC`]]
        </ul>
        <a href="#" class="close"></a>
    </nav>
    <header>
        <div class="container">
            <a href="[(site_url)]" class="logo"><img class="d" src="assets/images/sw_logo.svg" alt="Stepworks"></a>
            <a href="#" class="nav-toggle"></a>
            <ul>
                [[DocLister?&parents=`[[UltimateParent?&topLevel=`1`]]`&tpl=`top-nav-item-alt`&addWhereList=`hidemenu=0`&orderBy=`menuindex ASC`]]
            </ul>
        </div>
    </header>