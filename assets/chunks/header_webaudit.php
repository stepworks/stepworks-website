<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>[[if? &is=`[*titl*]:empty` &then=`[*pagetitle*]` &else=`[*titl*]`]] | [(site_name)]</title>
    <!--
    =========================================================================================================


     888       888 888               888          888                                888                 888
     888   o   888 888               888          888                                888                 888
     888  d8b  888 888               888          888                                888                 888
     888 d888b 888 88888b.   .d88b.  888  .d88b.  88888b.   .d88b.   8888b.  888d888 888888 .d88b.   .d88888
     888d88888b888 888 "88b d88""88b 888 d8P  Y8b 888 "88b d8P  Y8b     "88b 888P"   888   d8P  Y8b d88" 888
     88888P Y88888 888  888 888  888 888 88888888 888  888 88888888 .d888888 888     888   88888888 888  888
     8888P   Y8888 888  888 Y88..88P 888 Y8b.     888  888 Y8b.     888  888 888     Y88b. Y8b.     Y88b 888
     8882     Y889 88/  880  "Y88P5  88/  "Y8882  880  881  "Y8885  "Y888888 888      "Y888 "Y8888   "Y88888


    =========================================================================================================
    -->
    <base href="[(site_url)]">
    <meta name="description" content="[*desc*]">
    <meta name="keywords" content="[*keyw*]">
    <meta name="geo.position" content="22.280231;114.155575" />
    <meta name="geo.placename" content="29 LKF, 29 Wyndham St, Central, Hong Kong" />
    <meta name="geo.region" content="HK" />

    <meta property="og:type" content="website">
    <meta property="og:url" content="[(site_url)][~[*id*]~]">
    <meta property="og:title" content="[[if? &is=`[*titl*]:empty` &then=`[*pagetitle*]` &else=`[*titl*]`]] | [(site_name)]">
    <meta property="og:description" content="[*desc*]">
    [[if?&is=`[*ogimg*]:notempty`&then=`<meta property="og:image" content="[(site_url)][*ogimg*]">`]]

    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="[(site_url)][~[*id*]~]">
    <meta property="twitter:title" content="[[if? &is=`[*titl*]:empty` &then=`[*pagetitle*]` &else=`[*titl*]`]] | [(site_name)]">
    <meta property="twitter:description" content="[*desc*]">
    [[if?&is=`[*ogimg*]:notempty`&then=`<meta property="twitter:image" content="[(site_url)][*ogimg*]">`]]

    <link class="slickcss" rel="stylesheet" href="assets/css/slick.min.css" disabled>
    <link class="slickcss" rel="stylesheet" href="assets/css/slick-theme.min.css" disabled>
    <link rel="stylesheet" href="assets/css/fa-regular.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/css/nouislider.min.css">
    <link rel="stylesheet" href="assets/css/main.min.css?v=202212091125">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--[if gte IE 11]>
    <script src="https://browser.sentry-cdn.com/5.6.2/bundle.min.js" integrity="sha384-H4chu/XQ3ztniOYTpWo+kwec6yx3KQutpNkHiKyeY05XCZwCSap7KSwahg16pzJo" crossorigin="anonymous"></script>
    <script>Sentry.init({ dsn: 'https://93f3bce841844fab919522df6dec8d0b@sentry.io/1545056' });</script>
    -->
    <!-- force GA -->
    <script type="text/javascript">
      (function(c,l,a,r,i,t,y){
          c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
          t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
          y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
      })(window, document, "clarity", "script", "ex41vxizlp");
    </script>
    <script>
      var _hmt = _hmt || [];
      (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?ff7ee3bfc4de3fbe7bd3bd1574e424d5";
        var s = document.getElementsByTagName("script")[0]; 
        s.parentNode.insertBefore(hm, s);
      })();
    </script>
    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
</head>