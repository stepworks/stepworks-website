<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>[[if? &is=`[*titl*]:empty` &then=`[*pagetitle*] | [(site_name)]` &else=`[*titl*]`]]</title>
    <base href="[(site_url)]">
    <meta name="description" content="[*desc*]">

    <meta property="og:type" content="website">
    <meta property="og:url" content="[(site_url)][~[*id*]~]">
    <meta property="og:title" content="[[if? &is=`[*titl*]:empty` &then=`[*pagetitle*]` &else=`[*titl*]`]] | [(site_name)]">
    <meta property="og:description" content="[*desc*]">
    [[if?&is=`[*ogimg*]:notempty`&then=`<meta property="og:image" content="[(site_url)][*ogimg*]">`]]

    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="[(site_url)][~[*id*]~]">
    <meta property="twitter:title" content="[[if? &is=`[*titl*]:empty` &then=`[*pagetitle*]` &else=`[*titl*]`]] | [(site_name)]">
    <meta property="twitter:description" content="[*desc*]">
    [[if?&is=`[*ogimg*]:notempty`&then=`<meta property="twitter:image" content="[(site_url)][*ogimg*]">`]]

    <!-- <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/slick-theme.css"> -->
    <link rel="stylesheet" href="assets/css/main.min.css?v=202401261813">

    <link rel="stylesheet" type="text/css" href="assets/css/fancybox/jquery.fancybox.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="assets/css/fancybox/jquery.fancybox-buttons.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/fancybox/jquery.fancybox-thumbs.min.css" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="assets/css/fa-light.min.css">
    <link rel="stylesheet" href="assets/css/fa-regular.min.css">
    <link rel="stylesheet" href="assets/css/fa-solid.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/css/simplemde.min.css">
    <link rel="stylesheet" href="assets/css/nouislider.min.css">
    <link rel="stylesheet" href="assets/css/select2.min.css">

    <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="manifest.json">
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Global site tag (gtag.js) - Google Ads: 707539319 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-707539319"></script>
    <script async>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
          dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'AW-707539319');
    </script>
    <script type="text/javascript">
      (function(c,l,a,r,i,t,y){
          c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
          t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
          y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
      })(window, document, "clarity", "script", "ex41vxizlp");
    </script>
    <script>
      var _hmt = _hmt || [];
      (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?ff7ee3bfc4de3fbe7bd3bd1574e424d5";
        var s = document.getElementsByTagName("script")[0]; 
        s.parentNode.insertBefore(hm, s);
      })();
    </script>
    <script>
        history.scrollRestoration = "manual"
        let env = 'prod';
        if (window.location.href.indexOf('localhost') >= 0) env = 'local';
    </script>
        <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
</head>