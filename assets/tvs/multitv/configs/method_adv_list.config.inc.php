<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image' => array(
        'caption' => 'Image',
        'type' => 'image'
    ),
    'heading' => array(
        'caption' => 'Heading',
        'type' => 'text'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<div class="cell"><figure><object data="[+image+]" type="image/svg+xml"><img src="[+image+]" alt="[+heading+]"></object><figcaption>[+heading+]</figcaption></figure></div>'
);