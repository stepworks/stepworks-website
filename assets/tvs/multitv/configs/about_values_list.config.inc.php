<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'heading' => array(
        'caption' => 'Heading',
        'type' => 'text'
    ),
    'content' => array(
        'caption' => 'Content',
        'type' => 'textarea'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<h4><a href="#">[+heading+]</a></h4><div class="values-content"><p>[+content+]</p></div>'
);