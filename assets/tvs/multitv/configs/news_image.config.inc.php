<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'type' => array(
        'caption' => 'Type',
        'type' => 'option',
        'elements' => 'Image==image||Video==video'
    ),
    'image' => array(
        'caption' => 'Image/Video',
        'type' => 'image'
    ),
    'alt' => array(
        'caption' => 'Alt text',
        'type' => 'text'
    ),
    'caption' => array(
        'caption' => 'Caption',
        'type' => 'text'
    ),
    'thumb' => array(
        'caption' => 'Thumbnail',
        'type' => 'thumb',
        'thumbof' => 'image'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<figure data-type="[+type+]">[+type:is=`video`:then=`<video autoplay loop muted playsinline data-keepplaying width="100%" height="100%"><source src="[+image+]" type="video/mp4"></video>`:else=`<img src="[+image+]" alt="[+alt+]">`+][+caption:ne=``:then=`<figcaption>[+caption+]</figcaption>`+]</figure>'
);