<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'thumb' => array(
        'caption' => 'Thumbnail',
        'type' => 'thumb',
        'thumbof' => 'image'
    ),
    'image' => array(
        'caption' => 'Image (full width, will override content)',
        'type' => 'image'
    ),
    'align' => array(
        'caption' => 'Image alignment',
        'type' => 'option',
        'elements' => 'Left==left||Right==right'
    ),
    'col1content' => array(
        'caption' => 'Left content (pull quotes)',
        'type' => 'richtext'
        // 'theme' => 'mini'
    ),
    'col2content' => array(
        'caption' => 'Right content',
        'type' => 'richtext'
        // 'theme' => 'mini'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '
        <div data-index="[+iteration+]" class="insight-block-row grid grid-6">
            [[if?&is=`[+image+]:notempty`&then=`
                <figure class="[+align+]"><img src="[+image+]"></figure>
            `&else=`
                <div class="cell start1 end3" style="-ms-grid-column-span:3;">[+col1content+]</div>
                <div class="cell start3 end6" style="-ms-grid-column-span:3;">[+col2content+]</div>
            `]]
        </div>'
);