<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'client_logo' => array(
        'caption' => 'Client Logo',
        'type' => 'image'
    ),
    'client_name' => array(
        'caption' => 'Client Name',
        'type' => 'text'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<img src="[+client_logo+]" alt="[+client_name+]" />'
);