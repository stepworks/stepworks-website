<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'heading' => array(
        'caption' => 'Heading',
        'type' => 'text'
    ),
    'content' => array(
        'caption' => 'Content',
        'type' => 'richtext'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<div class="cap-list-group"><h2>[+heading+]</h2>[+content+]</div>'
);