<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'colcount' => array(
        'caption' => 'Columns',
        'type' => 'option',
        'elements' => 'Single==1||Two columns==2'
    ),
    'col1start' => array(
        'caption' => 'Column 1 start',
        'type' => 'dropdown',
        'elements' => '1==start1||2==start2||3==start3||4==start4||5==start5||6==start6'
    ),
    'col1end' => array(
        'caption' => 'Column 1 end',
        'type' => 'dropdown',
        'elements' => '2==end2||3==end3||4==end4||5==end5||6==end6||7==end7'
    ),
    'col2start' => array(
        'caption' => 'Column 2 start',
        'type' => 'dropdown',
        'elements' => '1==start1||2==start2||3==start3||4==start4||5==start5||6==start6'
    ),
    'col2end' => array(
        'caption' => 'Column 2 end',
        'type' => 'dropdown',
        'elements' => '2==end2||3==end3||4==end4||5==end5||6==end6||7==end7'
    ),
    'col1content' => array(
        'caption' => 'Column 1 content',
        'type' => 'richtext'
        // 'theme' => 'mini'
    ),
    'col2content' => array(
        'caption' => 'Column 2 content',
        'type' => 'richtext'
        // 'theme' => 'mini'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<div class="work-block-row grid grid-6 animate"><div class="cell [+col1start+] [+col1end+]">[+col1content+]</div>[[if?&is=`[+colcount+]:is:2`&then=`<div class="cell [+col2start+] [+col2end+]">[+col2content+]</div>`]]</div>'
);