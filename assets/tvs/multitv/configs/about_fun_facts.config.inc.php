<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'icon' => array(
        'caption' => 'Icon',
        'type' => 'image'
    ),
    'heading' => array(
        'caption' => 'Heading',
        'type' => 'text'
    ),
    'subtext' => array(
        'caption' => 'Sub text',
        'type' => 'text'
    )
);
$settings['templates'] = array(
    'outerTpl' => '[+wrapper+]',
    'rowTpl' => '<div class="cell"><img src="[+icon+]" /><p><strong>[+heading+]</strong><br>[+subtext+]</p></div>'
);