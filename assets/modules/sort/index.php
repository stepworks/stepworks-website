<?php
if(IN_MANAGER_MODE!='true' && !$modx->hasPermission('exec_module')) die('<b>INCLUDE_ORDERING_ERROR</b><br /><br />Please use the MODX Content Manager instead of accessing this file directly.');
$site_url = $modx->config['site_url'];

$filterStr = $_GET['filter'];
$type = explode('|', $filterStr)[0];
$val = explode('|', $filterStr)[1];

?>
<html>
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
        <title>Sort case studies</title>
        <link rel="stylesheet" type="text/css" href="media/style/default/style.css">
        <!--- <link rel="stylesheet" type="text/css" href="media/style/default/store.css" /> -->
        <link rel="stylesheet" href="media/style/common/font-awesome/css/font-awesome.min.css">
        <style>
        .wrapper {
            padding: 1rem;
        }
        select {
            width: auto;
        }
        #list {
            padding: 1rem 0;
            margin: 0;
        }
        #list li {
            border: 1px solid #000;
            padding: 0.5rem 1rem;
            margin-bottom: 0.25rem;
            background-color: #fff;
            cursor: grab;
        }
        #list .ui-sortable-helper {
            cursor: grabbing;
        }
        </style>
    </head>
    <body>
        <h1 class="pagetitle">Sort case studies</h1>
        <div class="wrapper">
            <form id="filterlist" method="get">
                <input type="hidden" name="a" value="<?=$_GET['a']?>">
                <input type="hidden" name="id" value="<?=$_GET['id']?>">
                <input type="hidden" name="action" value="filter">
                <select name="filter" id="filter">
                    <option value="">Select category to sort</option>
                    <option value="all">All</option>
                    <optgroup label="Capabilities">
                    <?php
                        $capres = $modx->db->query("SELECT value FROM fi76_site_tmplvar_contentvalues WHERE contentid=2 and tmplvarid=26");
                        $caprow = $modx->db->getRow($capres);
                        $caprows = explode("<br />", nl2br($caprow['value']));

                        foreach ($caprows as $row) {

                            $label = trim(explode("==", $row)[0]);
                            $value = "cap|" . trim(explode("==", $row)[1]);
                            echo "<option value='".$value."' ".($_GET['filter']==$value?'selected':'').">".$label."</option>" . "\r\n";
                        }
                    ?>
                    </optgroup>
                    <optgroup label="Context">
                    <?php
                        $ctxres = $modx->db->query("SELECT value FROM fi76_site_tmplvar_contentvalues WHERE contentid=2 and tmplvarid=29");
                        $ctxrow = $modx->db->getRow($ctxres);
                        $ctxrows = explode("<br />", nl2br($ctxrow['value']));

                        foreach ($ctxrows as $row) {

                            $label = trim(explode("==", $row)[0]);
                            $value = "ctx|" . trim(explode("==", $row)[1]);
                            echo "<option value='".$value."' ".($_GET['filter']==$value?'selected':'').">".$label."</option>" . "\r\n";
                        }
                    ?>
                    </optgroup>
                    <optgroup label="Sector">
                    <?php
                        $ctxres = $modx->db->query("SELECT value FROM fi76_site_tmplvar_contentvalues WHERE contentid=2 and tmplvarid=31");
                        $ctxrow = $modx->db->getRow($ctxres);
                        $ctxrows = explode("<br />", nl2br($ctxrow['value']));

                        foreach ($ctxrows as $row) {

                            $label = trim(explode("==", $row)[0]);
                            $value = "sec|" . trim(explode("==", $row)[1]);
                            echo "<option value='".$value."' ".($_GET['filter']==$value?'selected':'').">".$label."</option>" . "\r\n";
                        }
                    ?>
                    </optgroup>
                    <optgroup label="Reach">
                    <?php
                        $ctxres = $modx->db->query("SELECT value FROM fi76_site_tmplvar_contentvalues WHERE contentid=2 and tmplvarid=43");
                        $ctxrow = $modx->db->getRow($ctxres);
                        $ctxrows = explode("<br />", nl2br($ctxrow['value']));

                        foreach ($ctxrows as $row) {

                            $label = trim(explode("==", $row)[0]);
                            $value = "rch|" . trim(explode("==", $row)[1]);
                            echo "<option value='".$value."' ".($_GET['filter']==$value?'selected':'').">".$label."</option>" . "\r\n";
                        }
                    ?>
                    </optgroup>
                </select>
            </form>
            <!-- <?=$type.", ".$val?> -->
            <form id="orderlist" action="<?=$site_url?>assets/modules/sort/process.php" method="post">
                <input type="hidden" name="dir" value="<?=MGR_DIR?>/">
                <input type="hidden" name="filter_type" value="<?=$type?>">
                <input type="hidden" name="filter_string" value="<?=$val?>">
                <?php
                    if (!empty($type)) {
                        echo '<ul id="list">' . "\r\n";
                        switch ($type) {
                            case "cap":
                                // check if all case studies that fits the specific filter criteria has a row in the sort order table
                                // if not, add it
                                $modx->db->query("INSERT INTO cs_sort_order (contentid, filter_type, filter_string) SELECT c.id as contentid, 'cap', '$val' FROM fi76_site_content c INNER JOIN fi76_site_tmplvar_contentvalues tv ON(c.id = tv.contentid) WHERE tv.tmplvarid = 27 AND tv.value LIKE '%$val%' AND c.template = 16 AND c.published = 1 AND c.deleted = 0 AND c.id NOT IN (SELECT contentid FROM cs_sort_order o WHERE filter_type='cap' AND filter_string='$val')");

                                $results = $modx->db->query("SELECT c.id AS content_id, c.parent, c.pagetitle, c2.pagetitle AS title, c2.menuindex, o.filter_type, o.filter_string, o.sort_order FROM fi76_site_content c LEFT JOIN cs_sort_order o ON(c.id = o.contentid) INNER JOIN fi76_site_content c2 ON(c.parent = c2.id) INNER JOIN fi76_site_tmplvar_contentvalues tv ON(c.id = tv.contentid) WHERE tv.tmplvarid = 27 AND tv.value LIKE '%$val%' AND c.template = 16 AND c.published = 1 AND c.deleted = 0 AND c2.hidemenu = 0 AND o.filter_type = 'cap' AND o.filter_string = '$val' ORDER BY o.sort_order ASC, c2.menuindex ASC");
                                break;

                            case "ctx":
                                $modx->db->query("INSERT INTO cs_sort_order (contentid, filter_type, filter_string) SELECT c.id as contentid, 'ctx', '$val' FROM fi76_site_content c INNER JOIN fi76_site_tmplvar_contentvalues tv ON(c.id = tv.contentid) WHERE tv.tmplvarid = 30 AND tv.value LIKE '%$val%' AND c.template = 11 AND c.published = 1 AND c.deleted = 0 AND c.id NOT IN (SELECT contentid FROM cs_sort_order o WHERE filter_type='ctx' AND filter_string='$val')");

                                $results = $modx->db->query("SELECT c.id AS content_id, c.pagetitle AS title, c.menuindex, o.filter_type, o.filter_string, o.sort_order FROM fi76_site_content c LEFT JOIN cs_sort_order o ON(c.id = o.contentid) INNER JOIN fi76_site_tmplvar_contentvalues tv ON(c.id = tv.contentid) WHERE tv.tmplvarid = 30 AND tv.value LIKE '%$val%' AND c.template = 11 AND c.published = 1 AND c.deleted = 0 AND c.hidemenu = 0 AND o.filter_type = 'ctx' AND o.filter_string = '$val' ORDER BY o.sort_order ASC, c.menuindex ASC");
                                break;

                            case "sec":
                                $modx->db->query("INSERT INTO cs_sort_order (contentid, filter_type, filter_string) SELECT c.id as contentid, 'sec', '$val' FROM fi76_site_content c INNER JOIN fi76_site_tmplvar_contentvalues tv ON(c.id = tv.contentid) WHERE tv.tmplvarid = 33 AND tv.value LIKE '%$val%' AND c.template = 11 AND c.published = 1 AND c.deleted = 0 AND c.id NOT IN (SELECT contentid FROM cs_sort_order o WHERE filter_type='sec' AND filter_string='$val')");

                                $results = $modx->db->query("SELECT c.id AS content_id, c.pagetitle AS title, c.menuindex, o.filter_type, o.filter_string, o.sort_order FROM fi76_site_content c LEFT JOIN cs_sort_order o ON(c.id = o.contentid) INNER JOIN fi76_site_tmplvar_contentvalues tv ON(c.id = tv.contentid) WHERE tv.tmplvarid = 33 AND tv.value LIKE '%$val%' AND c.template = 11 AND c.published = 1 AND c.deleted = 0 AND c.hidemenu = 0 AND o.filter_type = 'sec' AND o.filter_string = '$val' ORDER BY o.sort_order ASC, c.menuindex ASC");
                                break;

                            case "rch":
                                $modx->db->query("INSERT INTO cs_sort_order (contentid, filter_type, filter_string) SELECT c.id as contentid, 'rch', '$val' FROM fi76_site_content c INNER JOIN fi76_site_tmplvar_contentvalues tv ON(c.id = tv.contentid) WHERE tv.tmplvarid = 44 AND tv.value LIKE '%$val%' AND c.template = 11 AND c.published = 1 AND c.deleted = 0 AND c.id NOT IN (SELECT contentid FROM cs_sort_order o WHERE filter_type='rch' AND filter_string='$val')");

                                $results = $modx->db->query("SELECT c.id AS content_id, c.pagetitle AS title, c.menuindex, o.filter_type, o.filter_string, o.sort_order FROM fi76_site_content c LEFT JOIN cs_sort_order o ON(c.id = o.contentid) INNER JOIN fi76_site_tmplvar_contentvalues tv ON(c.id = tv.contentid) WHERE tv.tmplvarid = 44 AND tv.value LIKE '%$val%' AND c.template = 11 AND c.published = 1 AND c.deleted = 0 AND c.hidemenu = 0 AND o.filter_type = 'rch' AND o.filter_string = '$val' ORDER BY o.sort_order ASC, c.menuindex ASC");
                                break;

                            case "all":
                                $modx->db->query("INSERT INTO cs_sort_order (contentid, filter_type, filter_string) SELECT c.id as contentid, 'all', 'all' FROM fi76_site_content c WHERE c.template = 11 AND c.published = 1 AND c.deleted = 0 AND c.id NOT IN (SELECT contentid FROM cs_sort_order o WHERE filter_type='all' AND filter_string='all')");

                                $results = $modx->db->query("SELECT c.id AS content_id, c.pagetitle AS title, c.menuindex, o.filter_type, o.filter_string, o.sort_order FROM fi76_site_content c LEFT JOIN cs_sort_order o ON (c.id = o.contentid) WHERE c.template = 11 AND c.published = 1 AND c.deleted = 0 AND c.hidemenu = 0 AND o.filter_type = 'all' AND o.filter_string = 'all' ORDER BY o.sort_order ASC, c.menuindex ASC");
                                break;


                            // case "ind":
                            //     $modx->db->query("INSERT INTO cs_sort_order (contentid,filter_type,filter_string) SELECT c.id AS contentid,'ind','$val' FROM fi76_site_content c INNER JOIN fi76_site_content c2 ON (c.parent = c2.id) INNER JOIN fi76_site_tmplvar_contentvalues tv ON (c2.id = tv.contentid) WHERE tv.tmplvarid = 11 AND tv.value LIKE '%$val%' AND c.template = 8 AND c2.template = 7 AND c.published = 1 AND c.deleted = 0 AND c.id NOT IN(SELECT contentid FROM cs_sort_order o WHERE filter_type = 'ind' AND filter_string = '$val')");

                            //     $results = $modx->db->query("SELECT c.id AS content_id, c.pagetitle, c.published, c.deleted, c2.pagetitle AS parent_title, c2.menuindex, c2.published, c2.deleted, o.filter_type, o.filter_string, o.sort_order FROM fi76_site_content c LEFT JOIN cs_sort_order o ON(c.id = o.contentid) INNER JOIN fi76_site_content c2 ON(c.parent = c2.id) INNER JOIN fi76_site_tmplvar_contentvalues tv ON(tv.contentid = c2.id) WHERE c.template = 8 AND c2.template = 7 AND tv.tmplvarid = 11 AND tv.value LIKE '%$val%' AND c.published = 1 AND c.deleted = 0 AND c2.published = 1 AND c2.deleted = 0 AND o.filter_type = '$type' AND o.filter_string = '$val' ORDER BY o.sort_order ASC, c2.menuindex ASC");
                            //     break;
                        }

                        while ($row = $modx->db->getRow($results)) {
                            // $feature = $modx->getTemplateVarOutput('featured', $row['content_id']);
                            // $isFeatured = $feature['featured'] == 'yes' ? '[F] ' : '';
                            echo "<li>";
                            echo '<input type="hidden" name="docid'.$row['content_id'].'" value="">';
                            echo $row['title'];
                            echo "</li>";
                        }

                        echo "</ul>";
                        echo '<button type="submit">Save</button>';
                    }
                ?>

            </form>

        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script>
            function genSortOrder() {
                $('#list li').each(function() {
                    var idx = $(this).index();
                    $(this).find('input[type="hidden"]').val(idx);
                });
            }

            $(document).ready(function() {
                $('#orderlist input[name="url"]').val(location.href);

                $('#filter').on('change', function() {
                    $('#filterlist').submit();
                });

                $('#list').sortable({
                    axis: 'y'
                });
                $('#list').disableSelection();

                $('#orderlist button').on('click', function(e) {
                    e.preventDefault();
                    genSortOrder();

                    var $form = $('#orderlist');

                    $.ajax({
                        url     : $form.attr('action'),
                        type    : 'post',
                        data    : $form.serialize(),
                        success : function(data, status, xhr) {
                            if (data == "success") {
                                alert("Order saved!");
                                location.reload();
                            }
                        }
                    })
                });
            });
        </script>
    </body>
</html>