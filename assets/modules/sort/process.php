<?php
include_once('../../../' . $_POST['dir'] . '/includes/config.inc.php');

$link = mysqli_connect($database_server, $database_user, $database_password, 'db_stepworks_2019');
if (!$link) {
  echo "Error: " . mysqli_connect_error() . PHP_EOL;
  exit;
}

$filter_type = $_POST['filter_type'];
$filter_string = !empty($_POST['filter_string']) ? $_POST['filter_string'] : 'all';

$order = array();

foreach ($_POST as $key => $val) {
  if (substr($key, 0 , 5) == "docid") {
    $order[substr($key, 5)] = $val;
  }
}

$sql = "UPDATE cs_sort_order SET sort_order = ( CASE ";
foreach($order as $item => $val) {
  $sql .= "WHEN contentid='$item' THEN '$val' ";
}
$sql .= " END) WHERE filter_type='$filter_type' AND filter_string='$filter_string'";

$res = mysqli_query($link, $sql);

if ($res === false) {
  echo "failed";
} else {
  echo "success";
}

?>