(function() { 

    var Site = function(){
        var _selector = {
                
            },
            $selector = {};

        // map all static selectors to jquery selectors
        Object.entries(_selector).forEach(([key, value]) => {
            $selector[key] = $(value);
        });

        function init() {
          // add class to body to hide header and reduce padding on main container
          if ($('#website-brief').hasClass('iframe-mode')) {
            $('header').hide();
            $('#main-container').css('padding-top', '2rem');
          }
        }

        return {
            init: init
        }
    }

    var site = new Site();
    site.init();

    
})(jQuery);