(function() {
    // A (possibly faster) way to get the current timestamp as an integer. (taken from Underscorejs)
    function now() {
        return (
            Date.now ||
            function() {
                return new Date().getTime();
            }
        );
    }

    // it delays the execution of a function (taken from Underscorejs)
    function debounce(func, wait, immediate) {
        var timeout, args, context, timestamp, result;

        var later = function() {
            var last = now() - timestamp;
            if (last < wait) {
                timeout = setTimeout(later, wait - last);
            } else {
                timeout = null;

                if (!immediate) {
                    result = func.apply(context, args);
                    context = args = null;
                }
            }
        };

        return function() {
            context = this;
            args = arguments;
            timestamp = now();
            var callNow = immediate && !timeout;

            if (!timeout) {
                timeout = setTimeout(later, wait);
            }

            if (callNow) {
                result = func.apply(context, args);
                context = args = null;
            }

            return result;
        };
    }

    // function isScrolledIntoView(elem, buffer) {
    //     if (elem) {
    //         var docViewTop = $(window).scrollTop();
    //         var docViewBottom = docViewTop + $(window).height();

    //         var elemTop = $(elem).offset().top - buffer;
    //         var elemBottom = elemTop + $(elem).height();

    //         return elemBottom <= docViewBottom && elemTop >= docViewTop;
    //     }
    // }

    // throttle function
    var throttle = function(func, wait, options) {
        var context,
            args,
            result,
            timeout = null,
            previous = 0;

        if (!options) {
            options = {};
        }

        var later = function() {
            previous = options.leading === false ? 0 : new Date().getTime();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) {
                context = args = null;
            }
        };

        return function() {
            var now = new Date().getTime();

            if (!previous && options.leading === false) {
                previous = now;
            }

            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);

                if (!timeout) {
                    context = args = null;
                }
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    };

    var Site = function() {
        var $window = $(window),
            $body = $("body"),
            isIE = /MSIE \d|Trident.*rv:/.test(navigator.userAgent),
            // iPad = /iPad/i.test(navigator.userAgent),

            inittop = 427; // magic number

        //variables needed for hiding header on scroll
        lastScrollTop = 0;
        let didScroll;
        let delta = 250;
        let navbarHeight = $("header").outerHeight();

        // Debounce functions
        var scrollWindow = debounce(scrollControl, 250),
            scrollThrottle = throttle(scrollThrottle, 10),
            resizeWindow = debounce(resizeControl, 250);
        var scrollTableThrottle = throttle(scrollTableThrottle, 100);

        function scrollControl() {}

        function resizeControl() {}

        function scrollThrottle() {
            if (!$body.hasClass("new2")) {
                $(window).scroll(function(event) {
                    didScroll = true;
                });

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 250);
            }

            if ($body.hasClass("page-glossary")) {
                var top = $(".alphatop").offset().top;
                if (top > inittop) {
                    $(".alphatop").addClass("stuck");
                } else {
                    $(".alphatop").removeClass("stuck");
                }
            }

            // work hero fade
            // var ratio = 1 - (($window.scrollTop() / $window.height()) * 0.9);
            // $(".page-work-item .hero img").css("opacity", ratio);

            // insights progress bar
            if ($(".progress").length) {
                var height = Math.round(
                        $body.height() +
                        $("header").outerHeight() -
                        $window.height() -
                        $(".signup").outerHeight() -
                        $("footer").outerHeight()
                    ),
                    pos = $window.scrollTop() / height;

                $(".bar").css("transform", "scaleX(" + pos + ")");
            }

            // reveal animation
            $(".animate:not(.animated,.animating)").each(function(i) {
                var $e = $(this),
                    offset = ($e.offset().top - $window.scrollTop()) / $window.height();

                if (offset < 0.85) {
                    $e.addClass("animating");
                    var delay = i * 200;
                    setTimeout(function() {
                        $e.addClass("animated");
                    }, delay);
                }
            });
        }

        function hasScrolled() {
            let st = $(this).scrollTop();

            if (Math.abs(lastScrollTop - st) <= delta) return;
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                $("header").addClass("shrink");
            } else {
                if (st + $(window).height() < $(document).height()) {
                    $("header").removeClass("shrink");
                }
            }

            lastScrollTop = st;
        }

        // function getLastScrollDirection() {
        //     let delta = 5;
        //     var st = window.pageYOffset || document.documentElement.scrollTop;
        //     if(Math.abs(lastScrollTop - st) <= delta)
        //     return;
        //     var ret = true
        //     return ret;
        // }

        function init() {
            bindUI();
        }

        function bindUI() {
            // $('.video-background').on('click', function (event) {
            //     event.preventDefault();
            //     $('#sw-video').attr('src', '');
            //     $('body').css('overflow-y', 'auto');
            //     $(this).fadeOut();
            //     window.location.hash = '';
            // });

            //GA navigation
            $("header ul li a, footer .nav a").on("click", function() {
                if ("ga" in window) { //for UA
                    tracker = ga.getAll()[0];
                    if (tracker) {
                        tracker.send("event", "navigation", "Main Navigation", $(this).text());
                    }
                }
                if(window.google_tag_manager){ //for GA4
                    gtag("event", "click", {
                        "header_nav": $(this).text()
                    });
                }
            });
            $("footer .nav a").on("click", function() {
                if ("ga" in window) { //for UA
                    tracker = ga.getAll()[0];
                    if (tracker) {
                        tracker.send("event", "footer-nav", "Footer Navigation", $(this).text());
                    }
                }
                if(window.google_tag_manager){ //for GA4
                    gtag("event", "click", {
                        "footer_nav": $(this).text()
                    });
                }
            });

            //homepage filtering
            $("#home-sector").on("click", function() {
                if ($(this).hasClass("open")) {
                    $(this).removeClass("open");
                    $(".home-filters").slideUp();
                } else {
                    $(this).addClass("open")
                    $(".home-filters").slideDown();
                }
            })

            $(".home-filters a").on("click", function() {
                if ("ga" in window) {
                    tracker = ga.getAll()[0];
                    if (tracker) {
                        tracker.send("event", "home-filter", "Home Page Filter Sector", $(this).text());
                    }
                }
                if(window.google_tag_manager){ //for GA4
                    gtag("event", "user_engagement", {
                        "home_sector_filter": $(this).text()
                    });
                }
            });

            if (location.hash != "") {
                $(".animate").addClass("animated");
            }

            // IE fallback
            if (isIE) {
                $(".animate").addClass("animated");
            }

            // navigation
            $(".nav-toggle").on("click", function(e) {
                e.preventDefault();
                $body.toggleClass("nav-open");
            });

            $("nav .close").on("click", function(e) {
                e.preventDefault();
                $body.removeClass("nav-open");
            });

            $("header ul .more > a").on("click", function(e) {
                e.preventDefault();
            });

            //subscription form
            $(".subscribe-form").on("submit", function(e) {
                e.preventDefault();
                var $form = $(this);
                let url = "/createsend-php/subscriber/add.php";
                let emailRegex =
                    /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
                let email_check = emailRegex.test($form.find("#fieldEmail").val());
                if ($form.find(".name").val() !== "") {
                    return;
                }
                if (email_check) {
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: $form.serialize(),
                        success: function(data, status, xhr) {
                            if (data.includes("Subscribed with code 201")) {
                                // save the subscription to localstorage
                                localStorage.setItem("subscribed", "YES");
                                $form.slideUp();
                                $(".ty2").fadeIn();
                                // if ($form.parent().parent().hasClass("box-widget")) {
                                //     setTimeout(function() {
                                //         $(".box-widget").removeClass("visible");
                                //     }, 3000);
                                // }
                            } else {
                                $(".error").fadeIn();
                            }
                        },
                    });
                }
                if(window.google_tag_manager){ // for GA4
                    gtag("event", "user_engagement", {
                        "newsletter_form": location.href
                    });
                }
                if ("ga" in window) {
                    tracker = ga.getAll()[0];
                    if (tracker) {
                        tracker.send("event", "newsletter", "newsletter-signup", location.href);
                    }
                }
            });

            // scroll control
            $window.on("scroll", function() {
                scrollWindow();
                scrollThrottle();
            });

            // resize control
            $window.on("resize", function() {
                resizeWindow();
            });

            $window.on("load", function() {
                scrollThrottle();
            });
        }

        $(".ie-widget .close-widget").on("click", function() {
            $(".ie-widget").removeClass("visible");
            sessionStorage.setItem("hide-ie-widget", "YES");
        });

        let box_show = true;
        //default
        $(".box-widget .close-widget").on("click", function() {
            let parent = $(this).closest(".box-widget");
            if (parent.hasClass("subscription-form-widget")) {
                sessionStorage.setItem("hide-subscription-widget", "YES");
            }
            if (parent.hasClass("ie-widget")) {
                sessionStorage.setItem("hide-ie-widget", "YES");
            }
            $(this).closest(".box-widget").removeClass("visible");
            box_show = false;
        });

        if (isIE) {
            $("html").addClass("internet-explorer");

            var widget_cookie = sessionStorage.getItem("hide-ie-widget");

            if (widget_cookie === null || widget_cookie === "undefined") {
                //adjust the fixed header on internet explorer
                setTimeout(function() {
                    $(".ie-widget").addClass("visible");
                    // $(".subscription-form-widget").removeClass("visible");
                }, 1000);
            }
        }

        document.addEventListener("DOMContentLoaded", yall);

        return {
            init: init,
        };
    };

    var site = new Site();
    site.init();
})(jQuery);