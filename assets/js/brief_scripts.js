var arrMde = [];

function formatNumber(num, places) {
    var ret = null,
        places = places || false;
    if (num != null) {
        if (places) {
            // show 'places' decimal places
            ret = parseFloat(num).toFixed(places);
        } else {
            // whole number
            ret = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    }
    return ret;
}

(function() {
    // A (possibly faster) way to get the current timestamp as an integer. (taken from Underscorejs)
    function now() {
        return Date.now || function() {
            return new Date().getTime();
        }
    }

    // it delays the execution of a function (taken from Underscorejs)
    function debounce(func, wait, immediate) {
        var timeout, args, context, timestamp, result;

        var later = function() {
            var last = now() - timestamp;
            if (last < wait) {
                timeout = setTimeout(later, wait - last);
            } else {
                timeout = null;

                if (!immediate) {
                    result = func.apply(context, args);
                    context = args = null;
                }
            }
        };

        return function() {
            context = this;
            args = arguments;
            timestamp = now();
            var callNow = immediate && !timeout;

            if (!timeout) {
                timeout = setTimeout(later, wait);
            }

            if (callNow) {
                result = func.apply(context, args);
                context = args = null;
            }

            return result;
        };
    }

    // throttle function
    var throttle = function(func, wait, options) {
        var context, args, result,
            timeout = null,
            previous = 0;

        if (!options) {
            options = {};
        }

        var later = function() {
            previous = options.leading === false ? 0 : new Date().getTime();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) {
                context = args = null;
            }
        };

        return function() {
            var now = new Date().getTime();

            if (!previous && options.leading === false) {
                previous = now;
            }

            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);

                if (!timeout) {
                    context = args = null;
                }

            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    };

    var Site = function() {
        var $window = $(window),
            // $body = $('body'),
            $form = $('#form-brief'),
            // winWidth = $window.width(),
            firstLoad = true,
            autoScroll = false,
            mdeLoaded = false,
            ident = false,
            email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            processorUrl = (env === 'local' ? '/stepworks-website' : '') + '/en/brief_processor';

        // Debounce functions
        var scrollWindow = debounce(scrollControl, 250),
            scrollThrottle = throttle(scrollThrottle, 500),
            resizeWindow = debounce(resizeControl, 250);

        function scrollControl() {

        }

        function resizeControl() {

        }

        function scrollThrottle() {
            // if (!autoScroll) {
            //     $('#form-brief section').each(function() {
            //         let $sect = $(this),
            //             ratio = ( $(this).offset().top - $window.scrollTop() ) / $window.height();
            //         if (ratio < 0.6 && ratio > 0 && !$(this).hasClass('active')) {
            //             $(this).addClass('active').siblings().removeClass('active');
            //             // addhash($sect.attr('id'));
            //             $('#container').removeAttr('class').addClass('page-' + $sect.attr('id'));
            //         }
            //     });
            // }
            // return false;
        }

        // e.g. goToSect('basic');
        function goToSect(sect) {

            if (sect.indexOf('#') == 0) {
                sect = sect.substr(1);
            }

            if (sect != 'summary') {
                var $sect = $('section#' + sect);

                if (!$sect.hasClass('active')) {
                    $('section').not($sect).removeClass('active');
                    $sect.addClass('active');
                    // addhash($sect.attr('id'));
                    $('#container').removeAttr('class').addClass('page-' + sect);

                    var offset = $sect.offset().top - $('header').height();

                    if (firstLoad) {
                        setTimeout(function() {
                            $window.scrollTop(offset);
                        }, 100);
                    } else {
                        autoScroll = true;
                        $('html,body').animate({
                            scrollTop: offset + 'px'
                        }, 'slow', function() {
                            autoScroll = false;
                        });
                    }

                    // focuses first input field in the section
                    setTimeout(function() {
                        $sect.find('input:first, textarea:first, [contenteditable]:first').focus();
                    }, 1000);
                }
            } else {
                $('#summary').addClass('open');
                $('body').addClass('summary-open');
            }
        }

        function isValid(section) {
            var $sect = $('section#' + section),
                valid = true;

            $sect.find('[required]').each(function() {
                if ($(this).val().trim() == "") {
                    $(this).addClass('error');
                    valid = false;
                } else {
                    $(this).removeClass('error');
                }
            });

            return valid;
        }

        // takes the current section and the targeted next section
        // returns the next section name
        function process(currSection, nextSection) {
            var $sect = $('section#' + currSection),
                $next = $('section#' + nextSection);

            switch (currSection) {
                case "landing":
                    break;
                case "ident":
                    if (isValid(currSection)) {
                        if (window.localStorage !== undefined) {
                            window.localStorage.setItem('be-ident-name', $sect.find('input#name').val());
                            window.localStorage.setItem('be-ident-email', $sect.find('input#email').val());
                        }
                    } else {
                        $('section#' + currSection + ' .error').addClass('shake').delay(1000).queue(function() {
                            $(this).removeClass('shake').dequeue();
                        });
                        nextSection = currSection;
                    }
                    break;
                case "library":
                    break;
                case "basic":
                    if ($('#ident_email').val() != '') {
                        var data = {
                            action:         'email',
                            email:          $('#ident_email').val(),
                            brief_id:       $('#brief_id').val(),
                            view_id:        $('#view_id').val(),
                            project_name:   $('#project-name').val(),
                            client_name:    $('#client-name').val()
                        };
                        $.post(processorUrl, data).done(function(response) {
                            // $btn.removeClass('wait');
                            // $('#email-links').addClass('alt');
                            // setTimeout(function() { $('#email-links').removeClass('alt'); }, 3000);
                        });
                    }
                    break;
                case "q-audience":
                    var knowledge = $('[name="audience-knowledge"]:checked').val(),
                        output = "",
                        checked = [];
                    switch (knowledge) {
                        case "known":
                            output += "Our audience know our brand: ";
                            $('[name="known-list[]"]:checked').each(function() {
                                checked.push($(this).val());
                            });
                            break;
                        case "unknown":
                            output += "Our audience don't know our brand: ";
                            $('[name="unknown-list[]"]:checked').each(function() {
                                checked.push($(this).val());
                            });
                            break;
                        case "halfknown":
                            output += "Some of our audience know us, some don't: ";
                            $('[name="halfknown-list[]"]:checked').each(function() {
                                checked.push($(this).val());
                            });
                            break;
                    }
                    output += checked.join(', ').toLowerCase();
                    $('.from-audience').text(output);
                    break;
                case "q-promise":
                    // console.log('process promise');
                    $('#from-promise').text(function() {
                        switch ($('[name="promise_type"]:checked').val()) {
                            case 'promise_text':
                                return $('#input-promise').val();
                                break;
                            case 'promise_fill':
                                var str = "",
                                    $label = $('[name="promise-choice"]:checked').next('label');
                                $($label[0].childNodes).each(function() {
                                    if ($(this)[0].nodeType == 3) {
                                        str += $(this)[0].nodeValue;
                                    } else if ($(this)[0].nodeType == 1) {
                                        str += $(this).val() != '' ? $(this).val() : '<' + $(this).attr('placeholder') + '>';
                                    }
                                });
                                $('#input-promise').val(str);
                                $('#promise_text').trigger('click');
                                return str;
                                break;
                        }
                    });
                    break;
                case "q-belief":

            }

            if ($sect.find('textarea').length > 0) {
                var ta = $sect.find('textarea').get(0);
                for (let i = 0; i < arrMde.length; i++) {
                    if (arrMde[i].element == ta) {
                        $(ta).val(arrMde[i].value());
                        break;
                    }
                }
            }

            if (isValid(currSection)) {
                $('.modal.open').removeClass('open');
                if (currSection == 'basic') {
                    $('body').removeClass('locked');
                }
            } else {
                $('section#' + currSection + ' .error').addClass('shake').delay(1000).queue(function() {
                    $(this).removeClass('shake').dequeue();
                });
                nextSection = currSection;
                save2(ident);
            }

            return nextSection;
        }

        function load() {
          // console.log('load');
          // console.log('id: ' + $('#brief_id').val());
          var data = {
              action: 'load',
              brief_id: $("#brief_id").val(),
          };

          $.post(processorUrl, data).done(function(response) {
              data = JSON.parse(response);
              if (data.status === 'success') {

                  ident = true;

                  var dataObj = JSON.parse(decodeURIComponent(data.json));

                  $('#ident_email').val(data.ident_email);
                  $('#sum_lastmod').html(data.modified == null ? '' : data.modified);
                  if (data.view_id != null) {
                      $('#view_id').val(data.view_id);
                  }
                  $('#brief_id').val(data.brief_id);
                  location.hash = '#' + data.brief_id;

                  for (var key in dataObj) {
                      var $input = $('[name="' + key + '"]'),
                          val = dataObj[key];

                      // handle multiple answers i.e. checkboxes
                      if (key.indexOf('[]') != -1) {

                          // skip schedule and belief
                          if (key.indexOf('sch') == -1 && key.indexOf('belief') == -1) {
                              var valArr = val.split('||');
                              for (var i = 0; i < valArr.length; i++) {
                                  $input.filter('[value="' + valArr[i] + '"]').prop('checked', true);
                              }
                          }
                      } else {
                          switch ($input.attr('type')) {
                              case 'radio':
                                  $input.filter('[value="' + val + '"]').prop('checked', true);
                                  break;
                              case 'text':
                                  $input.val(val);
                                  break;
                              default:
                                  // if ($input.prop('tagName').toLowerCase() == 'textarea') {
                                  $input.val(val);
                                  // }
                          }
                      }
                  }

                  // console.log(dataObj);

                  // handle schedule items
                  if (dataObj.hasOwnProperty('schItem[]')) {
                      var items = dataObj['schItem[]'].split('||'),
                          times = dataObj['schTime[]'].split('||');

                      // sanity check
                      if (items.length == times.length) {
                          var $tbl = $('#q-deliverables .tblSchedule tbody');
                          $tbl.empty();

                          for (var i = 0; i < items.length; i++) {
                              $tbl.append('<tr><td class="handle"><i class="far fa-sort"></i></td><td><input spellcheck="true" placeholder="Item" type="text" name="schItem[]" value="' + items[i].trim() + '"></td><td><input spellcheck="true" placeholder="Launch date" type="text" name="schTime[]" value="' + times[i].trim() + '"></td></tr>');
                          }
                      }
                  }

                  // handle belief reasons
                  if (dataObj.hasOwnProperty('belief-reason[]')) {
                      var reasonArr = dataObj['belief-reason[]'].split('||'),
                          $list = $('.reason-list');

                      $list.empty();

                      for (var i = 0; i < reasonArr.length; i++) {
                          $list.append('<div class="reason"><div class="handle"><i class="far fa-sort"></i></div> <input spellcheck="true" type="text" name="belief-reason[]" value="' + reasonArr[i] + '"></div>');
                      }
                  }

                  // handle sliders
                  if (dataObj.hasOwnProperty('gender-ratio')) {
                      var gr = dataObj['gender-ratio'];
                      grVal = gr.split("/")[0].trim();
                      $('#gender').text(gr);
                      $('.gender_slider').slider('option', 'value', grVal);
                  } else {
                      $('#gender').text('50 / 50');
                      $('.gender_slider').slider('option', 'value', 50);
                  }

                  if (dataObj.hasOwnProperty('agemin') && dataObj.hasOwnProperty('agemax')) {
                      var min = dataObj['agemin'].trim(),
                          max = dataObj['agemax'].trim();

                      if (min.length > 0 && max.length > 0) {
                          $('.age_slider').slider('option', 'values', [min, max]);
                          $('#agemin').text(min);
                          $('#agemax').text(max);
                      } else {
                          $('.age_slider').slider('option', 'values', [20, 35]);
                          $('#agemin').text('20');
                          $('#agemax').text('35');
                      }
                  }

                  if (dataObj.hasOwnProperty('currency') && dataObj['currency'] != '') {
                      $('#currency-' + dataObj['currency']).trigger('click');
                  }

                  // if (dataObj.hasOwnProperty('budget-min') && dataObj.hasOwnProperty('budget-max')) {
                  //     var min = dataObj['budget-min'].trim(),
                  //         max = dataObj['budget-max'].trim();

                  //     if (min.length > 0 && max.length > 0) {
                  //         minval = parseFloat(min);
                  //         maxval = parseFloat(max);

                  //         minval = minval % 1 === 0 ? minval : minval * 1000;
                  //         maxval = maxval % 1 === 0 ? maxval : maxval * 1000;

                  //         $('.budget_slider').slider('option', 'values', [minval, maxval]);
                  //         $('#budget-min').text(min);
                  //         $('#budget-max').text(max);
                  //     } else {
                  //         $('.budget_slider').slider('option', 'values', [20,40]);
                  //         $('#budget-min').text('20k');
                  //         $('#budget-max').text('40k');
                  //     }

                  // }

                  // if (dataObj.hasOwnProperty('prodbud-min') && dataObj.hasOwnProperty('prodbud-max')) {
                  //     var min = dataObj['prodbud-min'].trim(),
                  //         max = dataObj['prodbud-max'].trim();

                  //     if (min.length > 0 && max.length > 0) {

                  //         minval = parseInt(min);
                  //         maxval = parseInt(max);

                  //         $('.prodbudget_slider').slider('option', 'values', [minval, maxval]);
                  //         $('#prodbud-min').text(min);
                  //         $('#prodbud-max').text(max);
                  //     } else {
                  //         $('.prodbudget_slider').slider('option', 'values', [10,20]);
                  //         $('#prodbud-min').text('20k');
                  //         $('#prodbud-max').text('40k');
                  //     }
                  // }

                  summary();

                  $('body').removeClass('locked');
                  goToSect('summary');
              } else {
                  if (data.status == 'fail') {
                      alert('Error\r\nBrief not found.');
                      $('#brief_id').val('');
                  }
              }


              loadMde();
          });
        }

        function share_sw() {
          var data = {
            action:         'share-sw',
            email:          $('#ident_email').val(),
            client_name:    $('#client-name').val(),
            project_name:   $('#project-name').val(),
            brief_id:       $('#brief_id').val()
          };
          $.post(processorUrl, data).done(function(response) {
              console.log(response);
          });
        }

        function summary() {

            // var m = new SimpleMDE();
            var m = arrMde[0];

            // basic
            $('#sum_brief_title').html($('#project-name').val() != '' ? $('#project-name').val() : 'Creative brief');
            $("#sum_project-name").html($("#project-name").val());
            $("#sum_client-name").html($("#client-name").val());
            $("#sum_client-contact").html($("#client-contact").val());
            $("#sum_ref").html($("#ref").val());
            $("#sum_date").html($("#date").val());
            // $('#sum_prep_by').html($('#email').val());

            // objectives
            $("#sum_objectives").html( m.options.previewRender( $("#input-objectives").val() ) );

            // deliverables
            $("#sum_deliverables").html(function() {
                var $tbl = $('.tblSchedule tbody'),
                    output =  "<table><thead><tr><th>Due date</th><th>Item</th></tr></thead>";
                output += "<tbody>";
                $tbl.children('tr').each(function() {
                    output += "<tr><td>" + $(this).find('[name="schTime[]"]').val() + "</td><td>" + $(this).find('[name="schItem[]"]').val().trim() + "</td></tr>";
                    // output += $(this).find('[name="schItem[]"]').val() + ": " + $(this).find('[name="schTime[]"]').val() + "<br>";
                });
                output += "</tbody></table>";

                return output;
            });

            // audience
            $("#sum_audience").html(function() {
                var output = "<p>";

                switch ($('[name="audience-personas"]:checked').val()) {
                    case 'yes':
                        output += "I have developed audience personas (see Notes)";
                        break;
                    case 'no':
                        output += "I'm trying to reach: ";
                        $('[name="audience-reach[]"]:checked').each(function() {
                            output += "<br>- " + $(this).val();
                        });
                        output += "<br>";

                        switch ($('[name="audience-knowledge"]:checked')) {
                            case 'known':
                                output += "They already know us.";
                                break;
                            case 'unknown':
                                output += "They don't know us.";
                                break;
                            case 'halfknown':
                                output += "Some know us, some don't.";
                                break;
                        }
                        output += "<br>";

                        output += "Additional information:<br>";
                        output += "Male / female ratio: " + $('[name="gender-ratio"]').val() + "<br>";
                        output += "Age range: " + $('[name="agemin"]').val() + ' - ' + $('[name="agemax"]').val() + "<br>";
                        output += m.options.previewRender( $('#input-more-about-audience').val() );

                        break;
                }

                return output + "</p>";
            });

            // budget
            // $("#sum_budget").html(function() {
            //     var currency = $('[name="currency"]:checked').val().toUpperCase();

            //     // budget = budget.map(function(x) { return x.indexOf('k') > 0 ? parseInt(x) * 1000 : x.indexOf('m') > 0 ? parseFloat(x) * 1000000 : parseInt(x); });

            //     // var mBudget = (budget[0] + budget[1]) / 2,
            //     //     mProd = (budget[2] + budget[3]) / 2,
            //     //     med = mBudget * (mProd / 100);

            //     output = "<p>";
            //     output += "Total campaign budget:<br>";
            //     output += currency + $('[name="budget-min"]').val() + ' to ' + $('[name="budget-max"]').val() + '<br><br>';
            //     output += "Production budget:<br>";
            //     output += $('[name="prodbud-min"]').val() + ' to ' + $('[name="prodbud-max"]').val() + ' of the above<br>';
            //     output += "Approximately: " + $('#budest').text();
            //     output += "<br><span class='small'>(median of campaign budget x production budget)</span>";
            //     output += "</p>";
            //     return output;
            // });

            // promise
            $("#sum_promise").html(function() {
                return $('#input-promise').val();
                // var output;

                // switch ($('[name="promise_type"]:checked')) {
                //     case 'promise_text':
                //         output = m.options.previewRender( $("#input-promise").val() );
                //         break;
                //     case 'promise_fill':
                //         console.log('fill');
                //         var str = "",
                //             $label = $('[name="promise-choice"]:checked').next('label');
                //         $($label[0].childNodes).each(function() {
                //             if ($(this)[0].nodeType == 3) {
                //                 str += $(this)[0].nodeValue;
                //             } else if ($(this)[0].nodeType == 1) {
                //                 str += $(this).val() != '' ? $(this).val() : '<' + $(this).attr('placeholder') + '>';
                //             }
                //         });
                //         output = str;
                //         break;
                // }
                // return output;
            });

            // background
            $("#sum_background").html( m.options.previewRender( $("#input-background").val() ) );

            // channel
            $("#sum_channels").html( m.options.previewRender( $("#input-channels").val() ) );

            // reasons to believe
            $("#sum_belief").html(function() {
                var output = "";
                $('[name="belief-reason[]"]').each(function() {
                    if ($(this).val() != '') {
                        output += "<p>" + $(this).val() + "</p>";
                    }
                });
                return output;
            });

            // attitude
            // $("#sum_attitude").html(function() {
            //     var output = "<p>My brand, as a person, is: ",
            //         checked = [];
            //     $('[name="attitude[]"]:checked').each(function() {
            //         if ($(this).val() == 'Others') {
            //             checked.push('Others: ' + $(this).nextAll('.sub-row').find('input').val());
            //         } else {
            //             checked.push($(this).val().toLowerCase());
            //         }
            //     });

            //     switch (checked.length) {
            //         case 3:
            //             output += checked[0] + ', ' + checked[1] + ', and ' + checked[2];
            //             break;
            //         case 2:
            //             output += checked[0] + ' and ' + checked[1];
            //             break;
            //         case 1:
            //             output += checked[0];
            //             break;
            //     }
            //     return output + "</p>";
            // });

            // personality
            $('#sum_personality').html($('[name="personality"]:checked').val());

            // response
            $("#sum_response").html(function() {
                var output = "";
                $('[name="response[]"]:checked').each(function() {

                    if ($(this).attr('id') == "response8") {
                        output += "I'll act on this: " + $('[name="custom_response"]').val() + "<br>";
                    } else {
                        output += $(this).val() + "<br>";
                    }
                });
                return output;
            });

            // metrics
            $('#sum_metrics').html(function() {
                var output = "";
                $('[name="metric[]"]:checked').each(function() {

                    if ($(this).attr('id') == "metric5") {
                        output += "Response rates: " + $('[name="custom_metric"]').val() + "<br>";
                    } else if ($(this).attr('id') == "metric6") {
                        output += "Others: " + $('[name="custom_metric_other"]').val() + "<br>";
                    } else {
                        output += $(this).val() + "<br>";
                    }
                });
                return output;
            });

            // mandatory
            $("#sum_mandatory").html(function() {
                var output = "";
                $('[name="mandatory[]"]:checked').each(function() {

                    if ($(this).attr('id') == "mandatory5") {
                        output += "More:<br>" + m.options.previewRender($('[name="custom_mandatory"]').val());
                    } else {
                        output += $(this).val() + "<br>";
                    }
                });
                return output;
            });

            // notes
            $("#sum_notes").html(m.options.previewRender($("#input-notes").val()));

            // populateBriefId($('#brief_id').val());

            var url = location.protocol + '//' + location.hostname + location.pathname + '#';
            var shareurl = location.protocol + '//' + location.hostname + '/en/view-brief?id=';
            $('#share_edit').val(shareurl + $('#view_id').val());
            if ($('#view_id') != '') {
                $('#share_view').val(url + $('#brief_id').val());
                $('#share_view').show();
            } else {
                $('#share_view').hide();
            }
        }

        function loadMde() {
            if (!mdeLoaded) {

                $('textarea').not('.nomde').each(function(i) {
                    $(this).data('index', i);
                    var mde = new SimpleMDE({
                        element: $(this).get(0),
                        forceSync: true,
                        hideIcons: ['guide','side-by-side','fullscreen'],
                        spellChecker: true,
                        status: ["lines", "words"]
                    });
                    // console.log($(this).val());
                    mde.value($(this).val());
                    arrMde.push(mde);
                });

                mdeLoaded = true;
            } else { // update its content
                $('textarea').each(function () {
                    if ($(this).closest('.CodeMirror').length == 0 && $(this).data('index') != undefined) {
                        var idx = parseInt($(this).data('index'));
                        arrMde[idx].value($(this).val());
                    }
                });
            }
        }

        function save2(hasIdent) {
            var dataRaw = $('form').serialize(),
                dataArr = dataRaw.split("&"),
                dataObj = {};

            for (var i = 0; i < dataArr.length; i++) {
                var grep = dataArr[i].substr(0, 4);
                switch (grep) {
                    case 'note':
                    default:
                        var pair = decodeURIComponent(dataArr[i]).split('=');
                        break;

                }
                var key = pair[0],
                    val = pair[1] == "" ? " " : pair[1];

                // ignore basic info
                var basics = ['action', 'brief_id', 'project_name', 'client_name', 'client_contact', 'ref', 'date', 'ident_email', 'returning-email', 'email'];
                if (basics.indexOf(key) != -1) {
                    continue;
                }

                // check if it's part of an array of answers i.e. checkboxes
                if (key.indexOf('[]') != -1) {
                    if (!dataObj.hasOwnProperty(key)) {
                        dataObj[key] = val;
                    } else {
                        dataObj[key] += "||" + val;
                    }
                } else {
                    dataObj[key] = val;
                }
            }

            var myJSONString = encodeURIComponent(JSON.stringify(dataObj));
            var myEscapedJSONString = myJSONString.escapeSpecialChars();

            var data = {
                action: 'save',
                brief_id: $("#brief_id").val(),
                ident_email: $("#email").val() == '' ? $('#ident_email').val() : $("#email").val(),
                signup: $('#signup_insight').val(),
                project_name: $("#project-name").val(),
                client_name: $("#client-name").val(),
                client_contact: $("#client-contact").val(),
                temp: (hasIdent ? '0' : '1'),
                ref: $("#ref").val(),
                date: $("#date").val(),
                json: myEscapedJSONString
            }

            $.post(processorUrl, data).done(function(response) {
                data = JSON.parse(response);
                if (data.status === 'success') {
                    $("#brief_id").val(data.brief_id);
                    if (data.view_id) {
                        $('#view_id').val(data.view_id);
                    }
                }
            });
        }

        function nl2br(str) {
            if (typeof str === 'undefined' || str === null) {
                return '';
            }
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + "<br>" + '$2');
        }

        // function updateBudgetEst() {
        //     var budget = [
        //         $('[name="budget-min"]').val(),
        //         $('[name="budget-max"]').val(),
        //         $('[name="prodbud-min"]').val().replace('%',''),
        //         $('[name="prodbud-max"]').val().replace('%','')
        //         ],
        //         currency = $('[name="currency"]:checked').val().toUpperCase();

        //     budget = budget.map(function(x) { return x.indexOf('k') > 0 ? parseInt(x) * 1000 : x.indexOf('m') > 0 ? parseFloat(x) * 1000000 : parseInt(x); });

        //     var mBudget = (budget[0] + budget[1]) / 2,
        //         mProd = (budget[2] + budget[3]) / 2,
        //         med = mBudget * (mProd / 100),
        //         budest = currency + (med > 1000000 ? (Math.round(med / 100000) / 10)  + 'm' : (Math.round(med / 100) / 10) + 'k');

        //     $('#budest').text(budest);
        //     $('[name="budget-est"]').val(budest);
        // }

        function init() {
            bindUI();
        }

        function bindUI() {

            $window.scrollTop(0);

            $window.on('hashchange', function() {
                location.reload();
            });

            // $window.on('popstate', function() {
            //     checkHash();
            // });

            $('#tos').on('click', function() {
                if ($(this).is(':checked')) {
                    $('#start').removeClass('disabled');
                } else {
                    $('#start').addClass('disabled');
                }
            });

            $('a[href="#tos"]').on('click', function(e) {
                e.preventDefault();
                $('#tosmodal').addClass('open');
            });

            $('.popup_modal .close').on('click', function(e) {
                e.preventDefault();
                $(this).closest('.popup_modal').removeClass('open');
            });
            $('#summary .close').on('click', function(e) {
                e.preventDefault();
                $('#summary').removeClass('open');
                $('body').removeClass('summary-open');
            });

            // prevent tab key navigation
            $window.on('keydown', function(e) {
                if (navigator.userAgent.indexOf('Safari') != -1) {
                    if ($(e.target).closest('section').attr('id') == 'landing' && e.keyCode == 9) {
                        return false;
                    }
                } else {
                    // console.log(e);
                    var t = e.target
                        last = $(e.target).closest('section').find('input:last')[0];
                    if (t === last && e.keyCode == 9) {
                        $(t).closest('section').find('input:first').focus();
                        return false;
                    }
                }
            });

            // $('section:not(.active)').on('click',function(e) {
            //     e.preventDefault();
            //     goToSect($(this).attr('id'));
            // });

            // $('input[readonly]').on('click', function(e) {
            //     e.preventDefault();
            //     $(this).blur();
            // });

            $('#optin').on('click', function() {
                var test = email_re.test($('#email').val().toLowerCase());
                if ($(this).is(':checked') && !test) {
                    alert('Please enter a valid email address.');
                    $('#email').addClass('error').focus();
                    return false;
                }
            });
            $('#optin2').on('click', function() {
                var test = email_re.test($('#email2').val().toLowerCase());
                if ($(this).is(':checked') && !test) {
                    alert('Please enter a valid email address.');
                    $('#email2').addClass('error').focus();
                    return false;
                }
            });
            $('#feedback .btn-feedback').on('click', function(e) {
                e.preventDefault();
                $('#feedback').addClass('open');
                $('#feedback_email').focus();
            });            
            // feedback form
            $('#feedback_form').on('submit', function(e) {
                e.preventDefault();
            
                var $form = $(this),
                    data = {
                        action: 'report',
                        from: encodeURIComponent($('#feedback_email').val()),
                        message: encodeURIComponent($('#feedback_content').val())
                    };
                $.post(processorUrl, data).done(function(res) {
                    console.log(res);
            
                    $form.hide();
                    $('#q').hide();
                    $('#t').show();
            
                    setTimeout(function() {
                        $('#feedback').removeClass('open');
                        $form.show()[0].reset();
                        $('#q').show();
                        $('#t').hide();
                    }, 3000);
                });
            });
            $('#feedback button.close').on('click', function(e) {
                e.preventDefault();
                $('#feedback').removeClass('open');
            });

            $('.copyversion').on('click', function(e) {
                e.preventDefault();
                var win = window.open('', '', 'menubar=no,statusbar=no');
                win.location = 'http://stepworks.com.hk/en/view-brief?id=' + $('#brief_id').val();

                // if save
            });
            // $('#summary .share').on('click', function(e) {
            //     e.preventDefault();
            //     if ($(e.target).hasClass('share')) {
            //         $('#sharemodal').toggleClass('open');
            //     }
            // });
            // $('#sharemodal input[type="text"]').on('focus', function() {
            //     $(this).select();
            // });

            $('#summary .share input').on('focus', function(e) {
                $(this)[0].select();
            });

            // button navigation
            $('.btn').on('click', function(e) {
                e.preventDefault();
                var $btn = $(this),
                    btnId = $btn.attr('id');

                switch (btnId) {
                    case 'gen_brief':
                        share_sw();
                        summary();
                        save2(ident);
                        if (!ident) {
                            $('#signup2modal').addClass('open');
                        }
                        else {
                            $('#summary').addClass('open');
                            $('body').addClass('summary-open');
                        }
                        break;
                    case 'signup2':
                        var test = email_re.test($('#email2').val().toLowerCase());
                        if (test) {
                            $('#ident_email').val($('#email2').val());
                            ident = true;
                            if ($('#optin2').is(':checked')) {
                                $('#signup_insight').val('1');
                            }
                            save2(ident);
                            $('#signup2modal').removeClass('open');
                            $('#summary').addClass('open');
                            $('body').addClass('summary-open');
                        } else {
                            alert('Please enter a valid email address.');
                            $('#email2').addClass('error').focus();
                        }
                        break;
                    case 'cancelsignup2':
                    case 'contsummary':
                        $('#signup2modal, #emailnotifmodal').removeClass('open');
                        $('#summary').addClass('open');
                        $('body').addClass('summary-open');
                        break;
                    case 'email-links':
                        $btn.addClass('wait');
                        var data = {
                            action:         'email',
                            email:          $('#ident_email').val(),
                            brief_id:       $('#brief_id').val(),
                            view_id:        $('#view_id').val(),
                            client_name:    $('#client-name').val()
                        };
                        $.post(processorUrl, data).done(function(response) {
                            $btn.removeClass('wait');
                            $('#email-links').addClass('alt');
                            setTimeout(function() { $('#email-links').removeClass('alt'); }, 3000);
                        });
                        break;
                    case 'share-sw':
                        $btn.addClass('wait');
                        var data = {
                            action:         'share-sw',
                            email:          $('#ident_email').val(),
                            client_name:    $('#client-name').val(),
                            project_name:   $('#project-name').val(),
                            brief_id:       $('#brief_id').val()
                        };
                        $.post(processorUrl, data).done(function(response) {
                            $btn.removeClass('wait');
                            $('#share-sw').addClass('alt');
                            setTimeout(function() { $('#share-sw').removeClass('alt'); }, 3000);
                        });
                        break;
                    case 'start':
                        if ($('#email').val().trim() != '') {
                            $('#ident_email').val($('#email').val());
                            ident = true;
                            if ($('#optin').is(':checked')) {
                                $('#signup_insight').val('1');
                            }
                        }
                        save2(ident);
                        // $('body').removeClass('locked');
                        goToSect('basic');
                        break;
                    case 'add-deliverable':
                        $('.tblSchedule tbody').append($('.tblSchedule tbody tr:last').clone().find('input').val('').end());
                        break;
                    default:
                        if ($btn.hasClass('next') || $btn.hasClass('prev')) {
                            var sect = $btn.closest('section').attr('id'),
                                next = $btn.attr('href').substr(1);

                            next = process(sect, next);
                            save2(ident);
                            goToSect(next);
                        } else if ($btn.hasClass('goto_summary')) {
                            goToSect('summary');
                            $('.actions').removeClass('reveal');
                        }
                }
            });

            $('.inline-modal').on('click', function() {
                $(this).next('.modal').addClass('open');
            });

            $('.modal .close').on('click', function(e) {
                e.preventDefault();
                $(this).closest('.modal').removeClass('open');
            });

            $('ul.append').on('click', 'li', function() {
                var $elem = $($(this).parent().data('target')),
                    thisText = $(this).text(),
                    text = $elem.val();

                text += text.length == 0 ? thisText + "\r\n" : "\r\n" + thisText;

                $elem.val(text);
                arrMde[$elem.data('index')].value(text);
            });

            $('.relog').on('click', function() {
                $('#returning-user-info').show();
                $('#returning-email').val('').focus();
            });

            $('#viewlib').on('click', function(e) {
                if ($('#returning-email').val() != '') {

                } else {
                    e.preventDefault();
                }
            });

            $('.more').on('click', function(e) {
                e.preventDefault();
                $(this).parent().toggleClass('reveal');
            });

            $('.copy_link').on('click', function() {
                var $elem = $(this),
                    $inp = $('#box_brief-id');
                // $box = $elem.closest('.actions');
                $inp.select();
                document.execCommand("copy");
                $elem.addClass('copied');
                setTimeout(function() {
                    $elem.removeClass('copied');
                }, 2000);
            });

            $('.email_brief').on('click', function(e) {
                e.preventDefault();
                alert('Work in progress!');
            });

            $('label input[type="text"]').on('focus', function() {
                $('#' + $(this).parent().attr('for')).trigger('click');
            });

            $('[name="promise-choice"]').on('change', function() {
                $('#promise_fill').trigger('click');
            });

            $('#input-promise').on('focus', function() {
                $('#promise_text').trigger('click');
            });

            $('input[maxlength]').on('keyup', function() {
                var len = parseInt($(this).attr('maxlength')),
                    cur = $(this).val().length;
                $(this).next('.charcount').text(cur + ' / ' + len);
            }).trigger('keyup');

            $('.sortable').sortable({
                axis: 'y',
                containment: 'parent',
                opacity: 0.7
            });

            $('.sortable').disableSelection();

            $('#add-reason').on('click', function(e) {
                e.preventDefault();
                $('.reason-list').append('<div class="reason"><div class="handle"><i class="far fa-sort"></i></div> <input spellcheck="true" type="text" name="belief-reason[]"></label></div>');
            });

            $('.datepicker').datepicker({
                dateFormat: "dd-mm-yy",
                minDate: 0
            });

            // $('.gender_slider').slider({
            //     // value:
            //     step: 10,
            //     create: function() {
            //         var male = $(this).slider('value'),
            //             ratio = male + ' / ' + (100 - male);
            //         $(this).find('.handle').text(ratio);
            //     },
            //     slide: function(event, ui) {
            //         var male = ui.value,
            //             ratio = male + ' / ' + (100 - male);
            //         $(this).find('.handle').text(ratio);
            //         $('[name="gender-ratio"]').val(ratio);
            //     }
            // });

            // $('.age_slider').slider({
            //     range: true,
            //     min: 10,
            //     max: 80,
            //     step: 5,
            //     values: [35, 55],
            //     create: function(event, ui) {
            //         $('#agemin').text(35);
            //         $('#agemax').text(55);
            //     },
            //     slide: function(event, ui) {
            //         var minval = ui.values[0],
            //             maxval = ui.values[1];
            //         if (minval == $(this).slider('option', 'min')) {
            //             minval = '< ' + minval;
            //         }
            //         if (maxval == $(this).slider('option', 'max')) {
            //             maxval = maxval + ' >';
            //         }
            //         $('#agemin').text(minval);
            //         $('#agemax').text(maxval);
            //         $('[name="agemin"]').val(minval);
            //         $('[name="agemax"]').val(maxval);
            //     }
            // });

            var gender_slider = document.getElementById('gender_slider');
            noUiSlider.create(gender_slider, {
                start: 0,
                tooltips: true,
                range: {
                    'min': 0,
                    'max': 100
                },
                step: 10,
                behaviour: 'tap-drag',
                animate: true,
                format: {
                    to: function(val) {
                        return parseInt(val) + ' / ' + (100 - val);
                    },
                    from: function(val) {
                        return Number(val.substr(0,val.indexOf('/')));
                    }
                }
            });
            gender_slider.noUiSlider.on('update', function() {
                var vals = gender_slider.noUiSlider.get();
                $('[name="gender-ratio"]').val(vals);
            });

            var age_slider = document.getElementById('age_slider');
            noUiSlider.create(age_slider, {
                start: [35, 55],
                tooltips: [true, true],
                connect: true,
                range: {
                    'min': 10,
                    'max': 80
                },
                step: 5,
                margin: 5,
                behaviour: 'tap-drag',
                animate: true,
                format: {
                    to: function(val) {
                        return parseInt(val);
                    },
                    from: function(val) {
                        return Number(val);
                    }
                }
            });
            age_slider.noUiSlider.on('update', function() {
                var vals = age_slider.noUiSlider.get();
                $('[name="agemin"]').val(vals[0]);
                $('[name="agemax"]').val(vals[1]);
            });

            // $('.budget_slider').slider({
            //     range: true,
            //     min: 10,
            //     max: 250,
            //     step: 5,
            //     values: [20,40],
            //     slide: function(event, ui) {
            //         var minval = ui.values[0],
            //             maxval = ui.values[1],
            //             minout = '',
            //             maxout = '';

            //         if (minval >= 100) {
            //             $(this).slider('option','step', 10);
            //         } else {
            //             $(this).slider('option','step', 5);
            //         }

            //         if (maxval >= 100) {
            //             $(this).slider('option','step', 10);
            //         } else {
            //             $(this).slider('option','step', 5);
            //         }
            //         minout = minval + 'k';
            //         maxout = maxval + 'k';

            //         if (minval == $(this).slider('option', 'min')) {
            //             minout = '< ' + minout;
            //         }
            //         if (maxval == $(this).slider('option', 'max')) {
            //             maxout = maxout + ' +';
            //         }

            //         $(this).find('#budget-min').text(minout);
            //         $(this).find('#budget-max').text(maxout);
            //         $('[name="budget-min"]').val(minout);
            //         $('[name="budget-max"]').val(maxout);

            //         updateBudgetEst();
            //     },
            //     create: function(event, ui) {
            //         $('#budget-min').text('20k');
            //         $('#budget-max').text('40k');
            //     }
            // });

            // var budget_slider = document.getElementById('budget_slider');
            // noUiSlider.create(budget_slider, {
            //     start: [20, 40],
            //     tooltips: [true, true],
            //     connect: true,
            //     range: {
            //         'min': 10,
            //         'max': 250
            //     },
            //     step: 5,
            //     margin: 5,
            //     behaviour: 'tap-drag',
            //     animate: true,
            //     format: {
            //         to: function(val) {
            //             return parseInt(val) + 'k';
            //         },
            //         from: function(val) {
            //             return Number(val.replace(/[A-Za-z]/g, ''));
            //         }
            //     }
            // });
            // budget_slider.noUiSlider.on('update', function() {
            //     var vals = budget_slider.noUiSlider.get();
            //     $('[name="budget-min"]').val(vals[0]);
            //     $('[name="budget-max"]').val(vals[1]);
            //     updateBudgetEst();
            // });

            // var prodbudget_slider = document.getElementById('prodbudget_slider');
            // noUiSlider.create(prodbudget_slider, {
            //     start: [10, 20],
            //     tooltips: [true, true],
            //     connect: true,
            //     range: {
            //         'min': 10,
            //         'max': 100
            //     },
            //     step: 10,
            //     margin: 10,
            //     behaviour: 'tap-drag',
            //     animate: true,
            //     format: {
            //         to: function(val) {
            //             return parseInt(val) + '%';
            //         },
            //         from: function(val) {
            //             return Number(val.replace('%', ''));
            //         }
            //     }
            // });
            // prodbudget_slider.noUiSlider.on('update', function() {
            //     var vals = prodbudget_slider.noUiSlider.get();
            //     $('[name="prodbud-min"]').val(vals[0]);
            //     $('[name="prodbud-max"]').val(vals[1]);
            //     // console.log(vals);
            //     updateBudgetEst();
            // });


            // $('.prodbudget_slider').slider({
            //     range: true,
            //     min: 10,
            //     max: 100,
            //     step: 10,
            //     values: [10, 20],
            //     create: function(event, ui) {
            //         $('#prodbud-min').text('10%');
            //         $('#prodbud-max').text('20%');
            //     },
            //     slide: function(event, ui) {
            //         var minval = ui.values[0],
            //             maxval = ui.values[1];

            //         $('#prodbud-min').text(minval + '%');
            //         $('#prodbud-max').text(maxval + '%');
            //         $('[name="prodbud-min"]').val(minval + '%');
            //         $('[name="prodbud-max"]').val(maxval + '%');

            //         updateBudgetEst();
            //     }
            // });

            $('[name="currency"]').on('click', function() {
                var curr = $(this).val();
                if (curr == 'hkd') {
                    budget_slider.noUiSlider.updateOptions({
                        start: [200, 350],
                        range: {
                            'min': 100,
                            'max': 2000
                        },
                        step: 50,
                        margin: 50,
                        format: {
                            to: function(val) {
                                var iVal = parseInt(val);
                                if (iVal >= 1000) {
                                    return parseFloat(iVal / 1000) + 'm';
                                } else {
                                    return iVal + 'k';
                                }
                            },
                            from: function(val) {
                                if (val.indexOf('m') != -1) {
                                    return parseInt(val) * 1000;
                                }
                                return parseInt(val);
                            }
                        }
                    })
                } else if (curr == 'usd') {
                    budget_slider.noUiSlider.updateOptions({
                        start: [20, 40],
                        range: {
                            'min': 10,
                            'max': 250
                        },
                        step: 5,
                        margin: 5,
                        format: {
                            to: function(val) {
                                return parseInt(val) + 'k';
                            },
                            from: function(val) {
                                return Number(val.replace(/[A-Za-z]/g, ''));
                            }
                        }
                    })
                }
            });
            //         $('.budget_slider').slider('option', 'min', 100);
            //         $('.budget_slider').slider('option', 'max', 2000);
            //         $('.budget_slider').slider('option', 'step', 50);
            //         $('.budget_slider').slider('option', 'values', [200,350]);
            //         $('.budget_slider').find('#budget-min').text('200k');
            //         $('.budget_slider').find('#budget-max').text('350k');
            //         $('.budget_slider').slider('option', 'slide', function(event, ui) {
            //             var minval = ui.values[0],
            //                 maxval = ui.values[1],
            //                 minout = '',
            //                 maxout = '';

            //             if (minval >= 1000) {
            //                 minout = '' + parseFloat(minval / 1000) + 'm';
            //                 $(this).slider('option','step', 100);
            //             } else {
            //                 minout = minval + 'k';
            //                 $(this).slider('option','step', 50);
            //             }

            //             if (maxval >= 1000) {
            //                 maxout = '' + parseFloat(maxval / 1000) + 'm';
            //                 $(this).slider('option','step', 100);
            //             } else {
            //                 maxout = maxval + 'k';
            //                 $(this).slider('option','step', 50);
            //             }

            //             if (minval == $(this).slider('option', 'min')) {
            //                 minout = '< ' + minout;
            //             }
            //             if (maxval == $(this).slider('option', 'max')) {
            //                 maxout = maxout + ' +';
            //             }

            //             $('#budget-min').text(minout);
            //             $('#budget-max').text(maxout);
            //             $('[name="budget-min"]').val(minout);
            //             $('[name="budget-max"]').val(maxout);

            //             updateBudgetEst();
            //         });
            //     } else if (curr == 'usd') {
            //         $('.budget_slider').slider('option', 'min', 10);
            //         $('.budget_slider').slider('option', 'max', 250);
            //         $('.budget_slider').slider('option', 'step', 5);
            //         $('.budget_slider').slider('option', 'values', [20,40]);
            //         $('.budget_slider').find('#budget-min').text('20k');
            //         $('.budget_slider').find('#budget-max').text('40k');
            //         $('.budget_slider').slider('option', 'slide', function(event, ui) {
            //             var minval = ui.values[0],
            //                 maxval = ui.values[1],
            //                 minout = '',
            //                 maxout = '';

            //             if (minval >= 100) {
            //                 $(this).slider('option','step', 10);
            //             } else {
            //                 $(this).slider('option','step', 5);
            //             }

            //             if (maxval >= 100) {
            //                 $(this).slider('option','step', 10);
            //             } else {
            //                 $(this).slider('option','step', 5);
            //             }
            //             minout = minval + 'k';
            //             maxout = maxval + 'k';

            //             if (minval == $(this).slider('option', 'min')) {
            //                 minout = '< ' + minout;
            //             }
            //             if (maxval == $(this).slider('option', 'max')) {
            //                 maxout = maxout + ' +';
            //             }

            //             $('#budget-min').text(minout);
            //             $('#budget-max').text(maxout);
            //             $('[name="budget-min"]').val(minout);
            //             $('[name="budget-max"]').val(maxout);

            //             updateBudgetEst();
            //         });
            //     }

            //     updateBudgetEst();
            // });

            $('[name="attitude[]"]').on('click', function(e) {
                if ($('[name="attitude[]"]:checked').length > 3) {
                    $('#q-attitude .three').addClass('highlight');

                    // prevents checking if at or over limit but allow unchecking if somehow more than 3 were checked
                    if ($(e.target).is(':checked')) {
                        return false;
                    }
                }
            });

            $('.trigger-custom-response').on('change', function() {
                if (this.checked) {
                    $(this).nextAll('.sub-row:first').find('.custom_response').focus();
                }
            });

            $('.trigger:checked').trigger('click');

            // scroll control
            $window.on('scroll', function(e) {
                e.preventDefault();
                scrollWindow();
                scrollThrottle();
            });

            // $window.on('mousewheel', function(e) {
            //     return false;
            // });

            // resize control
            $window.on('resize', function() {
                resizeWindow();
            });

            $window.on('load', function() {
                // if (window.localStorage !== undefined && window.localStorage.getItem('be-ident-email') != null) {
                //     var lsEmail = window.localStorage.getItem('be-ident-email');
                //     $("#ident_email").val(lsEmail);
                //     $("#email").val(lsEmail);
                //     $('#returning-email').val(lsEmail);
                // }

                // if ($("#brief_id").val() && $("#brief_id").val().length === 8) {
                //     load();
                // } else {
                //     // goToSect('landing');
                // }

                // setTimeout(function() {
                //     $('html').removeClass('no-anim');
                // }, 500);

                if (location.hash != '') {
                    var hash = location.hash.substr(1);
                    if (hash.length == 8) {
                        $('#brief_id').val(hash);
                        load();
                    }
                } else {
                    $window.scrollTop(0);
                }

                if (firstLoad) {
                    // checkHash();
                    firstLoad = !firstLoad;
                }

                loadMde();
                // updateBudgetEst();
            });

            $('body').on('unload', function() {

            });
        }

        return {
            init: init,
            goToSect: goToSect
        }
    }

    var site = new Site();
    site.init();
})(jQuery)

String.prototype.escapeSpecialChars = function() {
    return this.replace(/\\n/g, "\\n")
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");
};