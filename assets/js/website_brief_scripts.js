(function() {
    
    var Site = function() {
        var $window = $(window),
            _selector = {
                addReferenceWebsiteBtn: '#add-reference-website',
                additionalSecurityLayersOptionList: '[name="security-layer[]"]',
                additionalSecurityLayersOption: '[value="Additional security layers"]',
                attachmentTentativeScopeFile: '[name="attachment-tentative-scope-file"]',
                attachmentaudiencePersonasFile: '[name="attachment-audience-personas-file"]',
                attachmentVisitorDataFile: '[name="attachment-visitor-data-file"]',
                briefId: '#brief_id',
                errorMessage: '.form-row__error',
                clientContactInput: '#client-contact',
                clientNameInput: '#client-name',
                cmsTriggerOption: '[value="Content management system (CMS)"]',
                cmsDunnoFunctionCheckBox: '[value="We don’t yet know what we require for our CMS"]',
                cmsFunctionCheckBox: '[name="cms-function[]"]',
                cmsPart: '.form-row__conditional--cms',
                customDropdown: '.custom-dropdown',
                dateInput: '#date',
                datepickerInput: '.datepicker',
                emailInput: '#email',
                funcBtn: '.btn:not(.next)',
                htmlBody: 'html, body',
                identEmailInput: '#ident_email',
                nextBtn: '.btn.next',
                objectiveResultInput: '#objective-result',
                optionSortAudience: '.option-sort--audience',
                optionSortBehaviour: '.option-sort--behaviour',
                optionSortBucket: '.option-sort__bucket',
                optionSortBucketItem: '.option-sort__bucket-item',
                optionSortClearBtn: '.option-sort__clear-btn',
                optionSortObjectives: '.option-sort--objectives',
                optionSortResult: '.option-sort__result',
                optionSortTrayItem: '.option-sort__tray-item',                
                optinCheckbox: '#optin',
                popupModalCloseBtn: '.popup_modal .close',
                projectNameInput: '#project-name',
                referenceNumberInput: '#ref',
                referenceWebsiteList: '.reference-website__list',
                signupInsightInput: '#signup_insight',
                summaryCloseBtn: '#summary .close',
                startBtn: '#start',
                tentativeScopeFileInput: '[name="tentative-scope-file"]',
                tosCheckboxInput: '#tos',
                tosLink: 'a[href="#tos"]',
                tosModal: '#tosmodal',
                viewId: '#view_id',
                visitorDataFileInput: '[name="visitor-data-file"]',
                audiencePersonasFileInput: '[name="audience-personas-file"]',
            },
            $selector = {},
            $form = $('#form-website-brief'),
            step = 1,
            hasFile = false,
            formDataIgnoreList = ['brief_id', 'view_id', 'ident_email', 'signup_insight', 'email', 'terms', 'optin', 'project-name', 'client-name', 'client-contact', 'ref', 'date'],
            emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            processorUrl = (env === 'local' ? '/stepworks-website' : '') + '/en/website_brief_processor';
        
        // map all static selectors to jquery selectors
        Object.entries(_selector).forEach(([key, value]) => {
            $selector[key] = $(value);
        });

        function init() {
            showStep(step);
            bindUI();
            bindFormUI();
            bindFormBeforeUnload();
            if (window.location.hash.length > 0) {
              const reportHash = window.location.hash.slice(1);
              openReport(reportHash);
            }
        }

        function bindUI() {
            $selector.nextBtn.on('click', function(e) {
                e.preventDefault();
                var $this = $(this),
                    $curr = $this.closest('section').attr('id'),
                    $next = $this.attr('href').substr(1);
                    $this.closest('section').find('.form-row__error').empty();

                if (isValid($curr)) {

                    switch ($curr) {
                        case 'landing':
                            $selector.identEmailInput.val($selector.emailInput.val());
                            if ($selector.optinCheckbox.is(':checked')) {
                                $selector.signupInsightInput.val('1');
                                // add optin script
                                const emailAddress = $('#email').val();
                                let url = "/createsend-php/subscriber/add.php";
                                let emailRegex =
                                    /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
                                let email_check = emailRegex.test(emailAddress);
                                if (emailAddress == "") {
                                    return;
                                }
                                if (email_check) {
                                    $.ajax({
                                        url: url,
                                        method: "POST",
                                        data: {
                                          email: emailAddress
                                        },
                                        success: function(data, status, xhr) {
                                            if (data.includes("Subscribed with code 201")) {
                                                // save the subscription to localstorage
                                                localStorage.setItem("subscribed", "YES");
                                            }
                                        },
                                    });
                                }
                                if ("ga" in window) {
                                    tracker = ga.getAll()[0];
                                    if (tracker) {
                                        tracker.send("event", "newsletter", "newsletter-signup", location.href);
                                    }
                                }
                            }
                            $this.hide();
                            showStep(++step);
                            gotoSect($next);
                            break;

                        case 'basic':
                            var data = JSON.stringify({
                                action:                 'create',
                                email:                  $selector.identEmailInput.val(),
                                signup:                 $selector.signupInsightInput.val(),
                                project_name:           $selector.projectNameInput.val(),
                                client_name:            $selector.clientNameInput.val(),
                                client_contact:         $selector.clientContactInput.val(),
                                ref:                    $selector.referenceNumberInput.val(),
                                date:                   $selector.dateInput.val(),
                                temp:                   1,               
                            });
                            var postData = new FormData();
                            postData.append("data", data);
                            $.ajax({
                                url: processorUrl,
                                method: "post",
                                processData: false,
                                contentType: false,
                                data: postData,
                                success: function(response){
                                    data = (response);
                                    if (data.status === 'success') {
                                        $selector.briefId.val(data.brief_id);
                                        if (data.view_id) {
                                            $selector.viewId.val(data.view_id);
                                        }
                                    }
                                    $this.hide();
                                    showStep(++step);
                                    gotoSect($next);
                                }
                            });
                            break;
                        default:
                            save2(function(){
                                $this.hide();
                                showStep(++step);
                                gotoSect($next);
                            });
                            break;
                    }                 
                } else {
                    gotoSect($curr);
                }
            });

            $selector.funcBtn.on('click', function(e){
                e.preventDefault();
                var $btn = $(this),
                    btnId = $btn.attr('id');

                switch (btnId) {
                    case 'update_brief':
                        save2(function(){
                            if (confirm("Website brief is updated successfully") == true) {
                                window.location.reload();   
                            }
                        });
                        break;
                    case 'gen_brief':
                        save2(openReport($selector.briefId.val()), true);
                        break;
                    case 'open_brief':
                      window.open($(this).attr('href'), '_blank');
                      break;
                    case 'reset_brief':
                    default:
                        window.location.reload();
                        break;
                }
            });

            $selector.tosCheckboxInput.on('click', function() {
                if ($(this).is(':checked')) {
                    $selector.startBtn.removeClass('disabled');
                } else {
                    $selector.startBtn.addClass('disabled');
                }
            });

            $selector.tosLink.on('click', function(e) {
                e.preventDefault();
                $selector.tosModal.addClass('open');
            });

            $selector.popupModalCloseBtn.on('click', function(e) {
                e.preventDefault();
                $(this).closest('.popup_modal').removeClass('open');
            });

            $selector.summaryCloseBtn.on('click', function(e) {
                e.preventDefault();
                $('#summary').removeClass('open');
                $('body').removeClass('summary-open');
            });

            $selector.optinCheckbox.on('click', function() {
                var test = emailRegex.test($('#email').val().toLowerCase());
                if ($(this).is(':checked') && !test) {
                    alert('Please enter a valid email address.');
                    $('#email').addClass('error').focus();
                    return false;
                }
            });

            $('#feedback .btn-feedback').on('click', function(e) {
                e.preventDefault();
                $('#feedback').addClass('open');
                $('#feedback_email').focus();
            });
            $('#feedback_form').on('submit', function(e) {
                e.preventDefault();
            
                var $form = $(this),
                    data = {
                        action: 'report',
                        from: encodeURIComponent($('#feedback_email').val()),
                        message: encodeURIComponent($('#feedback_content').val())
                    };
                $.post(processorUrl, data).done(function(res) {
                    console.log(res);
            
                    $form.hide();
                    $('#q').hide();
                    $('#t').show();
            
                    setTimeout(function() {
                        $('#feedback').removeClass('open');
                        $form.show()[0].reset();
                        $('#q').show();
                        $('#t').hide();
                    }, 3000);
                });
            });
            $('#feedback button.close').on('click', function(e) {
                e.preventDefault();
                $('#feedback').removeClass('open');
            });

            $selector.cmsDunnoFunctionCheckBox.on('click', function(){
                if ($(this).is(':checked')) {
                    $selector.cmsFunctionCheckBox.not(_selector.cmsDunnoFunctionCheckBox).prop('disabled', true);
                } else {
                    $selector.cmsFunctionCheckBox.prop('disabled', false);
                }
            });

            $selector.additionalSecurityLayersOptionList.on('click', function() {
                $selector.additionalSecurityLayersOption.prop('checked', 'checked');
            });

            $selector.cmsTriggerOption.on('click', function() {
                if ($(this).prop('checked')) {
                    $selector.cmsPart.show();
                } else {
                    $selector.cmsPart.hide();
                }
            });

            // prevent tab key navigation
            $window.on('keydown', function(e) {
                if (navigator.userAgent.indexOf('Safari') != -1) {
                    if ($(e.target).closest('section').attr('id') == 'landing' && e.keyCode == 9) {
                        return false;
                    }
                } else {
                    // console.log(e);
                    var t = e.target
                        last = $(e.target).closest('section').find('input:last')[0];
                    if (t === last && e.keyCode == 9) {
                        $(t).closest('section').find('input:first').focus();
                        return false;
                    }
                }
            });

            bindConditionals('website', 'existing_website', true);
            bindConditionalsCheckbox('#audience-personas', true);
            bindConditionalsCheckbox('#visitor-data', true);
            bindConditionals('tentative-scope', 'yes', true);
            bindConditionals('reference-websites', 'yes', true);
            bindConditionals('secure-domain', 'yes', true);
            bindConditionals('cms', 'yes', true);
            bindConditionalsCheckbox('#cms-function8', true); // Blog functionality
            bindConditionalsCheckbox('#cms-function9', true); // Event management
            bindConditionalsCheckbox('#cms-function10', true); // eCommerce management
            bindConditionalsCheckbox('#cms-function11', true); // Newsletter management (newsletter and email marketing system)
            bindConditionalsCheckbox('#cms-function12', true); // Membership management
            bindConditionalsCheckbox('#form-function4', true); // Forms integrated with third party system (eg. Mailchimp, Campaign Monitor, Salesforce)
            bindConditionals('website-maintenance-support', 'yes', true);
            bindConditionals('hosting-preference', 'yes', true);
            bindConditionals('coding-preference', 'yes', true);
            bindConditionals('database-preference', 'yes', true);
        }

        function bindFormUI() {
            $('textarea').not('.nomde').each(function(i) {
                $(this).data('index', i);
                var mde = new SimpleMDE({
                    element: $(this).get(0),
                    forceSync: true,
                    hideIcons: ['guide','side-by-side','fullscreen'],
                    spellChecker: false,
                    status: ["lines", "words"]
                });
                mde.value($(this).val());
            });

            $selector.datepickerInput.datepicker({
                dateFormat: "dd MM yy",
                minDate: 0
            });

            $selector.tentativeScopeFileInput.on('change', function(){
                hasFile = true;
            });
            $selector.audiencePersonasFileInput.on('change', function(){
                hasFile = true;
            });
            $selector.visitorDataFileInput.on('change', function(){
                hasFile = true;
            });

            $selector.customDropdown.select2({
                width: '100%',
                tags: true,
                tokenSeparators: [',', ' '],
            });

            var referenceWebsiteId = 1;
            $selector.addReferenceWebsiteBtn.on('click', function(){
                $selector.referenceWebsiteList.append(`<li><span><label for='reference-url${referenceWebsiteId}'>Website address</label><input type='text' spellcheck='true' id='reference-url${referenceWebsiteId}' name='reference-url[]' /></span><span><label for='reference-reason${referenceWebsiteId}'>Reason for reference</label><input type='text' spellcheck='true' name='reference-reason[]' id='reference-reason${referenceWebsiteId}' /></span></li>`);
                referenceWebsiteId++;
            });

            $('.sortable').sortable({
                axis: 'y',
                cursor: 'move',
                opacity: 0.7,
                handle: '.option-sort__handle',
            }).on('sortupdate DOMSubtreeModified', function(){
                var result = [];
                $(this).find(_selector.optionSortBucketItem).each(function(){
                    result.push($(this).text());
                });
                $(this).next(_selector.optionSortResult).val(result.join('||'));
            });
            $('.sortable').disableSelection();

            $selector.optionSortAudience
            .on('click', _selector.optionSortTrayItem, function(){
                var optionId = $(this).attr('id').substr(-1, 1),
                    optionText = $(this).text();
                $selector.optionSortAudience.find(_selector.optionSortBucket)
                .append(`<div class='form-row__option option-sort__bucket-item' id='audience-selected${optionId}'><i class='fas fa-sort option-sort__handle'></i>${optionText}<i class='fas fa-trash-alt option-sort__clear-btn'></i></div>`);
                $(this).hide();
            })
            .on('click', _selector.optionSortClearBtn, function(){
                var optionId = $(this).parent('.option-sort__bucket-item').attr('id').substr(-1, 1);
                $(`${_selector.optionSortTrayItem}#audience${optionId}`).show();
                $(this).parent('.option-sort__bucket-item').remove();
            });

            $selector.optionSortBehaviour
            .on('click', _selector.optionSortTrayItem, function(){
                var optionId = $(this).attr('id').substr(-1, 1),
                    optionText = $(this).text();
                $selector.optionSortBehaviour.find(_selector.optionSortBucket)
                .append(`<div class='form-row__option option-sort__bucket-item' id='behaviour-selected${optionId}'><i class='fas fa-sort option-sort__handle'></i>${optionText}<i class='fas fa-trash-alt option-sort__clear-btn'></i></div>`);
                $(this).hide();
            })
            .on('click', _selector.optionSortClearBtn, function(){
                var optionId = $(this).parent('.option-sort__bucket-item').attr('id').substr(-1, 1);
                $(`${_selector.optionSortTrayItem}#behaviour${optionId}`).show();
                $(this).parent('.option-sort__bucket-item').remove();
            });
            
            $selector.optionSortObjectives
            .on('click', _selector.optionSortTrayItem, function(){
                var optionId = $(this).attr('id').substr(-1, 1),
                    optionText = $(this).text();
                $selector.optionSortObjectives.find(_selector.optionSortBucket)
                .append(`<div class='form-row__option option-sort__bucket-item' id='objective-selected${optionId}'><i class='fas fa-sort option-sort__handle'></i>${optionText}<i class='fas fa-trash-alt option-sort__clear-btn'></i></div>`);
                $(this).hide();
            })
            .on('click', _selector.optionSortClearBtn, function(){
                var optionId = $(this).parent('.option-sort__bucket-item').attr('id').substr(-1, 1);
                $(`${_selector.optionSortTrayItem}#objective${optionId}`).show();
                $(this).parent('.option-sort__bucket-item').remove();
            });
        }

        function bindFormBeforeUnload() {
            $form.change(function(){
                window.onbeforeunload = function() {
                    window.scrollTo(0, 0);
                };
            });
        }

        function openReport(briefId) {
          var prefixPath = (env === 'local' ? '/stepworks-website' : '');
          var reportIframe = $('<iframe>').attr({
            src: `${prefixPath}/en/website-brief-engine/reports?id=${briefId}&iframe=yes`,
            id: 'wbe-report',
          });
          if ($('#summary > .container').find('iframe').length == 0) {
            $('#summary > .container').append(reportIframe);
          }
          $('#summary').find('.btn').attr('href', `${prefixPath}/en/website-brief-engine/reports?id=${briefId}`);
          $('#summary').addClass('open');
        }
        
        function bindConditionals(fieldName, value, animated) {
            var duration = animated ? 250 : 0;
            $(`[name="${fieldName}"]`).on('change', function() {
                if ($(this).val() === value) {
                    $(this)
                    .parents('.form-row')
                    .find('> .form-row__conditional')
                    .stop().slideDown(duration);
                } else {
                    $(this)
                    .parents('.form-row')
                    .find('> .form-row__conditional')
                    .stop().slideUp(duration);
                }
            });
        }

        function bindConditionalsCheckbox(id, animated) {
            var duration = animated ? 250 : 0;
            $(`${id}`).on('click', function() {
                if ($(this).prop('checked')) {
                    $(this)
                    .closest('.form-row__option')
                    .next('.form-row__conditional')
                    .stop().slideDown(duration);
                } else {
                    $(this)
                    .closest('.form-row__option')
                    .next('.form-row__conditional')
                    .stop().slideUp(duration);
                }
            });
        }

        function save2(callback, isLastStep = false) {
            var dataObj = {},
                formData = $form.serializeArray();
                
            for (i=0;i<formData.length;i++) {
                var key = formData[i].name,
                    val = formData[i].value;
                if (formDataIgnoreList.indexOf(key) != -1) {
                    continue;
                }
                if (key.indexOf('[]') != -1) {
                    var emptystr = val.trim().length === 0;
                    if (!dataObj.hasOwnProperty(key)) {
                        dataObj[key] = (emptystr ? ' ' : val);
                    } else {
                        dataObj[key] += "||" + (emptystr ? ' ' : val);
                    }
                } else {
                    dataObj[key] = val;
                }
            }

            var myJSONString = encodeURIComponent(JSON.stringify(dataObj));
            var myEscapedJSONString = myJSONString.escapeSpecialChars();
            var data = JSON.stringify({
                action:                      'save',
                brief_id:                    $selector.briefId.val(),
                email:                       $selector.identEmailInput.val(),
                signup:                      $selector.signupInsightInput.val(),
                project_name:                $selector.projectNameInput.val(),
                client_name:                 $selector.clientNameInput.val(),
                client_contact:              $selector.clientContactInput.val(),
                ref:                         $selector.referenceNumberInput.val(),
                date:                        $selector.dateInput.val(),
                temp:                        1,
                json:                        myEscapedJSONString,
                has_file:                    hasFile,
                edit_mode:                   $('#form-website-brief').hasClass('edit-mode'),
                tentative_scope_file_path:   $selector.attachmentTentativeScopeFile.val(),
                audience_personas_file_path: $selector.attachmentaudiencePersonasFile.val(),
                visitor_data_file_path:      $selector.attachmentVisitorDataFile.val(),
                last_step:                   isLastStep,
            });
            var postData = new FormData();
            postData.append("data", data);
            if (!$('#form-website-brief').hasClass('edit-mode')) {
                postData.append("tentativeScopeFile", $selector.tentativeScopeFileInput[0].files[0]);
                postData.append("audiencePersonasFile", $selector.audiencePersonasFileInput[0].files[0]);
                postData.append("visitorDataFile", $selector.visitorDataFileInput[0].files[0]);
            }            

            $.ajax({
                url: processorUrl,
                method: "post",
                processData: false,
                contentType: false,
                data: postData,
                success: function(response){
                    res = JSON.parse(response);
                    if (res.status === 'success') {
                        $selector.briefId.val(res.brief_id);
                    }
                    callback();
                }
            });
        }

        function showStep(stepId) {
            $(`.step${stepId}`).show();
        }

        function gotoSect(sectionId) {
            var top = $(`#${sectionId}`).offset().top;
            $selector.htmlBody.animate({'scrollTop': top});
        }

        function isValid(sectionId) {
            var $section = $(`#${sectionId}`),
                valid = true;

            $section.find('input[required]').map(function(key, item) {
                var inputType = item.type,
                    inputName = item.name.replaceAll('-', ' '),
                    $item = $(item),
                    $errorField = $item.siblings(_selector.errorMessage);
                $item.removeClass('error');
                switch (inputType) {
                    case 'text':
                    case 'email':
                        var val = item.value.trim();
                        if (val.length === 0) {
                            alertField(item);
                            $errorField.append(` ${inputName} must not be empty!`);
                            valid = false;
                        }
                        break;
                    default:
                        break;
                }
            });

            $section.find('input[type=email]').map(function(key, item){
                var email = item.value.trim(),
                    $item = $(item),
                    $errorField = $item.siblings(_selector.errorMessage);
                if (!emailRegex.test(String(email).toLowerCase())) {
                    alertField(item);
                    $errorField.append(' Invalid email format!');
                    valid = false;
                }
            })
            return valid;
        }

        function alertField(elem) {
            $(elem).addClass('error shake').delay(1000).queue(function(){
                $(this).removeClass('shake').dequeue();
            });
        }

        return {
            init: init,
        }
    }

    var site = new Site();
    site.init();
})(jQuery);

String.prototype.escapeSpecialChars = function() {
    return this.replace(/\\n/g, "\\n")
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");
};