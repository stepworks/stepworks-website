var data = {
  property: {
    energy_direct:      { 2018: 166, 2017: 143,  2016: 154 },
    energy_indirect:    { 2018: 1035, 2017: 959,  2016: 1023 },
    energy_total:       { 2018: 1201, 2017: 1102, 2016: 1177 },

    gas_direct:         { 2018: 10, 2017: 11, 2016: 13 },
    gas_indirect:       { 2018: 205, 2017: 204, 2016: { value: 216, note: 5 } },
    gas_total:          { 2018: 215, 2017: 215, 2016: 229 },

    water_total:        { 2018: 1599, 2017: 1390, 2016: 1522 },

    waste_haz_dis:      { 2018: 0.4, 2017: 1, 2016: 0 },
    waste_nonhaz_dis:   { 2018: 30800, 2017: 32316, 2016: { value: 38377, note: 5 } },
    waste_haz_rec:      { 2018: 0.4, 2017: 0, 2016: { value: 1, note: 5 } },
    waste_nonhaz_rec:   { 2018: 10642, 2017: 7272, 2016: { value: 4350, note: 5 } },
    waste_total:        { 2018: 41442.8, 2017: 39589, 2016: 42728 },

    hs_ltir:            { 2018: 1.47, 2017: 1.43, 2016: 1.80 },
    hs_ldr:             { 2018: 54.47, 2017: 48.36, 2016: 36.18 },
    hs_fatalities:      { 2018: 0, 2017: 0, 2016: 0 },

    staff_gender_male:  { 2018: 3490, 2017: 3207, 2016: 3309 },
    staff_gender_female: { 2018: 2390, 2016: 2112, 2016: 2105 },
    staff_gender_total: { 2018: 5880, 2017: 5319, 2016: 5414 },

    staff_region_hk:    { 2018: 55, 2017: 60, 2016: { value: 60, note: 5 } },
    staff_region_cn:    { 2018: 36, 2017: 39, 2016: { value: 39, note: 5 } },
    staff_region_tw:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_us:    { 2018: 9, 2017: 0, 2016: 0 },
    staff_region_other: { 2018: 0, 2017: 0, 2016: 0 },

    staff_cat_topman:   { 2018: 2, 2017: 2, 2016: -1 },
    staff_cat_midman:   { 2018: 24, 2017: 23, 2016: -1 },
    staff_cat_cf:       { 2018: 53, 2017: 53, 2016: -1 },
    staff_cat_optech:   { 2018: 11, 2017: 11, 2016: -1 },
    staff_cat_other:    { 2018: 10, 2017: 11, 2016: -1 },

    staff_age_under30:  { 2018: 25, 2017: 27, 2016: 29 },
    staff_age_30to50:   { 2018: 56, 2017: 56, 2016: 54 },
    staff_age_over50:   { 2018: 19, 2017: 18, 2016: 17 },

    vpet_age_under30:   { 2018: 0, 2017: 37, 2016: 35 },
    vpet_age_30to50:    { 2018: 0, 2017: 18, 2016: 16 },
    vpet_age_over50:    { 2018: 0, 2017: 11, 2016: 11 },

    vpet_gender_male:   { 2018: 21, 2017: 20, 2016: 21 },
    vpet_gender_female: { 2018: 30, 2017: 24, 2016: 22 },

    vpet_region_hk:     { 2018: 22, 2017: 22, 2016: 20 },
    vpet_region_cn:     { 2018: 24, 2017: 22, 2016: 23 },
    vpet_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_us:     { 2018: 0, 2017: 67, 2016: 0 },
    vpet_region_other:  { 2018: 20, 2017: 0, 2016: 0 },

    vpet_total_rate:    { 2018: 25, 2017: 22, 2016: 21 },

    hire_age_under30:   { 2018: 54, 2017: 47, 2016: { value: 48, note: 5 } },
    hire_age_30to50:    { 2018: 21, 2017: 17, 2016: { value: 24, note: 5 } },
    hire_age_over50:    { 2018: 16, 2017: 13, 2016: 16 },

    hire_gender_male:   { 2018: 25, 2017: 22, 2016: { value: 30, note: 5 } },
    hire_gender_female: { 2018: 34, 2017: 28, 2016: { value: 29, note: 5 } },

    hire_region_hk:     { 2018: 25, 2017: 24, 2016: 25 },
    hire_region_cn:     { 2018: 30, 2017: 25, 2016: { value: 36, note: 5 } },
    hire_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_us:     { 2018: 47, 2017: 0, 2016: { value: 100, note: 5 } },
    hire_region_other:  { 2018: 20, 2017: 0, 2016: 0 },

    hire_total_rate:    { 2018: 29, 2017: 24, 2016: 28 },

    training_cat_topman:    { 2018: 37.9, 2017: 34.76, 2016: 24.93 },
    training_cat_midman:    { 2018: 28.33, 2017: 22.20, 2016: 15.88 },
    training_cat_cf:        { 2018: 19.5, 2017: 9.61, 2016: 10.02 },
    training_cat_optech:    { 2018: 19.32, 2017: 18.88, 2016: 13.06 },
    training_cat_other:     { 2018: 7.62, 2017: 11.51, 2016: 6.88 },

    training_total_avg_hour: { 2018: 20.9, 2017: 14.09, 2016: 11.57 }
  },

  cathay: {
    energy_direct:      { 2018: 256677, 2017: 249292, 2016: { value: 245730, note: 5 } },
    energy_indirect:    { 2018: 500, 2017: 504, 2016: 682 },
    energy_total:       { 2018: 257177, 2017: 249796, 2016: 246412 },

    gas_direct:         { 2018: 18406, 2017: 18221, 2016: 17702 },
    gas_indirect:       { 2018: 75, 2017: 80, 2016: 80 },
    gas_total:          { 2018: 18481, 2017: 18301, 2016: 17782 },

    water_total:        { 2018: 975, 2017: 950, 2016: 866 },

    waste_haz_dis:      { 2018: 0.4, 2017: 0, 2016: 1 },
    waste_nonhaz_dis:   { 2018: 15206, 2017: 14218, 2016: 13794 },
    waste_haz_rec:      { 2018: 0, 2017: 0, 2016: 0 },
    waste_nonhaz_rec:   { 2018: 3491, 2017: 4268, 2016: 4414 },
    waste_total:        { 2018: 18697.4, 2017: 18486, 2016: 18209 },

    hs_ltir:            { 2018: 3.85, 2017: 3.28, 2016: 2.81 },
    hs_ldr:             { 2018: 86.61, 2017: 100.95, 2016: 70.38 },
    hs_fatalities:      { 2018: 2, 2017: 1, 2016: 0 },

    staff_gender_male:  { 2018: 13951, 2017: 14122, 2016: 17058 },
    staff_gender_female: { 2018: 17157, 2017: 17272, 2016: 19018 },
    staff_gender_total: { 2018: 31108, 2017: 31394, 2016: 36076 },

    staff_region_hk:    { 2018: 82, 2017: 81, 2016: 83 },
    staff_region_cn:    { 2018: 2, 2017: 3, 2016: 2 },
    staff_region_tw:    { 2018: 2, 2017: 2, 2016: 2 },
    staff_region_us:    { 2018: 2, 2017: 2, 2016: 1 },
    staff_region_other: { 2018: 11, 2017: 12, 2016: 12 },

    staff_cat_topman:   { 2018: 1, 2017: 1, 2016: -1 },
    staff_cat_midman:   { 2018: 8, 2017: 8, 2016: -1 },
    staff_cat_cf:       { 2018: 51, 2017: 51, 2016: -1 },
    staff_cat_optech:   { 2018: 31, 2017: 31, 2016: -1 },
    staff_cat_other:    { 2018: 8, 2017: 8, 2016: -1 },

    staff_age_under30:  { 2018: 18, 2017: 21, 2016: 26 },
    staff_age_30to50:   { 2018: 64, 2017: 62, 2016: 58 },
    staff_age_over50:   { 2018: 18, 2017: 17, 2016: 16 },

    vpet_age_under30:   { 2018: 24, 2017: 18, 2016: 15 },
    vpet_age_30to50:    { 2018: 7, 2017: 6, 2016: 5 },
    vpet_age_over50:    { 2018: 6, 2017: 10, 2016: 6 },

    vpet_gender_male:   { 2018: 10, 2017: 9, 2016: 9 },
    vpet_gender_female: { 2018: 11, 2017: 9, 2016: 7 },

    vpet_region_hk:     { 2018: 11, 2017: 10, 2016: 8 },
    vpet_region_cn:     { 2018: 8, 2017: 7, 2016: 11 },
    vpet_region_tw:     { 2018: 7, 2017: 4, 2016: 4 },
    vpet_region_us:     { 2018: 12, 2017: 2, 2016: 10 },
    vpet_region_other:  { 2018: 10, 2017: 8, 2016: 6 },

    vpet_total_rate:    { 2018: 10, 2017: 9, 2016: 8 },

    hire_age_under30:   { 2018: 25, 2017: 21, 2016: 20 },
    hire_age_30to50:    { 2018: 5, 2017: 4, 2016: 5 },
    hire_age_over50:    { 2018: 6, 2017: 5, 2016: 5 },

    hire_gender_male:   { 2018: 10, 2017: 10, 2016: 11 },
    hire_gender_female: { 2018: 8, 2017: 6, 2016: 7 },

    hire_region_hk:     { 2018: 10, 2017: 8, 2016: 10 },
    hire_region_cn:     { 2018: 7, 2017: 4, 2016: 1 },
    hire_region_tw:     { 2018: 1, 2017: 2, 2016: 1 },
    hire_region_us:     { 2018: 2, 2017: 3, 2016: 4 },
    hire_region_other:  { 2018: 5, 2017: 7, 2016: 9 },

    hire_total_rate:    { 2018: 9, 2017: 8, 2016: 9 },

    training_cat_topman:    { 2018: 6, 2017: -1, 2016: 2.75 },
    training_cat_midman:    { 2018: 20, 2017: -1, 2016: 14.47 },
    training_cat_cf:        { 2018: 52, 2017: -1, 2016: 26.62 },
    training_cat_optech:    { 2018: 82, 2017: -1, 2016: 12.30 },
    training_cat_other:     { 2018: 8, 2017: -1, 2016: 1.13 },

    training_total_avg_hour: { 2018: 30.59, 2017: 49.10, 2016: 21.28 }
  },

  haeco: {
    energy_direct:      { 2018: 423, 2017: 384, 2016: 349 },
    energy_indirect:    { 2018: 490, 2017: 470, 2016: 481 },
    energy_total:       { 2018: 913, 2017: 854, 2016: 830 },

    gas_direct:         { 2018: 33, 2017: 29, 2016: 27 },
    gas_indirect:       { 2018: 73, 2017: 77, 2016: 78 },
    gas_total:          { 2018: 106, 2017: 106, 2016: 105 },

    water_total:        { 2018: 633, 2017: 651, 2016: 710 },

    waste_haz_dis:      { 2018: 1470, 2017: 1442, 2016: 1135 },
    waste_nonhaz_dis:   { 2018: 4176, 2017: 4291, 2016: 4921 },
    waste_haz_rec:      { 2018: 0, 2017: 0, 2016: 148 },
    waste_nonhaz_rec:   { 2018: 1330, 2017: 1341, 2016: 717 },
    waste_total:        { 2018: 6976, 2017: 7074, 2016: 6921 },

    hs_ltir:            { 2018: 1.31, 2017: 1.46, 2016: 1.52 },
    hs_ldr:             { 2018: 48.57, 2017: 73.94, 2016: 59.11 },
    hs_fatalities:      { 2018: 0, 2017: 0, 2016: 0 },

    staff_gender_male:  { 2018: 12026, 2017: 11903, 2016: 12241 },
    staff_gender_female: { 2018: 2682, 2017: 2670, 2016: 2871 },
    staff_gender_total: { 2018: 14708, 2017: 14573, 2016: 15112 },

    staff_region_hk:    { 2018: 47, 2017: 48, 2016: 48 },
    staff_region_cn:    { 2018: 37, 2017: 36, 2016: 34 },
    staff_region_tw:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_us:    { 2018: 16, 2017: 16, 2016: 17 },
    staff_region_other: { 2018: 0, 2017: 0, 2016: 0 },

    staff_cat_topman:   { 2018: 1, 2017: 1, 2016: -1 },
    staff_cat_midman:   { 2018: 12, 2017: 11, 2016: -1 },
    staff_cat_cf:       { 2018: 9, 2017: 10, 2016: -1 },
    staff_cat_optech:   { 2018: 67, 2017: 67, 2016: -1 },
    staff_cat_other:    { 2018: 12, 2017: 12, 2016: -1 },

    staff_age_under30:  { 2018: 25, 2017: 26, 2016: 29 },
    staff_age_30to50:   { 2018: 56, 2017: 55, 2016: 51 },
    staff_age_over50:   { 2018: 19, 2017: 19, 2016: 19 },

    vpet_age_under30:   { 2018: 19, 2017: 16, 2016: 14 },
    vpet_age_30to50:    { 2018: 8, 2017: 9, 2016: 9 },
    vpet_age_over50:    { 2018: 7, 2017: 9, 2016: 9 },

    vpet_gender_male:   { 2018: 10, 2017: 11, 2016: 10 },
    vpet_gender_female: { 2018: 12, 2017: 11, 2016: 12 },

    vpet_region_hk:     { 2018: 11, 2017: 10, 2016: 10 },
    vpet_region_cn:     { 2018: 7, 2017: 7, 2016: 8 },
    vpet_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_us:     { 2018: 19, 2017: 22, 2016: 17 },
    vpet_region_other:  { 2018: 0, 2017: 0, 2016: 0 },

    vpet_total_rate:    { 2018: 11, 2017: 11, 2016: 10 },

    hire_age_under30:   { 2018: 30, 2017: 29, 2016: 22 },
    hire_age_30to50:    { 2018: 7, 2017: 7, 2016: 11 },
    hire_age_over50:    { 2018: 8, 2017: 8, 2016: 9 },

    hire_gender_male:   { 2018: 13, 2017: 13, 2016: 12 },
    hire_gender_female: { 2018: 14, 2017: 14, 2016: 20 },

    hire_region_hk:     { 2018: 11, 2017: 11, 2016: 16 },
    hire_region_cn:     { 2018: 9, 2017: 7, 2016: 4 },
    hire_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_us:     { 2018: 27, 2017: 30, 2016: 26 },
    hire_region_other:  { 2018: 0, 2017: 0, 2016: 0 },

    hire_total_rate:    { 2018: 13, 2017: 13, 2016: 14 },

    training_cat_topman:    { 2018: 12.20, 2017: 16.09, 2016: 27.39 },
    training_cat_midman:    { 2018: 59.48, 2017: 52.85, 2016: 83.95 },
    training_cat_cf:        { 2018: 40.69, 2017: 78.10, 2016: 40.88 },
    training_cat_optech:    { 2018: 55.33, 2017: 45.32, 2016: 62.19 },
    training_cat_other:     { 2018: 80.98, 2017: 53.94, 2016: 163.54 },

    training_total_avg_hour: { 2018: 57.36, 2017: 50.64, 2016: 65.27 }
  },

  beverages: {
    energy_direct:      { 2018: 1130, 2017: 767, 2016: 781 },
    energy_indirect:    { 2018: 1807, 2017: 1125, 2016: 1170 },
    energy_total:       { 2018: 2937, 2017: 1892, 2016: 1951 },

    gas_direct:         { 2018: 81, 2017: 52, 2016: 55 },
    gas_indirect:       { 2018: 275, 2017: 193, 2016: 194 },
    gas_total:          { 2018: 356, 2017: 245, 2016: 249 },

    water_total:        { 2018: 13439, 2017: 7493, 2016: 7585 },

    waste_haz_dis:      { 2018: 51, 2017: 64, 2016: 110 },
    waste_nonhaz_dis:   { 2018: 12752, 2017: 774, 2016: 722 },
    waste_haz_rec:      { 2018: 43, 2017: 2, 2016: 0 },
    waste_nonhaz_rec:   { 2018: 16668, 2017: 7827, 2016: 10342 },
    waste_total:        { 2018: 29514, 2017: 8667, 2016: 11174 },

    hs_ltir:            { 2018: 0.54, 2017: 0.51, 2016: 0.50 },
    hs_ldr:             { 2018: 27.72, 2017: 13.54, 2016: 14.99 },
    hs_fatalities:      { 2018: 0, 2017: 1, 2016: 0 },

    staff_gender_male:  { 2018: 22472, 2017: 22120, 2016: 16573 },
    staff_gender_female: { 2018: 7385, 2017: 6948, 2016: 4598 },
    staff_gender_total: { 2018: 29857, 2017: 29068, 2016: 21171 },

    staff_region_hk:    { 2018: 5, 2017: 5, 2016: 7 },
    staff_region_cn:    { 2018: 69, 2017: 69, 2016: 68 },
    staff_region_tw:    { 2018: 3, 2017: 3, 2016: 4 },
    staff_region_us:    { 2018: 23, 2017: 23, 2016: 20 },
    staff_region_other: { 2018: 0, 2017: 0, 2016: 0 },

    staff_cat_topman:   { 2018: 1, 2017: 1, 2016: -1 },
    staff_cat_midman:   { 2018: 9, 2017: 13, 2016: -1 },
    staff_cat_cf:       { 2018: 54, 2017: 51, 2016: -1 },
    staff_cat_optech:   { 2018: 31, 2017: 30, 2016: -1 },
    staff_cat_other:    { 2018: 6, 2017: 6, 2016: -1 },

    staff_age_under30:  { 2018: 25, 2017: 26, 2016: 31 },
    staff_age_30to50:   { 2018: 66, 2017: 65, 2016: 61 },
    staff_age_over50:   { 2018: 9, 2017: 9, 2016: 8 },

    vpet_age_under30:   { 2018: 39, 2017: 34, 2016: 29 },
    vpet_age_30to50:    { 2018: 15, 2017: 14, 2016: 12 },
    vpet_age_over50:    { 2018: 2, 2017: 8, 2016: 5 },

    vpet_gender_male:   { 2018: 21, 2017: 20, 2016: 18 },
    vpet_gender_female: { 2018: 16, 2017: 16, 2016: 13 },

    vpet_region_hk:     { 2018: 27, 2017: 26, 2016: 19 },
    vpet_region_cn:     { 2018: 17, 2017: 17, 2016: 14 },
    vpet_region_tw:     { 2018: 10, 2017: 14, 2016: 7 },
    vpet_region_us:     { 2018: 26, 2017: 25, 2016: 29 },
    vpet_region_other:  { 2018: 0, 2017: 6, 2016: 0 },

    vpet_total_rate:    { 2018: 20, 2017: 19, 2016: 17 },

    hire_age_under30:   { 2018: 55, 2017: 57, 2016: 47 },
    hire_age_30to50:    { 2018: 17, 2017: 47, 2016: 19 },
    hire_age_over50:    { 2018: 8, 2017: 26, 2016: 28 },

    hire_gender_male:   { 2018: 25, 2017: 46, 2016: 30 },
    hire_gender_female: { 2018: 25, 2017: 53, 2016: 23 },

    hire_region_hk:     { 2018: 34, 2017: 24, 2016: 27 },
    hire_region_cn:     { 2018: 23, 2017: 55, 2016: 19 },
    hire_region_tw:     { 2018: 17, 2017: 12, 2016: 10 },
    hire_region_us:     { 2018: 32, 2017: 35, 2016: 64 },
    hire_region_other:  { 2018: 0, 2017: 11, 2016: 0 },

    hire_total_rate:    { 2018: 25, 2017: 48, 2016: 29 },

    training_cat_topman:    { 2018: 38.22, 2017: 41.41, 2016: 58.49 },
    training_cat_midman:    { 2018: 29.83, 2017: 23.90, 2016: 28.57 },
    training_cat_cf:        { 2018: 21.98, 2017: 16.35, 2016: 15.15 },
    training_cat_optech:    { 2018: 39.82, 2017: 26.71, 2016: 32.95 },
    training_cat_other:     { 2018: 25.91, 2017: 18.86, 2016: 22.25 },

    training_total_avg_hour: { 2018: 28.63, 2017: 20.70, 2016: 23.68 }
  },

  offshore: {
    energy_direct:      { 2018: 1260, 2017: 1417, 2016: 1337 },
    energy_indirect:    { 2018: 5, 2017: 5, 2016: 5 },
    energy_total:       { 2018: 1265, 2017: 1422, 2016: 1342 },

    gas_direct:         { 2018: 93, 2017: 106, 2016: 108 },
    gas_indirect:       { 2018: 0.5, 2017: 1, 2016: 1 },
    gas_total:          { 2018: 94, 2017: 107, 2016: 109 },

    water_total:        { 2018: -1, 2017: -1 , 2016: -1 },

    waste_haz_dis:      { 2018: 1581, 2017: 1424, 2016: 1973 },
    waste_nonhaz_dis:   { 2018: 0, 2017: 0, 2016: 0 },
    waste_haz_rec:      { 2018: 0, 2017: 0, 2016: 0 },
    waste_nonhaz_rec:   { 2018: 2, 2017: 1, 2016: 16 },
    waste_total:        { 2018: 1583, 2017: 1425, 2016: 1989 },

    hs_ltir:            { 2018: 0.15, 2017: 0.07, 2016: 0.07 },
    hs_ldr:             { 2018: 17.22, 2017: 2.56, 2016: 0.85 },
    hs_fatalities:      { 2018: 0, 2017: 0, 2016: 0 },

    staff_gender_male:  { 2018: 2187, 2017: 1889, 2016: 2013 },
    staff_gender_female: { 2018: 211, 2017: 245, 2016: 251 },
    staff_gender_total: { 2018: 2398, 2017: 2134, 2016: 2264 },

    staff_region_hk:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_cn:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_tw:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_us:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_other: { 2018: 100, 2017: 100, 2016: 100 },

    staff_cat_topman:   { 2018: 1, 2017: 1, 2016: -1 },
    staff_cat_midman:   { 2018: 14, 2017: 17, 2016: -1 },
    staff_cat_cf:       { 2018: 0, 2017: 0, 2016: -1 },
    staff_cat_optech:   { 2018: 85, 2017: 0, 2016: -1 },
    staff_cat_other:    { 2018: 0, 2017: 82, 2016: -1 },

    staff_age_under30:  { 2018: 10, 2017: 16, 2016: 14 },
    staff_age_30to50:   { 2018: 69, 2017: 67, 2016:  68},
    staff_age_over50:   { 2018: 21, 2017: 17, 2016: 18 },

    vpet_age_under30:   { 2018: 25, 2017: 6, 2016: 12 },
    vpet_age_30to50:    { 2018: 3, 2017: 6, 2016: 5 },
    vpet_age_over50:    { 2018: 27, 2017: 10, 2016: 4 },

    vpet_gender_male:   { 2018: 9, 2017: 7, 2016: 5 },
    vpet_gender_female: { 2018: 18, 2017: 6, 2016: 12 },

    vpet_region_hk:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_cn:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_us:     { 2018: 50, 2017: 0, 2016: 0 },
    vpet_region_other:  { 2018: 10, 2017: 7, 2016: 6 },

    vpet_total_rate:    { 2018: 10, 2017: 7, 2016: 6 },

    hire_age_under30:   { 2018: 23, 2017: 7, 2016: 17 },
    hire_age_30to50:    { 2018: 2, 2017: 6, 2016: 6 },
    hire_age_over50:    { 2018: 22, 2017: 6, 2016: 3 },

    hire_gender_male:   { 2018: 6, 2017: 6, 2016: 6 },
    hire_gender_female: { 2018: 21, 2017: 6, 2016: 12 },

    hire_region_hk:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_cn:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_us:     { 2018: 0, 2017: 100, 2016: 0 },
    hire_region_other:  { 2018: 9, 2017: 6, 2016: 7 },

    hire_total_rate:    { 2018: 9, 2017: 6, 2016: 7 },

    training_cat_topman:    { 2018: 3.70, 2017: 0.47, 2016: 1.80 },
    training_cat_midman:    { 2018: 6.24, 2017: 0.03, 2016: 15.73 },
    training_cat_cf:        { 2018: 0.00, 2017: 0.00, 2016: 0.00 },
    training_cat_optech:    { 2018: 66.41, 2017: 0.00, 2016: 4.41 },
    training_cat_other:     { 2018: 0.00, 2017: 25.77, 2016: 155.71 },

    training_total_avg_hour: { 2018: 57.35, 2017: 22.88, 2016: 123.08 }
  },

  hud: {
    energy_direct:      { 2018: 345, 2017: 341, 2016: 329 },
    energy_indirect:    { 2018: 15, 2017: 17, 2016: 19 },
    energy_total:       { 2018: 360, 2017: 358, 2016: 348 },

    gas_direct:         { 2018: 28, 2017: 28, 2016: 27 },
    gas_indirect:       { 2018: 2.3, 2017: 3, 2016: 3 },
    gas_total:          { 2018: 30, 2017: 31, 2016: 30 },

    water_total:        { 2018: 85, 2017: 115, 2016: 77 },

    waste_haz_dis:      { 2018: 0, 2017: 0, 2016: 0 },
    waste_nonhaz_dis:   { 2018: 0, 2017: 0, 2016: 0 },
    waste_haz_rec:      { 2018: 0, 2017: 0, 2016: 0 },
    waste_nonhaz_rec:   { 2018: 208, 2017: 84, 2016: 184 },
    waste_total:        { 2018: 208, 2017: 84, 2016: 184 },

    hs_ltir:            { 2018: 0.44, 2017: 0.86, 2016: 1.20 },
    hs_ldr:             { 2018: 102.84, 2017: 83.95, 2016: 131.71 },
    hs_fatalities:      { 2018: 0, 2017: 0, 2016: 0 },

    staff_gender_male:  { 2018: 595, 2017: 652, 2016: 592 },
    staff_gender_female: { 2018: 61, 2017: 57, 2016: 58 },
    staff_gender_total: { 2018: 656, 2017: 709, 2016: 650 },

    staff_region_hk:    { 2018: 100, 2017: 100, 2016: 100 },
    staff_region_cn:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_tw:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_us:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_other: { 2018: 0, 2017: 0, 2016: 0 },

    staff_cat_topman:   { 2018: 5, 2017: 3, 2016: -1 },
    staff_cat_midman:   { 2018: 12, 2017: 8, 2016: -1 },
    staff_cat_cf:       { 2018: 0, 2017: 0, 2016: -1 },
    staff_cat_optech:   { 2018: 83, 2017: 89, 2016: -1 },
    staff_cat_other:    { 2018: 0, 2017: 0, 2016: -1 },

    staff_age_under30:  { 2018: 20, 2017: 16, 2016: 18 },
    staff_age_30to50:   { 2018: 42, 2017: 36, 2016: 37 },
    staff_age_over50:   { 2018: 38, 2017: 48, 2016: 44 },

    vpet_age_under30:   { 2018: 27, 2017: 20, 2016: 10 },
    vpet_age_30to50:    { 2018: 22, 2017: 15, 2016: 12 },
    vpet_age_over50:    { 2018: 7, 2017: 6, 2016: 8 },

    vpet_gender_male:   { 2018: 17, 2017: 12, 2016: 10 },
    vpet_gender_female: { 2018: 18, 2017: 9, 2016: 9 },

    vpet_region_hk:     { 2018: 17, 2017: 12, 2016: 10 },
    vpet_region_cn:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_us:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_other:  { 2018: 0, 2017: 0, 2016: 0 },

    vpet_total_rate:    { 2018: 17, 2017: 12, 2016: 10 },

    hire_age_under30:   { 2018: 30, 2017: 31, 2016: 50 },
    hire_age_30to50:    { 2018: 14, 2017: 34, 2016: 45 },
    hire_age_over50:    { 2018: 7, 2017: 33, 2016: 49 },

    hire_gender_male:   { 2018: 13, 2017: 34, 2016: 47 },
    hire_gender_female: { 2018: 23, 2017: 19, 2016: 50 },

    hire_region_hk:     { 2018: 15, 2017: 33, 2016: 48 },
    hire_region_cn:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_us:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_other:  { 2018: 0, 2017: 0, 2016: 0 },

    hire_total_rate:    { 2018: 15, 2017: 33, 2016: 48 },

    training_cat_topman:    { 2018: 6.83, 2017: 7.69, 2016: 5.11 },
    training_cat_midman:    { 2018: 7.71, 2017: 5.68, 2016: 6.11 },
    training_cat_cf:        { 2018: 0.00, 2017: 0.00, 2016: 0.00 },
    training_cat_optech:    { 2018: 17.08, 2017: 8.00, 2016: 8.48 },
    training_cat_other:     { 2018: 0.00, 2017: 0.00, 2016: 0.00 },

    training_total_avg_hour: { 2018: 15.46, 2017: 7.81, 2016: 7.91 }
  },

  trading: {
    energy_direct:      { 2018: 84, 2017: 67, 2016: 51 },
    energy_indirect:    { 2018: 207, 2017: 265, 2016: 244 },
    energy_total:       { 2018: 291, 2017: 332, 2016: 295 },

    gas_direct:         { 2018: 6, 2017: 7, 2016: 6 },
    gas_indirect:       { 2018: 37, 2017: 50, 2016: 46 },
    gas_total:          { 2018: 43, 2017: 57, 2016: 51 },

    water_total:        { 2018: 400, 2017: 455, 2016: 178 },

    waste_haz_dis:      { 2018: 2, 2017: 42, 2016: 1 },
    waste_nonhaz_dis:   { 2018: 0, 2017: 0, 2016: 0 },
    waste_haz_rec:      { 2018: 0, 2017: 0, 2016: 0 },
    waste_nonhaz_rec:   { 2018: 782, 2017: 1353, 2016: 457 },
    waste_total:        { 2018: 784, 2017: 1395, 2016: 458 },

    hs_ltir:            { 2018: 0.92, 2017: 0.92, 2016: 1.08 },
    hs_ldr:             { 2018: 30.99, 2017: 33.88, 2016: 37.78 },
    hs_fatalities:      { 2018: 0, 2017: 2, 2016: 1 },

    staff_gender_male:  { 2018: 3222, 2017: 3470, 2016: 3593 },
    staff_gender_female: { 2018: 4880, 2017: 5214, 2016: 5505 },
    staff_gender_total: { 2018: 8102, 2017: 8684, 2016: 9098 },

    staff_region_hk:    { 2018: 38, 2017: 33, 2016: 33 },
    staff_region_cn:    { 2018: 45, 2017: 52, 2016: 53 },
    staff_region_tw:    { 2018: 16, 2017: 14, 2016: 13 },
    staff_region_us:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_other: { 2018: 1, 2017: 1, 2016: 1 },

    staff_cat_topman:   { 2018: 1, 2017: 1, 2016: -1 },
    staff_cat_midman:   { 2018: 5, 2017: 6, 2016: -1 },
    staff_cat_cf:       { 2018: 59, 2017: 57, 2016: -1 },
    staff_cat_optech:   { 2018: 21, 2017: 24, 2016: -1 },
    staff_cat_other:    { 2018: 13, 2017: 12, 2016: -1 },

    staff_age_under30:  { 2018: 39, 2017: 45, 2016: 48 },
    staff_age_30to50:   { 2018: 54, 2017: 51, 2016: 47 },
    staff_age_over50:   { 2018: 7, 2017: 4, 2016: 4 },

    vpet_age_under30:   { 2018: 98, 2017: 99, 2016: 67 },
    vpet_age_30to50:    { 2018: 28, 2017: 31, 2016: 33 },
    vpet_age_over50:    { 2018: 13, 2017: 19, 2016: 19 },

    vpet_gender_male:   { 2018: 47, 2017: 55, 2016: 43 },
    vpet_gender_female: { 2018: 60, 2017: 67, 2016: 52 },

    vpet_region_hk:     { 2018: 93, 2017: 90, 2016: 59 },
    vpet_region_cn:     { 2018: 42, 2017: 56, 2016: 49 },
    vpet_region_tw:     { 2018: 12, 2017: 26, 2016: 22 },
    vpet_region_us:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_other:  { 2018: 34, 2017: 23, 2016: 56 },

    vpet_total_rate:    { 2018: 55, 2017: 62, 2016: 48 },

    hire_age_under30:   { 2018: 116, 2017: 105, 2016: 80 },
    hire_age_30to50:    { 2018: 33, 2017: 36, 2016: 39 },
    hire_age_over50:    { 2018: 16, 2017: 19, 2016: 29 },

    hire_gender_male:   { 2018: 59, 2017: 60, 2016: 51 },
    hire_gender_female: { 2018: 68, 2017: 70, 2016: 63 },

    hire_region_hk:     { 2018: 101, 2017: 91, 2016: 61 },
    hire_region_cn:     { 2018: 51, 2017: 61, 2016: 64 },
    hire_region_tw:     { 2018: 20, 2017: 36, 2016: 25 },
    hire_region_us:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_other:  { 2018: 32, 2017: 40, 2016: 79 },

    hire_total_rate:    { 2018: 65, 2017: 66, 2016: 58 },

    training_cat_topman:    { 2018: 13.81, 2017: 17.29, 2016: 13.07 },
    training_cat_midman:    { 2018: 14.88, 2017: 14.87, 2016: 11.22 },
    training_cat_cf:        { 2018: 24.58, 2017: 5.41, 2016: 6.49 },
    training_cat_optech:    { 2018: 9.56, 2017: 15.59, 2016: 13.25 },
    training_cat_other:     { 2018: 7.32, 2017: 7.20, 2016: 6.06 },

    training_total_avg_hour: { 2018: 18.51, 2017: 8.46, 2016: 8.35 }
  },

  head: {
    energy_direct:      { 2018: -1, 2017: -1, 2016: -1 },
    energy_indirect:    { 2018: -1, 2017: -1, 2016: -1 },
    energy_total:       { 2018: -1, 2017: -1, 2016: -1 },

    gas_direct:         { 2018: -1, 2017: -1, 2016: -1 },
    gas_indirect:       { 2018: -1, 2017: -1, 2016: -1 },
    gas_total:          { 2018: -1, 2017: -1, 2016: -1 },

    water_total:        { 2018: -1, 2017: -1, 2016: -1 },

    waste_haz_dis:      { 2018: -1, 2017: -1, 2016: -1 },
    waste_nonhaz_dis:   { 2018: -1, 2017: -1, 2016: -1 },
    waste_haz_rec:      { 2018: -1, 2017: -1, 2016: -1 },
    waste_nonhaz_rec:   { 2018: -1, 2017: -1, 2016: -1 },
    waste_total:        { 2018: -1, 2017: -1, 2016: -1 },

    hs_ltir:            { 2018: 0.00, 2017: 0.00, 2016: 0.00 },
    hs_ldr:             { 2018: 0.00, 2017: 0.00, 2016: 0.00 },
    hs_fatalities:      { 2018: 0, 2017: 0, 2016: 0 },

    staff_gender_male:  { 2018: 11, 2017: 13, 2016: 14 },
    staff_gender_female: { 2018: 27, 2017: 28, 2016: 31 },
    staff_gender_total: { 2018: 38, 2017: 41, 2016: 45 },

    staff_region_hk:    { 2018: 100, 2017: 100, 2016: 96 },
    staff_region_cn:    { 2018: 0, 2017: 0, 2016: 4 },
    staff_region_tw:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_us:    { 2018: 0, 2017: 0, 2016: 0 },
    staff_region_other: { 2018: 0, 2017: 0, 2016: 0 },

    staff_cat_topman:   { 2018: 29, 2017: 27, 2016: -1 },
    staff_cat_midman:   { 2018: 26, 2017: 29, 2016: -1 },
    staff_cat_cf:       { 2018: 0, 2017: 0, 2016: -1 },
    staff_cat_optech:   { 2018: 0, 2017: 0, 2016: -1 },
    staff_cat_other:    { 2018: 45, 2017: 44, 2016: -1 },

    staff_age_under30:  { 2018: 3, 2017: 10, 2016: 7 },
    staff_age_30to50:   { 2018: 63, 2017: 54, 2016: 67 },
    staff_age_over50:   { 2018: 34, 2017: 37, 2016: 27 },

    vpet_age_under30:   { 2018: 40, 2017: 29, 2016: 0 },
    vpet_age_30to50:    { 2018: 9, 2017: 4, 2016: 0 },
    vpet_age_over50:    { 2018: 0, 2017: 0, 2016: 0 },

    vpet_gender_male:   { 2018: 25, 2017: 7, 2016: 0 },
    vpet_gender_female: { 2018: 0, 2017: 3, 2016: 0 },

    vpet_region_hk:     { 2018: 8, 2017: 5, 2016: 0 },
    vpet_region_cn:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_us:     { 2018: 0, 2017: 0, 2016: 0 },
    vpet_region_other:  { 2018: 0, 2017: 0, 2016: 0 },

    vpet_total_rate:    { 2018: 8, 2017: 5, 2016: 0 },

    hire_age_under30:   { 2018: 100, 2017: 25, 2016: 0 },
    hire_age_30to50:    { 2018: 4, 2017: 5, 2016: 7 },
    hire_age_over50:    { 2018: 0, 2017: 0, 2016: 0 },

    hire_gender_male:   { 2018: 18, 2017: 15, 2016: 14 },
    hire_gender_female: { 2018: 0, 2017: 0, 2016: 0 },

    hire_region_hk:     { 2018: 5, 2017: 5, 2016: 5 },
    hire_region_cn:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_tw:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_us:     { 2018: 0, 2017: 0, 2016: 0 },
    hire_region_other:  { 2018: 0, 2017: 0, 2016: 0 },

    hire_total_rate:    { 2018: 5, 2017: 5, 2016: 4 },

    training_cat_topman:    { 2018: 15.56, 2017: 11.55, 2016: 14.88 },
    training_cat_midman:    { 2018: 24.38, 2017: 25.58, 2016: 22.29 },
    training_cat_cf:        { 2018: 0, 2017: 0.00, 2016: 0.00 },
    training_cat_optech:    { 2018: 0, 2017: 0.00, 2016: 0.00 },
    training_cat_other:     { 2018: 8.25, 2017: 19.93, 2016: 1.00 },

    training_total_avg_hour: { 2018: 14.61, 2017: 19.34, 2016: 18.75 }
  },

  total: {
    energy_direct:      { 2018: 260085, 2017: 252412, 2016: 248730 },
    energy_indirect:    { 2018: 4059, 2017: 3345, 2016: 3624 },
    energy_total:       { 2018: 264144, 2017: { value: 255756, note: 'R' }, 2016: 252355 },

    gas_direct:         { 2018: 18657, 2017: 18454, 2016: 17938 },
    gas_indirect:       { 2018: 667.8, 2017: 608, 2016: 618 },
    gas_total:          { 2018: 19325, 2017: { value: 19062, note: 'R' }, 2016: 18555 },

    water_total:        { 2018: 17131, 2017: { value: 11054, note: 'R' }, 2016: 10938 },

    waste_haz_dis:      { 2018: 3104.8, 2017: 2973, 2016: 3220 },
    waste_nonhaz_dis:   { 2018: 62934, 2017: 51599, 2016: 57814 },
    waste_haz_rec:      { 2018: 43.4, 2017: 2, 2016: 149 },
    waste_nonhaz_rec:   { 2018: 33123, 2017: 22146, 2016: 20480 },
    waste_total:        { 2018: 99205.2, 2017: 76720, 2016: 81663 },

    hs_ltir:            { 2018: 1.79, 2017: 1.73, 2016: 1.62 },
    hs_ldr:             { 2018: 52.13, 2017: 59.40, 2016: 47.00 },
    hs_fatalities:      { 2018: 2, 2017: { value: 4, note: 'R' }, 2016: 1 },

    staff_gender_male:  { 2018: 57954, 2017: 57376, 2016: { value: 55393, note: 5 } },
    staff_gender_female: { 2018: 34793, 2017: 34546, 2016: { value: 34437, note: 5 } },
    staff_gender_total: { 2018: 92747, 2017: 91922, 2016: { value: 89830, note: 5 } },

    staff_region_hk:    { 2018: 44, 2017: 44, 2016: { value: 51, note: 5 } },
    staff_region_cn:    { 2018: 35, 2017: 36, 2016: 31 },
    staff_region_tw:    { 2018: 3, 2017: 3, 2016: 3 },
    staff_region_us:    { 2018: 11, 2017: 11, 2016: 8 },
    staff_region_other: { 2018: 6, 2017: 7, 2016: 7 },

    staff_cat_topman:   { 2018: 1, 2017: 1, 2016: -1 },
    staff_cat_midman:   { 2018: 10, 2017: 11, 2016: -1 },
    staff_cat_cf:       { 2018: 45, 2017: 44, 2016: -1 },
    staff_cat_optech:   { 2018: 36, 2017: 35, 2016: -1 },
    staff_cat_other:    { 2018: 8, 2017: 10, 2016: -1 },

    staff_age_under30:  { 2018: 24, 2017: 26, 2016: 30 },
    staff_age_30to50:   { 2018: 62, 2017: 60, 2016: 56 },
    staff_age_over50:   { 2018: 14, 2017: 14, 2016: 14 },

    vpet_age_under30:   { 2018: 40, 2017: 37, 2016: { value: 30, note: 5 } },
    vpet_age_30to50:    { 2018: 12, 2017: 12, 2016: { value: 10, note: 5 } },
    vpet_age_over50:    { 2018: 7, 2017: 10, 2016: { value: 7, note: 5 }, },

    vpet_gender_male:   { 2018: 17, 2017: 17, 2016: { value: 14, note: 5 } },
    vpet_gender_female: { 2018: 21, 2017: 21, 2016: { value: 16, note: 5 } },

    vpet_region_hk:     { 2018: 18, 2017: 17, 2016: { value: 13, note: 5 } },
    vpet_region_cn:     { 2018: 19, 2017: 22, 2016: { value: 18, note: 5 } },
    vpet_region_tw:     { 2018: 16, 2017: 17, 2016: { value: 13, note: 5 } },
    vpet_region_us:     { 2018: 23, 2017: 22, 2016: { value: 18, note: 5 } },
    vpet_region_other:  { 2018: 10, 2017: 8, 2016: { value: 7, note: 5 }, },

    vpet_total_rate:    { 2018: 19, 2017: 19, 2016: { value: 15, note: 5 } },

    hire_age_under30:   { 2018: 52, 2017: 50, 2016: 39 },
    hire_age_30to50:    { 2018: 13, 2017: 23, 2016: 14 },
    hire_age_over50:    { 2018: 8, 2017: 12, 2016: 12 },

    hire_gender_male:   { 2018: 21, 2017: 29, 2016: 22 },
    hire_gender_female: { 2018: 23, 2017: 28, 2016: 21 },

    hire_region_hk:     { 2018: 17, 2017: 17, 2016: 16 },
    hire_region_cn:     { 2018: 45, 2017: 45, 2016: 25 },
    hire_region_tw:     { 2018: 20, 2017: 20, 2016: 14 },
    hire_region_us:     { 2018: 33, 2017: 33, 2016: 45 },
    hire_region_other:  { 2018: 8, 2017: 8, 2016: 9 },

    hire_total_rate:    { 2018: 21, 2017: 29, 2016: 21 },

    training_cat_topman:    { 2018: 26.59, 2017: 51.69, 2016: 15.52 },
    training_cat_midman:    { 2018: 31.39, 2017: 27.21, 2016: 39.93 },
    training_cat_cf:        { 2018: 33.94, 2017: 20.27, 2016: 23.04 },
    training_cat_optech:    { 2018: 55.19, 2017: 33.27, 2016: 26.55 },
    training_cat_other:     { 2018: 29.07, 2017: 26.48, 2016: 69.88 },

    training_total_avg_hour: { 2018: 40.72, 2017: 33.19, 2016: 30.08 }
  }
};

var cat_unit = {
  "energy_": "(thousand GJ)",
  "gas_": "(thousand tonnes CO<sub>2</sub>e)",
  "water_": "(thousands cbm)",
  "waste_": "(tonnes)"
}

var units = {
    percent: ["staff_age", "staff_region", "staff_cat", "vpet_age", "vpet_gender", "vpet_region", "vpet_total", "hire_age", "hire_gender", "hire_region", "hire_total"]
};

var yoy = ["energy_total", "gas_total", "water_total", "waste_total", "hs_ltir", "hs_ldr"];

var groupColors = {
  "Swire Properties"            : ["#763f98","#9165ad","#ad8cc1","#c8b2d6","#e4d9ea"],
  "Cathay Pacific Group"        : ["#00a3b5","#33b5c4","#66c8d3","#99dae1","#ccedf0"],
  "HAECO Group"                 : ["#66c8d3","#85d3dc","#a3dee5","#c2e9ed","#e0f4f6"],
  "Beverages"                   : ["#ef413d","#f26764","#f58d8b","#f9b3b1","#fcd9d8"],
  "Swire Pacific Offshore"      : ["#286fb7","#538cc5","#7ea9d4","#a9c5e2","#d4e2f1"],
  "HUD Group"                   : ["#7ea9d4","#98badd","#b2cbe5","#cbddee","#e5eef6"],
  "Trading & Industrial"        : ["#715e54","#8d7e76","#aa9e98","#c6bfbb","#e3dfdd"],
  "Swire Pacific (Head Office)" : ["#002f87","#33599f","#6682b7","#99accf","#ccd5e7"],
  "All"                         : ["#ea0029","#ee3354","#f2667f","#f799a9","#fbccd4"]
};

var metrics = {
  hs_: [
      ["ltir", "Lost time injury rate (LTIR)"],
      ["ldr", "Lost day rate (LDR)"],
      ["fatalities", "Total fatalities"]
  ],
  energy_: [
      ["direct", "Direct energy consumption"],
      ["indirect", "Indirect energy consumption"],
      ["total", "Total <sup>1</sup>"]
  ],
  gas_: [
      ["direct", "Direct (scope 1) <sup>2</sup>"],
      ["indirect", "Indirect (scope 2)"],
      ["total", "Total <sup>1</sup>"]
  ],
  water_: [["total", "Water used <sup>3</sup>"]],
  waste_: [
      ["haz_dis", "Hazardous waste disposed"],
      ["nonhaz_dis", "Non-hazardous waste disposed"],
      ["haz_rec", "Hazardous waste recycled"],
      ["nonhaz_rec", "Non-hazardous waste recycled"],
      ["total", "Total"]
  ],
  gender_: [
      ["male", "Male"],
      ["female", "Female"],
      ["total", "Total"]
  ],
  region_: [
      ["hk", "Hong Kong & Macau"],
      ["cn", "Mainland China"],
      ["tw", "Taiwan"],
      ["us", "USA"],
      ["other", "Others"]
  ],
  cat_: [
      ["topman", "Executive &ndash; top/senior management"],
      [
          "midman",
          "Executive &ndash; middle/junior management & supervisory"
      ],
      ["cf", "Non-exec &ndash; customer facing staff"],
      [
          "optech",
          "Non-exec &ndash; non-customer facing operational/technical staff"
      ],
      ["other", "Others"]
  ],
  age_: [
      ["under30", "Under 30 years old"],
      ["30to50", "30 to 50 years old"],
      ["over50", "Over 50 years old"]
  ],
  vpet_total_: [
      ["rate", "Total voluntary turnover rate of permanent employees"]
  ],
  hire_total_: [["rate", "Total new hires rate"]],
  training_total_: [["avg_hour", "Total average hours of training"]]
};
