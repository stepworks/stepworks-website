(function() {
    // A (possibly faster) way to get the current timestamp as an integer. (taken from Underscorejs)
    function now() {
        return (
            Date.now ||
            function() {
                return new Date().getTime();
            }
        );
    }

    // it delays the execution of a function (taken from Underscorejs)
    function debounce(func, wait, immediate) {
        var timeout, args, context, timestamp, result;

        var later = function() {
            var last = now() - timestamp;
            if (last < wait) {
                timeout = setTimeout(later, wait - last);
            } else {
                timeout = null;

                if (!immediate) {
                    result = func.apply(context, args);
                    context = args = null;
                }
            }
        };

        return function() {
            context = this;
            args = arguments;
            timestamp = now();
            var callNow = immediate && !timeout;

            if (!timeout) {
                timeout = setTimeout(later, wait);
            }

            if (callNow) {
                result = func.apply(context, args);
                context = args = null;
            }

            return result;
        };
    }

    function isScrolledIntoView(elem, buffer) {
        if (elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top - buffer;
            var elemBottom = elemTop + $(elem).height();

            return elemBottom <= docViewBottom && elemTop >= docViewTop;
        }
    }

    // throttle function
    var throttle = function(func, wait, options) {
        var context,
            args,
            result,
            timeout = null,
            previous = 0;

        if (!options) {
            options = {};
        }

        var later = function() {
            previous = options.leading === false ? 0 : new Date().getTime();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) {
                context = args = null;
            }
        };

        return function() {
            var now = new Date().getTime();

            if (!previous && options.leading === false) {
                previous = now;
            }

            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);

                if (!timeout) {
                    context = args = null;
                }
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    };

    var Site = function() {
        var $window = $(window),
            $body = $("body"),
            isIE = /MSIE \d|Trident.*rv:/.test(navigator.userAgent),
            // iPad = /iPad/i.test(navigator.userAgent),

            inittop = 427; // magic number

        //variables needed for hiding header on scroll
        lastScrollTop = 0;
        let didScroll;
        let delta = 250;
        let navbarHeight = $("header").outerHeight();

        // Debounce functions
        var scrollWindow = debounce(scrollControl, 250),
            scrollThrottle = throttle(scrollThrottle, 10);
            //resizeWindow = debounce(resizeControl, 250);
        //var scrollTableThrottle = throttle(scrollTableThrottle, 100);

        function scrollControl() {}

        // function resizeControl() {
        //     $(".insight-item").each(function() {
        //         let image = $(this).find("img");
        //         let image_width = image.width();
        //         // (original height / original width) x new width = new height
        //         let new_height = 200 / 315 * image_width;
        //         image.css("height", new_height)
        //     });
        //     $(".siblings .cell").each(function() {
        //         let image = $(this).find("img");
        //         let image_width = image.width();
        //         // (original height / original width) x new width = new height
        //         if(image_width) {
        //             let new_height = 200 / 315 * image_width;
        //             image.css("height", new_height)
        //         }
        //     });
        // }

        function scrollThrottle() {
            if (!$body.hasClass("new2")) {
                $(window).scroll(function(event) {
                    didScroll = true;
                });

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 250);
            }

            if ($body.hasClass("page-glossary")) {
                var top = $(".alphatop").offset().top;
                if (top > inittop) {
                    $(".alphatop").addClass("stuck");
                } else {
                    $(".alphatop").removeClass("stuck");
                }
            }

            // work hero fade
            // var ratio = 1 - (($window.scrollTop() / $window.height()) * 0.9);
            // $(".page-work-item .hero img").css("opacity", ratio);

            // insights progress bar
            if ($(".progress").length) {
                var height = Math.round(
                        $body.height() +
                        $("header").outerHeight() -
                        $window.height() -
                        $(".signup").outerHeight() -
                        $("footer").outerHeight()
                    ),
                    pos = $window.scrollTop() / height;

                $(".bar").css("transform", "scaleX(" + pos + ")");
            }

            // reveal animation
            $(".animate:not(.animated,.animating)").each(function(i) {
                var $e = $(this),
                    offset = ($e.offset().top - $window.scrollTop()) / $window.height();

                if (offset < 0.85) {
                    $e.addClass("animating");
                    var delay = i * 200;
                    setTimeout(function() {
                        $e.addClass("animated");
                    }, delay);
                }
            });
        }

        function hasScrolled() {
            let st = $(this).scrollTop();

            if (Math.abs(lastScrollTop - st) <= delta) return;
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                $("header").addClass("shrink");
            } else {
                if (st + $(window).height() < $(document).height()) {
                    $("header").removeClass("shrink");
                }
            }

            lastScrollTop = st;
        }

        // function getLastScrollDirection() {
        //     let delta = 5;
        //     var st = window.pageYOffset || document.documentElement.scrollTop;
        //     if(Math.abs(lastScrollTop - st) <= delta)
        //     return;
        //     var ret = true
        //     return ret;
        // }

        function init() {
            bindUI();
        }

        function bindUI() {
            // $('.video-background').on('click', function (event) {
            //     event.preventDefault();
            //     $('#sw-video').attr('src', '');
            //     $('body').css('overflow-y', 'auto');
            //     $(this).fadeOut();
            //     window.location.hash = '';
            // });

            //GA navigation
            //GA navigation
            $("header ul li a, footer .nav a").on("click", function() {
                if(window.google_tag_manager){ 
                    gtag("event", "click", {
                        "header_nav": $(this).text()
                    });
                }
            });
            $("footer .nav a").on("click", function() {
                if(window.google_tag_manager){ 
                    gtag("event", "click", {
                        "footer_nav": $(this).text()
                    });
                }
            });

            //homepage filtering
            $("#home-sector").on("click", function() {
                if ($(this).hasClass("open")) {
                    $(this).removeClass("open");
                    $(".home-filters").slideUp();
                } else {
                    $(this).addClass("open")
                    $(".home-filters").slideDown();
                }
            })

            $(".home-filters a").on("click", function() {
                if ("ga" in window) {
                    tracker = ga.getAll()[0];
                    if (tracker) {
                        tracker.send("event", "home-filter", "Home Page Filter Sector", $(this).text());
                    }
                }
                if(window.google_tag_manager){ //for GA4
                    gtag("event", "user_engagement", {
                        "home_sector_filter": $(this).text()
                    });
                }
            });

            $(".work-filter a").on("click", function() {
                if ("ga" in window) {
                    tracker = ga.getAll()[0];
                    if (tracker) {
                        tracker.send("event", "work-filter", "Portfolio Filter", $(this).text());
                    }
                }
                if(window.google_tag_manager){ //for GA4
                    gtag("event", "user_engagement", {
                        "portfolio_filter": $(this).text()
                    });
                }
            });

            if ($(".page-work").length) {
                var url = new URL(location.href),
                    type = url.searchParams.get("type"),
                    term = url.searchParams.get("filter");

                if (type != "all") {
                    $(".currentfilter")
                        .text($('[data-param="' + term + '"]').text())
                        .slideDown();
                }
            }

            $window.on("popstate", function() {
                // console.log('pop');

                if ($(".page-news.latest").length) {
                    var url = new URL(location.href),
                        cat = url.searchParams.get("category");
                    if (url.search == "") {
                        cat = cat == null ? "all" : cat;
                        history.replaceState("", "", location.pathname + "?category=all");
                    } else {
                        $('.news-filter a[href="#' + cat + '"]').trigger("click");
                    }
                }

                if ($(".page-insights").length) {
                    var url = new URL(location.href),
                        type = url.searchParams.get("type"),
                        term = url.searchParams.get("filter");

                    if (url.search == "") {
                        // if ($('.page-work.new').length) {
                        term = term == null ? "all" : term.replace(/-/g, "");
                        type = type == null ? "all" : type;
                        // history.replaceState(
                        //   "",
                        //   "",
                        //   location.pathname + "?filter=all&type=all"
                        // );
                        // } else {
                        //     term = term == null ? 'featured' : term.replace(/-/g,'');
                        //     type = type == null ? 'featured' : type;
                        //     history.replaceState('','',location.pathname + '?filter=featured&type=featured');
                        // }
                    }
                    //assinging class length
                    // $(".insights-list .insight-item").each(function() {
                    //     let classlength = $(this).attr("class").split(/\s+/).length;
                    //     $(this).attr("data-classlength", classlength);
                    // });

                    // console.log(term, type);

                    $(".insights-filter .selected").removeClass("selected");

                    // $(".insight-list").isotope({
                    //     filter: "." + type,
                    // });

                    if (type == "featured") {
                        // console.log('this?');
                        $(".insight-list").isotope({
                            filter: ".featured",
                            sortBy: "featured",
                        });
                        $('.insights-filter [data-param="featured"]').addClass("selected");
                        $(".insights-list").attr("data-filter", "featured");
                    } else if (type == "all") {
                        $(".insights-list").isotope({
                            filter: "*",
                        });
                        $('.insights-filter [data-param="all"]').addClass("selected");
                        $(".insights-list").attr("data-filter", "all");
                    } else {
                        if (type == "cli") {
                            $(".insights-list").isotope({
                                filter: '[data-client="' + term + '"]',
                            });
                        } else {
                            $(".insights-list").isotope({
                                filter: ".filter_" + term,
                                sortBy: term,
                            });
                        }

                        // $('.work-filter [data-param="'+term+'"]').addClass('selected');
                        $(".insights-list").attr("data-filter", term);
                        $('.insights-filter [data-param="' + term + '"]').addClass("selected");
                        $(".list." + type)
                            .children("a:first")
                            .addClass("selected");
                    }
                }

                // ========================================================

                if ($(".page-work").length) {
                    var url = new URL(location.href),
                        type = url.searchParams.get("type"),
                        term = url.searchParams.get("filter");

                    if (url.search == "") {
                        // if ($('.page-work.new').length) {
                        term = term == null ? "all" : term.replace(/-/g, "");
                        type = type == null ? "all" : type;
                        // history.replaceState(
                        //   "",
                        //   "",
                        //   location.pathname + "?filter=all&type=all"
                        // );
                        // } else {
                        //     term = term == null ? 'featured' : term.replace(/-/g,'');
                        //     type = type == null ? 'featured' : type;
                        //     history.replaceState('','',location.pathname + '?filter=featured&type=featured');
                        // }
                    }
                    //assinging class lenght
                    $(".project-list .work-item").each(function() {
                        let classlength = $(this).attr("class").split(/\s+/).length;
                        $(this).attr("data-classlength", classlength);
                    });

                    // console.log(term, type);

                    $(".work-filter .selected").removeClass("selected");
                    if (type == "featured") {
                        // console.log('this?');
                        $(".project-list").isotope({
                            filter: ".featured",
                            sortBy: "featured",
                        });
                        $('.work-filter [data-param="featured"]').addClass("selected");
                        $(".project-list").attr("data-filter", "featured");
                    } else if (type == "all") {
                        $(".project-list").isotope({
                            filter: ".full",
                            sortBy: "showall",
                        });
                        $('.work-filter [data-param="all"]').addClass("selected");
                        $(".project-list").attr("data-filter", "all");
                    } else {
                        if (type == "cli") {
                            $(".project-list").isotope({
                                filter: '[data-client="' + term + '"]',
                            });
                        } else {
                            $(".project-list").isotope({
                                filter: ".filter_" + term,
                                sortBy: term,
                            });
                        }

                        // $('.work-filter [data-param="'+term+'"]').addClass('selected');
                        $(".project-list").attr("data-filter", term);
                        $('.work-filter [data-param="' + term + '"]').addClass("selected");
                        $(".list." + type)
                            .children("a:first")
                            .addClass("selected");
                        $(".list ul li").each(function() {
                            if ($(this).find("a.selected")) {
                                $('.currentfilterblurb').html($(this).find("a.selected").data("blurb")).slideDown();
                            }
                        })

                    }
                }
            });

            $('.open-url').on('click', function(){
              const url = decodeURIComponent($(this).data('url'));
              window.open(url);
            });

            if (location.hash != "") {
                $(".animate").addClass("animated");
            }

            // IE fallback
            if (isIE) {
                $(".animate").addClass("animated");
            }

            // if (iPad) {
            //     $('video').attr('controls');
            // }

            // homepage carousel
            if ($(".work-carousel").length) {
                $(".slickcss").removeAttr("disabled");

                $(".work-carousel").slick({
                    lazyload: "progressive",
                    centerMode: true,
                    centerPadding: 0,
                    dots: false,
                    arrows: true,
                    autoplay: false,
                    autoplaySpeed: 3000,
                    pauseOnHover: true,
                });
            }

            $(window).scroll(function() {
                $(".illustrations figure img").each(function() {
                    if (isScrolledIntoView(this, 0) === true) {
                        $(".steps .active").removeClass("active");
                        var id = $(this).closest("figure").attr("id");
                        $('.steps [href="#' + id + '"]').addClass("active");
                    }
                });
            });

            // if ($body.hasClass('page-work')) $window.trigger('popstate');

            // navigation
            $(".nav-toggle").on("click", function(e) {
                e.preventDefault();
                $body.toggleClass("nav-open");
            });

            $("nav .close").on("click", function(e) {
                e.preventDefault();
                $body.removeClass("nav-open");
            });

            $("header ul .more > a").on("click", function(e) {
                e.preventDefault();
            });

            // leadership
            $(".leadership-list h4 a").on("click", function(e) {
                e.preventDefault();
                $(".leadership-list h4 a").not(this).parent().removeClass('open').next().slideUp('fast');
                $(this).parent().toggleClass('open').next().slideToggle('fast');
            });

            // values
            $(".values-list h4 a").on("click", function(e) {
                e.preventDefault();
                $(".values-list h4 a").not(this).parent().removeClass('open').next().slideUp('fast');
                $(this).parent().toggleClass('open').next().slideToggle('fast');
            });

            // contact accordion
            $(".contact-content h4 a").on("click", function(e) {
                e.preventDefault();
                let $head = $(this).parent(),
                    $cont = $head.next(".contact-expand-content");

                if (!$cont.is(":visible")) {
                    $(".contact-content .contact-expand-content:visible")
                        .slideUp("fast")
                        .prev()
                        .removeClass("open");
                    $head.addClass("open");
                    $cont.slideDown("fast", function() {
                        $("html,body").animate({ scrollTop: $head.offset().top - 150 + "px" },
                            "slow"
                        );
                    });
                }
            });

            // 3 steps
            $(".page-methods .steps a").on("click", function(e) {
                e.preventDefault();
                var target = $(this).attr("href");
                $("html, body").animate({
                        scrollTop: $(target).offset().top - 160 + "px",
                    },
                    "slow"
                );
            });

            // team member
            if ($(".team-list").length > 0) {
                animateCSSGrid.wrapGrid($(".team-list")[0], {
                    duration: 600,
                });
            }

            $(".team-item img").on("click", function(e) {
                e.preventDefault();
                if ($window.width() > 480) {
                    var $elem = $(this).closest(".team-item");
                    if (!$elem.hasClass("open")) {
                        $(".team-list .open").removeClass("open");
                    }
                    $elem.toggleClass("open");
                }
            });

            $(".page-work-item iframe").each(function() {
                $(this).wrap('<div class="iframe-container"></div>');
            });

            $("#contact-form").on("submit", function(e) {
                e.preventDefault();
                var $form = $(this);
                if ($form.find(":invalid").length == 0) {
                    $.ajax({
                        url: $form.attr("action"),
                        type: "post",
                        data: $form.serialize(),
                        beforeSend: function(xhr, settings) {
                            $(this).find(".button button").text("Sending...");
                        },
                        success: function(data, status, xhr) {
                            $form.slideUp();
                            $(".ty").slideDown(function() {
                                $("html, body").animate({
                                    scrollTop: $(".ty").offset().top - 160 + "px",
                                });
                            });
                            if (window.ga) { //for UA
                                ga("send", "event", "Contact form", "submit", "");
                            }
                            if(window.google_tag_manager){ // for GA4
                                gtag("event", "user_engagement", {
                                    "contact_form": "submit"
                                });
                                gtag("event", "conversion", {
                                    send_to: "AW-707539319/K5BwCKn8j64BEPfisNEC",
                                });
                            }
                        },
                    });
                }
            });

            // case study filter
            $(".page-work .work-filter .list > a").on("click", function(e) {
                e.preventDefault();
                var $list = $(this).parent();
                if ($list.hasClass("open")) {
                    $list.removeClass("open");
                    $(".work-filter").removeClass("list-open");
                } else {
                    $list.addClass("open");
                    $list.siblings(".list").removeClass("open");
                    $(".work-filter").addClass("list-open");
                    // setTimeout(function() {
                    // }, 250);
                }
                // $(this).parent().toggleClass('open').siblings('.list').removeClass('open');
            });

            $(".page-insights .insights-filter .list > a").on("click", function(e) {
                e.preventDefault();
                var $list = $(this).parent();
                if ($list.hasClass("open")) {
                    $list.removeClass("open");
                    $(".insights-filter").removeClass("list-open");
                } else {
                    $list.addClass("open");
                    $list.siblings(".list").removeClass("open");
                    $(".insights-filter").addClass("list-open");
                    // setTimeout(function() {
                    // }, 250);
                }
                // $(this).parent().toggleClass('open').siblings('.list').removeClass('open');
            });

            // news filter
            $(".page-news .news-filter a[data-param]").on("click", function(e) {
                e.preventDefault();
                $(".animate").addClass("animating animated"); // skip animation on filter
                var $el = $(this);

                if (!$el.hasClass("selected")) {
                    $el.addClass("selected").siblings().removeClass("selected");

                    var param = $el.data("param");
                    if (param == "all") {
                        $(".news-list").isotope({
                            filter: "",
                        });
                    } else {
                        $(".news-list").isotope({
                            filter: '[data-tags*="' + param + '"]',
                        });
                    }
                    if(window.google_tag_manager){ //for GA4
                        gtag("event", "user_engagement", {
                            "news_filter": param
                        });
                    }
                    history.pushState("", "", location.pathname + "?category=" + param);
                }
            });


            $(".page-insights .insights-filter a[data-param]").on("click", function(e) {
                e.preventDefault();
                $(".animate").addClass("animating animated"); // skip animation on filter
                var $el = $(this);

                if (!$el.hasClass("selected")) {
                    if ($el.attr("href") == "#all") {
                        $(".insights-list").isotope({
                            filter: "*",
                        });

                        var params = new URL(location.href).searchParams;

                        history.pushState(
                            "",
                            "",
                            location.pathname
                        );

                        $(".insights-filter .selected").removeClass("selected");
                        $el.addClass("selected");
                        $(".insights-list").attr("data-filter", "all");
                    } else {
                        if ($el.data("param") != undefined) {
                            var term = $el.data("param").replace(/-/g, ""),
                                type = $el.data("type");

                            if (type == "cli") {
                                // client
                                $(".insights-list").isotope({
                                    filter: '[data-client="' + term + '"]',
                                });
                            } else {
                                // other
                                $(".insights-list").isotope({
                                    filter: ".filter_" + term,
                                });
                            }

                            var params = new URL(location.href).searchParams;

                            if (params.get("type") == null) {
                                params.append("type", encodeURIComponent(type));
                            } else {
                                params.set("type", encodeURIComponent(type));
                            }

                            if (params.get("filter") == null) {
                                params.append("filter", term);
                            } else {
                                params.set("filter", term);
                            }

                            history.pushState(
                                "",
                                "",
                                location.pathname + "?" + params.toString()
                            );

                            $(".insights-filter .selected").removeClass("selected");
                            $el.closest(".list").children("a:first").addClass("selected");
                            $el.addClass("selected");
                            $(".insights-list").attr("data-filter", term);
                        }
                    }
                    $(".list.open").removeClass("open");
                    $(".insights-filter").removeClass("list-open");
                }
            });

            // ===========================================================

            $(".page-work .work-filter a[data-param]").on("click", function(e) {
                e.preventDefault();
                $(".animate").addClass("animating animated"); // skip animation on filter
                var $el = $(this);

                if (!$el.hasClass("selected")) {
                    if ($el.attr("href") == "#featured") {
                        $(".project-list").isotope({
                            filter: ".featured",
                            sortBy: "featured",
                        });

                        var params = new URL(location.href).searchParams;

                        if (params.get("type") == null) {
                            params.append("type", "featured");
                        } else {
                            params.set("type", "featured");
                        }

                        if (params.get("filter") == null) {
                            params.append("filter", "featured");
                        } else {
                            params.set("filter", "featured");
                        }
                        //dont show all query in the URL
                        if (params.get("type") !== "all") {
                            console.log("here")
                            history.pushState(
                                "",
                                "",
                                location.pathname + "?" + params.toString()
                            );
                        }

                        $(".work-filter .selected").removeClass("selected");
                        $el.addClass("selected");
                        $(".project-list").attr("data-filter", "featured");
                        $(".currentfilter").text("").slideUp();
                        $(".currentfilterblurb").text("").slideUp();
                    } else if ($el.attr("href") == "#all") {
                        $(".project-list").isotope({
                            filter: ".full",
                            sortBy: "showall",
                        });

                        var params = new URL(location.href).searchParams;

                        // if (params.get("type") == null) {
                        //   params.append("type", "all");
                        // } else {
                        //   params.set("type", "all");
                        // }

                        // if (params.get("filter") == null) {
                        //   params.append("filter", "all");
                        // } else {
                        //   params.set("filter", "all");
                        // }

                        history.pushState(
                            "",
                            "",
                            location.pathname
                        );

                        $(".work-filter .selected").removeClass("selected");
                        $el.addClass("selected");
                        $(".project-list").attr("data-filter", "all");
                        $(".currentfilter").text("").slideUp();
                        $(".currentfilterblurb").text("").slideUp();
                    } else {
                        if ($el.data("param") != undefined) {
                            var term = $el.data("param").replace(/-/g, ""),
                                type = $el.data("type");

                            if (type == "cli") {
                                // client
                                $(".project-list").isotope({
                                    filter: '[data-client="' + term + '"]',
                                });
                            } else {
                                // other
                                $(".project-list").isotope({
                                    filter: ".filter_" + term,
                                    sortBy: term,
                                });
                            }

                            var params = new URL(location.href).searchParams;

                            if (params.get("type") == null) {
                                params.append("type", encodeURIComponent(type));
                            } else {
                                params.set("type", encodeURIComponent(type));
                            }

                            if (params.get("filter") == null) {
                                params.append("filter", term);
                            } else {
                                params.set("filter", term);
                            }

                            if(window.google_tag_manager){ //for GA4
                                gtag("event", "user_engagement", {
                                    "work_filter": params.toString()
                                });
                            }

                            history.pushState(
                                "",
                                "",
                                location.pathname + "?" + params.toString()
                            );

                            $(".work-filter .selected").removeClass("selected");
                            $el.closest(".list").children("a:first").addClass("selected");
                            $el.addClass("selected");
                            $(".project-list").attr("data-filter", term);
                            $(".currentfilter").text($el.text()).slideDown();
                            $(".currentfilterblurb").text($el.data('blurb')).slideDown();
                        }
                    }
                    $(".list.open").removeClass("open");
                    $(".work-filter").removeClass("list-open");
                }
            });

            $(".page-work .project-list a, .page-work-item .related a").on(
                "click",
                function() {
                    var url = new URL(location.href);
                    if (url.search != "") {
                        $(this).attr("href", $(this).attr("href") + url.search);
                    }
                }
            );

            // insight article tl;dr
            $(".page-insights .tldr_link").on("click", function() {
                // e.preventDefault();
                $("#tldr_content").show();
                if (window.ga) {
                    ga("send", "event", "clicks", "insight tl;dr", location.href); // for UA
                }
                if(window.google_tag_manager) { //for GA4
                    gtag("event", "click", {
                        "insight_tldr": location.href
                    });
                }
            });

            if ($(".page-insights .tldr_link").length) {
                if (window.location.hash === "#tldr") {
                    $("#tldr_content").show();
                    $(window).scrollTop($("#tldr_content").offset().top);
                }
            }

            //subscription form
            $(".subscribe-form").on("submit", function(e) {
                e.preventDefault();
                var $form = $(this);
                let url = "/createsend-php/subscriber/add.php";
                let emailRegex =
                    /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
                let email_check = emailRegex.test($form.find("#fieldEmail").val());
                if ($form.find(".name").val() !== "") {
                    return;
                }
                if (email_check) {
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: $form.serialize(),
                        success: function(data, status, xhr) {
                            if (data.includes("Subscribed with code 201")) {
                                // save the subscription to localstorage
                                localStorage.setItem("subscribed", "YES");
                                $form.slideUp();
                                $(".ty2").fadeIn();
                                // if ($form.parent().parent().hasClass("box-widget")) {
                                //     setTimeout(function() {
                                //         $(".box-widget").removeClass("visible");
                                //     }, 3000);
                                // }
                            } else {
                                $(".error").fadeIn();
                            }
                        },
                    });
                }
                if(window.google_tag_manager){ // for GA4
                    gtag("event", "user_engagement", {
                        "newsletter_form": location.href
                    });
                }
                if ("ga" in window) {
                    tracker = ga.getAll()[0];
                    if (tracker) {
                        tracker.send("event", "newsletter", "newsletter-signup", location.href);
                    }
                }
            });

            // scroll control
            $window.on("scroll", function() {
                scrollWindow();
                scrollThrottle();
            });

            // resize control
            // $window.on("resize", function() {
            //     resizeWindow();
            // });

            $window.on("load", function() {
                scrollThrottle();

                // if ($('.leadership-list').length) {
                //     $('.leadership-list h4:first a').trigger('click');
                // }

                // news filter
                if ($(".page-news.latest").length) {
                    $(".news-list").isotope({
                        layoutMode: "fitRows",
                        itemSelector: ".news-item",
                        percentPosition: true,
                        fitRows: {
                            gutter: ".gutter-sizer",
                        },
                    });

                    $window.trigger("popstate");
                }

                // work filter
                if ($(".page-work").length) {
                    $(".project-list").isotope({
                        layoutMode: "fitRows",
                        itemSelector: "article",
                        percentPosition: true,
                        fitRows: {
                            gutter: ".gutter-sizer",
                        },
                        getSortData: {
                            branding: "[data-sort-brand] parseInt",
                            digital: "[data-sort-digital] parseInt",
                            comms: "[data-sort-comms] parseInt",
                            campaign: "[data-sort-campaign] parseInt",
                            featured: "[data-sort-featured] parseInt",
                            // newbrand: "[data-sort-new-brand] parseInt",
                            // upgrade: "[data-sort-upgrade] parseInt",
                            // expansion: "[data-sort-expansion] parseInt",
                            // strategicshift: "[data-sort-strategic-shift] parseInt",
                            // manda: "[data-sort-m-and-a] parseInt",
                            // salesandengagement: "[data-sort-sales-and-engagement] parseInt",
                            // reporting: "[data-sort-reporting] parseInt",
                            showall: "[data-sort-all] parseInt",
                        },
                    });

                    $window.trigger("popstate");

                    // remove unused filters
                    $(".work-filter ul a").each(function() {
                        var p = $(this).data("param");
                        if ($(this).closest(".cli").length) {
                            if (!$('.project-list [data-client="' + p + '"]').length) {
                                $(this).parent().remove();
                            }
                        } else {
                            if (!$(".project-list .filter_" + p).length) {
                                $(this).parent().remove();
                            }
                        }
                    });
                }
                //insights-isotope
                // if ($(".page-insights").length) {
                //     $(".insights-list").isotope({
                //         layoutMode: "fitRows",
                //         itemSelector: ".insight-item",
                //         fitRows: {
                //             gutter: 24,
                //         },
                //     });
                //     $window.trigger("popstate");

                //     // remove unused filters
                //     $(".insights-filter ul a").each(function() {
                //         var p = $(this).data("param");
                //         if ($(this).closest(".cli").length) {
                //             if (!$('.insights-list [data-client="' + p + '"]').length) {
                //                 $(this).parent().remove();
                //             }
                //         } else {
                //             if (!$(".insights-list .filter_" + p).length) {
                //                 $(this).parent().remove();
                //             }
                //         }
                //     });
                // }

                if ($(".logo-list").length && typeof logo_queue != "undefined") {
                    var numLogos = 12,
                        curr = 0,
                        last = 0;

                    if ($window.width() <= 1024) numLogos = 9;
                    if ($window.width() <= 640) numLogos = 8;

                    // initial load
                    for (let i = 0; i < numLogos; i++) {
                        $(".logo-list").append('<img src="' + logo_queue[i] + '" />');
                        curr++;
                    }

                    // loop
                    var interval = setInterval(function() {
                        var idx = Math.floor(Math.random() * numLogos) + 1;

                        // avoid swapping logo on the same spot twice in row
                        while (idx == last) {
                            idx = Math.floor(Math.random() * numLogos) + 1;
                        }
                        last = idx;

                        // avoid swapping in a logo that's already visible
                        while (
                            $('.logo-list [src*="' + logo_queue[curr] + '"]').length > 0
                        ) {
                            curr++;
                        }

                        $(".logo-list img:nth-child(" + idx + ")").fadeOut(
                            "slow",
                            function() {
                                $(this).attr("src", logo_queue[curr]);
                                $(this).fadeIn("slow");
                                curr = curr % logo_queue.length;
                            }
                        );
                    }, 1500);
                }
                if ($(".page-wholehearted").length) {
                    $(".slickcss").removeAttr("disabled");
                    $(".dg-slider").slick({
                        dots: false,
                        arrows: false,
                        rows: 1,
                        infinite: false,
                        centerMode: true,
                        centerPadding: '0px',
                        slidesToShow: 1,
                        dots: false,
                        responsive: [{
                            breakpoint: 800,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 3,
                                centerMode: true,
                                centerPadding: '40px',
                                infinite: false,
                                dots: false
                            }
                        }, ]
                    });
                }
            });

            $('.capabilities-block .link-btn').click(function(e){
                e.preventDefault();
                $(this).toggleClass('open').next().slideToggle('fast');

                if ($(this).text() == "See more") {
                    $(this).text("See less");
                } else {
                    $(this).text("See more");
                }
            });
        }

        // insights images ratio 
        $window.on("load resize", function() {
            
        });

        $(".ie-widget .close-widget").on("click", function() {
            $(".ie-widget").removeClass("visible");
            sessionStorage.setItem("hide-ie-widget", "YES");
        });

        // if (
        //     sessionStorage.getItem("hide-subscription-widget") !== "YES" &&
        //     localStorage.getItem("subscribed") !== "YES"
        // ) {
        //     if (
        //         $(".page-work-item").length === 0 &&
        //         $(".page-insights-item").length === 0 &&
        //         $(".page-news-item").length === 0 &&
        //         $(".page-home").length === 0
        //     ) {
        //         setTimeout(function() {
        //             if (localStorage.getItem("subscribed") !== "YES")
        //                 $(".subscription-form-widget").addClass("visible");
        //         }, 12000);
        //     }
        // }
        let box_show = true;
        //default
        $(".box-widget .close-widget").on("click", function() {
            let parent = $(this).closest(".box-widget");
            if (parent.hasClass("subscription-form-widget")) {
                sessionStorage.setItem("hide-subscription-widget", "YES");
            }
            if (parent.hasClass("ie-widget")) {
                sessionStorage.setItem("hide-ie-widget", "YES");
            }
            $(this).closest(".box-widget").removeClass("visible");
            box_show = false;
        });

        // if($(".page-insights-item").length > 0) {
        //     console.log("opa")
        //     setTimeout(function() {
        //         $(".insights-item-widget").addClass("visible");
        //     }, 1000)
        // }

        //brands we've built page mouse over mouse in for a tag underlining

        $(".work-item a").on("mouseover", function() {
            $(this).closest(".work-item").find("h2").find("a").addClass("active")
        })
        $(".work-item a").on("mouseleave", function() {
            $(this).closest(".work-item").find("h2").find("a").removeClass("active")
        })


        widget_showed = false;

        $(window).scroll(function() {
            if (isScrolledIntoView(".signup-footer", 500) === true) {
                // if ($(".subscription-form-widget").hasClass("visible"))
                //     $(".subscription-form-widget").removeClass("visible");
            }
            height_multiplier = 2
            if ($window.width() < 800) {
                height_multiplier = 1.1;
            } else if ($window.width() < 1400) {
                height_multiplier = 2
            } else {
                height_multiplier = 3.5
            }
            if ($(".insights-item-widget").length) {
                if ($(".takeaway").offset().top - $(window).scrollTop() > $(".takeaway").height() * height_multiplier && $(".insights-item-widget").length) {
                    $(".insights-item-widget").removeClass("show");
                } else {
                    $(".insights-item-widget").addClass("show");
                }
            }
            let variable = 1.8;
            if ($window.width() < 800) {
                variable = 3;
            }
            if ($(".insights-item-widget-hidden").length) {
                if (
                    isScrolledIntoView(".takeaway", $(".takeaway").outerHeight()) === true
                ) {
                    if ($(".insights-item-widget").hasClass("show")) {
                        setTimeout(function() {
                            $(".insights-item-widget-hidden")
                                .addClass("visible")
                                .addClass("relative");
                        }, 700)

                    }


                }
                if (
                    isScrolledIntoView(
                        ".insights-item-widget-hidden", -$(".insights-item-widget-hidden").outerHeight() / variable
                    ) === true
                ) {
                    $(".insights-item-widget-hidden")
                        .addClass("visible").addClass("relative");;
                    $(".insights-item-widget").css("display", "none");
                }
                if (isScrolledIntoView(".insights-item-widget-hidden", -$(".insights-item-widget-hidden").outerHeight()) === true) {
                    $(".insights-item-widget-hidden").css("position", "relative")
                }
            }
        });

        if (isIE) {
            $("html").addClass("internet-explorer");

            var widget_cookie = sessionStorage.getItem("hide-ie-widget");

            if (widget_cookie === null || widget_cookie === "undefined") {
                //adjust the fixed header on internet explorer
                setTimeout(function() {
                    $(".ie-widget").addClass("visible");
                    // $(".subscription-form-widget").removeClass("visible");
                }, 1000);
            }
        }

        document.addEventListener("DOMContentLoaded", yall);

        return {
            init: init,
        };
    };

    var site = new Site();
    site.init();
})(jQuery);