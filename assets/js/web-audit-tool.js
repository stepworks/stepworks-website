//js script for standalone Website Audit Tool
$("#start").on("submit", function (e) {
    e.preventDefault();
    //check if fields and checkboxes are correct
    $("#main").removeClass("hidden");
    $('#main').slideDown(function() {
        $('html,body').animate({
            scrollTop: $(this).offset().top + 'px'
        });
    });
})


$('a[href="#tos"]').on('click', function(e) {
    e.preventDefault();
    $('#tosmodal').addClass('open');
});


//open Terms of Services
$('#tos').on('click', function(e) {
    if ($(this).is(':checked')) {
        $('#start').removeClass('disabled');
    } else {
        $('#start').addClass('disabled');
    }
});

//Closing pop up modal 
$('.popup_modal .close').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.popup_modal').removeClass('open');
});

var scored = false;

$('input[type="range"]').each(function() {
    var $elem = $(this),
        $p = $elem.parent();

    $p.append('<div class="slider"></div>');
    $p.addClass('slider-parent');
    var $sl = $p.find('.slider');
    var slider = noUiSlider.create($sl[0], {
        range: {
            min: parseInt($elem.attr('min')),
            max: parseInt($elem.attr('max'))
        },
        step: 1,
        start: 3
    });

    slider.on('set', function() {
        $elem.val(slider.get()).trigger('change');
    });

    $elem.css({
        position: 'absolute',
        visibility: 'hidden'
    });
});

$('[name="vip"]').on('change', function() {
    $('.vip').text($(this).val());
});

$('#feedback .btn-feedback').on('click', function(e) {
    e.preventDefault();
    $('#feedback').addClass('open');
    $('#feedback_email').focus();
});

// feedback form
$('#feedback_form').on('submit', function(e) {
    e.preventDefault();

    var $form = $(this),
        data = {
            // action: 'report',
            from: encodeURIComponent($('#feedback_email').val()),
            message: encodeURIComponent($('#feedback_content').val())
        };
    $.post('en/wae_feedback', data).done(function(res) {
        console.log(res);

        $form.hide();
        $('#q').hide();
        $('#t').show();

        setTimeout(function() {
            $('#feedback').removeClass('open');
            $form.show()[0].reset();
            $('#q').show();
            $('#t').hide();
        }, 3000);
    });
});
$('#feedback button.close').on('click', function(e) {
    e.preventDefault();
    $('#feedback').removeClass('open');
});

$("#print").on("click", function(e) {
    e.preventDefault();
    window.print();
})

$('.step-wrapper .next').on('click', function(e) {
    e.preventDefault();
    $(this).hide();


    let $nq = "";

    if($(this).hasClass('next-step')) {
        $nq = $(this).closest('.step-wrapper').next('.step-wrapper');
    }
    else {
        $nq = $(this).closest('.answer').next('.question')
    }

    if ($nq.length > 0 && !scored) {
        $nq.slideDown();
        $('html,body').animate({
            scrollTop: $nq.offset().top - ($(window).width() < 640 ? 0 : 150) + 'px'
        });
    } else {
        // enter final scoring

        // add trigger for recalculation
        $('#webaudit').find('input[type="range"], input[type="radio"]').on('change', function() {
            $('#last').trigger('click');
        });

        var expscore =
                parseInt($('[name="und"]').val()) +
                parseInt($('[name="review-seo"]:checked').val()) +
                parseInt($('[name="notfound"]').val()) -
                2,
            valscore = parseInt($('[name="acc"]').val()) - 1,
            artscore =
                parseInt($('[name="modern"]').val()) +
                parseInt($('[name="match"]').val()) +
                parseInt($('[name="mobilemsg"]:checked').val()) -
                2,
            pexp = Math.round(expscore / 12 * 100),
            pval = Math.round(valscore / 4 * 100),
            part = Math.round(artscore / 12 * 100);

        $('#experience-bar').css('width', pexp + '%');
        $('#experience-score').text(pexp);

        $('#business-bar').css('width', pval + '%');
        $('#business-score').text(pval);

        $('#design-bar').css('width', part + '%');
        $('#design-score').text(part);

        var bscore = (pexp + pval + part) / 3;
        $('[name="bb"]').val(bscore);

        var pscore = parseFloat($('[name="lh"]').val());
        var tscore = Math.round(pscore / 2 + bscore / 2);

        $('#overall-bar').css('width', tscore + '%');
        $('#overall-score').text(tscore);

        $('.totalbar ~ .result.active').remove();
        $('.allresults .result').removeClass('active');

        if (tscore < 60) {
            $('.result.poor').addClass('active').clone().insertBefore($('.action-buttons'));
            $('#overall-bar').removeClass('average great').addClass('poor');
        } else if (tscore >= 60 && tscore < 80) {
            $('.result.average').addClass('active').clone().insertBefore($('.action-buttons'));
            $('#overall-bar').removeClass('poor great').addClass('average');
        } else if (tscore >= 80) {
            $('.result.great').addClass('active').clone().insertBefore($('.action-buttons'));
            $('#overall-bar').removeClass('poor average').addClass('great');
        }

        if (!scored) {
            $('.results').slideDown(function() {
                $('html,body').animate({
                    scrollTop: $(this).offset().top - 150 + 'px'
                });
            });
            $('.scores, .takeaway').show();
            scored = true;
            if (window.ga) {
                ga(
                    'send',
                    'event',
                    'Website audit',
                    'complete',
                    'url:' + $('.url').text() + ';' + pexp + '|' + pval + '|' + part
                );
            }
            if(window.google_tag_manager){ //for GA4
                gtag("event", "user_engagement", {
                    "website_audit_end": $('.url').text() + ';' + pexp + '|' + pval + '|' + part
                });
            }
            createReport(); //saving report to database
        }
    }
});

$('.question:not(#q1) input').on('change', function() {
    $(this).parent().find(".noUi-touch-area").addClass("no-animation");
    if (!scored) {
        var $q = $(this).closest('.question'),
            $a = $q.next('.answer');
        // $nq = $a.next('.question');
        $a.css("opacity", "1");
        if(!isElementInViewport($a)) {
            $('html,body').animate({
                scrollTop: $a.offset().top - ($a.outerHeight() / 2) + 'px'
            });
        }
    }
});

function isElementInViewport (el) {

    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }
    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || $(window).height()) && 
        rect.right <= (window.innerWidth || $(window).width()) 
    );
}
function getElementBottom (el) {

    let rect = el[0].getBoundingClientRect();
    return(rect.bottom)
}

$('.showall').on('click', function(e) {
    e.preventDefault();
    $('.allresults').slideToggle();
    var sh = 'Hide';
    if ($(this).text() == 'Hide other available assessment outcomes') {
        sh = 'See';
    }
    $(this).text(sh + ' other available assessment outcomes');
});

let form = document.getElementById('submit');



function createReport() {
    let processorUrl = '/en/war_processor';
    if(window.location.hostname === "localhost") {
        processorUrl = '/stepworks-website/en/war_processor';
    }

    var data = {
        action: 'save',
        email: $("#email").val(),
        website: $('#url').val(),
        final_score: $("#overall-score").text(),
        brand_experience: $("#experience-score").text(),
        value_delivered: $("#business-score").text(),
        design_effectivenes: $("#design-score").text(),
        performance: $("#performance-score").text(),
        accessibility: $("#accessibility-score").text(),
        best_practices: $("#bestpractices-score").text(),
        seo:$("#seo-score").text()
    }

    $.post(processorUrl, data).done(function(response) {
        data = JSON.parse(response);
        if (data.status === 'success') {
            sendEmail(data.id);
            $("#report-url").attr("href", window.location.href + "/reports?id=" + data.id)
        }
    });

}

function sendEmail(war_id) {
    let processorUrl = '/en/war_processor';
    if(window.location.hostname === "localhost") {
        processorUrl = '/stepworks-website/en/war_processor';
    }
    var data = {
        action: 'email',
        email: $("#email").val(),
        website: $('#url').val(),
        id : war_id
    }

    $.post(processorUrl, data).done(function(response) {
        data = JSON.parse(response);
        if (data.status === 'success') {
            console.log("Email sent.")
        }
    });

}

$("#report-url").on("click", function(e) {
    e.preventDefault();
    copy();
    $(this).text("Copied!");
    setTimeout(function() {
        $("#report-url").text("Copy Report's URL!");
    }, 3000)
}) 

function copy() {
    var $temp = $("<input>");
    var $url = $(location).attr('href');

    $("body").append($temp);
    $temp.val($url).select();
    document.execCommand("copy");
    $temp.remove();

  }

var atextlist = [
        'metatags',
        'site structure',
        'performance',
        'accessibility',
        'SEO',
        'best practices',
        'results'
    ],
    atextidx = 0;
if(form) {
form.addEventListener('submit', (event) => {
    event.preventDefault();

    if ($('#share-url').text() == 'Restart') {
        // reset everything
        window.location.hash = 'webaudit';
        window.location.reload();
    } else {
        var tmpurl = document.getElementById('url').value;
        $('.url').text(tmpurl);
        if (window.ga) {
            ga('send', 'event', 'Website audit', 'start', 'url:' + tmpurl);
        }
        if(window.google_tag_manager){ //for GA4
            gtag("event", "user_engagement", {
                "website_audit_start": tmpurl
            });
        }

        // append protocol
        tmpurl =
            tmpurl.indexOf('http://') == 0 || tmpurl.indexOf('https://') == 0
                ? tmpurl
                : 'https://' + tmpurl;
        // remove trailing slash
        tmpurl =
            tmpurl.charAt(tmpurl.length - 1) == '/' ? tmpurl.substr(0, tmpurl.length - 1) : tmpurl;
        const url_query = tmpurl;
        $('.urlhref').attr('href', tmpurl);

        if (validURL(url_query)) {
            $('#submit').find('input').removeClass('invalid');
            $('#submit').find('input').removeClass('invalid');
            $('input[name="url"]').attr('readonly', 'readonly');
            $('#share-url').hide();

            $('#loader').css('display', 'flex');
            $('#loader .analyse').text(atextlist[atextidx++]);
            var loadInterval = setInterval(function() {
                $('#loader .analyse').text(
                    atextlist[atextidx == atextlist.length - 1 ? atextidx : atextidx++]
                );
            }, Math.round(Math.random() * 3000 + 2000));
            $('#error').hide();

            const url = setUpQuery(url_query, 'd');
            let metadata = '';
            let retry = false;

            // check x-frame-options header

            // get metadata
            fetchAPI(url, url_query, loadInterval);
        } else {
            $('#submit').find('input').addClass('invalid');
        }
    }
});
}

function fetchOnSuccess() {
    $('#step1').slideDown();
    $('html,body').animate({
        scrollTop: $('#step1').offset().top + 'px'
    });
    $('#q2').slideDown();
    $('#loader').hide();
    $('#share-url').text('Restart').show();
    document.title = document.title + "- " + $("#url").val();
}

function fetchOnError() {
    $('#loader').hide();
    $('#error').show();
    $('#share-url').text('Restart').show();
}

function set404Image(url_query) {
    let url404 = url_query + '/randomstring1234';
    $('.urlnfhref').each(function() {
        $(this).attr('href', url404);
    });
    fetch(
        `https://shot.screenshotapi.net/screenshot?token=SIUVKYJRWSOMOWPILDJFVKGHJEPPEXYG&url=${url404}&width=1920&height=1080&delay=3000&no_cookie_banners=true`
    )
        .then((response) => response.json())
        .then((json) => {
            if (!json.errors) {
                let screenshot_404 = json.screenshot;
                $('#404_image').attr('src', screenshot_404);
            } else {
                //@TODO DO THIS NICER !!!!!!!!!!!!
                fetch(
                    `https://shot.screenshotapi.net/screenshot?token=SIUVKYJRWSOMOWPILDJFVKGHJEPPEXYG&url=${url404}&width=1920&height=1080&delay=3000`
                )
                    .then((response) => response.json())
                    .then((json) => {
                        if (!json.errors) {
                            let screenshot_404 = json.screenshot;
                            $('#404_image').attr('src', screenshot_404);
                        } else {
                            $('#q9 .iframe-wrapper')
                                .hide()
                                .after(
                                    '<p><a href="' +
                                        url404 +
                                        '" target="_blank" rel="noopener nofollow">Open your 404 page</a> <a href="' +
                                        url404 +
                                        '" target="_blank" rel="noopener nofollow"><i class="far fa-external-link"></i></a></p>'
                                );
                            $('.sl').text('link');
                        }
                    });
            }
        });
}

//creating recursive function
function fetchAPI(url, url_query, loadInterval, retry) {
    retry = retry || false;
    fetch(url).then((response) => response.json()).then((json) => {
        if (json.error) {
            if (json.error === 429 || retry) {
                //429 error - no sense to call the api again since we have to wait for the quota
                fetchOnError();
                retry = true; //stop the loop, we tried different domain already, no sense to keep looping
            } else {
                retry = true;
                let new_url = url_query;
                if (new_url.indexOf('www') > -1) {
                    new_url = new_url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, 'https://');
                } else {
                    new_url = new_url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, 'https://www.');
                }
                let alt_ready = setUpQuery(new_url, 'd');
                console.log('Trying again with the new url: ' + new_url);
                fetchAPI(alt_ready, new_url, loadInterval, retry);
            }
        } else {
            // showMetaTags(metadata);
            let screenshot_desktop = '';
            fetch(
                `https://shot.screenshotapi.net/screenshot?token=SIUVKYJRWSOMOWPILDJFVKGHJEPPEXYG&url=${url_query}&width=1920&height=1080&delay=5000&no_cookie_banners=true`
            )
                .then((response) => response.json())
                .then((json) => {
                    if (!json.errors) {
                        screenshot_desktop = json.screenshot;
                        showScreenshotFromAPI(screenshot_desktop, 'desktopss');
                        fetchOnSuccess();
                    } else {
                        //@TODO DO THIS NICER!!!!
                        fetch(
                            `https://shot.screenshotapi.net/screenshot?token=SIUVKYJRWSOMOWPILDJFVKGHJEPPEXYG&url=${url_query}&width=1920&height=1080&delay=5000`
                        )
                            .then((response) => response.json())
                            .then((json) => {
                                if (!json.errors) {
                                    screenshot_desktop = json.screenshot;
                                    showScreenshotFromAPI(screenshot_desktop, 'desktopss');
                                    fetchOnSuccess();
                                } else {
                                    showScreenshots(
                                        json.lighthouseResult.audits['final-screenshot'],
                                        'desktopss'
                                    );
                                    fetchOnSuccess();
                                }
                            });
                    }
                });
            let screenshot_mobile = '';
            fetch(
                `https://shot.screenshotapi.net/screenshot?token=SIUVKYJRWSOMOWPILDJFVKGHJEPPEXYG&url=${url_query}&width=375&height=667&delay=5000&no_cookie_banners=true`
            )
                .then((response) => response.json())
                .then((json) => {
                    if (!json.errors) {
                        screenshot_mobile = json.screenshot;
                        showScreenshotFromAPI(screenshot_mobile, 'mobiless');
                    } else {
                        //@TODO Do this nicer!!
                        fetch(
                            `https://shot.screenshotapi.net/screenshot?token=SIUVKYJRWSOMOWPILDJFVKGHJEPPEXYG&url=${url_query}&width=375&height=667&delay=5000&`
                        )
                            .then((response) => response.json())
                            .then((json) => {
                                if (!json.errors) {
                                    screenshot_mobile = json.screenshot;
                                    showScreenshotFromAPI(screenshot_mobile, 'mobiless');
                                } else {
                                    setTimeout(function() {
                                        console.log(json); // should try to catch 429 here as well
                                        fetch(setUpQuery(url_query, 'm'))
                                            .then((response) => response.json())
                                            .then((json) => {
                                                showScreenshots(
                                                    json.lighthouseResult.audits[
                                                        'final-screenshot'
                                                    ],
                                                    'mobiless'
                                                );
                                            }, 2000);
                                    });
                                }
                            });
                    }
                });
            set404Image(url_query); //set the image if API call was successfull
            showFinalResults(json.lighthouseResult.categories);
            clearInterval(loadInterval);
        }
    });
}

function validURL(str) {
    var pattern = new RegExp(
        '^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$',
        'i'
    ); // fragment locator
    return !!pattern.test(str);
}

function setUpQuery(url_query, device) {
    const api = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed';
    const parameters = {
        url: encodeURIComponent(url_query),
        '&key': 'AIzaSyBOC_QIXIMkwa9FC1lt7X8AuYXALfl0nik'
    };
    let query = `${api}?`;
    for (key in parameters) {
        query += `${key}=${parameters[key]}`;
    }
    switch (device) {
        case 'm':
            query += '&strategy=mobile&screenshot=true';
            break;
        case 'd':
        default:
            query +=
                '&category=performance&category=pwa&category=best-practices&category=accessibility&category=seo&screenshot=true';
            break;
    }
    return query;
}

function showFinalResults(categories) {
    const performance_score = categories.performance.score;
    const seo_score = categories.seo.score;
    const accessibility_score = categories.accessibility.score;
    const pwa_score = categories.pwa.score;
    const bp_score = categories['best-practices'].score;

    $('#performance-bar').css('width', performance_score * 100 + '%');
    $('#performance-score').text(Math.round(performance_score * 100));

    $('#accessibility-bar').css('width', accessibility_score * 100 + '%');
    $('#accessibility-score').text(Math.round(accessibility_score * 100));

    $('#seo-bar').css('width', seo_score * 100 + '%');
    $('#seo-score').text(Math.round(seo_score * 100));

    $('#bestpractices-bar').css('width', bp_score * 100 + '%');
    $('#bestpractices-score').text(Math.round(bp_score * 100));

    var lh = (performance_score + accessibility_score + seo_score + bp_score) / 0.04;
    $('[name="lh"]').attr('value', lh);
}

function showScreenshots(thumbnails, targetclass) {
    //let thumbnails_array = thumbnails.details.data;
    const final_screenshot = thumbnails.details.data;
    const screenshot = document.createElement('img');
    screenshot.src = final_screenshot;
    $('.' + targetclass).html(screenshot);
}

function showScreenshotFromAPI(screenshot_url, targetclass) {
    //let thumbnails_array = thumbnails.details.data;
    const final_screenshot = screenshot_url;
    const screenshot = document.createElement('img');
    screenshot.src = final_screenshot;
    $('.' + targetclass).html(screenshot);
}

/*
*
*
*
*
REPORT PAGE JS
*
*
*
* 
*/

if($(".page-website-audit-tool-report").length > 0) {
        let processorUrl = '/en/war_processor';
        if(window.location.hostname === "localhost") {
            processorUrl = '/stepworks-website/en/war_processor';
        }
        url = new URL(window.location.href);

        if (!url.searchParams.get('id')) {
            $(".loader-inner").hide();
            $(".loader-text").hide();
            $("#error").html("<b>Sorry we couldn't retrieve your report.</b>");
        }
        else {
            var id_param = decodeURIComponent(window.location.search.match(/(\?|&)id\=([^&]*)/)[2]);
            var data = {
                action: 'load',
                war_id: id_param
            }
        
            $.ajax({
                type: "POST",
                url: processorUrl,
                data: data,    
                timeout: 3000, 
                error: function () {
                    $(".loader-inner").hide();
                    $(".loader-text").hide();
                    $("#error").html("<b>Sorry we couldn't retrieve your report.</b>");
                },
                success: function(response) {
                    data = JSON.parse(response);
                if (data.status === 'success') {
                    $(".fetching").hide();
                    $('#url').text(data.website);

                    $("#performance-score").text(data.performance);
                    $("#accessibility-score").text(data.accessibility);
                    $("#bestpractices-score").text(data.best_practices);
                    $("#seo-score").text(data.seo);
                    
                    $('#experience-bar').css('width', data.brand_experience + '%');
                    $('#experience-score').text(data.brand_experience);
            
                    $('#business-bar').css('width', data.value_delivered + '%');
                    $('#business-score').text(data.value_delivered);
            
                    $('#design-bar').css('width', data.design_effectivenes + '%');
                    $('#design-score').text(data.design_effectivenes);

                    $("#performance-score").text(data.performance);
                    $('#performance-bar').css('width', data.performance + '%');

                    $("#accessibility-score").text(data.accessibility);
                    $('#accessibility-bar').css('width', data.accessibility + '%');

                    $("#bestpractices-score").text(data.best_practices);
                    $('#bestpractices-bar').css('width', data.best_practices + '%');

                    $("#seo-score").text(data.seo);
                    $('#seo-bar').css('width', data.seo + '%');

                    $('#overall-bar').css('width', data.final_score + '%');
                    $('#overall-score').text(data.final_score);
            
                    $('.totalbar ~ .result.active').remove();
                    $('.allresults .result').removeClass('active');
            
                    if (data.final_score < 60) {
                        $('.result.poor').addClass('active').clone().insertBefore($('.action-buttons'));
                        $('#overall-bar').removeClass('average great').addClass('poor');
                    } else if (data.final_score >= 60 && data.final_score < 80) {
                        $('.result.average').addClass('active').clone().insertBefore($('.action-buttons'));
                        $('#overall-bar').removeClass('poor great').addClass('average');
                    } else if (data.final_score >= 80) {
                        $('.result.great').addClass('active').clone().insertBefore($('.action-buttons'));
                        $('#overall-bar').removeClass('poor average').addClass('great');
                    }
                    $("#webaudit").slideDown();
                }
            }
            });
        }
}