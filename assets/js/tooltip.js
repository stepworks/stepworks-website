// 
// Author : http://osvaldas.info/elegant-css-and-jquery-tooltip-responsive-mobile-friendly
// 
(function($, window, undefined){

    function ToolTip (){

        var targets = $( '.glossarizer_replaced' ),
            target  = false,
            tooltip = false,
            title   = false,
            tip = false,
            img = false
     
        targets.bind( 'mouseenter', function()
        {
            target  = $( this );
            title =  target.attr( 'title' );
            tip     = target.attr( 'data-title' );
            img     = target.attr( 'data-img' );
            tooltip = $( '<div id="tooltip"></div>' );
     
            if( !tip || tip == '' )
                return false;
            
            let tip_final = "<div><span>" + tip + "</span></div>";

            if(img) {
                tip_final = "<div><img src='"+ img + "'/><span>" + tip + "</span></div>"; 
            }
            target.removeAttr( 'data-title' );
            target.removeAttr( 'title' );
            target.removeAttr( 'data-img' );
            tooltip.css( 'opacity', 0 )
                   .html(tip_final)
                   .appendTo( 'body' );
     
            var init_tooltip = function()
            {
                if( $( window ).width() < tooltip.outerWidth() * 1.5 ) {
                    if($( window ).width() / 2 > 300) {
                        tooltip.css( 'max-width', 300 );
                    }
                    else {
                        tooltip.css( 'max-width', 240 );
                        tooltip.addClass( 'mobile' );
                    }
                }
                else {
                    tooltip.css( 'max-width', 300 );
                }
                let container = document.querySelector('div.cell.start3.end6 p');
                let rect = container.getBoundingClientRect();

                let target_height = target.outerHeight();
                let two_rows = false;
                let aligned_right = false;
                if(target_height > 28) { // 28px means sentence falls into 2 rows
                    two_rows = true;   
                    aligned_right = true;                
                }
                
                let pos_left = target.offset().left //( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 )
                let pos_arrow = target.outerWidth() / 2 ;
                let pos_right = "unset";
                if(aligned_right) {
                    if($( window ).width() < 748) {
                        pos_left = "unset";
                        pos_right = "2rem";
                        pos_arrow = "70%";
                        calc_width = rect.width - target.outerWidth();
                        console.log("1")
                        console.log(rect.width - target.outerWidth())
                        if(calc_width < 55) {
                            pos_arrow = "70%";
                            console.log("2")
                        }
                        else {
                            pos_arrow = "50%";
                        }
                    }
                    else {
                        // if(target.outerWidth() < rect.width - 10) {
                        pos_left =  rect.left + target.outerWidth() / 2;
                        pos_arrow = "70%";
                        // }
                        // else {
                        //     console.log(2)
                        //     pos_left =  rect.left + target.outerWidth() / 2;
                        //     pos_arrow = "70%";
                        // }
                    }
                }
                let pos_top  = target.offset().top - tooltip.outerHeight() - 20;

                // if( pos_left < 0 )
                // {
                //     //pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                //     //tooltip.addClass( 'left' );
                // }
                // else
                //     tooltip.removeClass( 'left' );
     
                if( pos_left + tooltip.outerWidth() > $( window ).width() )
                {
                    pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                    tooltip.addClass( 'right' );
                }
                else
                    tooltip.removeClass( 'right' );
     
                if( pos_top < 0 )
                {
                    pos_top  = target.offset().top + target.outerHeight();
                    tooltip.addClass( 'top' );
                }
                else
                    tooltip.removeClass( 'top' );
     
                tooltip.css( { "--leftoffset":pos_arrow, left: pos_left, top: pos_top, right: pos_right } )
                tooltip.animate( { top: '+=10', opacity: 1 }, 50 );
            };
     
            init_tooltip();
            $( window ).resize( init_tooltip );
     
            var remove_tooltip = function()
            {
                tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
                {
                    $( this ).remove();
                });
                target.attr( 'title', title );
                target.attr( 'data-title', tip );
                target.attr( 'data-img', img );
            };
     
            target.bind( 'mouseleave', remove_tooltip );
            tooltip.bind( 'click', remove_tooltip );
        });

    }

    return window.tooltip = ToolTip;

})(jQuery, window)