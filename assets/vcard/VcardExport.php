<?php
use JeroenDesloovere\VCard\VCard;

class VcardExport
{

    public function contactVcardExportService($contactResult)
    {
        require_once 'vendor/Behat-Transliterator/Transliterator.php';
        require_once 'vendor/jeroendesloovere-vcard/VCard.php';
        // define vcard
        $vcardObj = new VCard();

        // add personal data
        $firstname = $contactResult['team_first_name'][1];
        $lastname = $contactResult['team_last_name'][1];
        //email
        if(!empty($contactResult['team_email'][1])){
            $email = $contactResult['team_email'][1];
        }else{
            $email = strtolower(implode('.',explode(' ', str_replace('-', '', $contactResult['pagetitle'])))).'@stepworks.co';
        }

        //mobile
        if(!empty($contactResult['ext'][1])) {
            $tel = '+852 3678 '.$contactResult['ext'][1];
        } else{
            $tel = '+852 3678 8700';
        }
        $vcardObj->addName($lastname, $firstname);
        $vcardObj->addCompany('Stepworks - Wholehearted Brand Building');
        $vcardObj->addJobtitle($contactResult["longtitle"]);
        $vcardObj->addEmail($email);
        $vcardObj->addPhoneNumber($contactResult["mobile"][1], 'MOBILE');
        $vcardObj->addPhoneNumber($tel, 'WORK');
        $vcardObj->addAddress();
        $vcardObj->addURL('https://stepworks.co/');
        $vcardObj->addPhoto($contactResult["team_photo"][1]);
        
        return $vcardObj->download();
    }
}
