<?php
$arr = array();

//Footer
$arr['en']['contact_heading'] = "Let’s talk about building your brand";
$arr['sc']['contact_heading'] = "联络我们，携手演变";
$arr['en']['contact_link'] = "Or use our enquiry form";
$arr['sc']['contact_link'] = "或使用查询表格";
$arr['en']['address'] = "702, LKF 29<br>29 Wyndham Street<br>Central, Hong Kong";
$arr['sc']['address'] = "香港中环云咸街29号<br>LKF29大厦702室";
$arr['en']['newsletter_heading'] = "Get Wholehearted Brand Building – regular insights via email";
$arr['sc']['newsletter_heading'] = "获取品牌建议，助力业务增长";
$arr['en']['newsletter_field'] = "Enter your email";
$arr['sc']['newsletter_field'] = "输入你的电邮地址";
$arr['en']['newsletter_button'] = "Sign up";
$arr['sc']['newsletter_button'] = "提交";
$arr['en']['newsletter_thankyou'] = "Thank you for joining Stepworks email list.";
$arr['sc']['newsletter_thankyou'] = "感谢您加入 Stepworks 电子邮件列表。";
$arr['en']['newsletter_error'] = "Oh no! An error occured, please try again later.";
$arr['sc']['newsletter_error'] = "不好了！发生错误，请稍后再试。";

$arr['en']['footer_content'] = "Stepworks is an international branding agency located in Hong Kong and Singapore. Our multicultural team applies a global mindset to create valuable competitive advantages for organisations with brand strategy, brand design, and brand communications.";
$arr['sc']['footer_content'] = "Stepworks 是位于香港及新加坡的国际品牌咨询公司。我们以不同文化及最前沿的思维，为客户制定品牌策略、讯息框架、设计、营销活动及数字化创意，务求为您的企业创造更多优势。";

$arr['en']['legal'] = "Legal";
$arr['sc']['legal'] = "法律页面";

//Contact page
$arr['en']['label_name'] = "Name *";
$arr['sc']['label_name'] = "名字";
$arr['en']['label_email'] = "Email *";
$arr['sc']['label_email'] = "电邮";
$arr['en']['label_company'] = "Company";
$arr['sc']['label_company'] = "Company";
$arr['en']['label_message'] = "Tell us more about your business situation, opportunity or organisational challenge *";
$arr['sc']['label_message'] = "简介您的企业概况及想要回应的挑战或机会";
$arr['en']['label_send'] = "Send";
$arr['sc']['label_send'] = "发出";
$arr['sc']['thank_you_message'] = "感谢您的留言。 我们真诚地感谢您对Stepworks的兴趣";
$arr['en']['thank_you_message'] = "Thank you for your message. We sincerely appreciate your interest in Stepworks.";
    
    
$arr['en']['news_title'] = "News";
$arr['sc']['news_title'] = "消息";


$arr['en']['hong_kong'] = "Hong Kong";
$arr['sc']['hong_kong'] = "香港";
$arr['en']['hong_kong_address'] = "702, LKF29<br>29 Wyndham Street<br>Central";
$arr['sc']['hong_kong_address'] = "香港中环云咸街29号<br>LKF29大厦702室";
$arr['en']['hong_kong_tel'] = "+852 3678 8700";
$arr['sc']['hong_kong_tel'] = "+852 3678 8700";

$arr['en']['singapore'] = "Singapore";
$arr['sc']['singapore'] = "新加坡";
$arr['en']['singapore_address'] = "160 Robinson Road, #14-04 <br>Singapore 068914";
$arr['sc']['singapore_address'] = "160 罗敏申路 14-04室 <br>新加坡 068914";
$arr['en']['singapore_tel'] = "+65 6668 7233";
$arr['sc']['singapore_tel'] = "+65 6668 7233";

$arr['en']['see_more'] = "See more";
$arr['sc']['see_more'] = "查看更多";

$arr['en']['see_less'] = "See less";
$arr['sc']['see_less'] = "少看";

//Brands we've built
$arr['en']['filter_all'] = "All";
$arr['sc']['filter_all'] = "全部";
$arr['en']['filter_sector'] = "Sector";
$arr['sc']['filter_sector'] = "行业";
$arr['en']['filter_objective'] = "Business objectives";
$arr['sc']['filter_objective'] = "业务目标";
$arr['en']['filter_solutions'] = "Solutions";
$arr['sc']['filter_solutions'] = "解决方案";
$arr['en']['filter_reach'] = "Reach";
$arr['sc']['filter_reach'] = "目标地域";

//Blog
$arr['en']['blog_objectives'] = "Business objectives";
$arr['sc']['blog_objectives'] = "业务目标";


$arr['en']['Acquisition'] = "Acquisition";
$arr['sc']['Acquisition'] = "收购";
$arr['en']['Align brand with biz strategy'] = "Align brand with biz strategy";
$arr['sc']['Align brand with biz strategy'] = "使品牌与业务策略一致";
$arr['en']['Attract and retain talent'] = "Attract and retain talent";
$arr['sc']['Attract and retain talent'] = "吸引及保留人才";
$arr['en']['Brand consistency'] = "Brand consistency";
$arr['sc']['Brand consistency'] = "提升品牌一致性";
$arr['en']['Brand management'] = "Brand management";
$arr['sc']['Brand management'] = "品牌管理";
$arr['en']['Build awareness'] = "Build awareness";
$arr['sc']['Build awareness'] = "提升品牌知名度";
$arr['en']['Elevate customer experience'] = "Elevate customer experience";
$arr['sc']['Elevate customer experience'] = "提升客户体验";
$arr['en']['Expansion'] = "Expansion";
$arr['sc']['Expansion'] = "业务扩展";
$arr['en']['Increase sales'] = "Increase sales";
$arr['sc']['Increase sales'] = "增加销售";
$arr['en']['IPO'] = "IPO";
$arr['sc']['IPO'] = "上市";
$arr['en']['New offering'] = "New offering";
$arr['sc']['New offering'] = "推出新产品或服务";
$arr['en']['Raise funds'] = "Raise funds";
$arr['sc']['Raise funds'] = "资金筹集";
$arr['en']['Shape perceptions'] = "Shape perceptions";
$arr['sc']['Shape perceptions'] = "塑造品牌感知价值";
$arr['en']['Strategic shift'] = "Strategic shift";
$arr['sc']['Strategic shift'] = "业务战略改变";
$arr['en']['Strengthen culture'] = "Strengthen culture";
$arr['sc']['Strengthen culture'] = "加强企业文化";
$arr['en']['Sustainability reporting'] = "Sustainability reporting";
$arr['sc']['Sustainability reporting'] = "可持续发展报告";
$arr['en']['Upgrade brand'] = "Upgrade brand";
$arr['sc']['Upgrade brand'] = "提升品牌形象及价值";
?>