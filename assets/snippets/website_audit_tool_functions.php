<?php

const DB_HOST = "localhost";
// const DB_USER = "web_audit_tool";
// const DB_PASS = "os!83B2t";
// const DB_NAME = "stepworks_wat";

const DB_USER = "stepworks_root";
const DB_PASS = "rootStep1929";
const DB_NAME = "stepworks_wat";

function getDB() {
    $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    return $mysqli;
}

function get($war_id) {
    $mysqli = getDB();
    $result = mysqli_query($mysqli, "SELECT * FROM `reports` WHERE `war_id`='$war_id'");
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        return $row;
    } else {
        return false;
    }
}

function createWAR($email, $website, $final_score, $brand_experience, $value_delivered, $design_effectivenes, $performance, $accessibility, $best_practices, $seo) {

    $mysqli = getDB();
    $key = generateToken();

    // sanitize inputs
    $email = mysqli_real_escape_string($mysqli, $email);
    $website = mysqli_real_escape_string($mysqli, $website);


    $query = "INSERT INTO reports (`war_id`, `email`, `website`, `final_score`, `brand_experience`, `value_delivered`, `design_effectivenes`,`performance`, `accessibility`, `best_practices`, `seo`) VALUES ('$key', '$email', '$website', '$final_score', '$brand_experience', '$value_delivered', '$design_effectivenes', '$performance', '$accessibility' , '$best_practices', '$seo')";
    // echo $query;
    if ($mysqli->query($query) === true) {
        return $key;
    } else {
        return $mysqli->error;
    }
}

function generateToken() {
    $mysqli = getDB();
    do {
        $random = token(8);
        $check = mysqli_query($mysqli, "SELECT war_id FROM reports WHERE war_id = '".$random."'");
    } while (mysqli_fetch_assoc($check) != null);
    return $random;
}

function token($length) {
    $characters = array(
        "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M",
        "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "m",
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "1", "2", "3", "4", "5", "6", "7", "8", "9");
    if ($length < 0 || $length > count($characters))
        return null;
    shuffle($characters);
    return implode("", array_slice($characters, 0, $length));
}