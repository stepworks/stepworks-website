<?php

const DB_HOST = "localhost";
// const DB_USER = "root";
//const DB_PASS = "1zKb%8w2";
// const DB_PASS = "root";
const DB_NAME = "stepworks_brief";
const DB_USER = "stepworks_root";
const DB_PASS = "rootStep1929";

function getDB() {
    $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    return $mysqli;
}

function getView($brief_id) {
    $mysqli = getDB();
    $result = mysqli_query($mysqli, "SELECT * FROM `view_link` WHERE `brief_id`='$brief_id'");
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        return $row['this_id'];
    } else {
        return false;
    }
}

function token($length) {
    $characters = array(
        "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M",
        "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "m",
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "1", "2", "3", "4", "5", "6", "7", "8", "9");
    if ($length < 0 || $length > count($characters))
        return null;
    shuffle($characters);
    return implode("", array_slice($characters, 0, $length));
}

function getBrief($brief_id) {
    $mysqli = getDB();
    $sql = "SELECT b.*, v.this_id as view_id FROM `brief` b LEFT JOIN `view_link` v ON (b.brief_id = v.brief_id) WHERE b.brief_id='$brief_id' OR v.this_id='$brief_id'";
    $result = mysqli_query($mysqli, $sql);
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        return $row;
    } else {
        return FALSE;
    }
}

// assumes $user = email address
function getBriefByUser($user) {
    $link = getDB();
    $user = mysqli_real_escape_string($link, $user);
    $res = mysqli_query($link, "SELECT * FROM `brief` WHERE ident_email = '$user' ORDER BY date DESC");
    $ret = [];
    while ($row = mysqli_fetch_assoc($res)) {
        $ret[] = $row;
    }
    return $ret;
}

function createBrief($project_name, $ident_email, $client_name, $client_contact, $ref, $date, $signup, $temp) {
    $mysqli = getDB();
    $key = generateToken();

    // sanitize inputs
    $project_name = mysqli_real_escape_string($mysqli, $project_name);
    $ident_email = mysqli_real_escape_string($mysqli, $ident_email);
    $client_name = mysqli_real_escape_string($mysqli, $client_name);
    $client_contact = mysqli_real_escape_string($mysqli, $client_contact);
    $ref = mysqli_real_escape_string($mysqli, $ref);
    $date = mysqli_real_escape_string($mysqli, $date);

    $query = "INSERT INTO brief (`brief_id`, `ident_email`, `project_name`, `client_name`, `client_contact`, `ref`, `date`, `json`, `signup`, `temp`) VALUES ('$key', '$ident_email', '$project_name', '$client_name', '$client_contact', '$ref', '$date', '{}', '$signup', '$temp')";
    // echo $query;
    if ($mysqli->query($query) === true) {
        return $key;
    } else {
        return $mysqli->error;
    }
}

function saveBrief($ident_email, $brief_id, $project_name, $client_name, $client_contact, $ref, $date, $json, $signup, $temp) {
    $mysqli = getDB();
    $json = mysqli_real_escape_string($mysqli, $json);
    $query = "UPDATE brief SET `ident_email` = '$ident_email', `project_name` = '$project_name', `client_name` = '$client_name', `client_contact` = '$client_contact', `ref` = '$ref', `date` = '$date', `json` = '$json', `signup`='$signup', `temp` = '$temp' WHERE `brief_id` = '$brief_id'";
    if ($mysqli->query($query) === true) {
        return $brief_id;
    } else {
        return $mysqli->error;
    }
}

function createViewOnlyBrief($brief_id) {
    $mysqli = getDB();
    $view_id = generateToken();
    $result = mysqli_query($mysqli, "INSERT INTO `view_link` (`this_id`, `brief_id`) VALUES ( '$view_id', '$brief_id')");
    return $view_id;
}

function deleteBrief($brief_id) {
    $mysqli = getDB();
    $query = "DELETE FROM `brief` WHERE `brief_id` = '$brief_id'";
    if ($mysqli->query($query) === true) {
        return true;
    } else {
        return $mysqli->error;
    }
}

function generateToken() {
    $mysqli = getDB();
    do {
        $random = token(8);
        $check = mysqli_query($mysqli, "SELECT b.brief_id FROM brief b WHERE b.brief_id = '".$random."' UNION SELECT v.brief_id FROM view_link v WHERE v.brief_id = '".$random."'");
    } while (mysqli_fetch_assoc($check) != null);
    return $random;
}

function cleanTemp() {
    $mysqli = getDB();
    // remove temp briefs over 7 days old
    mysqli_query($mysqli, "DELETE FROM `brief` b WHERE DATEDIFF(CURRENT_DATE(), b.date) > 7 AND b.temp = 1");
}