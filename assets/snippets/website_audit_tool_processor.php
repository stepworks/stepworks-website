<?php
global $modx;
$data = $_POST;

if (!isset($data['action'])) {
    echo 'nothing here!';
    return false;
}
switch ($data['action']) {
    case 'report':
        $message = trim($data['message']);
        if (strlen($message) > 0) {
            $body = "";
            $body .= "From: " . urldecode($data['from']) . "<br>";
            $body .= "Message:<br>";
            $body .= urldecode($message);

            $modx->loadExtension('MODxMailer');
            $modx->mail->isHTML(true);
            $modx->mail->From = 'WAT@stepworks.co';
            $modx->mail->FromName = 'Stepworks Website Assessment Tool';
            $modx->mail->Subject ="WAT feedback";
            $modx->mail->Body = $body;

            $modx->mail->AddAddress('developers@stepworks.co');

            if (!$modx->mail->send()) return 'Error: ' . $modx->mail->ErrorInfo;
        }
        break;
        case 'save':
            $key = createWAR($data['email'], $data['website'], $data['final_score'], $data['brand_experience'], $data['value_delivered'], $data['design_effectivenes'], $data['performance'], $data['accessibility'],$data['best_practices'],$data['seo']);
            $ret = array('status' => 'success', 'id' => $key);
            // echo json_encode($data);
            echo json_encode($ret);
        break;
        case 'email':
            $subject = "Website assessment report | " . $data['website'];
            $body = "";
            $body .= "<strong>Here is your website assessment report for " . $data['website'] . " website:</strong><br><br>";
            $body .= "<a href='http://stepworks.co/en/website-assessment-tool/reports?id=".$data['id']."'>Print-friendly read-only version (in case you want to share it)</a><br><br>";
            $body .= "<a href='http://stepworks.co/en/website-assesment-tool/'>Visit website assesment tool again</a><br><br>";
            $body .= "Check out our <a href='https://stepworks.co/en/insights'>regular insights</a> into brand building.<br><br>";
            $body .= "Thank you for using our website assesment tool, hope you find it useful :-)<br><br>";
            $body .= "Should you need any assistance, simply reply to this message or email us at <a href='mailto:developers@stepworks.cok'>developers@stepworks.co</a><br><br>";
            $body .= "Wholeheartedly,<br>";
            $body .= "Stepworks<br><br><br>";
            $body .= "<a href='https://stepworks.co/'><img src='https://stepworks.com.hk/sw-logo-email-308.png' alt='Stepworks' style='width:154px;height:73px' width='154' height='73'></a><p style='font-family:Arial;font-size:12px;line-height:16px'></p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191'>Brand strategy, brand design, brand communications</p><p style='font-family:Arial;ont-size:12px;line-height:16px'></p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191;margin:0'>HK +852 3678 8700</p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191;margin:0'>SG +65 6668 7233</p><p style='font-family:Arial;font-size:12px;line-height:16px'></p><a href='https://stepworks.co/' style='font-family:Arial;font-size:10px;line-height:10px'>stepworks.co</a>";
    
    
            $modx->loadExtension('MODxMailer');
            $modx->mail->isHTML(true);
            $modx->mail->From = 'hello@stepworks.co';
            $modx->mail->FromName = 'Stepworks';
            $modx->mail->Subject = $subject;
            $modx->mail->Body = $body;
    
            $modx->mail->AddAddress($data['email']);
            $modx->mail->AddReplyTo('developers@stepworks.co');
    
            if (!$modx->mail->send()) {
                return 'Error: ' . $modx->mail->ErrorInfo;
            }
            else {
                $ret = array('status' => 'success');
                echo json_encode($ret);
            }
            break;
    case 'load':
        if (strlen($data['war_id']) === 8) {
            $result = get($data['war_id']);
            if ($result === FALSE) {
                $return = array('status' => 'fail', 'error' => 'Report not found');
            } else {
                $return = array('status' => 'success');
                $return_result = array_merge($return, $result);
            }
            echo json_encode($return_result);
        }
        break;
    }
?>
