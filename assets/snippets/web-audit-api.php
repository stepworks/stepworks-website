<?php


if( isset($_POST['url']) ){
    if (isset($_POST['header']) && !empty($_POST['header'])) {
        $res = url_get_contents($_POST['url'], 'cURL', 'headers only');
        echo $res;
    } else if (isset($_POST['links']) && !empty($_POST['links'])) {
        $html = url_get_contents($_POST["url"]);
        print_r( getLinks($html) );
    } else {
        $html = url_get_contents($_POST["url"]);
        echo getMetaData($html);
    }
}

function url_get_contents($url, $useragent='cURL', $headers=false, $follow_redirects=true, $debug=false) {
// initialise the CURL library
    $ch = curl_init();
    // specify the URL to be retrieved
    curl_setopt($ch, CURLOPT_URL,$url);
    // we want to get the contents of the URL and store it in a variable
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    // specify the useragent: this is a required courtesy to site owners
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    // ignore SSL errors
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // return headers as requested
    if ($headers==true){
        curl_setopt($ch, CURLOPT_HEADER,1);
    }
    // only return headers
    if ($headers=='headers only') {
        curl_setopt($ch, CURLOPT_NOBODY ,1);
    }
    // follow redirects - note this is disabled by default in most PHP installs from 4.4.4 up
    if ($follow_redirects==true) {
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    }
    // if debugging, return an array with CURL's debug info and the URL contents
    if ($debug==true) {
        $result['contents']=curl_exec($ch);
        $result['info']=curl_getinfo($ch);
    }
    // otherwise just return the contents as a variable
    else $result=curl_exec($ch);
    // free resources
    curl_close($ch);
    // send back the data
    return $result;
}

function getLinks($html) {
    $doc = new DOMDocument();
    @$doc->loadHTML($html);
    $nodes = $doc->getElementsByTagName('a');
    $iLinks = array();
    if ($nodes->length > 0) {
        for ($i = 0; $i < 20; $i++) {
            $link = $nodes->item($i);
            $href = $link->getAttribute('href');

            if ((strpos($href, "http") === FALSE || strpos($href, $url) == 0) && // relative or same site link
                substr($href, 0, 1) != "#" && // not an anchor
                substr_count($href, '/') > 1 &&
                !in_array($href, $iLinks)) {
                array_push($iLinks, $href);
            }
        }
    }
    return $iLinks;
}

function getMetaData($html) {
    $doc = new DOMDocument();
    @$doc->loadHTML($html);
    $nodes = $doc->getElementsByTagName('title');
    $title = "Cannot read title of the website.";
    $metas = [];
    $description = "Cannot read website's description.";
    // $keywords = "Cannot read website's description." ;
    //get and display what you need:
    if(is_object($nodes->item(0)))
        $title = $nodes->item(0)->nodeValue;


    $metas = $doc->getElementsByTagName('meta');

    if($metas->length > 0) {
    $description = "There is no description on the website.";
    // $keywords = "Website doesn't contain keywords." ;
    for ($i = 0; $i < $metas->length; $i++)
    {
        $meta = $metas->item($i);
        if(strtolower($meta->getAttribute('name')) == 'description')
            $description = $meta->getAttribute('content');
        // if($meta->getAttribute('name') == 'keywords')
        //     $keywords = $meta->getAttribute('content');
    }

    }
    $json = array('title' =>  $title, 'description' => $description);

    return json_encode($json);
}