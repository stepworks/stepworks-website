<?php
global $modx;

$data = $_POST;
if (!isset($data['action'])) {
    echo 'nothing here!';
    return false;
}
switch ($data['action']) {
    case 'report':
        $message = trim($data['message']);
        if (strlen($message) > 0) {
            $body = "";
            $body .= "From: " . urldecode($data['from']) . "<br>";
            $body .= "Message:<br>";
            $body .= urldecode($message);

            $modx->loadExtension('MODxMailer');
            $modx->mail->isHTML(true);
            $modx->mail->From = 'cbe@stepworks.co';
            $modx->mail->FromName = 'Stepworks Creative Brief Engine';
            $modx->mail->Subject ="CBE feedback";
            $modx->mail->Body = $body;

            $modx->mail->AddAddress('developers@stepworks.co');
            $modx->mail->AddAddress('hello@stepworks.co');

            if (!$modx->mail->send()) return 'Error: ' . $modx->mail->ErrorInfo;
        }
        break;
    case 'save':
        // echo 'save';
        $date = date('Y-m-d', strtotime($data['date']));
        if (isset($data['brief_id'][7])) {
            $key = saveBrief($data['ident_email'], $data['brief_id'], $data['project_name'], $data['client_name'], $data['client_contact'], $data['ref'], $date, $data['json'], $data['signup'], $data['temp']);
            if (strlen($key) === 8) {
                $ret = array('status' => 'success', 'brief_id' => $key);
            } else {
                $ret = array('error' => 'success', 'message' => $key);
            }

            // echo json_encode($data);
        } else {
            $key = createBrief($data['project_name'], $data['ident_email'], $data['client_name'], $data['client_contact'], $data['ref'], $date, $data['signup'], $data['temp']);
            $ret = array('status' => 'success', 'brief_id' => $key);
            // echo json_encode($data);
        }

        if ($data['temp']=='0' && !empty($data['ident_email'])) {
            $view = getView($key);
            if ($view === false) {
                $viewid = createViewOnlyBrief($key);
                $ret['view_id'] = $viewid;
            } else {
                $ret['view_id'] = $view;
            }
        }

        echo json_encode($ret);
        break;

    case 'email':
        $subject = "Creative brief" . (empty($data['client_name']) ? '' : ' for '. $data['client_name']);

        $body = "";
        $body .= "<strong>Creative brief" . (empty($data['client_name']) ? '' : ' for '. $data['client_name']) . "</strong><br><br>";
        $body .= "<a href='http://stepworks.co/en/creative-brief-engine/#".$data['brief_id']."'>Edit your creative brief</a><br>";
        $body .= "<a href='http://stepworks.co/en/view-brief?id=".$data['view_id']."'>Print-friendly read-only version (in case you want to share it)</a><br>";
        $body .= "<a href='http://stepworks.co/en/creative-brief-engine/'>Start a new creative brief</a><br><br>";
        $body .= "Check out our <a href='https://stepworks.co/en/insights'>regular insights</a> into brand building.<br><br>";
        $body .= "Thank you for using our Creative Brief Engine, hope you find it useful :-)<br><br>";
        $body .= "Should you need any assistance, simply reply to this message or email us at <a href='mailto:developers@stepworks.cok'>developers@stepworks.co</a><br><br>";
        $body .= "Wholeheartedly,<br>";
        $body .= "Stepworks<br><br><br>";
        $body .= "<a href='https://stepworks.co/'><img src='https://stepworks.com.hk/sw-logo-email-308.png' alt='Stepworks' style='width:154px;height:73px' width='154' height='73'></a><p style='font-family:Arial;font-size:12px;line-height:16px'></p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191'>Brand strategy, brand design, brand communications</p><p style='font-family:Arial;ont-size:12px;line-height:16px'></p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191;margin:0'>HK +852 3678 8700</p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191;margin:0'>SG +65 6668 7233</p><p style='font-family:Arial;font-size:12px;line-height:16px'></p><a href='https://stepworks.co/' style='font-family:Arial;font-size:10px;line-height:10px'>stepworks.co</a>";


        $modx->loadExtension('MODxMailer');
        $modx->mail->isHTML(true);
        $modx->mail->From = 'cbe@stepworks.co';
        $modx->mail->FromName = 'Stepworks Creative Brief Engine';
        $modx->mail->Subject = $subject;
        $modx->mail->Body = $body;

        $modx->mail->AddAddress($data['email']);
        $modx->mail->AddReplyTo('developers@stepworks.co');

        if (!$modx->mail->send()) return 'Error: ' . $modx->mail->ErrorInfo;

        break;
    case 'share-sw':

        $subject = "Creative brief" . (empty($data['client_name']) ? '' : ' from '. $data['client_name']);

        $body = "";
        $body .= "Someone has submitted a creative brief to us using the Creative Brief Engine!<br><br>";
        $body .= "<strong>Client name:</strong> " . $data['client_name'] . "<br>";
        $body .= "<strong>Project name:</strong> " . $data['project_name'] . "<br>";
        $body .= "<a href='http://stepworks.co/en/view-brief?id=".$data['brief_id']."'>View brief</a><br>";

        $modx->loadExtension('MODxMailer');
        $modx->mail->isHTML(true);
        $modx->mail->From = 'cbe@stepworks.co';
        $modx->mail->FromName = 'Stepworks Creative Brief Engine';
        $modx->mail->Subject = $subject;
        $modx->mail->Body = $body;

        $modx->mail->AddAddress('developers@stepworks.co');
        $modx->mail->AddAddress('kaman.wong@stepworks.co');

        if (!$modx->mail->send()) return 'Error: ' . $modx->mail->ErrorInfo;

        break;
    case 'load':
        if (strlen($data['brief_id']) === 8) {
            $result = getBrief($data['brief_id']);
            if ($result === FALSE) {
                $return = array('status' => 'fail', 'error' => 'Brief not found');
            } else {
                $return = array('status' => 'success');
                $return_result = array_merge($return, $result);
            }
            echo json_encode($return_result);
        }
        break;
    // case 'getlib':
    //     if (isset($data['user']) && !empty($data['user'])) {
    //         $result = ['results' => getBriefByUser($data['user'])];
    //         $status = array('status' => 'success');
    //         $return_result = array_merge($status, $result);
    //         echo json_encode($return_result);
    //     }
    //     break;
    // case 'duplicate':
    //     if (isset($data['brief_id']) && !empty($data['brief_id'])) {
    //         $brief = getBrief($data['brief_id']);
    //         // print_r($brief);
    //         $date = date('Y-m-d');
    //         $key = createBrief('', $brief['ident_email'], '', '', '' , $date);
    //         $json = json_decode(urldecode($brief['json']));
    //         $json->{'project-name'} = 'Copy of ' . $json->{'project-name'};
    //         $enc = json_encode($json);
    //         $key = saveBrief($key, $json->{'project-name'}, '', '', '', $date, $enc);
    //         echo $key;
    //     }
    //     break;
    // case 'delete':
    //     if (isset($data['brief_id']) && !empty($data['brief_id'])) {
    //         $res = deleteBrief($data['brief_id']);
    //         if ($res === true) {
    //             echo 'deleted';
    //         } else {
    //             echo "failed to delete brief " . $data['brief_id'] . "\r\n" . $res;
    //         }
    //     }
    //     break;
}
?>
