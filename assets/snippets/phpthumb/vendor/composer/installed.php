<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.4.x-dev',
        'version' => '1.4.9999999.9999999-dev',
        'reference' => 'd1b46408a166eff5588bebd00018f671b111c8c4',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.4.x-dev',
            'version' => '1.4.9999999.9999999-dev',
            'reference' => 'd1b46408a166eff5588bebd00018f671b111c8c4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'james-heinrich/phpthumb' => array(
            'pretty_version' => 'v1.7.21',
            'version' => '1.7.21.0',
            'reference' => '7ee966b38ddd7eb4d8091389aa514604710711c8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../james-heinrich/phpthumb',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
