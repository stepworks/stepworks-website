<?php 
global $modx;

$data = json_decode($_POST['data'], true);
if (!isset($data['action'])) {
    echo 'nothing here!';
    return false;
}

if ($data['edit_mode'] == true) {
    // break down $data['json'], add path(s) to json
    $json = json_decode(urldecode($data['json']), true);
    if (isset($data['tentative_scope_file_path'])) {
        $json['tentative-scope-file'] = $data['tentative_scope_file_path'];
    }
    if (isset($data['audience_personas_file_path'])) {
        $json['audience-personas-file'] = $data['audience_personas_file_path'];
    }
    if (isset($data['visitor_data_file_path'])) {
        $json['visitor-data-file'] = $data['visitor_data_file_path'];
    }   
    $data['json'] = urlencode(json_encode($json));
} else if ($data['has_file'] == true && $data['last_step'] == true) {        
    //TODO: create common function to reduce lines of code
    if (isset($_FILES['tenatativeScopeFile'])) {
        $file = $_FILES['tenatativeScopeFile'];
        $filename = date("YmdHms",time()) . '_' . basename($file['name']);
        $path = __DIR__ . '/../files/website-brief-uploads/' . $filename;
        $extension_list = ['pages', 'docx', 'doc', 'pdf'];
        $file_type = pathinfo($path, PATHINFO_EXTENSION);
        //TODO: check file size??
        if (!in_array($file_type, $extension_list)) {
            echo 'unaccepted file extension';
        } else if (!move_uploaded_file($file['tmp_name'], $path)) {
            echo 'file upload error!';
            return false;
        }
        // break down $data['json'], add path to json
        $json = json_decode(urldecode($data['json']), true);
        $json['tenatative-scope-file'] = 'https://' . $_SERVER['HTTP_HOST'] . '/assets/files/website-brief-uploads/' . $filename;
        // echo print_r($json, true) . '<br>';
        $data['json'] = urlencode(json_encode($json));
    }  
    if (isset($_FILES['audiencePersonasFile'])) {
        $file = $_FILES['audiencePersonasFile'];
        $filename = date("YmdHms",time()) . '_' . basename($file['name']);
        $path = __DIR__ . '/../files/website-brief-uploads/' . $filename;
        $extension_list = ['pages', 'docx', 'doc', 'pdf'];
        $file_type = pathinfo($path, PATHINFO_EXTENSION);
        //TODO: check file size??
        if (!in_array($file_type, $extension_list)) {
            echo 'unaccepted file extension';
        } else if (!move_uploaded_file($file['tmp_name'], $path)) {
            echo 'file upload error!';
            return false;
        }
        // break down $data['json'], add path to json
        $json = json_decode(urldecode($data['json']), true);
        $json['audience-personas-file'] = 'https://' . $_SERVER['HTTP_HOST'] . '/assets/files/website-brief-uploads/' . $filename;
        // echo print_r($json, true) . '<br>';
        $data['json'] = urlencode(json_encode($json));
    }  
    if (isset($_FILES['visitorDataFile'])) {
        $file = $_FILES['visitorDataFile'];
        $filename = date("YmdHms",time()) . '_' . basename($file['name']);
        $path = __DIR__ . '/../files/website-brief-uploads/' . $filename;
        $extension_list = ['pages', 'docx', 'doc', 'pdf'];
        $file_type = pathinfo($path, PATHINFO_EXTENSION);
        //TODO: check file size??
        if (!in_array($file_type, $extension_list)) {
            echo 'unaccepted file extension';
        } else if (!move_uploaded_file($file['tmp_name'], $path)) {
            echo 'file upload error!';
            return false;
        }
        // break down $data['json'], add path to json
        $json = json_decode(urldecode($data['json']), true);
        $json['visitor-data-file'] = 'https://' . $_SERVER['HTTP_HOST'] . '/assets/files/website-brief-uploads/' . $filename;
        // echo print_r($json, true) . '<br>';
        $data['json'] = urlencode(json_encode($json));
    }
}

$response = array();
switch ($data['action']) {
    case 'get-brief':
        if (strlen($data['brief_id']) === 8) {
            $result = getBrief($data['brief_id']);
        }
        break;
    case 'get-view':
        // check brief 
        if (strlen($data['brief_id']) === 8) {
            $result = getBrief($data['brief_id']);
        }
        break;
    case 'report':
        $message = trim($data['message']);
        if (strlen($message) > 0) {
            $body = "";
            $body .= "From: " . urldecode($data['from']) . "<br>";
            $body .= "Message:<br>";
            $body .= urldecode($message);

            $modx->loadExtension('MODxMailer');
            $modx->mail->isHTML(true);
            $modx->mail->From = 'wbe@stepworks.co';
            $modx->mail->FromName = 'Stepworks Website Brief Engine';
            $modx->mail->Subject ="WBE feedback";
            $modx->mail->Body = $body;

            $modx->mail->AddAddress('developers@stepworks.co');
            $modx->mail->AddAddress('hello@stepworks.co');

            if (!$modx->mail->send()) return 'Error: ' . $modx->mail->ErrorInfo;
        }
        break;
    case 'create':
        $date = date('Y-m-d', strtotime($data['date']));
        $key = createBrief($data['project_name'], $data['email'], $data['client_name'], $data['client_contact'], $data['ref'], $date, $data['signup'], $data['temp']);
        $response['status'] = 'success';
        $response['brief_id'] = $key;
        $view = getView($key);
        $response['view_id'] = $view ?: createViewOnlyBrief($key);
        echo json_encode($response);
        break;
        
    case 'save':
        $date = date('Y-m-d', strtotime($data['date']));
        if (isset($data['brief_id'][7])) {
            $key = saveBrief($data['email'], $data['brief_id'], $data['project_name'], $data['client_name'], $data['client_contact'], $data['ref'], $date, $data['json'], $data['signup'], $data['temp']);
            if (strlen($key) === 8) {

                if ($data['last_step'] && !$data['edit_mode']) {
                    $subject = "Website brief" . (empty($data['project_name']) ? '' : ' for '. $data['project_name']);
    
                    $body = "";
                    $body .= "<strong>Website brief" . (empty($data['project_name']) ? '' : ' for '. $data['project_name']) . "</strong><br><br>";
                    $body .= "<a href='https://stepworks.co/en/website-brief-engine/reports?id=".$data['brief_id']."'>View your website brief here</a><br>";
                    $body .= "<a href='https://stepworks.co/en/website-brief-engine/?id=".$data['brief_id']."'>Edit your website brief here</a><br>";
                    $body .= "<a href='http://stepworks.co/en/website-brief-engine/'>Start a new website brief here</a><br><br>";
                    $body .= "Thank you for using our Website Brief Engine, hope you found it useful :-)<br><br>";
                    $body .= "Check out our <a href='https://stepworks.co/en/insights'>regular brand building insights</a> here.<br><br>";
                    $body .= "Need a hand? Simply reply to this message or email us at <a href='mailto:developers@stepworks.co'>developers@stepworks.co</a><br><br>";
                    $body .= "Wholeheartedly,<br>";
                    $body .= "Stepworks<br><br><br>";
                    $body .= "<a href='https://stepworks.co/'><img src='https://stepworks.com.hk/sw-logo-email-308.png' alt='Stepworks' style='width:154px;height:73px' width='154' height='73'></a><p style='font-family:Arial;font-size:12px;line-height:16px'></p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191'>Brand strategy, brand design, brand communications</p><p style='font-family:Arial;ont-size:12px;line-height:16px'></p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191;margin:0'>HK +852 3678 8700</p><p style='font-family:Arial;font-size:12px;line-height:16px;color:#919191;margin:0'>SG +65 6668 7233</p><p style='font-family:Arial;font-size:12px;line-height:16px'></p><a href='https://stepworks.co/' style='font-family:Arial;font-size:10px;line-height:10px'>stepworks.co</a>";
            
            
                    $modx->loadExtension('MODxMailer');
                    $modx->mail->isHTML(true);
                    $modx->mail->From = 'wbe@stepworks.co';
                    $modx->mail->FromName = 'Stepworks Website Brief Engine';
                    $modx->mail->Subject = $subject;
                    $modx->mail->Body = $body;
            
                    $modx->mail->AddAddress($data['email']);
                    $modx->mail->AddAddress('developers@stepworks.co');
                    $modx->mail->AddReplyTo('developers@stepworks.co');
            
                    if (!$modx->mail->send()) return 'Error: ' . $modx->mail->ErrorInfo;
                }

                $response = array('status' => 'success', 'brief_id' => $key);
            } else {
                $response = array('error' => 'success', 'message' => $key);
            }
        } else {
            $key = createBrief($data['project_name'], $data['ident_email'], $data['client_name'], $data['client_contact'], $data['ref'], $date, $data['signup'], $data['temp']);   
            $response = array('status' => 'success', 'brief_id' => $key);
        }

        if ($data['temp']=='0' && !empty($data['ident_email'])) {
            $view = getView($key);
            if ($view === false) {
                $viewid = createViewOnlyBrief($key);
                $response['view_id'] = $viewid;
            } else {
                $reresponset['view_id'] = $view;
            }
        }
        echo json_encode($response);
        break;
        
        case 'email':

            $subject = "Website brief" . (empty($data['project_name']) ? '' : ' for '. $data['project_name']);
    
            $body = "";
            $body .= "<strong>Website brief" . (empty($data['project_name']) ? '' : ' for '. $data['project_name']) . "</strong><br><br>";
            $body .= "<a href='https://stepworks.co/en/website-brief-engine/reports?id=".$data['brief_id']."'>View your website brief here</a><br>";
            $body .= "<a href='https://stepworks.co/en/website-brief-engine/?id=".$data['brief_id']."'>Edit your website brief here</a><br>";
            $body .= "<a href='http://stepworks.co/en/website-brief-engine/'>Start a new website brief here</a><br><br>";
            $body .= "Thank you for using our Website Brief Engine, hope you found it useful :-)<br><br>";
            $body .= "Check out our <a href='https://stepworks.co/en/insights'>regular brand building insights</a> here.<br><br>";
            $body .= "Need a hand? Simply reply to this message or email us at <a href='mailto:developers@stepworks.co'>developers@stepworks.co</a><br><br>";
            $body .= "Wholeheartedly,<br>";
            $body .= "Stepworks<br><br><br>";
            $body .= "<a href='https://stepworks.co/'><img src='https://stepworks.co/sw-logo-email-308.png' alt='Stepworks' width='154' height='73' style='margin: 0 0 10px;'></a><p style='font-family: Arial; font-size: 12px; line-height: 16px; color: #919191; margin: 0 0 10px;'>Brand strategy, brand design, brand communications</p><p style='font-family: Arial; font-size: 10px; line-height: 14px; color: #919191; margin: 0 0 5px;'>HK +852 3678 8700<br>SG +65 6668 7233</p><a href='https://stepworks.co/' style='font-family:Arial;font-size:10px;line-height:10px;'>stepworks.co</a>";
            $modx->loadExtension('MODxMailer');
            $modx->mail->isHTML(true);
            $modx->mail->From = 'wbe@stepworks.co';
            $modx->mail->FromName = 'Stepworks Website Brief Engine';
            $modx->mail->Subject = $subject;
            $modx->mail->Body = $body;
    
            $modx->mail->AddAddress($data['email']);
            $modx->mail->AddAddress('developers@stepworks.co');
            $modx->mail->AddReplyTo('developers@stepworks.co');
    
            if (!$modx->mail->send()) return 'Error: ' . $modx->mail->ErrorInfo;
    
            break;

        case 'share-sw':
    
            $subject = "Website brief" . (empty($data['project_name']) ? '' : ' from '. $data['project_name']);
    
            $body = "";
            $body .= "Someone has submitted a website brief to us using the Website Brief Engine!<br><br>";
            $body .= "<strong>Client name:</strong> " . $data['client_name'] . "<br>";
            $body .= "<strong>Project name:</strong> " . $data['project_name'] . "<br>";
            $body .= "<a href='http://stepworks.co/en/view-brief?id=".$data['brief_id']."'>View brief</a><br>";
    
            $modx->loadExtension('MODxMailer');
            $modx->mail->isHTML(true);
            $modx->mail->From = 'wbe@stepworks.co';
            $modx->mail->FromName = 'Stepworks Website Brief Engine';
            $modx->mail->Subject = $subject;
            $modx->mail->Body = $body;
    
            $modx->mail->AddAddress('developers@stepworks.co');
            $modx->mail->AddAddress('kaman.wong@stepworks.co');
    
            if (!$modx->mail->send()) return 'Error: ' . $modx->mail->ErrorInfo;
    
            break;
            
        case 'load':
            if (strlen($data['brief_id']) === 8) {
                $result = getBrief($data['brief_id']);
                if ($result === FALSE) {
                    $return = array('status' => 'fail', 'error' => 'Brief not found');
                } else {
                    $return = array('status' => 'success');
                    $return_result = array_merge($return, $result);
                }
            } else {
                $return_result = array('status' => 'fail', 'error' => 'Brief not found');
            }
            echo json_encode($return_result);
            break;
    

}
?>