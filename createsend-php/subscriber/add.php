<?php

require_once '../csrest_subscribers.php';

$auth = array('api_key' => 'cEsmdg1e8+1bL0crrEUCkBTp3ewiDnc5SyNwxNb/mC9VbmcNy/7rsh4JYodAmOpdP/lAiXCn46LAL/yajb3vSFtu6ZB7ivy/4Aq26f3gRkDdr/GBEZDrhm2m43z9IK/H0fJ+3s7yP60iCC8dD5Rouw==');
$wrap = new CS_REST_Subscribers('5383492853dbf31d3fde89a2c6292daa', $auth);

$email = $_POST['email'];

$result = $wrap->add(array(
    'EmailAddress' => $email,
    'Name' => '',
    'ConsentToTrack' => 'yes',
    'Resubscribe' => true
));

echo "Result of POST /api/v3.1/subscribers/{list id}.{format}\n<br />";
if($result->was_successful()) {
    echo "Subscribed with code ".$result->http_status_code;
} else {
    echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
    var_dump($result->response);
    echo '</pre>';
}