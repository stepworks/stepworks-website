<?php
$modx_version      = '1.4.34'; // Current version number
$modx_release_date = 'January 31, 2024'; // Date of release
$modx_branch       = 'Evolution CE'; // Codebase name
$modx_full_appname = "{$modx_branch} {$modx_version} ({$modx_release_date})";
