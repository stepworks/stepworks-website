<!DOCTYPE html>
<html lang="en">
<head>
    <title>Signature generator</title>
    <style>
        body{margin: 0; padding: 30px;}
        .signature-form{max-width: 300px; border-radius: 20px; margin: 0 auto; padding: 40px; box-shadow: 0 0 20px rgba(0,0,0,0.1); font-family: Verdana;}
        .signature-form h1{font-size: 20px; font-weight: normal; line-height: 1.4; color: #000; margin: 0 0 20px; text-align: center;}
        .field{margin-bottom: 20px;}
        .field label{display: block; line-height: 1; font-size: 14px; margin-bottom: 5px;}
        .field input{display: block; width: 100%; font-size: 12px; line-height: 1.2; font-family: Verdana; box-sizing: border-box; border: 1px solid #000; border-radius: 5px; height: 40px; padding: 0 10px;}
        .btn{display: block; font-family: Verdana; font-size: 14px; line-height: 1; padding: 15px; text-align: center; border: 1px solid transparent; width: 100%; background-color: #eb0c38; color: #fff; border-radius: 5px; cursor: pointer; transition: all 0.2s ease-out}
        .btn:hover{background-color: white; border-color: #eb0c38; color: #eb0c38;}
        .message{display: none; color: #eb0c38; font-family: 'Verdana'; margin-top: 30px; text-align: center;}
        sup { color: #eb0c38 };
    </style>
    <script type="text/javascript">
      window.addEventListener('load', function () {
        if (navigator.userAgent.indexOf("Safari") >= 0 && navigator.userAgent.indexOf("Chrome") < 0) {
          document.querySelector('.message').style.display = 'block';
        }
      }, false);
    </script>
</head>
<body>
    <?php if (isset($_POST['fullname']) && isset($_POST['title'])) { ?>
        <p style="font-family: Arial; font-size: 10px; line-height: 10px; margin: 0 0 10px;">-</p>
        <p style="font-family: Arial; font-size: 12px; font-weight: 700; line-height: 16px; margin: 0 0 5px;"><?=$_POST['fullname']?></p>
        <p style="font-family: Arial; font-size: 10px; line-height: 14px; margin: 0 0 10px;"><?=$_POST['title']?></p>
        <a href="https://stepworks.co/"><img src="https://stepworks.co/sw-logo-email-308.png" alt="Stepworks" width="154" height="73" style="margin: 0 0 10px;" /></a>
        <p style="font-family: Arial; font-size: 12px; line-height: 16px; color: #919191; margin: 0 0 10px;">Brand strategy, brand design, brand communications</p>
        <p style="font-family: Arial; font-size: 10px; line-height: 14px; color: #919191; margin: 0 0 5px;"><?=$_POST['ext'] ? 'D +852 3678 ' . $_POST['ext'] . '<br>' : ''?>T +852 3678 8700</p>
        <a href="<?= $_POST['liteCard'] ?>" style="font-family: Arial; font-size: 10px; line-height: 10px;">Contact info</a> <span style="display: inline-block; vertical-align: middle; color: #919191;">|</span> <a href="https://stepworks.co/" style="font-family: Arial; font-size: 10px; line-height: 10px;">Hong Kong & Singapore</a>
    <?php } else { ?>
        <div class="signature-form">
            <h1>Generate your signature by filling out the below:</h1>
            <form action="" method="post">
                <div class="field">
                    <label for="fullname">Your full name<sup>*</sup></label>
                    <input type="text" name="fullname" id="fullname" required>
                </div>
                <div class="field">
                    <label for="title">Your title<sup>*</sup></label> <input type="text" name="title" id="title" required>
                </div>
                <div class="field">
                    <label for="ext">Your extension</label> <input type="text" name="ext" id="ext" placeholder="Last 4 digits of your phone number">
                </div>
                <div class="field">
                    <label for="liteCard">Your Lite card<sup>*</sup></label> <input type="text" name="liteCard" id="liteCard" required placeholder="Your Tiny URL Lite.cards link">
                </div>
                <button class="btn" type="submit">Get my signature</button>
            </form>
        </div>
        <div class="message">This tool does not supported by Safari.<br>Please use Google Chrome instead.</div>
    <?php } ?>
</span>
</body>
</html>